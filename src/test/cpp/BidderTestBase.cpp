#include "BidderTestBase.h"
#include "OpportunityContext.h"
#include "BeanFactory.h"
#include "ModuleContainer.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "TargetGroupTestHelper.h"

BidderTestBase::BidderTestBase() {
        std::string appName = "BidderTestBase";
        std::string appVersion = "1";
        std::string logDirectory = "";
        std::string commonPropertyFileName = "common-test.properties";
        std::string propertyFileName = "bidder-test.properties";
        beanFactory = std::make_shared<BeanFactory>(appVersion);
        beanFactory->commonPropertyFileName = commonPropertyFileName;
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->appName = appName;
        beanFactory->initializeModules();

        moduleContainer = std::make_unique<ModuleContainer>();
        moduleContainer->beanFactory = beanFactory.get();
        //we disable the sending message to kafka
        moduleContainer->enbaleSendingScoringMessageToKafka = false;
        moduleContainer->initializeModules();
        context = std::make_shared<OpportunityContext>();
        context->device =  std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP");

}

BidderTestBase::~BidderTestBase() {

}

void BidderTestBase::SetUp() {

}

void BidderTestBase::TearDown() {

}

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef BidderTestBase_H
#define BidderTestBase_H

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include "TestsCommon.h"
class BeanFactory;
class OpportunityContext;
class TargetGroup;
class ModuleContainer;


class BidderTestBase : public ::testing::Test {
public:

BidderTestBase();

std::shared_ptr<BeanFactory> beanFactory;
std::shared_ptr<ModuleContainer> moduleContainer;
std::shared_ptr<OpportunityContext> context;


virtual ~BidderTestBase();

void SetUp();

void TearDown();

};
#endif //BidderTestBase_H

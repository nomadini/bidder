//
// Created by Mahmoud Taabodi on 4/14/16.
//

#include "FilterTestsBase.h"
#include <boost/foreach.hpp>
#include "DomainUtil.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BeanFactory.h"
void FilterTestsBase::SetUp() {

        beanFactory->mySqlTargetGroupSegmentMapService()->deleteAll;
        beanFactory->mySqlTargetGroupDayPartTargetMapService()->deleteAll;
        beanFactory->mySqlTargetGroupGeoSegmentListMapService()->deleteAll;
        beanFactory->mySqlTargetGroupCreativeMapService()->deleteAll;
        beanFactory->mySqlGeoLocationService()->deleteAll;
        beanFactory->mySqlSegmentService()->deleteAll;
        beanFactory->mySqlAdvertiserService()->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;
        beanFactory->mySqlCreativeService()->deleteAll;
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlBWListService()->deleteAll;
        beanFactory->mySqlBWEntryService()->deleteAll;
        beanFactory->mySqlTargetGroupBWListMapService()->deleteAll;
        beanFactory->mySqlGeoSegmentService()->deleteAll;
        beanFactory->mySqlInventoryService()->deleteAll;
        beanFactory->mySqlGeoSegmentListService()->deleteAll;

        beanFactory->targetGroupCreativeCacheService()->clearCaches;

        beanFactory->targetGroupDayPartCacheService()->clearCaches;

        beanFactory->mySqlTargetGroupDayPartTargetMapService()->deleteAll;

        beanFactory->deviceSegmentHistoryCassandraService()->deleteAll;

        beanFactory->deviceFeatureHistoryCassandraService()->deleteAll;

        beanFactory->featureDeviceHistoryCassandraService()->deleteAll;

        beanFactory->targetGroupSegmentCacheService()->clearCaches;

        beanFactory->adHistoryCassandraService()->deleteAll;

        beanFactory->mySqlGlobalWhiteListService()->deleteAll;

        this->testService = std::make_shared<BidderTestService> ();
        this->testService->context = std::make_shared<OpportunityContext>();
        this->testService->context->allAvailableTgs = std::make_shared<std::vector<std::shared_ptr<TargetGroup> > >();
}

void FilterTestsBase::givenTargetGroupInContext (std::shared_ptr<TargetGroup> tg1) {
        testService->context->allAvailableTgs->push_back(tg1);
}

std::shared_ptr<BWList> FilterTestsBase::givenBWListWithDomains(
        std::string listType, std::vector<std::string> domains, int targetGroupId) {

        std::shared_ptr<BWList> bwList = std::make_shared<BWList>();
        bwList->setName("randomBwList");
        bwList->setListType(listType);
        // bwList->setAdvertiserId(beanFactory->advertiserCacheService->getAdvertiserOfTargetGroup(targetGroupId)->id);
        bwList->setCreatedAt(DateTimeUtil::getNowInMySqlFormat());
        bwList->setUpdatedAt (DateTimeUtil::getNowInMySqlFormat());
        beanFactory->mySqlBWListService->insert(bwList);
        auto newBWList = beanFactory->mySqlBWListService()->readByName(bwList->getName );

        for(std::string domain :  domains) {
                std::shared_ptr<BWEntry> bwEntry = std::make_shared<BWEntry>();
                bwEntry->setDomainName(DomainUtil::getDomainFromUrlString(domain));
                bwEntry->setBwListId(newBWList->getId());
                bwEntry->setCreatedAt(DateTimeUtil::getNowInMySqlFormat());
                bwEntry->setUpdatedAt(DateTimeUtil::getNowInMySqlFormat());
                MLOG(3) << "inserting domain : "<< domain << " as bwEntry";
                beanFactory->mySqlBWEntryService->insert(bwEntry);
        }

        return bwList;
}

void FilterTestsBase::givenTargetHaveBWList(int targetGroupId, int bwListId) {
        std::shared_ptr<TargetGroupBWListMap> targetGroupBWListMap = std::make_shared<TargetGroupBWListMap>();
        targetGroupBWListMap->targetGroupId = targetGroupId;
        targetGroupBWListMap->bwListId = bwListId;
        beanFactory->mySqlTargetGroupBWListMapService->insert(targetGroupBWListMap);
}

void FilterTestsBase::givenTheCreativeAndTargetGroupsExistInCache () {
        std::unordered_map<int, int> creativeIds;
        creativeIds.insert (std::make_pair(testService->creative->getId(),testService->creative->getId()));

        // beanFactory->targetGroupCreativeCacheService ()->getAllTargetGroupCreativesMap ->insert (
        //         std::make_pair (testService->targetGroup->getId(), creativeIds));

        beanFactory->targetGroupCacheService ()->reloadCaches;

        beanFactory->creativeCacheService ()->getAllEntitiesMap->insert (
                std::make_pair (testService->creative->getId(), testService->creative));
}


void FilterTestsBase::givenTargetGroupExistInCache(std::shared_ptr<TargetGroup> tg) {
        beanFactory->targetGroupCacheService ()->getAllEntitiesMap()->insert (std::make_pair (tg->getId, tg));
}

void FilterTestsBase::givenCampaignExistInCache(std::shared_ptr<Campaign> campaign) {
        beanFactory->campaignCacheService->addCampaignToCache(campaign);
}

void FilterTestsBase::givenAdvertiserExistInCache(std::shared_ptr<Advertiser> advertiser) {
        beanFactory->advertiserCacheService()->getallEntitiesMap->
        insert (std::make_pair (advertiser->id, advertiser));
}

void FilterTestsBase::givenTargetGroupWithProperParent(std::shared_ptr<TargetGroup> tg) {
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);

        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;
}

std::shared_ptr<TargetGroup> FilterTestsBase::givenTargetGroupWithProperParent() {
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);


        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;

        return tg;
}
void FilterTestsBase::TearDown() {

}

FilterTestsBase::~FilterTestsBase() {

}

FilterTestsBase::FilterTestsBase() {
        this->testService = std::make_shared<BidderTestService> ();
}

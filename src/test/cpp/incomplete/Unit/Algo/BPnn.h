//
// Created by Mahmoud Taabodi on 5/8/16.
//

#ifndef BIDDER_BPNN_H
#define BIDDER_BPNN_H


#include <iostream>
#include <armadillo>
#include <math.h>
#include <memory>

using namespace arma;
using namespace std;

#define elif else if
#define MAX_ITER 10000

class BPnn;


class BPnn {

private :
    double lrate = 0.1;
    double lambda = 0.0;

    int numHiddenLayerNode;
    int numOutputNodes;
    int numHiddenLayers = 1;

public :

    colvec vec2colvec(vector<double>& vec);

    rowvec vec2rowvec(vector<double>& vec);

    mat vec2mat(vector<vector<double> >&vec);

//    colvec log(colvec vec);
//
//    rowvec log(rowvec vec);

    double sigmoid(double z);
    rowvec sigmoid(rowvec z);

    colvec sigmoid(colvec z);

    double dsigmoid(double z);
    colvec dsigmoid(colvec a);

    rowvec dsigmoid(rowvec a);

    vector<colvec> getActivation(mat x, vector<mat>& weightsMatrix, int m);

//h(xi) is just last vector of a
    colvec gethm(vector<colvec> a);
    double getCostFunction(mat x, vector<mat>& weightsMatrix, mat y, double lambda);

    vector<mat> getdJ(mat x, mat y, vector<mat>& weightsMatrix, double lambda);

    colvec calculateY(colvec x, vector<mat> weightsMatrix);

    void bpnn(vector<vector<double> >&vecX, vector<vector<double> >&vecY, vector<vector<double> >& testX, vector<vector<double> >& testY);
    int mainAlgo(int argc, char** argv);

};

#endif //BIDDER_BPNN_H

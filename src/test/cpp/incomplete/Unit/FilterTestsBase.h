//
// Created by Mahmoud Taabodi on 4/14/16.
//

#ifndef BIDDER_FILTERTESTSBASE_H
#define BIDDER_FILTERTESTSBASE_H
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "Advertiser.h"


#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
class OpportunityContext;
#include "BWList.h"
class FilterTestsBase  : public ::testing::Test {

private:

public:

    BidderTestServicePtr testService;

    FilterTestsBase();

    std::shared_ptr<TargetGroup> givenTargetGroupWithProperParent();

    void givenTargetGroupWithProperParent(std::shared_ptr<TargetGroup> tg);

    void givenTargetGroupInContext(std::shared_ptr<TargetGroup> tg1);

    void givenTheCreativeAndTargetGroupsExistInCache();

    void givenTargetGroupExistInCache(std::shared_ptr<TargetGroup> tg);

    void givenCampaignExistInCache(std::shared_ptr<Campaign> campaign);

    void givenAdvertiserExistInCache(std::shared_ptr<Advertiser> advertiser);

    void givenTargetHaveBWList(int targetGroupId, int bwListId);

    std::shared_ptr<BWList> givenBWListWithDomains(std::string listType, std::vector<std::string> domains, int targetGroupId);

    void SetUp();

    void TearDown();

    virtual ~FilterTestsBase() ;

};
#endif //BIDDER_FILTERTESTSBASE_H

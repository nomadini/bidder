/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BidRequestHandlerTests_H
#define BidRequestHandlerTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class BidRequestCreator;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BidRequestHandler.h"
#include "BiddingMode.h"
#include "MockHttpResponse.h"
#include "MockHttpRequest.h"

class BidRequestHandlerTests : public ::testing::Test {

private:

public:
BidRequestHandlerPtr bidRequestHandler;
BidderTestServicePtr testService;
std::shared_ptr<BidRequestCreator> bidRequestCreator;

BidRequestHandlerTests();

void thenResponseIsNoBid(MockHttpResponse& response);

void SetUp();

void givenHandlerGoneToMode(BiddingMode mode);

void givenBidPercentage(int bidPercentage);

void thenBidGetsProcessed(int numberOfProcessed);

void thenBidGetsProcessedGreaterThan(int numberOfProcessed);

virtual ~BidRequestHandlerTests();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */


#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "BidderRequestHandlerFactoryTests.h"
#include "BidderRequestHandlerFactory.h"
#include "BidderTestsHelper.h"
#include "BidderTestService.h"
#include <boost/foreach.hpp>
#include "BidderRequestHandlerFactory.h"
#include "BidderMainPipelineProcessor.h"
#include "BeanFactory.h"
#include "EntityDeliveryInfoFetcher.h"
#include "AdServerStatusChecker.h"
#include "BidderMainPipelineProcessorMock.h"
#include "AerospikeDriver.h"
#include "CassandraDriverInterface.h"


BidderRequestHandlerFactoryTests::BidderRequestHandlerFactoryTests() : request("") {
}

void BidderRequestHandlerFactoryTests::SetUp() {
        this->requestHandler = NULL;
        this->request.requestStream = NULL;
        this->testService = std::make_shared<BidderTestService>();
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats> ();
        std::shared_ptr<BidderMainPipelineProcessorMock> bidderMainPipelineProcessorMock = std::make_shared<BidderMainPipelineProcessorMock>();
        BidderMainPipelineProcessorPtr bidderMainPipelineProcessor = std::make_shared<BidderMainPipelineProcessor>();

        auto configService = std::make_shared<gicapods::ConfigService>(
                "bidder-test.properties", "common-test.properties");
        auto aeroSpikeDriver = std::make_shared<AerospikeDriver>("127.0.0.1", 3000, nullptr);
        aeroSpikeDriver->entityToModuleStateStats = entityToModuleStateStats;
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        auto cassandraDriver = std::make_shared<CassandraDriver> (configService,
                                                                  entityToModuleStateStats);

        // bidderRequestHandlerFactory = std::make_shared<BidderRequestHandlerFactory> (bidderMainPipelineProcessorMock,
        //                                                                              entityToModuleStateStats,
        //                                                                              beanFactory->configService);


}

BidderRequestHandlerFactoryTests::~BidderRequestHandlerFactoryTests() {

}

void BidderRequestHandlerFactoryTests::givenPacerIsDown() {
        // EntityDeliveryInfoFetcher::isFetchingDeliveryInfoFromPacerHealthy->setValue(false);
        // areAdserversHealthy->setValue(true);
}

void BidderRequestHandlerFactoryTests::givenAdserverIsDown() {
        // areAdserversHealthy->setValue(false);
        // EntityDeliveryInfoFetcher::isFetchingDeliveryInfoFromPacerHealthy->setValue(true);
}

void BidderRequestHandlerFactoryTests::givenAdserverAndPacingUp() {
        // areAdserversHealthy->setValue(true);
        // EntityDeliveryInfoFetcher::isFetchingDeliveryInfoFromPacerHealthy->setValue(true);
}
void BidderRequestHandlerFactoryTests::givenRequestHittingNoBidModeEndpoint() {
        this->request.setURI("/no_bid_mode_turn_on");
}

void BidderRequestHandlerFactoryTests::givenRequestHittingBidModeEndpoint() {
        this->request.setURI("/no_bid_mode_turn_off");
}

void BidderRequestHandlerFactoryTests::givenRequestHittingSlowBidModeEndpoint(int percentage) {
        this->request.setURI("/slowBidMode/"+StringUtil::toStr(percentage) +"/asd");

}

void BidderRequestHandlerFactoryTests::givenRequestHittingSlowBidModeEndpointWithStr(std::string uri) {
        this->request.setURI(uri);
}

void BidderRequestHandlerFactoryTests::givenRequestComingForOpenRtb(){
        this->request.setURI("/bid/openrtb2.3");
        assertAndThrow (StringUtil::equalsIgnoreCase (this->request.getURI(), "/bid/openrtb2.3"));
}

void BidderRequestHandlerFactoryTests::thenBidderRequestHandlerFactoryIsInBiddingMode() {

}

void BidderRequestHandlerFactoryTests::thenBidderRequestHandlerFactoryIsInNoBiddingMode() {
        // EXPECT_THAT(bidderRequestHandlerFactory->biddingMode, testing::Eq(BiddingMode::NoBidding));
}
void BidderRequestHandlerFactoryTests::whenRequestHandlerIsCreated(){
        try {
                requestHandler = dynamic_cast<BidRequestHandler *>(bidderRequestHandlerFactory->createRequestHandler (
                                                                           this->request));
        }catch (...) {
                LOG(INFO)<<"caught exception";
        }

}

void BidderRequestHandlerFactoryTests::thenRequestHandlerIsInNoBidMode(std::string result){

        // EXPECT_THAT(requestHandler->bidMode, testing::Eq(BiddingMode::NoBidding));

        std::string expectedResult = "["+result+",\"NO_BID_BECAUSE_OF_HEALTH\"]";
        // EXPECT_THAT(bidderRequestHandlerFactory->entityToModuleStateStats->getAllPossibleStatesForModuleAndEntity ("BidderRequestHandlerFactory",
        //                                                                                                            "ALL"),testing::Eq (expectedResult));

}

void BidderRequestHandlerFactoryTests::thenRequestHandlerIsNull() {
        EXPECT_TRUE(this->requestHandler == NULL);
}

void BidderRequestHandlerFactoryTests::thenBidderRequestHandlerFactoryHasBidPercentage(int percentage) {
        EXPECT_THAT(bidderRequestHandlerFactory->bidPercentage, testing::Eq (percentage));
}

TEST_F(BidderRequestHandlerFactoryTests, testSettingBidRequestHandlerToNoBidModeWhenPacerIsDown) {

        givenPacerIsDown();
        givenRequestComingForOpenRtb();

        whenRequestHandlerIsCreated();

        thenRequestHandlerIsInNoBidMode("\"PACING_DOWN\"");
}


TEST_F(BidderRequestHandlerFactoryTests, testSettingBidRequestHandlerToNoBidModeWhenAdserverIsDown) {

        givenAdserverIsDown();
        givenRequestComingForOpenRtb();

        whenRequestHandlerIsCreated();

        thenRequestHandlerIsInNoBidMode("\"ADSERVER_DOWN\"");
}


TEST_F(BidderRequestHandlerFactoryTests, testGoingToNoBidModeViaHttpRequest) {

        givenAdserverAndPacingUp();
        givenRequestHittingNoBidModeEndpoint();

        whenRequestHandlerIsCreated();

        thenBidderRequestHandlerFactoryIsInNoBiddingMode();
}

TEST_F(BidderRequestHandlerFactoryTests, testGoingToBidModeViaHttpRequest) {

        givenAdserverAndPacingUp();
        givenRequestHittingBidModeEndpoint();

        whenRequestHandlerIsCreated();

        thenBidderRequestHandlerFactoryIsInBiddingMode();
}


TEST_F(BidderRequestHandlerFactoryTests, testSettingSlowBidMode) {

        givenAdserverAndPacingUp();
        givenRequestHittingSlowBidModeEndpoint(10);

        whenRequestHandlerIsCreated();

        thenBidderRequestHandlerFactoryHasBidPercentage(10);
}


TEST_F(BidderRequestHandlerFactoryTests, testSettingSlowBidModeWithNoEndingString) {

        givenAdserverAndPacingUp();
        givenRequestHittingSlowBidModeEndpointWithStr("/slowBidMode/20");

        whenRequestHandlerIsCreated();

        thenBidderRequestHandlerFactoryHasBidPercentage(20);
}

TEST_F(BidderRequestHandlerFactoryTests, testSettingSlowBidModeWithBadString) {

        givenAdserverAndPacingUp();
        givenRequestHittingSlowBidModeEndpointWithStr("/slowBidMode/");

        whenRequestHandlerIsCreated();

        thenRequestHandlerIsNull();
}

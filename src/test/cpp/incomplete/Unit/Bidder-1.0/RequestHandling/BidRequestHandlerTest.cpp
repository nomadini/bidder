
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidRequestHandlerTests.h"
#include "BidRequestCreator.h"
#include "BidRequestHandler.h"
#include "OpportunityContext.h"
#include "OpportunityContextBuilder.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include <boost/foreach.hpp>
#include "BidRequestHandler.h"
#include "MockHttpRequest.h"
#include "MockHttpResponse.h"
#include "OpenRtbBidRequest.h"
#include "EntityToModuleStateStats.h"
#include "BidderMainPipelineProcessorMock.h"

BidRequestHandlerTests::BidRequestHandlerTests() {
        bidRequestCreator = std::make_shared<BidRequestCreator> ();
}

BidRequestHandlerTests::~BidRequestHandlerTests() {

}

void BidRequestHandlerTests::SetUp() {
        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;

        OpenRtbBidRequestParserPtr openRtbBidRequestParser = std::make_shared<OpenRtbBidRequestParser>();
        OpenRtbBidResponseJsonConverterPtr openRtbBidResponseJsonConverter = std::make_shared<OpenRtbBidResponseJsonConverter> ();
        OpportunityContextBuilderPtr opportunityContextBuilder;// = std::make_shared<OpportunityContextBuilder>();
        std::shared_ptr<BidderMainPipelineProcessorMock> bidderMainPipelineProcessor = std::make_shared<BidderMainPipelineProcessorMock>();

        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();

        auto opportunityContext = std::make_shared<OpportunityContext>();
        ON_CALL(*bidderMainPipelineProcessor,
                process(_))
        .WillByDefault(Return(opportunityContext));

        // bidRequestHandler = std::make_shared<BidRequestHandler>(100,
        //                                                         opportunityContextBuilder,
        //                                                         "2.3",
        //                                                         bidderMainPipelineProcessor,
        //                                                         openRtbBidResponseJsonConverter,
        //                                                         openRtbBidRequestParser,
        //                                                         entityToModuleStateStats,
        //                                                         "adserveHostUrlExample.com");
}

void BidRequestHandlerTests::givenBidPercentage(int bidPercentage) {
        // bidRequestHandler->bidPercentage = bidPercentage;
}

void BidRequestHandlerTests::givenHandlerGoneToMode(BiddingMode mode) {

}

void BidRequestHandlerTests::thenBidGetsProcessed(int numberOfProcessed) {

        EXPECT_THAT(bidRequestHandler->entityToModuleStateStats->
                    getValueOfStateModuleForEntity ("numberOfBidsProcessed",
                                                    "BidRequestHandler",
                                                    "ALL"),
                    testing::Eq (numberOfProcessed));

}

void BidRequestHandlerTests::thenBidGetsProcessedGreaterThan(int numberOfProcessed) {
        int actualProcessed = bidRequestHandler->entityToModuleStateStats->
                              getValueOfStateModuleForEntity ("numberOfBidsProcessed",
                                                              "BidRequestHandler",
                                                              "ALL");
        auto lambdaGreaterComparison = [](int a, int b) {
                                               return a > b;
                                       };

        EXPECT_PRED2 ( lambdaGreaterComparison, actualProcessed, numberOfProcessed);

}

void BidRequestHandlerTests::thenResponseIsNoBid(MockHttpResponse& response) {
        EXPECT_THAT(response.getResponseBody().size(), testing::Eq(0));
        EXPECT_THAT(response.getStatus(),
                    testing::Eq(Poco::Net::HTTPResponse::HTTPStatus::HTTP_NO_CONTENT));
}

TEST_F(BidRequestHandlerTests, testSendingNoReponseWhenBidRequestHandlerIsInNoBidMode) {

        // givenHandlerGoneToMode(BiddingMode::NoBidding);

        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest = bidRequestCreator->createOpenRtbBidRequest ();

        std::string requestBody = bidRequest->toJson();
        MockHttpRequest request(requestBody);

        MockHttpResponse response;
        bidRequestHandler->handleRequest (request, response);

        thenResponseIsNoBid(response);
}

TEST_F(BidRequestHandlerTests, testSendingNoBidWhenBidPercentageIs0) {
        givenBidPercentage(0);

        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest = bidRequestCreator->createOpenRtbBidRequest ();

        std::string requestBody = bidRequest->toJson();
        MockHttpRequest request(requestBody);

        MockHttpResponse response;
        bidRequestHandler->handleRequest (request, response);

        thenResponseIsNoBid(response);
}

TEST_F(BidRequestHandlerTests, testSendingBidWhenBidPercentageIs100) {
        givenBidPercentage(100);

        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest = bidRequestCreator->createOpenRtbBidRequest ();

        std::string requestBody = bidRequest->toJson();
        MockHttpRequest request(requestBody);

        MockHttpResponse response;
        bidRequestHandler->handleRequest (request, response);


        thenBidGetsProcessed(1);
}

TEST_F(BidRequestHandlerTests, testSendingBidWhenBidPercentageIs50) {
        givenBidPercentage(50);

        for (int i=0; i < 10; i++) {
                std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest = bidRequestCreator->createOpenRtbBidRequest ();

                std::string requestBody = bidRequest->toJson();
                MockHttpRequest request(requestBody);

                MockHttpResponse response;
                try {
                        bidRequestHandler->handleRequest (request, response);
                } catch(...) {
                        LOG(INFO) <<"exception caught";
                }
        }

        thenBidGetsProcessedGreaterThan(2);
}

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BidderRequestHandlerFactoryTests_H
#define BidderRequestHandlerFactoryTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BidderRequestHandlerFactory.h"
#include "BiddingMode.h"
#include "MockHttpRequest.h"
#include "BidRequestHandler.h"

class BidderRequestHandlerFactoryTests  : public ::testing::Test {

private:

public:
    BidderRequestHandlerFactoryPtr bidderRequestHandlerFactory;
    BidderTestServicePtr testService;
    MockHttpRequest request;

    BidRequestHandler* requestHandler;
    BidderRequestHandlerFactoryTests() ;

    void SetUp();

    void givenPacerIsDown();

    void givenAdserverIsDown();

    void givenRequestComingForOpenRtb();

    void whenRequestHandlerIsCreated();

    void thenRequestHandlerIsInNoBidMode(std::string result);

    void givenAdserverAndPacingUp();

    void givenRequestHittingNoBidModeEndpoint();

    void thenBidderRequestHandlerFactoryIsInBiddingMode();

    void thenBidderRequestHandlerFactoryIsInNoBiddingMode();

    void thenBidderRequestHandlerFactoryHasBidPercentage(int percentage);

    void givenRequestHittingSlowBidModeEndpoint(int percentage);

    void givenRequestHittingSlowBidModeEndpointWithStr(std::string uri);

    void givenRequestHittingBidModeEndpoint();

    void thenRequestHandlerIsNull();

    virtual ~BidderRequestHandlerFactoryTests() ;

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

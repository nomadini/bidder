
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "CampaignImpressionCappingFilterModuleTests.h"
#include "CampaignImpressionCappingFilterModule.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"

CampaignImpressionCappingFilterModuleTests::CampaignImpressionCappingFilterModuleTests() {

        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;

}
CampaignImpressionCappingFilterModuleTests::~CampaignImpressionCappingFilterModuleTests() {

}

TEST_F(CampaignImpressionCappingFilterModuleTests, filteringCampaignsWithNoRealTimeInfoByImpression) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression + 1);
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression + 1);

        beanFactory->campaignCacheService()->reloadCaches;

        std::shared_ptr<CampaignImpressionCappingFilterModule> module;
        // =
        //         std::make_shared<CampaignImpressionCappingFilterModule>(
        //                 beanFactory->campaignCacheService,
        //                 beanFactory->entityToModuleStateStats,
        //                 beanFactory->campaignRealTimeInfoCassandraService);

        MLOG(3)<<"tg id is "<< tg->getId();

        module->process(context);
        std::string expectedResult = "[\"FAILED_FOR_NO_REAL_TIME_INFO\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(CampaignImpressionCappingFilterModuleTests, filteringCampaignsThatHaveCapped) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);


        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression + 1);
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression + 1);


        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;
        // beanFactory->campaignRealTimeInfoCassandraService()->addCampaignRealTimeInfoToCache(campaign->getId, campaignRealTimeInfo);

        std::shared_ptr<CampaignImpressionCappingFilterModule> module;
        // =
        //         std::make_shared<CampaignImpressionCappingFilterModule>(beanFactory->campaignCacheService,
        //                                                                 beanFactory->entityToModuleStateStats,
        //                                                                 beanFactory->campaignRealTimeInfoCassandraService);

        module->process(context);

        std::string expectedResult = "[\"FAILED\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        MLOG(3)<<"size of context->allAvailableTgs "<< context->getSizeOfUnmarkedTargetGroups()();
        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(CampaignImpressionCappingFilterModuleTests, passingCampaignsThatHaveNotCapped) {


        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);
        campaign->setMaxImpression(dailyMaxImpression);
        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression - 1);
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression - 1);

        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;
        // beanFactory->campaignRealTimeInfoCassandraService()->addCampaignRealTimeInfoToCache(campaign->getId, campaignRealTimeInfo);

        std::shared_ptr<CampaignImpressionCappingFilterModule> module;
        //  =
        //         std::make_shared<CampaignImpressionCappingFilterModule>(beanFactory->campaignCacheService,
        //                                                                 beanFactory->entityToModuleStateStats,
        //                                                                 beanFactory->campaignRealTimeInfoCassandraService);

        module->process(context);

        std::string expectedResult = "[\"PASSED\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));
        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(1));
        EXPECT_THAT(context->allAvailableTgs->at(0)->getId(),
                    testing::Eq(tg->getId()));
}

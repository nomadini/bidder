
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "CampaignBudgetCappingFilterModuleTests.h"
#include "CampaignBudgetCappingFilterModule.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
#include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"

CampaignBudgetCappingFilterModuleTests::CampaignBudgetCappingFilterModuleTests() {

        this->testService = std::make_shared<BidderTestService>();
}
CampaignBudgetCappingFilterModuleTests::~CampaignBudgetCappingFilterModuleTests() {

}

TEST_F(CampaignBudgetCappingFilterModuleTests, filteringCampaignsWithNoRealTimeInfo) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression + 1);
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression + 1);


        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<CampaignBudgetCappingFilterModule> module;
        // =
        //         std::make_shared<CampaignBudgetCappingFilterModule>(beanFactory->campaignCacheService,
        //                                                             beanFactory->entityToModuleStateStats,
        //                                                             beanFactory->campaignRealTimeInfoCassandraService);

        module->process(context);

        std::string expectedResult = "[\"FAILED_FOR_NO_REAL_TIME_INFO\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        ASSERT_EQ (context->getSizeOfUnmarkedTargetGroups()(), 0);

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(CampaignBudgetCappingFilterModuleTests, filteringCampaignsThatHaveCapped) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxBudget = 20;
        campaign->setDailyMaxBudget(dailyMaxBudget);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);


        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->platformCostSpentOverallUpToNow->setValue(dailyMaxBudget + 1);
        campaignRealTimeInfo->platformCostSpentInCurrentDateUpToNow->setValue(dailyMaxBudget + 1);


        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<CampaignBudgetCappingFilterModule> module;

        module->process(context);

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));
        MLOG(3)<<"size of context->allAvailableTgs "<< context->getSizeOfUnmarkedTargetGroups()();
        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(CampaignBudgetCappingFilterModuleTests, passingCampaignsThatHaveNotCapped) {


        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);
        campaign->setMaxImpression(dailyMaxImpression);
        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        CampaignRealTimeInfoPtr campaignRealTimeInfo = testService->createSampleCampaignRealtimInfo();
        campaignRealTimeInfo->campaignId = campaign->getId();
        campaignRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression - 1);
        campaignRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression - 1);

        beanFactory->campaignCacheService()->reloadCaches;
        // beanFactory->campaignRealTimeInfoCassandraService()->addCampaignRealTimeInfoToCache(campaign->getId, campaignRealTimeInfo);

        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<CampaignBudgetCappingFilterModule> module;
        //  =
        //         std::make_shared<CampaignBudgetCappingFilterModule>(beanFactory->campaignCacheService,
        //                                                             beanFactory->entityToModuleStateStats,
        //                                                             beanFactory->campaignRealTimeInfoCassandraService);

        module->process(context);

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CampaignBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(1));
        EXPECT_THAT(context->allAvailableTgs->at(0)->getId(),
                    testing::Eq(tg->getId()));
}

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupBudgetCappingFilterModuleTests_H
#define TargetGroupBudgetCappingFilterModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "CampaignBudgetCappingFilterModule.h"
#include "BeanFactory.h"
class TargetGroupBudgetCappingFilterModuleTests : public ::testing::Test {

private:

public:
BidderTestServicePtr testService;
BeanFactory* beanFactory;
TargetGroupBudgetCappingFilterModuleTests();

virtual ~TargetGroupBudgetCappingFilterModuleTests();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

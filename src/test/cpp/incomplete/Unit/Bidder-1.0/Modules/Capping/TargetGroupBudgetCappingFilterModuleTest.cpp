
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "TargetGroupBudgetCappingFilterModuleTests.h"
#include "TargetGroupBudgetCappingFilterModule.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"

TargetGroupBudgetCappingFilterModuleTests::TargetGroupBudgetCappingFilterModuleTests() {

        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;

}
TargetGroupBudgetCappingFilterModuleTests::~TargetGroupBudgetCappingFilterModuleTests() {

}

TEST_F(TargetGroupBudgetCappingFilterModuleTests, filteringTargetGroupsWithNoRealTimeInfo) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupBudgetCappingFilterModule> module;
        //  =
        //         std::make_shared<TargetGroupBudgetCappingFilterModule>
        //                 (beanFactory->entityToModuleStateStats);

        module->process(context);
        // EXPECT_THAT(module->entityToModuleStateStats->getFifteenMinuteStatePercForModuleAndEntity
        //                     ("IGNORED_FOR_NO_REAL_TIME_INFO", "TargetGroupBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(100));
        ASSERT_EQ (context->getSizeOfUnmarkedTargetGroups()(), 0);

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(TargetGroupBudgetCappingFilterModuleTests, filteringCampaignsThatHaveCapped) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        double dailyMaxBudget = 20;
        tg->setDailyMaxBudget(dailyMaxBudget);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);


        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeDeliveryInfo->targetGroupId = tg->getId();
        tgRealTimeDeliveryInfo->platformCostSpentInCurrentDateUpToNow->setValue(dailyMaxBudget + 0.001);
        tgRealTimeDeliveryInfo->platformCostSpentOverallUpToNow->setValue(dailyMaxBudget + 0.001);
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ->insert( std::make_pair(tgRealTimeDeliveryInfo->targetGroupId, tgRealTimeDeliveryInfo));

        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupBudgetCappingFilterModule> module;
        //  =
        // std::make_shared<TargetGroupBudgetCappingFilterModule>(
        // beanFactory->entityToModuleStateStats);

        module->process(context);

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("TargetGroupBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));
        //
        // EXPECT_THAT(module->entityToModuleStateStats->getFifteenMinuteStatePercForModuleAndEntity
        //                     ("FAILED", "TargetGroupBudgetCappingFilterModule",StringUtil::toStr(tg->getId())),
        //             testing::Eq(100));

        MLOG(3)<<"size of context->allAvailableTgs "<< context->getSizeOfUnmarkedTargetGroups()();

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(TargetGroupBudgetCappingFilterModuleTests, passingTargetGroupsThatHaveNotCapped) {


        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        campaign->setDailyMaxImpression(dailyMaxImpression);
        campaign->setMaxImpression(dailyMaxImpression);
        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        // std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfo = testService->createSampleTargetGroupRealtimInfo();
        // tgRealTimeDeliveryInfo->targetGroupId = tg->getId();
        // tgRealTimeDeliveryInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression - 1);
        // tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression - 1);
        //
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ->insert( std::make_pair(tgRealTimeDeliveryInfo->targetGroupId, tgRealTimeDeliveryInfo));
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupBudgetCappingFilterModule> module;
        // =
        //       std::make_shared<TargetGroupBudgetCappingFilterModule>
        //               (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //               beanFactory->entityToModuleStateStats);
        module->process(context);
        //
        // EXPECT_THAT(module->entityToModuleStateStats->getFifteenMinuteStatePercForModuleAndEntity
        //                     ("PASSED", "TargetGroupBudgetCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(100));
        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(1));
        EXPECT_THAT(context->allAvailableTgs->at(0)->getId(),
                    testing::Eq(tg->getId()));
}

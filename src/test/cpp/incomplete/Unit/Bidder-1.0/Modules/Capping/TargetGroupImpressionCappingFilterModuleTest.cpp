
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "TargetGroupImpressionCappingFilterModuleTests.h"
#include "TargetGroupImpressionCappingFilterModule.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"

TargetGroupImpressionCappingFilterModuleTests::TargetGroupImpressionCappingFilterModuleTests() {

        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;

}
TargetGroupImpressionCappingFilterModuleTests::~TargetGroupImpressionCappingFilterModuleTests() {

}

TEST_F(TargetGroupImpressionCappingFilterModuleTests, filteringTargetGroupsWithNoRealTimeInfo) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupImpressionCappingFilterModule> module;
        //  =
        //         std::make_shared<TargetGroupImpressionCappingFilterModule>
        //         (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //          beanFactory->entityToModuleStateStats);


        module->process(context);

        std::string expectedResult = "[\"FAILED_FOR_NO_REAL_TIME_INFO\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("TargetGroupImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        ASSERT_EQ (context->getSizeOfUnmarkedTargetGroups()(), 0);

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(TargetGroupImpressionCappingFilterModuleTests, filteringCampaignsThatHaveCapped) {
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;

        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        tg->setMaxImpression(dailyMaxImpression);
        tg->setDailyMaxImpression(dailyMaxImpression);

        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);


        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeDeliveryInfo->targetGroupId = tg->getId();
        tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression + 1);
        tgRealTimeDeliveryInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression + 1);

        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ->insert( std::make_pair(tgRealTimeDeliveryInfo->targetGroupId, tgRealTimeDeliveryInfo));
        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupImpressionCappingFilterModule> module;
        //  =
        //         std::make_shared<TargetGroupImpressionCappingFilterModule>
        //                 (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //                 beanFactory->entityToModuleStateStats);

        module->process(context);

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("TargetGroupImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));
        MLOG(3)<<"size of context->allAvailableTgs "<< context->getSizeOfUnmarkedTargetGroups()();
        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(0));
}

TEST_F(TargetGroupImpressionCappingFilterModuleTests, passingCampaignsThatHaveNotCapped) {


        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCampaignService()->deleteAll;

        std::shared_ptr<OpportunityContext> context = std::make_shared<OpportunityContext>();
        auto tg = testService->targetGroup;
        context->allAvailableTgs->push_back(tg);
        auto campaign = testService->campaign;
        int dailyMaxImpression = 20;
        tg->setDailyMaxImpression(dailyMaxImpression);
        tg->setMaxImpression(dailyMaxImpression);
        beanFactory->mySqlCampaignService->insert(campaign);
        //this should be done after insertion in mysql db
        tg->setCampaignId(campaign->getId());
        beanFactory->mySqlTargetGroupService->insert(tg);

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeDeliveryInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeDeliveryInfo->targetGroupId = tg->getId();
        tgRealTimeDeliveryInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(dailyMaxImpression - 1);
        tgRealTimeDeliveryInfo->numOfImpressionsServedOverallUpToNow->setValue(dailyMaxImpression - 1);
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ->insert( std::make_pair(tgRealTimeDeliveryInfo->targetGroupId, tgRealTimeDeliveryInfo));

        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupImpressionCappingFilterModule> module;
        //  =
        //         std::make_shared<TargetGroupImpressionCappingFilterModule>
        //                 (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //                 beanFactory->entityToModuleStateStats);

        module->process(context);

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("TargetGroupImpressionCappingFilterModule", StringUtil::toStr(tg->getId())),
        //             testing::Eq(expectedResult));

        EXPECT_THAT(context->getSizeOfUnmarkedTargetGroups()(),
                    testing::Eq(1));
        EXPECT_THAT(context->allAvailableTgs->at(0)->getId(),
                    testing::Eq(tg->getId()));
}

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef CampaignImpressionCappingFilterModuleTests_H
#define CampaignImpressionCappingFilterModuleTests_H



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BeanFactory.h"
class CampaignImpressionCappingFilterModuleTests : public ::testing::Test {

private:

public:
BidderTestServicePtr testService;
BeanFactory* beanFactory;
CampaignImpressionCappingFilterModuleTests();

virtual ~CampaignImpressionCappingFilterModuleTests();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

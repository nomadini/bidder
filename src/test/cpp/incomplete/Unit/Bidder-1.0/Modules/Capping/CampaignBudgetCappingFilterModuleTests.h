/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef CampaignBudgetCappingFilterModuleTests_H
#define CampaignBudgetCappingFilterModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BeanFactory.h"
#include "CampaignBudgetCappingFilterModule.h"

class CampaignBudgetCappingFilterModuleTests : public ::testing::Test {

private:

public:
BidderTestServicePtr testService;
BeanFactory* beanFactory;
CampaignBudgetCappingFilterModuleTests();

virtual ~CampaignBudgetCappingFilterModuleTests();

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

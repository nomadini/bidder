//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GlobalWhiteListModuleTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "GlobalWhiteListModuleTests.h"
#include "OpportunityContext.h"
#include "BeanFactory.h"
#include "BidderTestService.h"
#include "LastTimeSeenSource.h"

GlobalWhiteListModuleTests::GlobalWhiteListModuleTests() {

}

GlobalWhiteListModuleTests::~GlobalWhiteListModuleTests() {

}

void GlobalWhiteListModuleTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<GlobalWhiteListModule> (beanFactory->globalWhiteListedCacheService,
        //                                                   beanFactory->entityToModuleStateStats);
}

void GlobalWhiteListModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void GlobalWhiteListModuleTests::whenModuleRuns() {
        status = module->process (testService->context);
}

void GlobalWhiteListModuleTests::givenDomainInWhiteList (std::string domain) {
        beanFactory->mySqlGlobalWhiteListService->insert(domain);
        beanFactory->globalWhiteListedCacheService()->reloadCaches;
}

void GlobalWhiteListModuleTests::givenUserComingFromWebsite(std::string domain) {
        testService->context->siteDomain = domain;
}

TEST_F(GlobalWhiteListModuleTests, passingDomainIsInWhiteList) {

        givenDomainInWhiteList("abc.com");
        givenUserComingFromWebsite("abc.com");

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("GlobalWhiteListModule" ,
        //             "ALL") ,
        // testing::Eq (expectedResult));

        EXPECT_THAT(status, testing::Eq (CONTINUE_PROCESSING));

}

TEST_F(GlobalWhiteListModuleTests, notBiddingOnUserWhenComingFromAnUnknownDomain) {

        givenDomainInWhiteList("abc.com");
        givenUserComingFromWebsite("abcd.com");

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("GlobalWhiteListModule" ,
        //                                                 "ALL") ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(status, testing::Eq (STOP_PROCESSING));
}

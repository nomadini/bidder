//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_DeviceTypeFilterTests_H
#define BIDDER_DeviceTypeFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "DeviceTypeFilter.h"

class DeviceTypeFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<DeviceTypeFilter> module;

    void givenUserComingFromADeviceOfType(std::string deviceType);

    void givenTargetGroupHavingNoDaypartMapping(std::shared_ptr<TargetGroup> tg);

    DeviceTypeFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~DeviceTypeFilterTests() ;

};

#endif //BIDDER_DeviceTypeFilterTests_H

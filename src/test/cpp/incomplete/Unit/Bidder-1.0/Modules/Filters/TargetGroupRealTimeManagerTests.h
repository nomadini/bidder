//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_EntityDeliveryInfoFetcherTests_H
#define BIDDER_EntityDeliveryInfoFetcherTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "EntityDeliveryInfoFetcher.h"

#include "HttpUtilServiceMock.h"
#include "BidderApplicationContext.h"

class EntityDeliveryInfoFetcherTests : public FilterTestsBase {

private:

public:
std::shared_ptr<EntityDeliveryInfoFetcher> module;
HttpUtilServiceMockPtr httpUtilService;
EntityDeliveryInfoFetcherTests();

void whenModuleRuns();
bool resultsAreEqual(std::string actualResult, std::string expectedResult);
void SetUp();

void TearDown();

virtual ~EntityDeliveryInfoFetcherTests();

};

#endif //BIDDER_EntityDeliveryInfoFetcherTests_H

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_MatchingIabCategoryFilterTests_H
#define BIDDER_MatchingIabCategoryFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "MatchingIabCategoryFilter.h"

class MatchingIabCategoryFilterTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<MatchingIabCategoryFilter> module;

    void givenRequestHavingIabCategories(std::string iabCategory , std::string iabSubCategory);
    void givenTgHavingIabCategories(std::shared_ptr<TargetGroup> tg, std::string iabCategory , std::string iabSubCategory);
    MatchingIabCategoryFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~MatchingIabCategoryFilterTests();

};

#endif //BIDDER_MatchingIabCategoryFilterTests_H

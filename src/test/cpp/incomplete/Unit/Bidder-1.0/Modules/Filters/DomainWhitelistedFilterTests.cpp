//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "DomainWhitelistedFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

DomainWhiteListedFilterTests::DomainWhiteListedFilterTests() {

}

DomainWhiteListedFilterTests::~DomainWhiteListedFilterTests() {

}

void DomainWhiteListedFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<DomainWhiteListedFilter> (beanFactory->targetGroupBWListCacheService);
}

void DomainWhiteListedFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void DomainWhiteListedFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void DomainWhiteListedFilterTests::givenBidRequestComingFromDomain(std::string domain) {
        testService->context->siteDomain = domain;
}

TEST_F(DomainWhiteListedFilterTests, passingTargetGroupWithWhiteListedDomain_RequestFromWhiteListedDomain) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto tg2 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);

        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());
        tg2->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->mySqlTargetGroupService->insert(tg2);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;

        std::vector<std::string> domains;
        std::string domain = "aBC.com";//this ensures case insensitivity of the whitelist filter
        domains.push_back(domain);
        domains.push_back("cnn.COM");
        domains.push_back("fox.COM");
        domains.push_back("fox1.COM");

        std::vector<std::string> domains2;
        domains2.push_back("cnn2.COM");
        domains2.push_back("fox2.COM");
        domains2.push_back (domain); //this will make sure that the whitelist filter
        //works when whitelisted domain is not the first in the domain list


        auto whiteList = givenBWListWithDomains(BWList::WHITE_LIST_TYPE, domains, tg->getId ());
        givenTargetHaveBWList(tg->getId (), whiteList->getId ());

        auto whiteList2 = givenBWListWithDomains(BWList::WHITE_LIST_TYPE, domains2, tg2->getId ());
        givenTargetHaveBWList(tg2->getId (), whiteList2->getId ());


        beanFactory->targetGroupBWListCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService ()->printCacheContent;
        beanFactory->campaignCacheService()->printCacheContent;
        beanFactory->advertiserCacheService()->printCacheContent;



        givenBidRequestComingFromDomain(domain);
        givenTargetGroupInContext (tg);
        givenTargetGroupInContext (tg2);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainWhiteListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));
        //
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainWhiteListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + StringUtil::toStr (tg2->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (2));

}

TEST_F(DomainWhiteListedFilterTests, filteringTargetGroupWithWhiteListedDomainA_RequestFromWhiteListedDomainB) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto tg2 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);

        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());
        tg2->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->mySqlTargetGroupService->insert(tg2);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;

        std::vector<std::string> domains;
        std::string domain = "aBC.com";//this ensures case insensitivity of the whitelist filter
        domains.push_back(domain);
        domains.push_back("cnn.COM");
        domains.push_back("fox.COM");
        domains.push_back("fox1.COM");

        std::vector<std::string> domains2;
        domains2.push_back("cnn2.COM");
        domains2.push_back("fox2.COM");
        domains2.push_back (domain + domain); //we make tg2 coming from domain + domain to make it fail


        auto whiteList = givenBWListWithDomains(BWList::WHITE_LIST_TYPE, domains, tg->getId ());
        givenTargetHaveBWList(tg->getId (), whiteList->getId ());

        auto whiteList2 = givenBWListWithDomains(BWList::WHITE_LIST_TYPE, domains2, tg2->getId ());
        givenTargetHaveBWList(tg2->getId (), whiteList2->getId ());


        beanFactory->targetGroupBWListCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService ()->printCacheContent;
        beanFactory->campaignCacheService()->printCacheContent;
        beanFactory->advertiserCacheService()->printCacheContent;


        givenBidRequestComingFromDomain(domain);
        givenTargetGroupInContext (tg);
        givenTargetGroupInContext (tg2);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainWhiteListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));
        //
        // expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainWhiteListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + StringUtil::toStr (tg2->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_GeoFeatureFilterTests_H
#define BIDDER_GeoFeatureFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "GeoFeatureFilter.h"

class GeoFeatureFilterTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<GeoFeatureFilter> module;


    GeoFeatureFilterTests();

    void whenModuleRuns();

    void givenRequestHaving(double lat, double lon);
    
    void SetUp();

    void TearDown();

    virtual ~GeoFeatureFilterTests();

};

#endif //BIDDER_GeoFeatureFilterTests_H

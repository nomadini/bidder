//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "BlockedCreativeAttributeFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BlockedCreativeAttributeFilterTests.h"
#include "BlockedBannerAdTypeFilter.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "BlockedBannerAdTypeFilter.h"

BlockedCreativeAttributeFilterTests::BlockedCreativeAttributeFilterTests() {

}

BlockedCreativeAttributeFilterTests::~BlockedCreativeAttributeFilterTests() {

}


void BlockedCreativeAttributeFilterTests::SetUp() {
        CreativeFiltersTestsBase::SetUp();

        // std::shared_ptr<CreativeSizeFilter> creativeFilter = std::make_shared<CreativeSizeFilter> (
        //         beanFactory->targetGroupCreativeCacheService ,
        //         beanFactory->creativeCacheService );
        // module = std::make_shared<BlockedCreativeAttributeFilter> (creativeFilter);
}

void BlockedCreativeAttributeFilterTests::TearDown() {
        CreativeFiltersTestsBase::TearDown();
}

void BlockedCreativeAttributeFilterTests::givenBlockedCreativeAttributes(std::vector<int> creativeAttributes) {
        // this->testService->context->blockedAttributes = creativeAttributes;
}

void BlockedCreativeAttributeFilterTests::whenModuleRuns() {
        testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void BlockedCreativeAttributeFilterTests::givenCreativeTypeInTargetGroupHasAttributes(std::shared_ptr<std::vector<int> > creativeAttributes) {
        // this->testService->creative->setAttributes(creativeAttributes);
}

TEST_F(BlockedCreativeAttributeFilterTests, filteringCreativeHasBlockingAttributes) {

        std::vector<int> creativeAttributes;
        creativeAttributes.push_back(1);
        creativeAttributes.push_back(2);
        givenBlockedCreativeAttributes(creativeAttributes);

        std::shared_ptr<std::vector<int> > tgCreativeAttributes = std::make_shared<std::vector<int> >();
        tgCreativeAttributes->push_back(1);
        givenCreativeTypeInTargetGroupHasAttributes(tgCreativeAttributes);

        givenTheCreativeAndTargetGroupsExistInCache();
        givenTargetGroupInContext (this->testService->targetGroup);
        whenModuleRuns();

        std::string expectedResult = "[\"FAILED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId(testService->targetGroup->getId(), testService->creative->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedCreativeAttributeFilter" ,
        //                                                 stateId) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(BlockedCreativeAttributeFilterTests, passingCreativeThatHasNoBlockingAttributes) {
        std::vector<int> creativeAttributes;
        creativeAttributes.push_back(1);
        creativeAttributes.push_back(2);
        givenBlockedCreativeAttributes(creativeAttributes);

        std::shared_ptr<std::vector<int> > tgCreativeAttributes = std::make_shared<std::vector<int> >();
        tgCreativeAttributes->push_back(3);
        givenCreativeTypeInTargetGroupHasAttributes(tgCreativeAttributes);

        givenTheCreativeAndTargetGroupsExistInCache();
        givenTargetGroupInContext (this->testService->targetGroup);
        whenModuleRuns();

        std::string expectedResult = "[\"PASSED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId(testService->targetGroup->getId(), testService->creative->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedCreativeAttributeFilter" ,
        //                                                 stateId) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

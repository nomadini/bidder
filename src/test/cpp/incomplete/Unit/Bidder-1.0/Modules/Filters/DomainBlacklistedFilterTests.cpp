//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "DomainBlacklistedFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

DomainBlackListedFilterTests::DomainBlackListedFilterTests() {

}

DomainBlackListedFilterTests::~DomainBlackListedFilterTests() {

}

void DomainBlackListedFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<DomainBlackListedFilter> (beanFactory->targetGroupBWListCacheService);
}

void DomainBlackListedFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void DomainBlackListedFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void DomainBlackListedFilterTests::givenBidRequestComingFromDomain(std::string domain) {
        testService->context->siteDomain = domain;
}

TEST_F(DomainBlackListedFilterTests, filteringTargetGroupWithBlackListedDomain_RequestFromBlackListedDomain) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto tg2 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);

        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());
        tg2->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->mySqlTargetGroupService->insert(tg2);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;

        std::vector<std::string> domains;
        std::string domain = "aBC.com";//this ensures case insensitivity of the Blacklist filter
        domains.push_back(domain);
        domains.push_back("cnn.COM");
        domains.push_back("fox.COM");
        domains.push_back("fox1.COM");

        std::vector<std::string> domains2;
        domains2.push_back("cnn2.COM");
        domains2.push_back("fox2.COM");
        domains2.push_back (domain); //this will make sure that the Blacklist filter
        //works when Blacklisted domain is not the first in the domain list


        auto BlackList = givenBWListWithDomains("Blacklist", domains, tg->getId ());
        givenTargetHaveBWList(tg->getId (), BlackList->getId ());

        auto BlackList2 = givenBWListWithDomains("Blacklist", domains2, tg2->getId ());
        givenTargetHaveBWList(tg2->getId (), BlackList2->getId ());


        beanFactory->targetGroupBWListCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService ()->printCacheContent;
        beanFactory->campaignCacheService()->printCacheContent;
        beanFactory->advertiserCacheService()->printCacheContent;



        givenBidRequestComingFromDomain(domain);
        givenTargetGroupInContext (tg);
        givenTargetGroupInContext (tg2);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_BLACK_LIST\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainBlackListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));
        //
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainBlackListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + StringUtil::toStr (tg2->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}

TEST_F(DomainBlackListedFilterTests, passingOneTargetGroupWithBlackListedDomainA_RequestFromBlackListedDomainB) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto tg2 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();

        beanFactory->mySqlAdvertiserService->insert(advertiser);
        campaign->setAdvertiserId(advertiser->id);

        beanFactory->mySqlCampaignService->insert(campaign);
        tg->setCampaignId(campaign->getId());
        tg2->setCampaignId(campaign->getId());

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->mySqlTargetGroupService->insert(tg2);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->campaignCacheService()->reloadCaches;
        beanFactory->advertiserCacheService()->reloadCaches;

        std::vector<std::string> domains;
        std::string domain = "aBC.com";//this ensures case insensitivity of the Blacklist filter
        domains.push_back(domain);
        domains.push_back("cnn.COM");
        domains.push_back("fox.COM");
        domains.push_back("fox1.COM");

        std::vector<std::string> domains2; //this doesn't have "abc.com" in its blacklist
        domains2.push_back("cnn2.COM");
        domains2.push_back("fox2.COM");



        auto BlackList = givenBWListWithDomains("Blacklist", domains, tg->getId ());
        givenTargetHaveBWList(tg->getId (), BlackList->getId ());

        auto BlackList2 = givenBWListWithDomains("Blacklist", domains2, tg2->getId ());
        givenTargetHaveBWList(tg2->getId (), BlackList2->getId ());


        beanFactory->targetGroupBWListCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService ()->printCacheContent;
        beanFactory->campaignCacheService()->printCacheContent;
        beanFactory->advertiserCacheService()->printCacheContent;


        givenBidRequestComingFromDomain(domain);
        givenTargetGroupInContext (tg);
        givenTargetGroupInContext (tg2);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_BLACK_LIST\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainBlackListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));
        //
        // expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DomainBlackListedFilter" ,
        //                                                 StringUtil::toStr ("tg") + StringUtil::toStr (tg2->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_BlockedAdvertiserDomainFilterTests_H
#define BIDDER_BlockedAdvertiserDomainFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"

class BlockedAdvertiserDomainFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<BlockedAdvertiserDomainFilter> module;

    void givenBidRequestHasBlockingAdvDomain(std::vector<std::string> blockingDomains);

    void givenAdvertiserHavingBlockingDomains(std::shared_ptr<Advertiser> adv, std::vector<std::string> blockingDomains);

    BlockedAdvertiserDomainFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~BlockedAdvertiserDomainFilterTests() ;

};

#endif //BIDDER_BlockedAdvertiserDomainFilterTests_H

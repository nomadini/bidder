//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_GeoLocationFilterTests_H
#define BIDDER_GeoLocationFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "GeoLocationFilter.h"

class GeoLocationFilterTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<GeoLocationFilter> module;

    void givenRequestComingFromLocation(std::string country , std::string state,std::string city);

    GeoLocationFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~GeoLocationFilterTests();

};

#endif //BIDDER_GeoLocationFilterTests_H

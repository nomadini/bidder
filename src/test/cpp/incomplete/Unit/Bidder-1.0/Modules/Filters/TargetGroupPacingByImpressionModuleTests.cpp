//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "TargetGroupPacingByImpressionModuleTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "TargetGroupPacingByImpressionModuleTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>
#include "BidRequestHandler.h"


TargetGroupPacingByImpressionModuleTests::TargetGroupPacingByImpressionModuleTests() {

}

TargetGroupPacingByImpressionModuleTests::~TargetGroupPacingByImpressionModuleTests() {

}

void TargetGroupPacingByImpressionModuleTests::SetUp() {
        FilterTestsBase::SetUp ();
        // module = std::make_shared<TargetGroupPacingByImpressionModule> (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //                                                                 beanFactory->dateTimeServiceMock,
        //                                                                 beanFactory->entityToModuleStateStats );

        ON_CALL(*beanFactory->dateTimeServiceMock,
                getUtcOffsetBasedOnTimeZone(_))
        .WillByDefault(Return(-4));

        ON_CALL(*beanFactory->dateTimeServiceMock,
                getCurrentHourAsIntegerInUTC())
        .WillByDefault(Return(20));
}

void TargetGroupPacingByImpressionModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void TargetGroupPacingByImpressionModuleTests::whenModuleRuns() {
        module->process (testService->context);
}

TEST_F(TargetGroupPacingByImpressionModuleTests, filteringATargetGroupForNotHavingRealTimeInfo) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);


        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult =  "[\"TG_HAS_NO_REAL_TIME_INFO\",\"TG_FAILING_IMPRESSION_PACING\"]";

        std::string targetGroupId = StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByImpressionModule", targetGroupId),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}


TEST_F(TargetGroupPacingByImpressionModuleTests, filteringATargetGroupMeetingItsPacingByImpressionLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfo->targetGroupId = tg->getId();

        tgRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(tg->getMaxImpression() * 1.01);
        tgRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(tg->getDailyMaxImpression() * 1.01);

        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->insert(std::make_pair(tg->getId, tgRealTimeInfo));

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult =  "[\"TG_HAS_NO_IMPRESSION_TO_SPEND\",\"TG_FAILING_IMPRESSION_PACING\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByImpressionModule",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}


TEST_F(TargetGroupPacingByImpressionModuleTests, passingATargetGroupForNotMeetingItsPacingByImpressionLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfo->targetGroupId = tg->getId();

        tgRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->setValue(tg->getDailyMaxImpression() * 0.30);
        tgRealTimeInfo->numOfImpressionsServedOverallUpToNow->setValue(tg->getDailyMaxImpression() * 0.30);

        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->insert(std::make_pair(tg->getId, tgRealTimeInfo));

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult =  "[\"TG_HAS_IMPRESSION_TO_SPEND\",\"TG_PASSING_IMPRESSION_PACING\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByImpressionModule",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

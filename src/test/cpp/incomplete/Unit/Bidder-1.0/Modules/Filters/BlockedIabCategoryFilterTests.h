//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_BlockedIabCategoryFilterTests_H
#define BIDDER_BlockedIabCategoryFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedIabCategoryFilter.h"
#include "FilterTestsBase.h"

class BlockedIabCategoryFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<BlockedIabCategoryFilter> module;

    void givenBidRequestHasBlockingIabCategories(std::vector<std::string> blockingCategories);

    void givenTargetGroupHavingIabCategories(std::shared_ptr<TargetGroup> tg, std::string category, std::string subCategory);

    BlockedIabCategoryFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~BlockedIabCategoryFilterTests() ;

};

#endif //BIDDER_BlockedIabCategoryFilterTests_H

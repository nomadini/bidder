//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "EntityDeliveryInfoFetcherTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "BidderTestService.h"
#include <boost/foreach.hpp>
#include "BidderApplicationContext.h"
#include <thread>
EntityDeliveryInfoFetcherTests::EntityDeliveryInfoFetcherTests() {

}

EntityDeliveryInfoFetcherTests::~EntityDeliveryInfoFetcherTests() {

}

void EntityDeliveryInfoFetcherTests::SetUp() {
        FilterTestsBase::SetUp ();
        httpUtilService = std::make_shared<HttpUtilServiceMock> ();
        // module = std::make_shared<EntityDeliveryInfoFetcher> (beanFactory->tgRealTimeDeliveryInfoCacheService,
        //                                                        beanFactory->targetGroupCacheService,
        //                                                        httpUtilService,
        //                                                        bidderApplicationContext,
        //                                                        "fakeUrl",
        //                                                        "1",
        //                                                        beanFactory->entityToModuleStateStats);
        module->inTestMode = true;
}

void EntityDeliveryInfoFetcherTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void EntityDeliveryInfoFetcherTests::whenModuleRuns() {
//    gicapods::Util::sleepMiliSecond (1000); //wait till all the data are loaded
        std::thread thread(&EntityDeliveryInfoFetcher::start, module);
        thread.detach();

        gicapods::Util::sleepMiliSecond (1000);
//    while(!module->isRunning()) {
//        bidderApplicationContext->threadsInterruptedFlag->set(true);
//    }

        //this is how you run the module in a different thread
}

bool EntityDeliveryInfoFetcherTests::resultsAreEqual(std::string actualResult, std::string expectedResult) {
        std::vector<std::string> actualArray;
        JsonArrayUtil::getArrayOfObjectsFromJsonString(actualResult, actualArray);

        std::vector<std::string> expectedArray;
        JsonArrayUtil::getArrayOfObjectsFromJsonString(expectedResult, expectedArray);

        if (actualArray.size() != expectedArray.size()) {
                LOG(WARNING) << " actualArray.size() != expectedArray.size() ... actualArray : "<< actualArray.size() <<" vs expectedArray : " <<expectedArray.size();
                return false;
        }

        auto actualMap = CollectionUtil::convertListToMap(actualArray);
        auto expectedMap = CollectionUtil::convertListToMap(expectedArray);

        typedef std::unordered_map<int, std::string> map_type;
        //BOOST_FOREACH_MAP
        for(auto const& entry :  actualMap) {
                auto entryPtr = expectedMap.find(entry.first);
                if (entryPtr == expectedMap.end()) {
                        LOG(WARNING)<<"couldnt find the entry in expected map : "<< entry.second;
                        return false;
                }
                if (entryPtr->second.compare(entry.second) != 0) {
                        LOG(WARNING)<< " actual vs expected entries are different : " << entryPtr->second << " vs " << entry.second;
                        return false;
                }
        }


        //BOOST_FOREACH_MAP
        for(auto const& entry : expectedMap ) {
                auto entryPtr = actualMap.find(entry.first);
                if (entryPtr == actualMap.end()) {
                        LOG(WARNING)<<"couldnt find the entry in actualMap map : "<< entry.second;
                        return false;
                }
                if (entryPtr->second.compare(entry.second) != 0) {
                        LOG(WARNING)<< " actual vs expected entries are different : " << entryPtr->second << " vs " << entry.second;
                        return false;
                }
        }
        return true;
}
TEST_F(EntityDeliveryInfoFetcherTests, whenTargetGroupsAreEmpty) {

//whenModuleRuns ();
//
//    std::string expectedResult =  "[\"TG_HAS_NO_REAL_TIME_INFO\",\"TG_FAILING_IMPRESSION_PACING\"]";
//
//    std::string targetGroupId = StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ());
//
//    EXPECT_THAT(module->entityToModuleStateStats->
//    getAllPossibleStatesForModuleAndEntity("EntityDeliveryInfoFetcher", targetGroupId) ,
//    testing::Eq (expectedResult));
//
//    EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() () ,
//            testing::Eq (0));
}

TEST_F(EntityDeliveryInfoFetcherTests, whenTgRealTimeDeliveryInfoAlreadyExistsInBidder) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);


        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfoInBidder = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfoInBidder->targetGroupId = tg->getId();
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->insert(std::make_pair(tg->getId, tgRealTimeInfoInBidder));


        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfoFromPacer = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfoFromPacer->targetGroupId = tg->getId();

        // tgRealTimeInfoFromPacer->numOfImpressionsServedInCurrentDateUpToNow->setValue(tg->getMaxImpression() * 1.02);
        // tgRealTimeInfoFromPacer->numOfImpressionsServedOverallUpToNow->setValue(tg->getDailyMaxImpression() * 1.02);

        // tgRealTimeInfoFromPacer->platformBudgetSpentOverallUpToNow->setValue(tg->getMaxImpression() * 1.03);
        // tgRealTimeInfoFromPacer->platformBudgetSpentInCurrentDateUpToNow->setValue(tg->getDailyMaxImpression() * 1.04);


        ON_CALL(*httpUtilService,
                sendPostRequest(_,_,_))
        .WillByDefault(Return(response->toJson()));

        whenModuleRuns ();

        // auto pair = beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->find(tg->getId);
        // EXPECT_THAT(tgRealTimeInfoFromPacer->numOfImpressionsServedOverallUpToNow->getValue(),
        //             testing::Eq (pair->second->numOfImpressionsServedOverallUpToNow->getValue()));
        // EXPECT_THAT(tgRealTimeInfoFromPacer->numOfImpressionsServedInCurrentDateUpToNow->getValue(),
        //             testing::Eq (pair->second->numOfImpressionsServedInCurrentDateUpToNow->getValue()));
        //
        // EXPECT_THAT(tgRealTimeInfoFromPacer->platformBudgetSpentOverallUpToNow->getValue(),
        //             testing::Eq (pair->second->platformBudgetSpentOverallUpToNow->getValue()));
        //
        // EXPECT_THAT(tgRealTimeInfoFromPacer->platformBudgetSpentInCurrentDateUpToNow->getValue(),
        //             testing::Eq (pair->second->platformBudgetSpentInCurrentDateUpToNow->getValue()));
        //
        std::string expectedResult =  "[\"UPDATING_REAL_TIME_INFO_FOR_TG\",\"MARK_REAL_TIME_UPDATED\",\"COPYING_REAL_TIME_INFO_FOR_TG\",\"RESETTING_NUMBER_OF_BIDS_IN_LAST_PACING_PERIOD\"]";

        // std::string actualResult = module->entityToModuleStateStats->
        //                            getAllPossibleStatesForModuleAndEntity("EntityDeliveryInfoFetcher",
        //                                                                   StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(resultsAreEqual(actualResult,expectedResult), testing::Eq (true));


        /*   EXPECT_CALL(*httpUtilService, sendPostRequest(_,_,_))
           .WillOnce(Return(response->toJson()));*/
}

TEST_F(EntityDeliveryInfoFetcherTests, whenTgRealTimeDeliveryInfoDontExistsInBidder) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->clearCaches;
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfoFromPacer = testService->createSampleTargetGroupRealtimInfo();
        assertAndThrow(tgRealTimeInfoFromPacer!=NULL);
        assertAndThrow(tg!=NULL);

        tgRealTimeInfoFromPacer->targetGroupId = tg->getId();

        // tgRealTimeInfoFromPacer->numOfImpressionsServedInCurrentDateUpToNow->setValue(tg->getMaxImpression() * 1.02);
        // tgRealTimeInfoFromPacer->numOfImpressionsServedOverallUpToNow->setValue(tg->getDailyMaxImpression() * 1.02);
        //
        // tgRealTimeInfoFromPacer->platformBudgetSpentOverallUpToNow->setValue(tg->getMaxImpression() * 1.03);
        // tgRealTimeInfoFromPacer->platformBudgetSpentInCurrentDateUpToNow->setValue(tg->getDailyMaxImpression() * 1.04);


        ON_CALL(*httpUtilService,
                sendPostRequest(_,_,_))
        .WillByDefault(Return(response->toJson()));

        whenModuleRuns ();

        // auto pair = beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->find(tg->getId);
        //
        // if (pair == beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->end) {
        //         throwEx("no relatimeInfo found for tg : " +  StringUtil::toStr(tg->getId()));
        // }

        // EXPECT_THAT(tgRealTimeInfoFromPacer->numOfImpressionsServedOverallUpToNow->getValue(),
        //             testing::Eq (pair->second->numOfImpressionsServedOverallUpToNow->getValue()));
        // EXPECT_THAT(tgRealTimeInfoFromPacer->numOfImpressionsServedInCurrentDateUpToNow->getValue(),
        //             testing::Eq (pair->second->numOfImpressionsServedInCurrentDateUpToNow->getValue()));
        //
        // EXPECT_THAT(tgRealTimeInfoFromPacer->platformBudgetSpentOverallUpToNow->getValue(),
        //             testing::Eq (pair->second->platformBudgetSpentOverallUpToNow->getValue()));
        //
        // EXPECT_THAT(tgRealTimeInfoFromPacer->platformBudgetSpentInCurrentDateUpToNow->getValue(),
        //             testing::Eq (pair->second->platformBudgetSpentInCurrentDateUpToNow->getValue()));
        //
        // EXPECT_THAT(tgRealTimeInfoFromPacer->lastTimeAdShown,
        //             testing::Eq (pair->second->lastTimeAdShown));


        std::string expectedResult = "[\"UPDATING_REAL_TIME_INFO_FOR_TG\",\"MARK_REAL_TIME_UPDATED\",\"COPYING_REAL_TIME_INFO_FOR_TG\",\"RESETTING_NUMBER_OF_BIDS_IN_LAST_PACING_PERIOD\",\"CREATE_NEW_REAL_TIME_INFO_FOR_PACING_REQUEST\"]";
        // std::string actualResult = module->entityToModuleStateStats->
        //                            getAllPossibleStatesForModuleAndEntity("EntityDeliveryInfoFetcher",
        //                                                                   StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(resultsAreEqual(actualResult,expectedResult), testing::Eq (true));

        /*    EXPECT_CALL(*httpUtilService, sendPostRequest(_,_,_))
            .WillOnce(Return(response->toJson()));*/
}


TEST_F(EntityDeliveryInfoFetcherTests, whenPacerThrowsException) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->clearCaches;
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfoFromPacer = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfoFromPacer->targetGroupId = tg->getId();

        // tgRealTimeInfoFromPacer->numOfImpressionsServedInCurrentDateUpToNow->setValue(tg->getMaxImpression() * 1.02);
        // tgRealTimeInfoFromPacer->numOfImpressionsServedOverallUpToNow->setValue(tg->getDailyMaxImpression() * 1.02);
        //
        // tgRealTimeInfoFromPacer->platformBudgetSpentOverallUpToNow->setValue(tg->getMaxImpression() * 1.03);
        // tgRealTimeInfoFromPacer->platformBudgetSpentInCurrentDateUpToNow->setValue(tg->getDailyMaxImpression() * 1.04);


        EXPECT_CALL(*httpUtilService, sendPostRequest(_,_,_))
        .WillRepeatedly(Throw(std::logic_error("some exception happened")));

        whenModuleRuns ();
        std::string expectedResult =
                "[\"UPDATING_REAL_TIME_INFO_FOR_TG\",\"NO_RESPONSE_FROM_PACER_GONE_TO_NO_BID_MODE\",\"NO_RESPONSE_FROM_PACER\",\"CREATE_NEW_REAL_TIME_INFO_FOR_PACING_REQUEST\"]";

        // std::string actualResult = module->entityToModuleStateStats->
        //                            getAllPossibleStatesForModuleAndEntity("EntityDeliveryInfoFetcher",
        //                                                                   StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(resultsAreEqual(actualResult,expectedResult), testing::Eq (true));

        // EXPECT_THAT(EntityDeliveryInfoFetcher::isFetchingDeliveryInfoFromPacerHealthy->getValue(), testing::Eq (false));

        /*  EXPECT_CALL(*httpUtilService, sendPostRequest(_,_,_))
           .WillOnce(Return(response->toJson()));*/
}

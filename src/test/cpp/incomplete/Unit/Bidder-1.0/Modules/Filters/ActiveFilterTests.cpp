//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "ActiveFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "ActiveFilterTests.h"
#include "CreativeSizeFilter.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
#include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"

ActiveFilterModuleTests::ActiveFilterModuleTests() {

}

ActiveFilterModuleTests::~ActiveFilterModuleTests() {

}


void ActiveFilterModuleTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<ActiveFilterModule> ();
}

void ActiveFilterModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void ActiveFilterModuleTests::whenModuleRuns() {
        try {
                // this->testService->context->allAvailableTgs = module->process (testService->context);
        } catch (std::exception const &e) {
                exceptionThrown = e;
        }
}

void ActiveFilterModuleTests::givenTargetGroupHasStatus (std::shared_ptr<TargetGroup> tg, std::string status) {
        tg->setStatus (status);
}



TEST_F(ActiveFilterModuleTests, passingActiveTargetGroup) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasStatus (tg, "aCtive");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("ActiveFilterModule" ,
        //                                                 StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(ActiveFilterModuleTests, filteringInActiveTargetGroup) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasStatus (tg, "inaCtive");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("ActiveFilterModule" ,
        //                                                 StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(ActiveFilterModuleTests, failWhenWrongStatus) {


        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasStatus (tg, "inaCtive12");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_WRONG_STATUS\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("ActiveFilterModule" ,
        //                                                 StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

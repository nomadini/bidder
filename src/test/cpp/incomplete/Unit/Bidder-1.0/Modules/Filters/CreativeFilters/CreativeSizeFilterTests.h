//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_CreativeSizeFilterTests_H
#define BIDDER_CreativeSizeFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "CreativeFiltersTestsBase.h"
#include "CreativeSizeFilter.h"

class CreativeSizeFilterTests  : public CreativeFiltersTestsBase {

private:

public:

    std::shared_ptr<CreativeSizeFilter> module;

    void givenRequestHavingCreativeSize(int width, int height);

    void givenCreativeHavingSize (std::shared_ptr<Creative> crt1, int width, int height);

    void givenTargetGroupAndCreativeAreLinked(std::shared_ptr<TargetGroup> tg, std::shared_ptr<Creative> crt);

    void givenTargetGroupInContext(std::shared_ptr<TargetGroup> tg1);

    CreativeSizeFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~CreativeSizeFilterTests() ;

};

#endif //BIDDER_CreativeSizeFilterTests_H

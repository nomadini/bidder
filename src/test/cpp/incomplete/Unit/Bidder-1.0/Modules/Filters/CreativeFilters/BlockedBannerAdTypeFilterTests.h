/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BlockedBannerAdTypeFilterTests_H
#define BlockedBannerAdTypeFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "CreativeFiltersTestsBase.h"

class BlockedBannerAdTypeFilterTests  : public CreativeFiltersTestsBase {

private:

public:

    std::shared_ptr<BlockedBannerAdTypeFilter> module;

    std::string bannerAdType;

    void givenBannerAdType(std::string type);

    BlockedBannerAdTypeFilterTests();

    void givenCreativeTypeInTargetGroupHasType(const std::string& type);

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~BlockedBannerAdTypeFilterTests() ;

};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

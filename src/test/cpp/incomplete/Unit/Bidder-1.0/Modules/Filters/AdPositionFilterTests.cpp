//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "AdPositionFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "AdPositionFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"

AdPositionFilterTests::AdPositionFilterTests() {

}

AdPositionFilterTests::~AdPositionFilterTests() {

}


void AdPositionFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<AdPositionFilter> ();
}

void AdPositionFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void AdPositionFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void AdPositionFilterTests::givenTargetGroupHasPosition (std::shared_ptr<TargetGroup> tg, const std::string& pos) {
        // std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > positionMap =
        //         std::make_shared<tbb::concurrent_hash_map<std::string, int> >();
        //
        // tbb::concurrent_hash_map<std::string, int>::accessor accessor;
        // positionMap->insert(accessor, pos);
        // tg->setAdPositions (positionMap);
}

void AdPositionFilterTests::givenBidRequestHasPosition(const std::string& pos) {
        this->testService->context->adPosition = pos;
}

void AdPositionFilterTests::givenTargetGroupHasNoPositionSelected (std::shared_ptr<TargetGroup> tg) {
        // std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > positionMap =
        //         std::make_shared<tbb::concurrent_hash_map<std::string, int> >();
        //
        // tbb::concurrent_hash_map<std::string, int>::accessor accessor;
        // tg->setAdPositions (positionMap);
}

TEST_F(AdPositionFilterTests, passingTargetGroupWithNoPreferenceInAdPositions) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasNoPositionSelected (tg);
        givenBidRequestHasPosition("abovethefold");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_NO_PREFERENCE\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("AdPositionFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

TEST_F(AdPositionFilterTests, passingTargetGroupWithMatchingAdPosition) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasPosition (tg, "abovethefold");
        givenBidRequestHasPosition("abovethefold");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_MATCHING_AD_POSITIONS\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("AdPositionFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(AdPositionFilterTests, filteringTargetGroupWithNonMatchingAdPosition) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        givenTargetGroupHasPosition (tg, "abovethefold");
        givenBidRequestHasPosition("belowthefold");
        givenTargetGroupInContext(tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("AdPositionFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

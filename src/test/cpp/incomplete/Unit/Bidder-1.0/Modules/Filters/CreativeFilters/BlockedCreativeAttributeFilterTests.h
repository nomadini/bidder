//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_BLOCKEDCREATIVEATTRIBUTEFILTERTESTS_H
#define BIDDER_BLOCKEDCREATIVEATTRIBUTEFILTERTESTS_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "CreativeFiltersTestsBase.h"
#include "BlockedCreativeAttributeFilter.h"

class BlockedCreativeAttributeFilterTests  : public CreativeFiltersTestsBase {

private:

public:

    std::shared_ptr<BlockedCreativeAttributeFilter> module;

    std::vector<int> attributes;

    void givenBlockedCreativeAttributes(std::vector<int> creativeAttributes);

    void givenCreativeTypeInTargetGroupHasAttributes(std::shared_ptr<std::vector<int>> creativeAttributes);

    BlockedCreativeAttributeFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~BlockedCreativeAttributeFilterTests() ;

};

#endif //BIDDER_BLOCKEDCREATIVEATTRIBUTEFILTERTESTS_H

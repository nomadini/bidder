//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "DayPartFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "DayPartFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

DayPartFilterTests::DayPartFilterTests() {

}

DayPartFilterTests::~DayPartFilterTests() {

}

void DayPartFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<DayPartFilter> (beanFactory->targetGroupDayPartCacheService);
}

void DayPartFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void DayPartFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void DayPartFilterTests::givenUserHavingTimeZoneOffsetOf(int offset) {
        this->testService->context->userTimeZonDifferenceWithUTC = offset;
}

std::string DayPartFilterTests::createPassingHourJsonForUser(int userLocalTime, int userWeekDay) {
        auto doc = JsonUtil::createDcoumentAsObjectDoc ();

        RapidJsonValueTypeNoRef dayObject (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef hourArrayValue (rapidjson::kArrayType);
        JsonArrayUtil::addMemberToArray (doc, StringUtil::toStr (userLocalTime), hourArrayValue);
        // JsonArrayUtil::addMemberToDocument_From_Array (doc, StringUtil::toStr (userWeekDay), hourArrayValue);
        MLOG(3) << "hourJson created is : " <<JsonUtil::docToString (doc.get());
        return JsonUtil::docToString (doc.get());
}

void DayPartFilterTests::givenTargetGroupHavingNoDaypartMapping(std::shared_ptr<TargetGroup> tg) {
        beanFactory->mySqlTargetGroupDayPartTargetMapService->deleteAllMappingFor(tg);
}

TEST_F(DayPartFilterTests, passingTgWithRightHourByUser) {
        int userOffset = 0;
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        int userLocalTime = userOffset + DateTimeUtil::getCurrentHourAsIntegerInUTC ();

        std::string hourJson = createPassingHourJsonForUser(userLocalTime, DateTimeUtil::getWeekDayNumberBasedOnUserOffset (userOffset));
        std::shared_ptr<TargetGroupDayPartTargetMap> tgDayHourMapping = std::make_shared<TargetGroupDayPartTargetMap>();
        tgDayHourMapping->hours = hourJson;
        tgDayHourMapping->targetGroupId = tg->getId ();
        beanFactory->mySqlTargetGroupDayPartTargetMapService->insert(tgDayHourMapping);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->targetGroupDayPartCacheService()->reloadCaches;

        givenUserHavingTimeZoneOffsetOf(userOffset);
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_RIGHT_HOUR\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("DayPartFilter",
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

TEST_F(DayPartFilterTests, filteringTgForNoRightHourOnBidRequest) {
        int userOffset = 0;
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        int userLocalTime = userOffset + DateTimeUtil::getCurrentHourAsIntegerInUTC () + 1; //we add 1 to the hourJson in target group to make it unfit for current hour

        std::string hourJson = createPassingHourJsonForUser(userLocalTime, DateTimeUtil::getWeekDayNumberBasedOnUserOffset (userOffset));
        std::shared_ptr<TargetGroupDayPartTargetMap> tgDayHourMapping = std::make_shared<TargetGroupDayPartTargetMap>();
        tgDayHourMapping->hours = hourJson;
        tgDayHourMapping->targetGroupId = tg->getId ();
        beanFactory->mySqlTargetGroupDayPartTargetMapService->insert(tgDayHourMapping);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        beanFactory->targetGroupDayPartCacheService()->reloadCaches;

        givenUserHavingTimeZoneOffsetOf(userOffset);
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_INACTIVE_HOUR\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("DayPartFilter",
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}


TEST_F(DayPartFilterTests, passingTargetGroupForNoHavingNoHourSelection) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        givenTargetGroupHavingNoDaypartMapping(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_NO_HOUR_SETTING\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("DayPartFilter",
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

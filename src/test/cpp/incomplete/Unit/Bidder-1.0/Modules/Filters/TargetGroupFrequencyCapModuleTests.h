//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_TargetGroupFrequencyCapModuleTests_H
#define BIDDER_TargetGroupFrequencyCapModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "TargetGroupFrequencyCapModule.h"

class TargetGroupFrequencyCapModuleTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<TargetGroupFrequencyCapModule> module;

    void givenTargetGroupWithFrequency(std::shared_ptr<TargetGroup> tg, int seconds);

    void givenAdShownInLastXSeconds(int targetGroupId, std::string nomadiniDeviceId , std::string deviceType, int seconds);

    TargetGroupFrequencyCapModuleTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~TargetGroupFrequencyCapModuleTests();

};

#endif //BIDDER_TargetGroupFrequencyCapModuleTests_H

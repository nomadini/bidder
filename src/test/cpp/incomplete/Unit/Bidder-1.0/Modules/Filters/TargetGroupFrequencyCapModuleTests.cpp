//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "TargetGroupFrequencyCapModuleTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "TargetGroupFrequencyCapModuleTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>
#include "BidRequestHandler.h"
#include "MockHttpRequest.h"
#include "MockHttpResponse.h"
#include "EventLog.h"


TargetGroupFrequencyCapModuleTests::TargetGroupFrequencyCapModuleTests() {

}

TargetGroupFrequencyCapModuleTests::~TargetGroupFrequencyCapModuleTests() {

}

void TargetGroupFrequencyCapModuleTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<TargetGroupFrequencyCapModule>
        //                  (beanFactory->adHistoryCassandraService,
        //                  beanFactory->entityToModuleStateStats);
}

void TargetGroupFrequencyCapModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void TargetGroupFrequencyCapModuleTests::whenModuleRuns() {
        module->process (testService->context);
}


void TargetGroupFrequencyCapModuleTests::givenTargetGroupWithFrequency(std::shared_ptr<TargetGroup> tg, int seconds) {
        tg->setFrequencyInSec(seconds);
}

void TargetGroupFrequencyCapModuleTests::givenAdShownInLastXSeconds(int targetGroupId,
                                                                    std::string nomadiniDeviceId,
                                                                    std::string deviceType,
                                                                    int seconds) {

        std::shared_ptr<AdEntry> adEntry = std::make_shared<AdEntry>("transactionId", EventLog::EVENT_TYPE_IMPRESSION);
        adEntry->targetGroupId = targetGroupId;
        adEntry->creativeId =  12;
        adEntry->publisherId = 13;
        adEntry->segmentId = 13;
        adEntry->timeAdShownInMillis = DateTimeUtil::getNowInMilliSecond() - seconds * 1000;
        auto device = std::make_shared<Device>(nomadiniDeviceId,
                                               deviceType);
        beanFactory->adHistoryCassandraService->saveAdInteraction(device, adEntry);

}

TEST_F(TargetGroupFrequencyCapModuleTests, filteringATargetGroupMeetingItsFrequencyLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithFrequency(tg, 10);
        givenTargetGroupWithProperParent(tg);
        //given id in context
        //setting user device id and type
        std::string userdeviceType = "DESKTOP";
        std::string userDeviceId = StringUtil::random_string (36);
        testService->context->setDeviceType(userdeviceType);
        testService->context->setDeviceId(userDeviceId);

        givenAdShownInLastXSeconds(tg->getId(), userDeviceId, userdeviceType, 9);

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_MEETING_FRQ_LIMIT\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity("TargetGroupFrequencyCapModule",
        //                                                StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}



TEST_F(TargetGroupFrequencyCapModuleTests, passingATargetGroupForNotMeetingItsFrequencyLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithFrequency(tg, 10);
        givenTargetGroupWithProperParent(tg);
        //given id in context
        //setting user device id and type
        std::string userdeviceType = "DESKTOP";
        std::string userDeviceId = StringUtil::random_string (36);
        testService->context->setDeviceType(userdeviceType);
        testService->context->setDeviceId(userDeviceId);

        givenAdShownInLastXSeconds(tg->getId(), userDeviceId, userdeviceType, 100);

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSING_FOR_NOT_MEETING_FREQ_LIMIT\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        // getAllPossibleStatesForModuleAndEntity("TargetGroupFrequencyCapModule",
        // StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        // testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

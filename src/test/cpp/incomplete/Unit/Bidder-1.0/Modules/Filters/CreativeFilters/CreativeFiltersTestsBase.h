//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_CREATIVEFILTERSTESTSBASE_H
#define BIDDER_CREATIVEFILTERSTESTSBASE_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
class OpportunityContext;
#include "FilterTestsBase.h"

class CreativeFiltersTestsBase  : public FilterTestsBase {

private:

public:
    void SetUp();

    void TearDown();

    CreativeFiltersTestsBase();

    virtual ~CreativeFiltersTestsBase() ;

};
#endif //BIDDER_CREATIVEFILTERSTESTSBASE_H

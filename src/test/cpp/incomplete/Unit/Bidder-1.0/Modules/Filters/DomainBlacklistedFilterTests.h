//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_DomainBlackListedFilterTests_H
#define BIDDER_DomainBlackListedFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "DomainBlackListedFilter.h"

class DomainBlackListedFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<DomainBlackListedFilter> module;

    void givenBidRequestComingFromDomain(std::string domain);

    DomainBlackListedFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~DomainBlackListedFilterTests() ;

};

#endif //BIDDER_DomainBlackListedFilterTests_H

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "TargetGroupPacingByBudgetModuleTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "TargetGroupPacingByBudgetModuleTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>
#include "BidRequestHandler.h"
#include "DateTimeServiceMock.h"
#include "RandomUtil.h"
TargetGroupPacingByBudgetModuleTests::TargetGroupPacingByBudgetModuleTests() {

}

TargetGroupPacingByBudgetModuleTests::~TargetGroupPacingByBudgetModuleTests() {

}

void TargetGroupPacingByBudgetModuleTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<TargetGroupPacingByBudgetModule> (
        //                                                             beanFactory->dateTimeServiceMock,
        //                                                             beanFactory->entityToModuleStateStats );

        ON_CALL(*beanFactory->dateTimeServiceMock,
                getUtcOffsetBasedOnTimeZone(_))
        .WillByDefault(Return(-4));

        ON_CALL(*beanFactory->dateTimeServiceMock,
                getCurrentHourAsIntegerInUTC())
        .WillByDefault(Return(20));
}

void TargetGroupPacingByBudgetModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void TargetGroupPacingByBudgetModuleTests::whenModuleRuns() {
        module->process (testService->context);
}

TEST_F(TargetGroupPacingByBudgetModuleTests, filteringATargetGroupForNotHavingRealTimeInfo) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);

        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult =  "[\"TG_HAS_NO_REAL_TIME_INFO\",\"TG_FAILING_BUDGET_PACING\"]";

        std::string targetGroupId = StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByBudgetModule", targetGroupId),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(TargetGroupPacingByBudgetModuleTests, filteringATargetGroupMeetingItsPacingByBudgetLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfo->targetGroupId = tg->getId();

        // tgRealTimeInfo->platformBudgetSpentInCurrentDateUpToNow->setValue(tg->getDailyMaxBudget() * 1.01);
        // tgRealTimeInfo->platformBudgetSpentOverallUpToNow->setValue(tg->getDailyMaxBudget() * 1.01);

        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->insert(std::make_pair(tg->getId, tgRealTimeInfo));

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult =  "[\"TG_HAS_NO_MONEY_TO_SPEND\",\"TG_FAILING_BUDGET_PACING\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByBudgetModule",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(TargetGroupPacingByBudgetModuleTests, passingATargetGroupForNotMeetingItaPacingByBudgetLimit) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupWithProperParent(tg);

        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo = testService->createSampleTargetGroupRealtimInfo();
        tgRealTimeInfo->targetGroupId = tg->getId();
        // beanFactory->tgRealTimeDeliveryInfoCacheService()->getAllTgRealTimeDeliveryInfoMap ()->insert(std::make_pair(tg->getId, tgRealTimeInfo));

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult = "[\"TG_HAS_MONEY_TO_SPEND\",\"TG_PASSING_BUDGET_PACING\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("TargetGroupPacingByBudgetModule",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(TargetGroupPacingByBudgetModuleTests, testingGettingMaxHourIndexTest0) {
        ON_CALL(*beanFactory->dateTimeServiceMock,
                getUtcOffsetBasedOnTimeZone(_))
        .WillByDefault(Return(0));

        std::shared_ptr<PacingPlan> pacingPlan =
                PacingPlan::fromJson("{\"timeZone\":\"America/New_York\",\"hourPercentages\":[{\"10\":30},{\"20\":100},{\"21\":100}]}");

        //auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        std::shared_ptr<TargetGroup> tg = std::make_shared<TargetGroup>();
        tg->setId(RandomUtil::sudoRandomNumber(1000));
        tg->idAsString = StringUtil::toStr(tg->getId());
        tg->setPacingPlan(pacingPlan);
        int limit;
        for (int i=0; i<11; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (30));
        }

        for (int i=11; i<24; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (100));
        }
}

TEST_F(TargetGroupPacingByBudgetModuleTests, testingGettingMaxHourIndexTest1) {
        ON_CALL(*beanFactory->dateTimeServiceMock,
                getUtcOffsetBasedOnTimeZone(_))
        .WillByDefault(Return(0));

        std::shared_ptr<PacingPlan> pacingPlan =
                PacingPlan::fromJson("{\"timeZone\":\"America/New_York\",\"hourPercentages\":[{\"0\":30},{\"1\":30},{\"2\":30},{\"3\":30},{\"4\":30},{\"5\":30},"
                                     "{\"6\":30},{\"7\":50},{\"8\":50},{\"9\":50},{\"10\":50},{\"11\":80},{\"12\":80},{\"13\":80},{\"14\":80},{\"15\":80},"
                                     "{\"16\":80},{\"17\":100},{\"18\":100},{\"19\":100},{\"20\":100},{\"21\":100}]}");

        //auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        std::shared_ptr<TargetGroup> tg = std::make_shared<TargetGroup>();
        tg->setId(RandomUtil::sudoRandomNumber(1000));
        tg->idAsString = StringUtil::toStr(tg->getId());

        tg->setPacingPlan(pacingPlan);
        int limit;
        for (int i=0; i<7; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (30));
        }


        for (int i=7; i<11; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (50));
        }


        for (int i=11; i<17; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (80));
        }

        for (int i=18; i<24; i++) {
                ON_CALL(*beanFactory->dateTimeServiceMock,
                        getCurrentHourAsIntegerInUTC())
                .WillByDefault(Return(i));

                limit =  module->getLimitByHour (tg, beanFactory->dateTimeServiceMock(), beanFactory->entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
                EXPECT_THAT(limit, testing::Eq (100));
        }
}

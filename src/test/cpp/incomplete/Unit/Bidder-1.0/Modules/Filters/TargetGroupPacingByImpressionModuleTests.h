//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_TargetGroupPacingByImpressionModuleTests_H
#define BIDDER_TargetGroupPacingByImpressionModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "TargetGroupPacingByImpressionModule.h"

class TargetGroupPacingByImpressionModuleTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<TargetGroupPacingByImpressionModule> module;

    TargetGroupPacingByImpressionModuleTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~TargetGroupPacingByImpressionModuleTests();

};

#endif //BIDDER_TargetGroupPacingByImpressionModuleTests_H

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_CreativeBannerApiFilterTests_H
#define BIDDER_CreativeBannerApiFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "CreativeFiltersTestsBase.h"
#include "CreativeBannerApiFilter.h"

class CreativeBannerApiFilterTests  : public CreativeFiltersTestsBase {

private:

public:

    std::shared_ptr<CreativeBannerApiFilter> module;

    void givenCreativeHavingApi(std::shared_ptr<Creative> crt, const std::string& api);

    void givenBidRequestInBasedOnProtocol(const std::string& protocol);

    void givenRequestSupportingApi(std::vector<int> apis, std::string protocolVersion);

    void givenTargetGroupAndCreativeAreLinked(std::shared_ptr<TargetGroup> tg, std::shared_ptr<Creative> crt);

    void givenTargetGroupInContext(std::shared_ptr<TargetGroup> tg1);

    CreativeBannerApiFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~CreativeBannerApiFilterTests() ;

};

#endif //BIDDER_CreativeBannerApiFilterTests_H

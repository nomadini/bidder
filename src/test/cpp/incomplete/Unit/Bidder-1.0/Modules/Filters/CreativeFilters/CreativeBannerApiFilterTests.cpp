//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "CreativeBannerApiFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "TargetGroup.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "Creative.h"
#include "TestUtil.h"
#include "CreativeBannerApiFilterTests.h"
#include "CreativeBannerApiFilter.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeBannerApiFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"

CreativeBannerApiFilterTests::CreativeBannerApiFilterTests() {

}

CreativeBannerApiFilterTests::~CreativeBannerApiFilterTests() {

}


void CreativeBannerApiFilterTests::SetUp() {
        CreativeFiltersTestsBase::SetUp ();

        // module = std::make_shared<CreativeBannerApiFilter> (beanFactory->targetGroupCreativeCacheService ,
        //                                                     beanFactory->creativeCacheService );
}

void CreativeBannerApiFilterTests::TearDown() {
        CreativeFiltersTestsBase::TearDown ();
}

void CreativeBannerApiFilterTests::whenModuleRuns() {
        // this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void CreativeBannerApiFilterTests::givenBidRequestInBasedOnProtocol(const std::string &protocol) {
        this->testService->context->protocolVersion = "OpenRtb2.3";
}

void CreativeBannerApiFilterTests::givenRequestSupportingApi(std::vector<int> apis, std::string protocolVersion) {
        testService->context->creativeAPI = OpenRtb2_3_0::CreativeAPIConverter::convertApiToName (apis, protocolVersion);
}

void CreativeBannerApiFilterTests::givenTargetGroupAndCreativeAreLinked(std::shared_ptr<TargetGroup> tg, std::shared_ptr<Creative> crt) {
        std::unordered_map<int, int> creativeIds;
        creativeIds.insert (std::make_pair(crt->getId(),crt->getId()));
        // beanFactory->targetGroupCreativeCacheService ()->getAllTargetGroupCreativesMap ->insert (
        //         std::make_pair (tg->getId(), creativeIds));

        // beanFactory->creativeCacheService ()->getAllEntitiesMap()->insert (std::make_pair (crt->getId, crt));
}

void CreativeBannerApiFilterTests::givenCreativeHavingApi(std::shared_ptr<Creative> crt, const std::string &api) {
        auto apis = std::make_shared<std::vector<std::string> >();
        apis->push_back (api);
        // crt->setApis (apis);
}

void CreativeBannerApiFilterTests::givenTargetGroupInContext(std::shared_ptr<TargetGroup> tg1) {
        testService->context->allAvailableTgs->push_back(tg1);
}

TEST_F(CreativeBannerApiFilterTests, filteringTargetGroupThatHasNoCreative) {
        givenBidRequestInBasedOnProtocol ("OpenRtb2.3");

        std::vector<int> supportingApis;
        supportingApis.push_back (2);
        givenRequestSupportingApi (supportingApis, "2.3");

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupInContext(tg1);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_NO_CREATIVES\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeBannerApiFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg1->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(CreativeBannerApiFilterTests, filteringTargetGroupThatHasNoMatchingApiCreative) {
//Open Rtb Api numbers
//    1       VPAID 1.0
//    2       VPAID 2.0
//    3       MRAID-1
//    4       ORMMA
//    5       MRAID-2

        givenBidRequestInBasedOnProtocol ("OpenRtb2.3");

        std::vector<int> supportingApis;
        supportingApis.push_back (2);
        givenRequestSupportingApi (supportingApis, "2.3");

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto crt1 = CreativeTestHelper::createSampleCreative ();

        givenCreativeHavingApi (crt1, "VPAID1.0");

        givenTargetGroupAndCreativeAreLinked (tg1, crt1);
        givenTargetGroupInContext(tg1);


        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (tg1->getId(), crt1->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeBannerApiFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg1->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(CreativeBannerApiFilterTests, passingTargetGroupThatHasMatchingApiCreative) {

        givenBidRequestInBasedOnProtocol ("OpenRtb2.3");

        std::vector<int> supportingApis;
        supportingApis.push_back (2);
        givenRequestSupportingApi (supportingApis, "2.3");

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto crt1 = CreativeTestHelper::createSampleCreative ();

        givenCreativeHavingApi (crt1, "VPAID2.0");

        givenTargetGroupAndCreativeAreLinked (tg1, crt1);
        givenTargetGroupInContext(tg1);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (tg1->getId(), crt1->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeBannerApiFilter",
        //                                                     stateId),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

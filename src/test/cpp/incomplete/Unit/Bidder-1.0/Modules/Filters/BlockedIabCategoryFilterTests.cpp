//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "BlockedIabCategoryFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "BlockedIabCategoryFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

BlockedIabCategoryFilterTests::BlockedIabCategoryFilterTests() {

}

BlockedIabCategoryFilterTests::~BlockedIabCategoryFilterTests() {

}

void BlockedIabCategoryFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        module;// = std::make_shared<BlockedIabCategoryFilter> ();
}

void BlockedIabCategoryFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
        this->testService->context->getBlockedIabCategoriesMap()->clear();
}

void BlockedIabCategoryFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}


void BlockedIabCategoryFilterTests::givenBidRequestHasBlockingIabCategories(std::vector<std::string> blockingCategories) {
        for(std::string cat :  blockingCategories) {
                this->testService->context->addBlockedIabCategory(cat);
        }
}

void BlockedIabCategoryFilterTests::givenTargetGroupHavingIabCategories(std::shared_ptr<TargetGroup> tg, std::string category, std::string subCategory) {
        tg->setIabCategory (category);
        tg->setIabSubCategory (subCategory);
}

TEST_F(BlockedIabCategoryFilterTests, passingTargetGroupWithUnblockingIabCategories) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        tg->getIabCategory()->clear();
        tg->getIabSubCategory()->clear();

        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();
        tg->setCampaignId(campaign->getId());
        campaign->setAdvertiserId(advertiser->id);

        givenTargetGroupExistInCache(tg);
        givenCampaignExistInCache(campaign);
        givenAdvertiserExistInCache(advertiser);

        givenTargetGroupHavingIabCategories (tg, "IAB1", "IAB1-1");

        std::vector<std::string> blockingIabCategoriesOfBidRequest;
        blockingIabCategoriesOfBidRequest.push_back (StringUtil::toLowerCase ("IAB3"));
        givenBidRequestHasBlockingIabCategories (blockingIabCategoriesOfBidRequest);

        givenTargetGroupInContext (tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("BlockedIabCategoryFilter",
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

TEST_F(BlockedIabCategoryFilterTests, filteringTargetGroupWithBlockingIabCategory) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();
        tg->setCampaignId(campaign->getId());
        campaign->setAdvertiserId(advertiser->id);

        givenTargetGroupExistInCache(tg);
        givenCampaignExistInCache(campaign);
        givenAdvertiserExistInCache(advertiser);

        std::vector<std::string> blockingCategories;
        givenTargetGroupHavingIabCategories (tg, "IAB1", "IAB1-1");

        std::vector<std::string> blockingIabCategoriesOfBidRequest;

        blockingIabCategoriesOfBidRequest.push_back (StringUtil::toLowerCase ("IAB1"));
        blockingIabCategoriesOfBidRequest.push_back (StringUtil::toLowerCase ("IAB1-1"));

        blockingIabCategoriesOfBidRequest.push_back (StringUtil::toLowerCase ("IAB2"));
        blockingIabCategoriesOfBidRequest.push_back (StringUtil::toLowerCase ("IAB2-1"));
        givenBidRequestHasBlockingIabCategories (blockingIabCategoriesOfBidRequest);

        givenTargetGroupInContext (tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_BLOCKED_CATEGORY\",\"FAILED_FOR_BLOCKED_SUB_CATEGORY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("BlockedIabCategoryFilter",
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}

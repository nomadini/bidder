//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GeoFeatureFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "GeoFeatureFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>
#include "TargetGroupFilterStatistic.h"

GeoFeatureFilterTests::GeoFeatureFilterTests() {

}

GeoFeatureFilterTests::~GeoFeatureFilterTests() {

}

void GeoFeatureFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        auto filterNameToFailureCounts = std::make_shared<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic > >();

        auto treeOfGeoFeatures = std::make_shared<TreeOfGeoFeaturesType > ();
        // module = std::make_shared<GeoFeatureFilter> (filterNameToFailureCounts);
        module->maxNumebrOfWantedFeatures = 1;
        module->treeOfGeoFeatures = treeOfGeoFeatures;

        GeoFeature* feature = std::make_shared<GeoFeature>();
        feature->id = 1;
        feature->collectionId = 1;
        feature->collectionIdAsString = "1";
        feature->name= "Salt Mastering";
        std::string featuresInJson =
                "{\"type\":\"Polygon\",\"geometry\":[[[40.7300990706861,-73.9587392228485],[40.7301655104705,-73.9580410522096],[40.7299521677305,-73.958005928082],[40.7298857958069,-73.9587041885524],[40.730071500879,-73.9587346414405],[40.7300990706861,-73.9587392228485]]]}";
        std::vector<std::pair<float,float> > allPoints = feature->extractPointsFromFeatureInJson(featuresInJson);
        feature->polygon = feature->constructPolygon(allPoints);



        GeoFeature* feature2 = std::make_shared<GeoFeature>();
        feature2->id = 2;
        feature2->collectionId = 2;
        feature2->collectionIdAsString = "2";
        feature2->name= "WABC & ABC Headquarters";
        std::string featuresInJson2 = "{\"type\":\"Polygon\",\"geometry\":[[[40.7734102612093,-73.9808819759498],[40.7736444174955,-73.9807114757088],[40.7738363951017,-73.9805716080191],[40.7737057116107,-73.9802608807623],[40.773686867588,-73.9802746249862],[40.7736070015261,-73.9803328358166],[40.7735529184057,-73.980372182026],[40.7735129853063,-73.9804011976097],[40.7734095128875,-73.9801553287164],[40.7733597154529,-73.9801915308224],[40.7731759680389,-73.9803253797997],[40.7734102612093,-73.9808819759498]]]}";
        std::vector<std::pair<float,float> > allPoints2 = feature2->extractPointsFromFeatureInJson(featuresInJson2);
        feature2->polygon = feature2->constructPolygon(allPoints2);



        GeoFeature* feature3 = std::make_shared<GeoFeature>();
        feature3->id = 3;
        feature3->collectionId = 3;
        feature3->collectionIdAsString = "3";
        feature3->name= "WGHT Radio";
        std::string featuresInJson3 = "{\"type\":\"Polygon\",\"geometry\":[[[40.9813826725298,-74.2836317438461],[40.9813948793837,-74.2835031050974],[40.9812245935674,-74.283474808166],[40.981212386682,-74.2836032672516],[40.9813826725298,-74.2836317438461]]]}";
        std::vector<std::pair<float,float> > allPoints3 = feature3->extractPointsFromFeatureInJson(featuresInJson3);
        feature3->polygon = feature3->constructPolygon(allPoints3);

        module->entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        module->treeOfGeoFeatures->insert(std::make_pair(bg::return_envelope<Box>(*feature->polygon), feature));
        module->treeOfGeoFeatures->insert(std::make_pair(bg::return_envelope<Box>(*feature2->polygon), feature2));
        module->treeOfGeoFeatures->insert(std::make_pair(bg::return_envelope<Box>(*feature3->polygon), feature3));

}

void GeoFeatureFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void GeoFeatureFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}


void GeoFeatureFilterTests::givenRequestHaving(double lat, double lon) {
        testService->context->deviceLat = lat;
        testService->context->deviceLon = lon;

}

TEST_F(GeoFeatureFilterTests, passingTgWithRightGeoFeature) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupInContext(tg);
        givenRequestHaving(40.9813826725298,-74.2836317438461);// feature3
        module->targetGroupToGeoCollectionKeys = std::make_shared<std::unordered_set<std::string> >();
        module->targetGroupToGeoCollectionKeys->insert(tg->idAsString+"g3");//the key for feature3

        whenModuleRuns ();

        std::vector<std::string> expectedResult = { "FOUND_NEAR_FEATURES" };
        std::vector<std::string> actualResult = module->entityToModuleStateStats->
                                                getAllPossibleStatesForModuleAndEntity(this->module->getName(),
                                                                                       "tg" + tg->idAsString);

        EXPECT_THAT(CollectionUtil::areListsEqual(expectedResult, actualResult), testing::Eq (true));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(GeoFeatureFilterTests, failingTargetGroupWithoutMatchingGeoFeature) {

}

TEST_F(GeoFeatureFilterTests, passingTargetGroupWithoutGeoFeatureFilter) {

}

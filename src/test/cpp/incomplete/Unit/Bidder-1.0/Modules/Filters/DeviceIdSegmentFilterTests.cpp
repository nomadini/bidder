//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "DeviceIdSegmentFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "DeviceIdSegmentFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>
#include "BidRequestHandler.h"
#include "RandomUtil.h"
#include "DeviceSegmentHistory.h"

DeviceIdSegmentFilterTests::DeviceIdSegmentFilterTests() {

}

DeviceIdSegmentFilterTests::~DeviceIdSegmentFilterTests() {

}

void DeviceIdSegmentFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<DeviceIdSegmentFilter> (beanFactory->targetGroupSegmentCacheService  ,
        //                                                   beanFactory->deviceSegmentHistoryCassandraService ,
        //                                                   beanFactory->segmentCacheService);
}

void DeviceIdSegmentFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void DeviceIdSegmentFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void DeviceIdSegmentFilterTests::givenTgSegmentMappingExists(int targetGroupId, int segmentId) {
        std::shared_ptr<TargetGroupSegmentMap> targetGroupSegmentMap = std::make_shared<TargetGroupSegmentMap>();

        targetGroupSegmentMap->id = RandomUtil::sudoRandomNumber (1000);
        targetGroupSegmentMap->targetGroupId = targetGroupId;
        targetGroupSegmentMap->segmentId = segmentId;
        beanFactory->mySqlTargetGroupSegmentMapService->insert(targetGroupSegmentMap);

}

void DeviceIdSegmentFilterTests::givenDeviceSegmentExistsForDevice(std::string userDeviceId,
                                                                   std::string userdeviceType,
                                                                   int segmentId) {
        //given a deviceSegment for deviceId in cassandra
        auto device = std::make_shared<Device>(userDeviceId, userdeviceType);
        auto segment =  std::make_shared<Segment>(DateTimeUtil::getNowInMilliSecond());
        segment->setUniqueName(StringUtil::toStr(segmentId));
        segment->dateCreated = DateTimeUtil::getNowInYYYMMDDFormat();
        std::shared_ptr<DeviceSegmentHistory> deviceSegment = std::make_shared<DeviceSegmentHistory>(device, segment);
        beanFactory->deviceSegmentHistoryCassandraService->persist(deviceSegment);

}

TEST_F(DeviceIdSegmentFilterTests, passingTgWithRightDeviceIdSegment) {

        auto tg = givenTargetGroupWithProperParent();


        std::shared_ptr<Segment> segment  = SegmentTestHelper::createSampleSegment();
        beanFactory->mySqlSegmentService->insert(segment);
        beanFactory->segmentCacheService()->reloadCaches;

        //creating the sample tg segment mapping
        givenTgSegmentMappingExists(tg->getId (), segment->id);
        beanFactory->targetGroupSegmentCacheService()->reloadCaches;

        //setting user device id and type
        std::string userdeviceType = "DESKTOP";
        std::string userDeviceId = StringUtil::random_string (36);

        //given id in context
        testService->context->setDeviceType(userdeviceType);
        testService->context->setDeviceId(userDeviceId);

        givenDeviceSegmentExistsForDevice(userDeviceId, userdeviceType, segment->id);

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASS_TG_MATCH_DEVICE_SEGMENT\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("DeviceIdSegmentFilter",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(DeviceIdSegmentFilterTests, filteringTgWithoutGoodDeviceIdSegment) {
        auto tg = givenTargetGroupWithProperParent();

        std::shared_ptr<Segment> segment  = SegmentTestHelper::createSampleSegment();
        beanFactory->mySqlSegmentService->insert(segment);
        beanFactory->segmentCacheService()->reloadCaches;

        //creating the sample tg segment mapping
        givenTgSegmentMappingExists(tg->getId (), segment->id);
        beanFactory->targetGroupSegmentCacheService()->reloadCaches;

        //setting user device id and type
        std::string userdeviceType = "DESKTOP";
        std::string userDeviceId = StringUtil::random_string (36);

        //given id in context
        testService->context->setDeviceType(userdeviceType);
        testService->context->setDeviceId(userDeviceId);

        givenDeviceSegmentExistsForDevice(userDeviceId + StringUtil::toStr("makeItNotRight"), userdeviceType, segment->id);

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAIL_TG_HAS_NOT_RIGHT_DEVICE_SEGMENT\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("DeviceIdSegmentFilter",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}
//
TEST_F(DeviceIdSegmentFilterTests, failingTargetGroupWithoutDeviceIdSegment) {

        auto tg = givenTargetGroupWithProperParent();

        //setting user device id and type
        std::string userdeviceType = "DESKTOP";
        std::string userDeviceId = StringUtil::random_string (36);

        //given id in context
        testService->context->setDeviceType(userdeviceType);
        testService->context->setDeviceId(userDeviceId);

        givenTargetGroupInContext (tg);
        gicapods::Util::sleepMiliSecond (10);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAIL_TG_HAS_NO_SEGMENT\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity("DeviceIdSegmentFilter",
        //                                                    StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));


}

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "CappedBiddingPerHourFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "CappedBiddingPerHourFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

CappedBiddingPerHourFilterTests::CappedBiddingPerHourFilterTests() {

}

CappedBiddingPerHourFilterTests::~CappedBiddingPerHourFilterTests() {

}

void CappedBiddingPerHourFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        //  module = std::make_shared<CappedBiddingPerHourFilter> ();
}

void CappedBiddingPerHourFilterTests::TearDown() {
        FilterTestsBase::TearDown ();

}

void CappedBiddingPerHourFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void CappedBiddingPerHourFilterTests::givenTargetGroupHavingCappedBiddingLimitOf(int biddingLimit, std::shared_ptr<TargetGroup> tg) {
        auto num = std::make_shared<gicapods::AtomicLong>(biddingLimit);
        tg->setNumberOfBidsLimitInLastHourPerBidder(num);
}
void CappedBiddingPerHourFilterTests::givenTargetGroupHavingBidInLastHour(int numberOfBids, std::shared_ptr<TargetGroup> tg) {
        auto num = std::make_shared<gicapods::AtomicLong>(numberOfBids);
        // tg->setnumberOfBidsInLastHourByThisBidder(num);
}

TEST_F(CappedBiddingPerHourFilterTests, passingTargetGroupWithBidderPerhourLessThanLimit) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupHavingCappedBiddingLimitOf(1000, tg);
        givenTargetGroupHavingBidInLastHour(998, tg);

        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService ()->reloadCaches;
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_NOT_HITTING_THE_LIMIT\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("CappedBiddingPerHourFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(CappedBiddingPerHourFilterTests, filteringTargetGroupWithBiddingPerHourLimitMetOrExceeded) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupHavingCappedBiddingLimitOf(1000, tg);
        givenTargetGroupHavingBidInLastHour(10002, tg);

        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_HITTING_HOURLY_LIMIT\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("CappedBiddingPerHourFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

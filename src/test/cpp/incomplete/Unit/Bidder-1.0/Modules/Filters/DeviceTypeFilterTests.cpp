//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "DeviceTypeFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "DeviceTypeFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

DeviceTypeFilterTests::DeviceTypeFilterTests() {

}

DeviceTypeFilterTests::~DeviceTypeFilterTests() {

}

void DeviceTypeFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

}

void DeviceTypeFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void DeviceTypeFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void DeviceTypeFilterTests:: givenUserComingFromADeviceOfType(std::string deviceType) {
        // this->testService->context->deviceType = deviceType;
}

void DeviceTypeFilterTests::givenTargetGroupHavingNoDaypartMapping(std::shared_ptr<TargetGroup> tg) {
        beanFactory->mySqlTargetGroupDayPartTargetMapService->deleteAllMappingFor(tg);
}

TEST_F(DeviceTypeFilterTests, passingTgWithRightDeviceType) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenUserComingFromADeviceOfType("mobile_phone");
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_FOR_MATCHING_DEVICE_TYPE\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DeviceTypeFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

TEST_F(DeviceTypeFilterTests, filteringTgForNoRightHourOnBidRequest) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenUserComingFromADeviceOfType("mobile_phone");
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_UNMATCHING_DEVICE_TYPE\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DeviceTypeFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}


TEST_F(DeviceTypeFilterTests, passingTargetGroupForNoHavingDeviceTypeSelection) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);

        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenUserComingFromADeviceOfType("mobile_phone");
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_UNMATCHING_DEVICE_TYPE\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("DeviceTypeFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}

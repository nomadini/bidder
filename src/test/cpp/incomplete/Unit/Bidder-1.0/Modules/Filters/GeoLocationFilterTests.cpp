//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "GeoLocationFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "GeoLocationFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>

GeoLocationFilterTests::GeoLocationFilterTests() {

}

GeoLocationFilterTests::~GeoLocationFilterTests() {

}

void GeoLocationFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<GeoLocationFilter> (beanFactory->targetGroupGeoLocationCacheService);
}

void GeoLocationFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void GeoLocationFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}


void GeoLocationFilterTests::givenRequestComingFromLocation(std::string country, std::string state,std::string city) {
        testService->context->deviceCountry = country;
        testService->context->deviceCity = city;
        testService->context->deviceState = state;
}

TEST_F(GeoLocationFilterTests, passingTgWithRightGeoLocation) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert (tg);


        std::shared_ptr<GeoLocation> geoLocation  = std::make_shared<GeoLocation>();
        geoLocation->state = "NEW YORK";
        geoLocation->country = "USA";
        geoLocation->city = "NEW YORK CITY";

        beanFactory->mySqlGeoLocationService->insert(geoLocation);

        beanFactory->targetGroupGeoLocationCacheService()->reloadCaches;
        std::shared_ptr<TargetGroupGeoLocation> tgGeoLocation = std::make_shared<TargetGroupGeoLocation>();
        tgGeoLocation->targetGroupId = tg->getId ();
        tgGeoLocation->geoLocationId = geoLocation->id;
        beanFactory->mySqlTargetGroupGeoLocationService->insert(tgGeoLocation);

        beanFactory->targetGroupGeoLocationCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;


        givenRequestComingFromLocation(geoLocation->country, geoLocation->state, geoLocation->city);

        givenTargetGroupInContext (tg);

        whenModuleRuns ();
        std::string expectedResult = "[\"PASSED_FOR_MATCHING_GEO_LOCAITON\"]";
        // std::string actualResult = module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity(this->module->getName(),
        //                                                StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(actualResult, testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(GeoLocationFilterTests, failingTargetGroupWithoutMatchingGeoLocation) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert (tg);


        std::shared_ptr<GeoLocation> geoLocation  = std::make_shared<GeoLocation>();
        geoLocation->state = "NEW YORK";
        geoLocation->country = "USA";
        geoLocation->city = "NEW YORK CITY";

        beanFactory->mySqlGeoLocationService->insert(geoLocation);
        beanFactory->targetGroupGeoLocationCacheService()->reloadCaches;

        std::shared_ptr<TargetGroupGeoLocation> tgGeoLocation = std::make_shared<TargetGroupGeoLocation>();
        tgGeoLocation->targetGroupId = tg->getId ();
        tgGeoLocation->geoLocationId = geoLocation->id;
        beanFactory->mySqlTargetGroupGeoLocationService->insert(tgGeoLocation);

        beanFactory->targetGroupGeoLocationCacheService()->reloadCaches;
        beanFactory->targetGroupCacheService()->reloadCaches;


        givenRequestComingFromLocation("unknownCountry", geoLocation->state, geoLocation->city);

        givenTargetGroupInContext (tg);

        whenModuleRuns ();
        std::string expectedResult = "[\"FAILED_FOR_UN_MATCHING_GEO_LOCAITON\"]";
        // std::string actualResult = module->entityToModuleStateStats->
        //                            getAllPossibleStatesForModuleAndEntity(this->module->getName(),
        //                                                                   StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(actualResult, testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(GeoLocationFilterTests, passingTargetGroupWithoutGeoLocationFilter) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert (tg);
        beanFactory->targetGroupCacheService()->reloadCaches;

        std::shared_ptr<GeoLocation> geoLocation  = std::make_shared<GeoLocation>();
        geoLocation->state = "NEW YORK";
        geoLocation->country = "USA";
        geoLocation->city = "NEW YORK CITY";

        beanFactory->mySqlGeoLocationService->insert(geoLocation);
        beanFactory->targetGroupGeoLocationCacheService()->reloadCaches;
        givenRequestComingFromLocation(geoLocation->country, geoLocation->state, geoLocation->city);

        givenTargetGroupInContext (tg);

        whenModuleRuns ();
        std::string expectedResult = "[\"PASSED_NO_GEO_LOCAITON_FOR_TG\"]";

        // std::string actualResult = module->entityToModuleStateStats->
        //                            getAllPossibleStatesForModuleAndEntity(this->module->getName(),
        //                                                                   StringUtil::toStr ("tg") + StringUtil::toStr (tg->getId ()));
        // EXPECT_THAT(actualResult, testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

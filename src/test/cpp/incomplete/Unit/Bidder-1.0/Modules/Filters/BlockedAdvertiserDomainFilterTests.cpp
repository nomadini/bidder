//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "BlockedAdvertiserDomainFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "BlockedAdvertiserDomainFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "AdvertiserTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"
#include <boost/foreach.hpp>

BlockedAdvertiserDomainFilterTests::BlockedAdvertiserDomainFilterTests() {

}

BlockedAdvertiserDomainFilterTests::~BlockedAdvertiserDomainFilterTests() {

}

void BlockedAdvertiserDomainFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<BlockedAdvertiserDomainFilter> (beanFactory->advertiserCacheService,
        //                                                           beanFactory->campaignCacheService);
}

void BlockedAdvertiserDomainFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void BlockedAdvertiserDomainFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void BlockedAdvertiserDomainFilterTests::givenBidRequestHasBlockingAdvDomain(std::vector<std::string> blockingDomains) {

        for(std::string domain :  blockingDomains) {
                this->testService->context->addBlockedAdvertiser(domain);
        }

}

void BlockedAdvertiserDomainFilterTests::givenAdvertiserHavingBlockingDomains(std::shared_ptr<Advertiser> adv, std::vector<std::string> blockingDomains) {
        for(std::string domain :  blockingDomains) {
                tbb::concurrent_hash_map<std::string, int>::accessor accessor;
                adv->domainNames->insert (accessor, domain);
        }


}

TEST_F(BlockedAdvertiserDomainFilterTests, passingTargetGroupWithUnblockingAdvertiserDomains) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();
        tg->setCampaignId(campaign->getId());
        campaign->setAdvertiserId(advertiser->id);

        givenTargetGroupExistInCache(tg);
        givenCampaignExistInCache(campaign);
        givenAdvertiserExistInCache(advertiser);

        std::vector<std::string> blockingDomains;
        blockingDomains.push_back ("abc.com");
        blockingDomains.push_back ("fox.com");
        givenAdvertiserHavingBlockingDomains (advertiser, blockingDomains);
        std::vector<std::string> blockingDomainsOfBidRequest;
        blockingDomainsOfBidRequest.push_back ("cnn.com");
        givenBidRequestHasBlockingAdvDomain (blockingDomainsOfBidRequest);

        givenTargetGroupInContext (tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"TG_WAS_EVALUATED\",\"PASSED\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedAdvertiserDomainFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));

}

TEST_F(BlockedAdvertiserDomainFilterTests, filteringTargetGroupWithBlockingAdvertiserDomain) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        auto campaign = CampaignTestHelper::createSampleCampaign ();
        auto advertiser = AdvertiserTestHelper::createSampleAdvertiser ();
        tg->setCampaignId(campaign->getId());
        campaign->setAdvertiserId(advertiser->id);

        givenTargetGroupExistInCache(tg);
        givenCampaignExistInCache(campaign);
        givenAdvertiserExistInCache(advertiser);

        std::vector<std::string> blockingDomains;
        blockingDomains.push_back ("abc.com");
        blockingDomains.push_back ("fox.com");
        givenAdvertiserHavingBlockingDomains (advertiser, blockingDomains);
        MLOG(3)<< "beanFactory->advertiserCacheService()->getallEntitiesMap()->size : "<<
                beanFactory->advertiserCacheService()->getallEntitiesMap()->size;
        std::vector<std::string> blockingDomainsOfBidRequest;
        blockingDomainsOfBidRequest.push_back ("aBC.com");
        givenBidRequestHasBlockingAdvDomain (blockingDomainsOfBidRequest);

        givenTargetGroupInContext (tg);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_BLOCKED_ADVERTISER\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedAdvertiserDomainFilter" ,
        //                                                 StringUtil::toStr ("tg") + tg->idAsString) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));

}

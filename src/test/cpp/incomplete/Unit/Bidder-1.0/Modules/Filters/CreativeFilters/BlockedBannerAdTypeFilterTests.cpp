#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BlockedBannerAdTypeFilterTests.h"
#include "BlockedBannerAdTypeFilter.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"

BlockedBannerAdTypeFilterTests::BlockedBannerAdTypeFilterTests() {

}

BlockedBannerAdTypeFilterTests::~BlockedBannerAdTypeFilterTests() {

}


void BlockedBannerAdTypeFilterTests::SetUp() {
        CreativeFiltersTestsBase::SetUp ();

        // std::shared_ptr<CreativeSizeFilter> creativeFilter = std::make_shared<CreativeSizeFilter> (
        //         beanFactory->targetGroupCreativeCacheService ,
        //         beanFactory->creativeCacheService );
        //  module = std::make_shared<BlockedBannerAdTypeFilter> (creativeFilter);
}

void BlockedBannerAdTypeFilterTests::TearDown() {
        CreativeFiltersTestsBase::TearDown ();
}

void BlockedBannerAdTypeFilterTests::givenBannerAdType(std::string type) {
        this->bannerAdType = type;
        this->testService->context->addBlockedBannerType(bannerAdType);
}

void BlockedBannerAdTypeFilterTests::whenModuleRuns() {
        testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void BlockedBannerAdTypeFilterTests::givenCreativeTypeInTargetGroupHasType(const std::string &type) {
        this->testService->creative->setAdType(type);
}

TEST_F(BlockedBannerAdTypeFilterTests, filteringIframeBannersIfBidRequestBlocksIt) {
        givenTheCreativeAndTargetGroupsExistInCache ();
        givenBannerAdType ("IFRAME");
        givenCreativeTypeInTargetGroupHasType ("IFRAME");
        givenTargetGroupInContext (this->testService->targetGroup);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (testService->targetGroup->getId(),
                                                                          testService->creative->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedBannerAdTypeFilter" ,
        //                                                 stateId) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(BlockedBannerAdTypeFilterTests, filteringJavascriptBannersIfBidRequestBlocksIt) {
        givenTheCreativeAndTargetGroupsExistInCache ();
        givenBannerAdType ("JAVASCRIPT");
        givenCreativeTypeInTargetGroupHasType ("JAVASCRIPT");
        givenTargetGroupInContext (this->testService->targetGroup);
        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (testService->targetGroup->getId(),
                                                                          testService->creative->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedBannerAdTypeFilter" ,
        //                                                 stateId) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}


TEST_F(BlockedBannerAdTypeFilterTests, passingJavascriptBannersIfBidRequestDoesntBlockIt) {
        givenTheCreativeAndTargetGroupsExistInCache ();
        givenBannerAdType ("JAVASCRIPT");
        givenCreativeTypeInTargetGroupHasType ("IFRAME");
        givenTargetGroupInContext (this->testService->targetGroup);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (testService->targetGroup->getId(),
                                                                          testService->creative->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //         getAllPossibleStatesForModuleAndEntity ("BlockedBannerAdTypeFilter" ,
        //                                                 stateId) ,
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

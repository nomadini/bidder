//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_ActiveFilterModuleTests_H
#define BIDDER_ActiveFilterModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "ActiveFilterModule.h"
#include "FilterTestsBase.h"

class ActiveFilterModuleTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<ActiveFilterModule> module;
    std::exception exceptionThrown;

    void givenTargetGroupHasStatus (std::shared_ptr<TargetGroup> tg, std::string status);

    ActiveFilterModuleTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~ActiveFilterModuleTests() ;

};

#endif //BIDDER_ActiveFilterModuleTests_H

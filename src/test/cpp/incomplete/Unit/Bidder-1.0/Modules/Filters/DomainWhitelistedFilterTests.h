//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_DomainWhiteListedFilterTests_H
#define BIDDER_DomainWhiteListedFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "DomainWhiteListedFilter.h"

class DomainWhiteListedFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<DomainWhiteListedFilter> module;

    void givenBidRequestComingFromDomain(std::string domain);

    DomainWhiteListedFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~DomainWhiteListedFilterTests() ;

};

#endif //BIDDER_DomainWhiteListedFilterTests_H

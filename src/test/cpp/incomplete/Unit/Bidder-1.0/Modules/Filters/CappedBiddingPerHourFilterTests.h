//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_CappedBiddingPerHourFilterTests_H
#define BIDDER_CappedBiddingPerHourFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "CappedBiddingPerHourFilter.h"

class CappedBiddingPerHourFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<CappedBiddingPerHourFilter> module;

    void givenTargetGroupHavingCappedBiddingLimitOf(int biddingLimit, std::shared_ptr<TargetGroup> tg);

    void givenTargetGroupHavingBidInLastHour(int numberOfBids, std::shared_ptr<TargetGroup> tg);

    CappedBiddingPerHourFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~CappedBiddingPerHourFilterTests() ;

};

#endif //BIDDER_CappedBiddingPerHourFilterTests_H

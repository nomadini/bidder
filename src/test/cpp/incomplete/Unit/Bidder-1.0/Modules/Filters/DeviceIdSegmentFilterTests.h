//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_DeviceIdSegmentFilterTests_H
#define BIDDER_DeviceIdSegmentFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "DeviceIdSegmentFilter.h"

class DeviceIdSegmentFilterTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<DeviceIdSegmentFilter> module;

    void givenTgSegmentMappingExists(int targetGroupId , int segmentId);

    void givenDeviceSegmentExistsForDevice(std::string userDeviceId , std::string userdeviceType , int segmentId);

    DeviceIdSegmentFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~DeviceIdSegmentFilterTests();

};

#endif //BIDDER_DeviceIdSegmentFilterTests_H

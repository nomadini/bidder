//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_TargetGroupPacingByBudgetModuleTests_H
#define BIDDER_TargetGroupPacingByBudgetModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "TargetGroupPacingByBudgetModule.h"

class TargetGroupPacingByBudgetModuleTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<TargetGroupPacingByBudgetModule> module;

    TargetGroupPacingByBudgetModuleTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~TargetGroupPacingByBudgetModuleTests();

};

#endif //BIDDER_TargetGroupPacingByBudgetModuleTests_H

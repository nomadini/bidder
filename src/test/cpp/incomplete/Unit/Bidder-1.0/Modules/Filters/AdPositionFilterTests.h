//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_AdPositionFilterTests_H
#define BIDDER_AdPositionFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "AdPositionFilter.h"
#include "FilterTestsBase.h"

class AdPositionFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<AdPositionFilter> module;

    void givenTargetGroupHasPosition (std::shared_ptr<TargetGroup> tg , const std::string& pos);

    void givenBidRequestHasPosition(const std::string& pos);

    void givenTargetGroupHasNoPositionSelected (std::shared_ptr<TargetGroup> tg);

    AdPositionFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~AdPositionFilterTests() ;

};

#endif //BIDDER_AdPositionFilterTests_H

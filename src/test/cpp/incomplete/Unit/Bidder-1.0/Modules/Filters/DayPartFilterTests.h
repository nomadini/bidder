//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_DayPartFilterTests_H
#define BIDDER_DayPartFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "DayPartFilter.h"

class DayPartFilterTests  : public FilterTestsBase {

private:

public:

    std::shared_ptr<DayPartFilter> module;

    void givenUserHavingTimeZoneOffsetOf(int offset);

    std::string createPassingHourJsonForUser(int userLocalTime, int userWeekDay);

    void givenTargetGroupHavingNoDaypartMapping(std::shared_ptr<TargetGroup> tg);

    DayPartFilterTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~DayPartFilterTests() ;

};

#endif //BIDDER_DayPartFilterTests_H

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "MatchingIabCategoryFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "MatchingIabCategoryFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "BidderTestService.h"
#include "SegmentTestHelper.h"
#include <boost/foreach.hpp>

MatchingIabCategoryFilterTests::MatchingIabCategoryFilterTests() {

}

MatchingIabCategoryFilterTests::~MatchingIabCategoryFilterTests() {

}

void MatchingIabCategoryFilterTests::SetUp() {
        FilterTestsBase::SetUp ();

        // module = std::make_shared<MatchingIabCategoryFilter> ();
}

void MatchingIabCategoryFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void MatchingIabCategoryFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}


void MatchingIabCategoryFilterTests::givenRequestHavingIabCategories(std::string iabCategory, std::string iabSubCategory) {
        testService->context->siteCategory.push_back(iabSubCategory);
}
void MatchingIabCategoryFilterTests::givenTgHavingIabCategories(std::shared_ptr<TargetGroup> tg, std::string iabCategory, std::string iabSubCategory) {
/*    tg->setIabCategory (CollectionUtil::convertToList<std::string>(iabCategory));
    tg->setIabSubCategory (CollectionUtil::convertToList<std::string>(iabSubCategory));*/

        tg->setIabCategory (iabCategory);
        tg->setIabSubCategory (iabSubCategory);
}
TEST_F(MatchingIabCategoryFilterTests, passingTgWithRightMatchingIabCategory) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        std::string iabCategory  = "IAB14";
        std::string iabSubCategory  = "IAB14-1";

        givenRequestHavingIabCategories(iabCategory, iabSubCategory);
        givenTgHavingIabCategories(tg, iabCategory, iabSubCategory);

        beanFactory->mySqlTargetGroupService ->insert (tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        givenTargetGroupInContext (tg);

        whenModuleRuns ();
        std::string expectedResult = "[\"PASSED_FOR_MATCHING_IAB_SUB_CATEGORY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        // getAllPossibleStatesForModuleAndEntity (this->module->getName(),
        //         StringUtil::toStr ("tg") + tg->idAsString) ,
        // testing::Eq (expectedResult));
        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

TEST_F(MatchingIabCategoryFilterTests, failingTargetGroupWithNoMatchingIabCategory) {
        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        std::string iabCategory  = "IAB14";
        std::string iabSubCategory  = "IAB14-1";

        givenRequestHavingIabCategories(iabCategory, "IAB15-1");
        givenTgHavingIabCategories(tg, iabCategory, iabSubCategory);

        beanFactory->mySqlTargetGroupService ->insert (tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        givenTargetGroupInContext (tg);

        whenModuleRuns ();
        std::string expectedResult = "[\"FAILED_FOR_UN_MATCHING_IAB_SUB_CATEGORY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        // getAllPossibleStatesForModuleAndEntity (this->module->getName(),
        //         StringUtil::toStr ("tg") + tg->idAsString) ,
        // testing::Eq (expectedResult));
        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(MatchingIabCategoryFilterTests, failingTargetGroupWithoutIabCategory) {

        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();

        std::string iabCategory  = "IAB14";
        std::string iabSubCategory  = "IAB14-1";

        givenRequestHavingIabCategories(iabCategory, iabSubCategory);

//    std::vector<string> empty;
        tg->setIabCategory ("");
        tg->setIabSubCategory ("");

        beanFactory->mySqlTargetGroupService ->insert (tg);

        beanFactory->targetGroupCacheService()->reloadCaches;

        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_UN_MATCHING_IAB_SUB_CATEGORY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        // getAllPossibleStatesForModuleAndEntity (this->module->getName(),
        // StringUtil::toStr ("tg") + tg->idAsString) ,
        // testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

#include "GUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "TestUtil.h"
#include "OldRealTimeInfoFilterTests.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
#include "BeanFactory.h"
#include "BidderTestService.h"
#include <boost/foreach.hpp>
#include <thread>

OldRealTimeInfoFilterTests::OldRealTimeInfoFilterTests() {

}

OldRealTimeInfoFilterTests::~OldRealTimeInfoFilterTests() {

}

void OldRealTimeInfoFilterTests::SetUp() {
        FilterTestsBase::SetUp ();
        // module = std::make_shared<OldRealTimeInfoFilter> (beanFactory->tgRealTimeDeliveryInfoCacheService);
}

void OldRealTimeInfoFilterTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void OldRealTimeInfoFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups(testService->context);
}

void OldRealTimeInfoFilterTests::givenRealTimeInfoBeingOld(std::shared_ptr<TargetGroup> tg) {

        //setting the realtimeInfo to expire in 1 micro
        // beanFactory->tgRealTimeDeliveryInfoCacheService
        // ->tgToLastTimeRealTimeInfoUpdatedCache.add(tg->getId(), IntVal(tg->getId (), 1));


}

void OldRealTimeInfoFilterTests::givenRealTimeInfoBeingNotOld(std::shared_ptr<TargetGroup> tg) {

        //setting the realtimeInfo to expire in 1 seconds
        // beanFactory->tgRealTimeDeliveryInfoCacheService
        // ->tgToLastTimeRealTimeInfoUpdatedCache.add(tg->getId(), IntVal(tg->getId (), 1000000));


}

TEST_F(OldRealTimeInfoFilterTests, whenTargetGroupRealTimeIsOldTgIsFiltered) {


        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenRealTimeInfoBeingOld(tg);
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_OLD_REAL_TIME_INFO\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity (module->getName (),
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(OldRealTimeInfoFilterTests, whenTargetGroupRealTimeIsNotOldTgIsNotFiltered) {


        auto tg = TargetGroupTestHelper::createSampleTargetGroup ();
        beanFactory->mySqlTargetGroupService->insert(tg);
        beanFactory->targetGroupCacheService ()->reloadCaches;

        givenRealTimeInfoBeingNotOld(tg);
        givenTargetGroupInContext (tg);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSING_FOR_FRESH_REAL_TIME_INFO\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity (module->getName (),
        //                                                     StringUtil::toStr ("tg") + tg->idAsString),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_OldRealTimeInfoFilterTests_H
#define BIDDER_OldRealTimeInfoFilterTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "BlockedBannerAdTypeFilter.h"
class OpportunityContext;
#include "BlockedAdvertiserDomainFilter.h"
#include "FilterTestsBase.h"
#include "OldRealTimeInfoFilter.h"

#include "HttpUtilServiceMock.h"
#include "BidderApplicationContext.h"

class OldRealTimeInfoFilterTests : public FilterTestsBase {

private:

public:

    std::shared_ptr<OldRealTimeInfoFilter> module;
    OldRealTimeInfoFilterTests();

    void givenRealTimeInfoBeingOld(std::shared_ptr<TargetGroup> tg);

    void givenRealTimeInfoBeingNotOld(std::shared_ptr<TargetGroup> tg);


    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~OldRealTimeInfoFilterTests();

};

#endif //BIDDER_OldRealTimeInfoFilterTests_H

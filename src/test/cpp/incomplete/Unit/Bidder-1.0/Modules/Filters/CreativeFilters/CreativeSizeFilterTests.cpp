//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "CreativeSizeFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "CreativeSizeFilterTests.h"
#include "CreativeSizeFilter.h"
#include "OpportunityContext.h"
#include "TargetGroupTestHelper.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "MySqlTargetGroupService.h"
#include "CampaignTestHelper.h"
#include "BidderTestService.h"
#include "CreativeTestHelper.h"
#include "BlockedCreativeAttributeFilter.h"
#include "CreativeSizeFilter.h"
#include "CreativeAPIConverter.h"
#include "BlockedBannerAdTypeFilter.h"

CreativeSizeFilterTests::CreativeSizeFilterTests() {

}

CreativeSizeFilterTests::~CreativeSizeFilterTests() {

}


void CreativeSizeFilterTests::SetUp() {
        CreativeFiltersTestsBase::SetUp ();

        // module = std::make_shared<CreativeSizeFilter> (beanFactory->targetGroupCreativeCacheService ,
        //                                                beanFactory->creativeCacheService );
}

void CreativeSizeFilterTests::TearDown() {
        CreativeFiltersTestsBase::TearDown ();
}

void CreativeSizeFilterTests::whenModuleRuns() {
        this->testService->context->allAvailableTgs = module->filterTargetGroups (testService->context);
}

void CreativeSizeFilterTests::givenTargetGroupAndCreativeAreLinked(std::shared_ptr<TargetGroup> tg, std::shared_ptr<Creative> crt) {
        std::unordered_map<int, int> creativeIds;
        creativeIds.insert (std::make_pair(crt->getId(),crt->getId()));
        // beanFactory->targetGroupCreativeCacheService ()->getAllTargetGroupCreativesMap ->insert (
        //         std::make_pair (tg->getId(), creativeIds));
        beanFactory->targetGroupCacheService ()->reloadCaches ;
        // beanFactory->creativeCacheService ()->getAllEntitiesMap()->insert (std::make_pair (crt->getId, crt));
}

void CreativeSizeFilterTests::givenRequestHavingCreativeSize(int width, int height) {
        testService->context->adSize = StringUtil::toStr(width) + "x" + StringUtil::toStr(height);
}

void CreativeSizeFilterTests::givenCreativeHavingSize(std::shared_ptr<Creative> crt1, int width, int height) {
        crt1->setSize(StringUtil::toStr(width) + "x" + StringUtil::toStr(height));
}

void CreativeSizeFilterTests::givenTargetGroupInContext(std::shared_ptr<TargetGroup> tg1) {
        testService->context->allAvailableTgs->push_back(tg1);
}

TEST_F(CreativeSizeFilterTests, filteringTargetGroupThatHasNoCreative) {

        givenRequestHavingCreativeSize (300, 250);

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        givenTargetGroupInContext(tg1);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_NO_CREATIVES\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeSizeFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg1->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(CreativeSizeFilterTests, filteringTargetGroupThatHasNoMatchingCreativeSize) {

        givenRequestHavingCreativeSize (300, 250);

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto crt1 = CreativeTestHelper::createSampleCreative ();

        givenCreativeHavingSize (crt1, 320, 250);

        givenTargetGroupAndCreativeAreLinked (tg1, crt1);
        givenTargetGroupInContext(tg1);


        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_FOR_MISMATCHING_SIZE\"]";

        std::string stateId = BlockedBannerAdTypeFilter::createElementId (tg1->getId(), crt1->getId());

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeSizeFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg1->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (0));
}

TEST_F(CreativeSizeFilterTests, passingTargetGroupThatHasMatchingCreativeSize) {


        givenRequestHavingCreativeSize (300, 250);

        auto tg1 = TargetGroupTestHelper::createSampleTargetGroup ();
        auto crt1 = CreativeTestHelper::createSampleCreative ();

        givenCreativeHavingSize (crt1, 300, 250);

        givenTargetGroupAndCreativeAreLinked (tg1, crt1);
        givenTargetGroupInContext(tg1);

        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED\"]";

        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("CreativeSizeFilter",
        //                                                     StringUtil::toStr("tg") + StringUtil::toStr(tg1->getId())),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(testService->context->getSizeOfUnmarkedTargetGroups() (),
                    testing::Eq (1));
}

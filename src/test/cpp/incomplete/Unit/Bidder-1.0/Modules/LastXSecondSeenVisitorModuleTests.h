//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_LastXSecondSeenVisitorModuleTests_H
#define BIDDER_LastXSecondSeenVisitorModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "LastTimeSeenSource.h"
class OpportunityContext;
#include "LastXSecondSeenVisitorModule.h"
#include "FilterTestsBase.h"
#include "Status.h"
class Device;

class LastXSecondSeenVisitorModuleTests : public FilterTestsBase {

private:

public:
gicapods::StatusPtr status;
std::shared_ptr<LastXSecondSeenVisitorModule> module;
LastTimeSeenSourcePtr lastTimeSeenSource;
void givenUserWasBidOnInLastXSeconds (std::shared_ptr<Device> device, int xSeconds);

LastXSecondSeenVisitorModuleTests();

void whenModuleRuns();

void SetUp();

void TearDown();

virtual ~LastXSecondSeenVisitorModuleTests();

};

#endif //BIDDER_LastXSecondSeenVisitorModuleTests_H

//
// Created by Mahmoud Taabodi on 4/12/16.
//

#ifndef BIDDER_GlobalWhiteListModuleTests_H
#define BIDDER_GlobalWhiteListModuleTests_H

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
#include <memory>
#include <string>
#include <vector>
#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "LastTimeSeenSource.h"
class OpportunityContext;
#include "GlobalWhiteListModule.h"
#include "FilterTestsBase.h"
#include "Status.h"

class GlobalWhiteListModuleTests  : public FilterTestsBase {

private:

public:
    gicapods::StatusPtr status;
    std::shared_ptr<GlobalWhiteListModule> module;

    void givenDomainInWhiteList (std::string domain);

    void givenUserComingFromWebsite(std::string domain);

    GlobalWhiteListModuleTests();

    void whenModuleRuns();

    void SetUp();

    void TearDown();

    virtual ~GlobalWhiteListModuleTests() ;

};

#endif //BIDDER_GlobalWhiteListModuleTests_H

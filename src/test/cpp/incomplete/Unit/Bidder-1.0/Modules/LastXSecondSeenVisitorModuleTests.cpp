//
// Created by Mahmoud Taabodi on 4/12/16.
//

#include "LastXSecondSeenVisitorModuleTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "LastXSecondSeenVisitorModuleTests.h"
#include "OpportunityContext.h"
#include "BeanFactory.h"
#include "BidderTestService.h"
#include "LastTimeSeenSource.h"
#include "EntityToModuleStateStats.h"

LastXSecondSeenVisitorModuleTests::LastXSecondSeenVisitorModuleTests() {

}

LastXSecondSeenVisitorModuleTests::~LastXSecondSeenVisitorModuleTests() {

}


void LastXSecondSeenVisitorModuleTests::SetUp() {
        FilterTestsBase::SetUp ();
        auto entityToModuleStateStats = std::make_shared<EntityToModuleStateStats>();
        auto aeroSpikeDriver = std::make_shared<AerospikeDriver>("127.0.0.1", 3000, nullptr);
        aeroSpikeDriver->entityToModuleStateStats = entityToModuleStateStats;
        // this->lastTimeSeenSource = std::make_shared<LastTimeSeenSource>(aeroSpikeDriver);
        // module = std::make_shared<LastXSecondSeenVisitorModule> (lastTimeSeenSource,
        //                                                          beanFactory->entityToModuleStateStats);
}

void LastXSecondSeenVisitorModuleTests::TearDown() {
        FilterTestsBase::TearDown ();
}

void LastXSecondSeenVisitorModuleTests::whenModuleRuns() {
        status = module->process (testService->context);
}

void LastXSecondSeenVisitorModuleTests::givenUserWasBidOnInLastXSeconds (
        std::shared_ptr<Device> device,
        int xSeconds) {
        testService->context->device = device;

        module->acceptableIntervalToBidOnAUserInSeconds = xSeconds;
        this->lastTimeSeenSource->markAsSeenInLastXSeconds(nomadiniDeviceId, deviceType, recencyInMinute * 60);
}


TEST_F(LastXSecondSeenVisitorModuleTests, passingBecauseDidntSeeUserLately) {

        givenUserWasBidOnInLastXSeconds("abc12933", "DESKTOP", 1);

        //sleeping for two seconds
        gicapods::Util::sleepViaBoost(_L_,2);
        whenModuleRuns ();

        std::string expectedResult = "[\"PASSED_BECAUSE_DIDNT_SEE_LATELY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("LastXSecondSeenVisitorModule",
        //                                                     "ALL"),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(status, testing::Eq (STOP_PROCESSING));

}

TEST_F(LastXSecondSeenVisitorModuleTests, notBiddingOnUserWhenRecentlySeen) {

        givenUserWasBidOnInLastXSeconds("abc12933", "DESKTOP", 12);

        whenModuleRuns ();

        std::string expectedResult = "[\"FAILED_BECAUSE_SAW_LATELY\"]";
        // EXPECT_THAT(module->entityToModuleStateStats->
        //             getAllPossibleStatesForModuleAndEntity ("LastXSecondSeenVisitorModule",
        //                                                     "ALL"),
        //             testing::Eq (expectedResult));

        EXPECT_THAT(status, testing::Eq (STOP_PROCESSING));
}

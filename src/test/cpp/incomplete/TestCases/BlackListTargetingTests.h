//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef BlackListTargetingTests_H
#define BlackListTargetingTests_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
class OpportunityContext;
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "BidderTestService.h"
#include "CampaignRealTimeInfo.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "BidderTestFixture.h"

class BlackListTargetingTests;

class BlackListTargetingTests : public ::testing::Test {
    public :

    BidderTestFixturePtr testFixture;
    BlackListTargetingTests();
     void createABlackListWith(int targetGroupId, const std::string& domain);
    void givenBidRequestComingFromBlackListed();

     void SetUp();

     void TearDown();
//
};
#endif //BlackListTargetingTests_H



#include "BlackListTargetingTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "DateTimeUtil.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "CacheManagerServiceTestHelper.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderTester.h"
#include "BidderApplicationContext.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "OpportunityContextTestUtil.h"
#include "BidderResponseTestUtil.h"
#include "Pacer.h"
#include "DateTimeUtil.h"
#include "BWList.h"
#include "BWEntry.h"
#include "MySqlBWEntryService.h"
#include "MySqlBWListService.h"
#include "MySqlTargetGroupBwListMapService.h"
#include "TargetGroupBwListMap.h"
#include "Advertiser.h"
#include "DomainUtil.h"

BlackListTargetingTests::BlackListTargetingTests() {
        this->testFixture = std::make_shared<BidderTestFixture>();
}
void BlackListTargetingTests::SetUp() {
        this->testFixture->setUp();
}

void BlackListTargetingTests::TearDown() {
        this->testFixture->tearDown();
}
void BlackListTargetingTests::createABlackListWith(int id, const std::string& domain) {
        std::shared_ptr<BWList> bwList = std::make_shared<BWList>();
        bwList->setName("randomBwList");
        bwList->setListType("blackList");
        // bwList->setAdvertiserId(beanFactory->advertiserCacheService->getAdvertiserOfTargetGroup(id)->id);
        bwList->setCreatedAt(DateTimeUtil::getNowInMySqlFormat());
        bwList->setUpdatedAt(DateTimeUtil::getNowInMySqlFormat());
        beanFactory->mySqlBWListService->insert(bwList);
        auto newBWList = beanFactory->mySqlBWListService->readByName(bwList->name);

        std::shared_ptr<BWEntry> bwEntry = std::make_shared<BWEntry>();
        bwEntry->setDomainName(DomainUtil::getDomainFromUrlString (domain));
        bwEntry->setBwListId(newBWList->getId());
        bwEntry->setCreatedAt(DateTimeUtil::getNowInMySqlFormat());
        bwEntry->setUpdatedAt(DateTimeUtil::getNowInMySqlFormat());
        beanFactory->mySqlBWEntryService->insert(bwEntry);

        std::shared_ptr<TargetGroupBWListMap> targetGroupBWListMap = std::make_shared<TargetGroupBWListMap>();
        targetGroupBWListMap->targetGroupId = id;
        targetGroupBWListMap->bwListId = newBWList->id;
        beanFactory->mySqlTargetGroupBWListMapService->insert(targetGroupBWListMap);

}
void BlackListTargetingTests::givenBidRequestComingFromBlackListed() {


        BlackListTargetingTests::createABlackListWith(this->testFixture->targetGroup->getId(), "blacklisted.com");
        this->testFixture->openRtbBidRequest->site->domain="blacklisted.com";
}
TEST_F(BlackListTargetingTests, sendABidRequestForBlackListedWebsiteAndGetNoBid) {

        /*
            preparing the data in mysql
         */
        testFixture->setupTargetGroupAndCreativeForBidRequest();
        testFixture->setupGeoSegments();
        /*
            preparing the bid request and fixing the data to target it
         */
        testFixture->givenABidRequest();
        BlackListTargetingTests::givenBidRequestComingFromBlackListed();

        testFixture->persistTheDataInMySql();
        std::shared_ptr<OpenRtbBidResponse> expectedBidderResponse(new OpenRtbBidResponse());
        testFixture->whenRequestIsSentToBidder();
        MLOG(3)<<"actual bidder response is "<<testFixture->actualOpenRtbBidResponse->toJson();
        BidderResponseTestUtil::areEqual(expectedBidderResponse, testFixture->actualOpenRtbBidResponse);

}

/*
 */
//    MLOG(3)<<"starting sendABidRequestGetNoBid test";
//
//
//    /*
//        preparing the data in mysql
//    */
//	BlackListTargetingTests::setupTargetGroupAndCreativeForBidRequest();
//	BlackListTargetingTests::setupGeoSegments();
//    /*
//        preparing the bid request and fixing the data to target it
//    */
//    BlackListTargetingTests::givenABidRequest();
//	std::shared_ptr<PacingPlan> pacingPlan(new PacingPlan());
//    pacingPlan->set(10, DateTimeUtil::getCurrentHourAsIntegerInUTC());
//
//	BlackListTargetingTests::givenTargetGroupWithUnMetPacingPlan(pacingPlan);
//
//
//    BlackListTargetingTests::persistTheDataInMySql();
//	/*
//	* we need to make sure that we get the proper bid
//	*/
//	std::shared_ptr<OpenRtbBidResponse> bidResponse(new OpenRtbBidResponse());
//	BlackListTargetingTests::whenRequestIsSentToBidder();
//	BlackListTargetingTests::thenBidderResponseIs(bidResponse);
//
//	std::shared_ptr<OpportunityContext> expectedContext(std::make_shared<OpportunityContext>());
//    BlackListTargetingTests::whenContextIsAskedFor();
//    BlackListTargetingTests::thenContextIs(expectedContext);
//
//	/*
//	calling the adserver , playing the role of exchange
//	*/
//    BidderTestsHelper::hitAdServer(bidResponse->toJson());
//    BlackListTargetingTests::whenContextIsAskedForFromAdServer();
//
//    std::shared_ptr<OpportunityContext> expectedAdServerContext(std::make_shared<OpportunityContext>());
//    BlackListTargetingTests::whenAdServerContextIs(expectedAdServerContext);
//
//    /*
//       asking the pacer to force flush and merge all the changes with cassandra
//    */
//    BlackListTargetingTests::whenRequestForForceFlushIsSentToPacing();
//
//    /*
//        calling the pacer to give us the real time infos,
//    */
//	BlackListTargetingTests::whenTgRealTimeInfoIsAskedFromPacing();
//	std::shared_ptr<EntityRealTimeDeliveryInfo> tgExpected = std::make_shared<EntityRealTimeDeliveryInfo>();
//	BlackListTargetingTests::thenTgRealTimeInfoIsAsExpected(tgExpected);
//
//	BlackListTargetingTests::whenCampaignRealTimeInfoIsAskedFromPacing();
//
//	std::shared_ptr<CampaignRealTimeInfo>  expectedCmpRealTimeInfo = std::make_shared<CampaignRealTimeInfo>();
//	BlackListTargetingTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);
//
//
//    /*
//        reading the real time info from cassandra to make sure that the changes are persisted
//        correctly
//    */
//	BlackListTargetingTests::whenTgRealTimeInfoIsReadFromCassandra();
//
//	BlackListTargetingTests::thenTgRealTimeInfoIsAsExpected(tgExpected);
//
//	BlackListTargetingTests::whenCampaignRealTimeInfoIsReadFromCassandra();
//
//	BlackListTargetingTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);
//
//    MLOG(3)<<"end of sendABidRequestGetNoBid test!";
//}

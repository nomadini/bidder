

#include "OpenRtbProcessingTest.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "CacheManagerServiceTestHelper.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderTester.h"
#include "BidderApplicationContext.h"
#include "MySqlCampaignService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "OfferTestHelper.h"
#include "MySqlInventoryService.h"
#include "MySqlGeoSegmentListService.h"
#include "BidderApplicationContext.h"
#include "OpenRtbBidResponseJsonConverter.h"

void OpenRtbProcessingTest::SetUp( ) {

        bidRequestCreator = std::make_shared<BidRequestCreator>();
        beanFactory->mySqlCampaignService()->deleteAll;
        beanFactory->mySqlTargetGroupService->deleteAll;
        beanFactory->mySqlCreativeService()->deleteAll;

        beanFactory->deviceFeatureHistoryCassandraService()->deleteAll;
        beanFactory->featureDeviceHistoryCassandraService()->deleteAll;
}

//       // code here will execute just before the test ensues
//
//      beanFactory->mySqlOfferService()->deleteAllOffersAndAssociatedPixelsAndModels;
//      beanFactory->mySqlModelService()->deleteAllModels;
//      /** truncate all the device histories */
//      DeviceFeatureHistoryCassandraService::getService()->deleteAll();
//      FeatureDeviceHistoryCassandraService::getService()->deleteAll(CassandraDriver::getSession());
//   }
//
void OpenRtbProcessingTest::TearDown( ) {
        // code here will be called just after the test completes
        // ok to through exceptions from here if need be
        MLOG(3)<<"going to sleep to  let the threads break out of their loops....";
        gicapods::Util::sleepViaBoost(_L_,5);


}
//
//std::shared_ptr<ModelRequest> OpenRtbProcessingTest::givenAModelRequestWithPositiveOffers(std::shared_ptr<Offer> offer) {
//    std::shared_ptr<ModelRequest> model(new  ModelRequest());
//    model->segmentSeedName =  StringUtil::toStr("cnn-visitor-today").append(StringUtil::random_string(4));
//
//    model->dateOfRequest = DateTimeUtil::getNowPlusSecondsInMySqlFormat(-10800);
//    model->bidDecisionResult = "requestWasSubmitted";
//    model->featureRecencyInSecond = 10000;
//    model->advertiserId = RandomUtil::sudoRandomNumber(1000);
//    model->name = StringUtil::toStr("svmSgdShogun").append(StringUtil::random_string(4));
//    model->pixelHitRecencyInSecond= 10000;
//    model->maxNumberOfDeviceHistoryPerFeature = 100;
//    model->modelType = "pixelModel";
//
//          model->positiveOfferIds = OfferTestHelper::getRandomOfferIds(10);
//            model->negativeOfferIds = OfferTestHelper::getRandomOfferIds(10);//have a test case that fixes
//        model->numberOfNegativeDevicesToBeUsed= 100;
//        model->numberOfPositiveDevicesToBeUsed  = 100;
//            model->numberOfTopFeatures =  10;
//            model->cutOffScore = 2;
//
//        model->featureRecencyInSecond = 3600;
//            model->pixelHitRecencyInSecond  = 1000;
//            model->algorithmToModelBasedOn = StringUtil::toStr("alpha");
//
//
//
//
//    model->positiveOfferIds.clear();
//    model->negativeOfferIds.clear();
//    model->positiveOfferIds.push_back((offer->id));
//    model->negativeOfferIds.push_back(offer->id);
//
//    model->validate();
//    return model;
//}
//
//std::shared_ptr<ModelRequest> OpenRtbProcessingTest::givenAModelRequestWithSeedWebsites() {
//
//    std::shared_ptr<ModelRequest> model(new  ModelRequest());
//    model->segmentSeedName =  StringUtil::toStr("cnn-visitor-today").append(StringUtil::random_string(4));
//    model->dateOfRequest = DateTimeUtil::getNowPlusSecondsInMySqlFormat(-10800);//3 hours ago
//    model->bidDecisionResult = "requestWasSubmitted";
//    model->featureRecencyInSecond = 10000;
//    model->advertiserId = RandomUtil::sudoRandomNumber(1000);
//    model->name = StringUtil::toStr("svmSgdShogun").append(StringUtil::random_string(4));
//    model->pixelHitRecencyInSecond= 10000;
//    model->maxNumberOfDeviceHistoryPerFeature = 100;
//    model->modelType = "seedModel";
//
//
//        model->seedWebsites.insert(StringUtil::toStr("www.cnn.com"));
//        model->numberOfNegativeDevicesToBeUsed = 100;
//            model->numberOfPositiveDevicesToBeUsed  = 100;
//          model->maxNumberOfDeviceHistoryPerFeature = 100;
//            model->numberOfTopFeatures =  10;
//            model->cutOffScore = 2;
//            model->maxNumberOfNegativeFeaturesToPick = 100;
//        model->featureRecencyInSecond = 3600;
//            model->algorithmToModelBasedOn = StringUtil::toStr("alpha");
//
//    model->validate();
//    return model;
//}
//
//void OpenRtbProcessingTest::givenTheRequestIsSubmittedInMySql(std::shared_ptr<ModelRequest> modelPersistentDto ) {
//
//    MySqlModelService::getService()->insert(modelPersistentDto);
//}
//
//void OpenRtbProcessingTest::whenAsynchronousModelerThreadIsRun(){
//
//        ModelerRequestHandler::runModellingOnRequestOlderThanNHours(2);
//      MLOG(3)<<"realTimeUpdater , waiting for async modeler to build the model, 10 seconds";
//      gicapods::Util::sleepViaBoost(_L_, 10);
//}
//
//void OpenRtbProcessingTest::thenRequestIsPickedUpAndModelIsBuiltAsExpected(
// std::shared_ptr<ModelRequest> expected, std::string modelType) {
//
//          std::shared_ptr<ModelRequest> modelFromDb = beanFactory->mySqlModelService->readModelByNameAndAdvertiserId(expected->name, expected->advertiserId);
//            MLOG(3)<<"this is the model read from db : "<<modelFromDb->toJson();
//
//            EXPECT_THAT(modelFromDb->name, testing::Eq(expected->name));
//            EXPECT_THAT(modelFromDb->modelType, testing::Eq(expected->modelType));
//            EXPECT_THAT(modelFromDb->algorithmToModelBasedOn, testing::Eq(expected->algorithmToModelBasedOn));
//            EXPECT_THAT(modelFromDb->segmentSeedName, testing::Eq(expected->segmentSeedName));
//            EXPECT_THAT(modelFromDb->bidDecisionResult, testing::Eq("modelCreated"));
//            EXPECT_THAT(modelFromDb->featureScoreMap.size(), testing::Gt(0));
//
//
//        if(modelType.compare("seedModel")==0) {
//
//            EXPECT_THAT(modelFromDb->seedWebsites.size(), testing::Eq(expected->seedWebsites.size()));
//            EXPECT_THAT(modelFromDb->maxNumberOfDeviceHistoryPerFeature, testing::Eq(expected->maxNumberOfDeviceHistoryPerFeature));
//            EXPECT_THAT(modelFromDb->maxNumberOfNegativeFeaturesToPick, testing::Eq(expected->maxNumberOfNegativeFeaturesToPick));
//
//        } else if(modelType.compare("pixelModel")==0) {
//
//            EXPECT_THAT(modelFromDb->negativeOfferIds.size(), testing::Eq(expected->negativeOfferIds.size()));
//            EXPECT_THAT(modelFromDb->positiveOfferIds.size(), testing::Eq(expected->positiveOfferIds.size()));
//            EXPECT_THAT(modelFromDb->pixelHitRecencyInSecond, testing::Eq(expected->pixelHitRecencyInSecond));
//        } else {
//            throwEx("unknow model type");
//        }
//
//     }
//

void OpenRtbProcessingTest::givenTgWithImpressionLimit(int limit) {
        testService->targetGroup->dailyMaxImpression = 1;
}


void OpenRtbProcessingTest::whenContextIsAskedFor() {
        testService->context = BidderTester::sendRequestToBidderForContext(testService->openRtbBidRequest->id);
}

void OpenRtbProcessingTest::thenContextIsAsExpected() {
        EXPECT_THAT(testService->context->chosenTargetGroupId,
                    testing::Eq(testService->targetGroup->getId()));

        EXPECT_THAT(testService->context->chosenCreativeId,
                    testing::Eq(testService->creative->getId()));

        EXPECT_THAT(testService->context->campaignId,
                    testing::Eq(testService->campaign->getId()));

        EXPECT_THAT(testService->context->advertiserId,
                    testing::Eq(testService->campaign->getAdvertiserId()));

        EXPECT_THAT(testService->context->bidPrice, testing::Eq(testService->openRtbBidRequest->imp->bidFloor));


        auto device = std::make_shared<Device>(
                testService->openRtbBidRequest->user->buyeruid, "");

        //soosan!!!!!!!!!!!!!!!!!!!!!! fix the lines below
//           std::vector <std::shared_ptr<DeviceFeatureHistory>> deviceFeatureHistory = beanFactory->deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(device , 300);
//           bool value = CollectionUtil::valueExistInMap<TimeType, std::string>(deviceFeatureHistory.at(0)->timeFeatureMap,
//           testService->openRtbBidRequest->site->domain);
//           EXPECT_THAT(value, testing::Eq(true));
//
//
//           auto featureDeviceHistory = beanFactory->featureDeviceHistoryCassandraService->readDeviceHistoryOfFeature(
//           testService->openRtbBidRequest->site->publisher->domain, (TimeType) 1000, 10);
//
//
//           EXPECT_THAT(featureDeviceHistory->devicehistories.size(), testing::Eq(1));
//           auto deviceHistory = featureDeviceHistory->devicehistories.at(0);

        EXPECT_THAT(deviceHistorydevice->getDeviceId(), testing::Eq(testService->openRtbBidRequest->user->buyeruid));
        EXPECT_THAT(deviceHistorygetDeviceType(), testing::Eq("DESKTOP"));

}

void OpenRtbProcessingTest::whenRequestIsSentToBidder() {
        auto resp = BidderTester::sendRequestToBidder(testService->openRtbBidRequest);

        if (resp.empty()) {
                MLOG(3)<<"response from bidder is empty.";
                throwEx(
                        "test failed because of no response from bidder");
        }

        testService->actualOpenRtbBidResponse = OpenRtbBidResponseJsonConverter::fromJson(resp);
        MLOG(3)<<"openRtbBidResponse : "<<testService->actualOpenRtbBidResponse;

}

void OpenRtbProcessingTest::givenABidRequest() {
        setupTargetGroupAndCreativeForBidRequest();
        setupGeoSegments();
        testService->openRtbBidRequest = bidRequestCreator->createOpenRtbBidRequest();
        std::shared_ptr<Inventory> inv (new Inventory());
        inv->name = HttpUtil::getDomainFromUrl(testService->openRtbBidRequest->site->domain);
        inv->id = RandomUtil::sudoRandomNumber(1000);
        inv->status = "active";
        MySqlInventoryService::addInventory(inv);
}

void OpenRtbProcessingTest::setupGeoSegments() {
        std::shared_ptr<GeoSegment> geoSegment (new GeoSegment());
        geoSegment->lat = 100;
        geoSegment->lon = 100;
        geoSegment->segmentRadiusInMile = 2;
        geoSegment->description = "macDonald's nyc";
        geoSegment->id=  RandomUtil::sudoRandomNumber(1000);


        std::shared_ptr<GeoSegmentList> geoSegmentList = std::make_shared<GeoSegmentList>();
        geoSegmentList->id = RandomUtil::sudoRandomNumber(1000);
        geoSegmentList->geoSegments.push_back(geoSegment);
        beanFactory->mySqlGeoSegmentListService()->getAllGeoSegmentLists->push_back(geoSegmentList);
        beanFactory->mySqlGeoSegmentListService()->getAllGeoSegmentListMap->insert(std::make_pair(geoSegmentList->id, geoSegmentList));

        beanFactory->targetGroupCacheService()->getAllTargetGroupGeoSegmentListIdMap()->insert(std::make_pair(testService->targetGroup->getId, geoSegmentList->id));
//		fix me later : TargetGroupCacheService::getAllTgGeoSegmentsListMap()->insert(std::make_pair(testService->targetGroup->getId(), MySqlGeoSegmentListService::getAllGeoSegmentListMap()));

}

void OpenRtbProcessingTest::setupTargetGroupAndCreativeForBidRequest() {

        testService->targetGroup = TargetGroupTestHelper::createSampleTargetGroup();
        testService->campaign = CampaignTestHelper::createSampleCampaign();
        testService->targetGroup->campaignId = testService->campaign->getId();
        testService->creative = CreativeTestHelper::createSampleCreative();
        testService->creative->setSize ("180x150");


        cacheManagerServiceTestHelper->putTgInRelativeMaps(testService->targetGroup, testService->campaign,
                                                           testService->creative);


        beanFactory->mySqlCampaignService->insert(testService->campaign);
        beanFactory->mySqlTargetGroupService->insert(testService->targetGroup);
        beanFactory->mySqlCreativeService->insert(testService->creative);
}

OpenRtbProcessingTest::OpenRtbProcessingTest() {
        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;
}

OpenRtbProcessingTest::~OpenRtbProcessingTest() {
}

TEST_F(OpenRtbProcessingTest, sendABidRequestGetBidResponse) {
        MLOG(3)<<"starting sendABidRequestGetNoBid test";

        OpenRtbProcessingTest::givenABidRequest();
        OpenRtbProcessingTest::givenTgWithImpressionLimit(1);
        OpenRtbProcessingTest::whenRequestIsSentToBidder();
        OpenRtbProcessingTest::thenBidderResponseIsAsExpected();
        OpenRtbProcessingTest::whenContextIsAskedFor();
        OpenRtbProcessingTest::thenContextIsAsExpected();
        MLOG(3)<<"end of sendABidRequestGetNoBid test!";

}



void OpenRtbProcessingTest::thenBidderResponseIsAsExpected() {

}

TEST_F(OpenRtbProcessingTest, sendNBidRequestGetBidResponse) {

//    for (int =0;i <1000; i++) {
//            OpenRtbProcessingTest::givenABidRequest();
//          OpenRtbProcessingTest::givenTgWithImpressionLimit(1);
//          OpenRtbProcessingTest::whenRequestIsSentToBidder();
//          OpenRtbProcessingTest::thenBidderResponseIsAsExpected();
//    }
}

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef CampaignDailyCapFilterTests_H
#define CampaignDailyCapFilterTests_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
class OpportunityContext;
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "BidderTestService.h"
#include "CampaignRealTimeInfo.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "BidderTestFixture.h"
class CampaignDailyCapFilterTests;

class CampaignDailyCapFilterTests : public ::testing::Test {
    public :

    BidderTestFixturePtr testFixture;
    CampaignDailyCapFilterTests();

     virtual ~CampaignDailyCapFilterTests();
    void givenBidRequestHasDailyCampaignCapOf1();
     void SetUp();

     void TearDown();

};
#endif //CampaignDailyCapFilterTests_H

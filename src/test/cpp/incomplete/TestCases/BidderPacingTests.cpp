

#include "BidderPacingTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "CacheManagerServiceTestHelper.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderTester.h"
#include "BidderApplicationContext.h"
#include "MySqlCampaignService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "OpportunityContextTestUtil.h"
#include "BidderResponseTestUtil.h"

#include "CampaignRealTimeInfoTestUtil.h"

#include "Pacer.h"
#include "BidderTestFixture.h"

BidderPacingTests::BidderPacingTests() {
        BidderTestServicePtr testService = std::make_shared<BidderTestService>();
        this->testService = testService;

        BidderTestFixturePtr testFixture = std::make_shared<BidderTestFixture>();
        this->testFixture = testFixture;
        bidderTester = new BidderTester();
}
BidderPacingTests::~BidderPacingTests() {
}


void BidderPacingTests::SetUp( ) {
        testFixture->setUp();
}

void BidderPacingTests::TearDown( ) {
        testFixture->tearDown();
}


void BidderPacingTests::givenTgWithImpressionLimit(int limit) {
        testService->targetGroup->dailyMaxImpression = 1;
}


void BidderPacingTests::whenContextIsAskedFor() {
        testService->context = bidderTester->sendRequestToBidderForContext(testService->openRtbBidRequest->id);
}

void BidderPacingTests::thenContextIsAsExpected() {
        OpportunityContextTestUtil::areEqual(testService);

}

void BidderPacingTests::givenTargetGroupWithUnMetPacingPlan(std::shared_ptr<PacingPlan> pacingPlan) {
        testService->targetGroup->pacingPlan = pacingPlan;
}


void BidderPacingTests::thenBidderResponseIs(std::shared_ptr<OpenRtbBidResponse> expectedBidderResponse) {
        MLOG(3)<<"actual bidder response is "<<testService->actualOpenRtbBidResponse->toJson();

        BidderResponseTestUtil::areEqual(expectedBidderResponse, testService->actualOpenRtbBidResponse);
}

void BidderPacingTests::thenContextIs(std::shared_ptr<OpportunityContext> expectedContext) {
        OpportunityContextTestUtil::areEqual(expectedContext, testService->context);
}

void BidderPacingTests::whenContextIsAskedForFromAdServer() {
        testService->context = bidderTester->sendRequestToAdServerForContext(testService->openRtbBidRequest->id);
}

void BidderPacingTests::whenAdServerContextIs(std::shared_ptr<OpportunityContext> expectedAdServerContext) {
        OpportunityContextTestUtil::areEqual(expectedAdServerContext, testService->context);
}
void BidderPacingTests::whenRequestForForceFlushIsSentToPacing() {
        bidderTester->sendForceFlushRequestToPacer();
}

void BidderPacingTests::whenTgRealTimeInfoIsAskedFromPacing(){
        testService->tgRealTimeInfo = bidderTester->sendTgRealtimeFetchRequestToPacer(testService->targetGroup->getId());
}
void BidderPacingTests::thenTgRealTimeInfoIsAsExpected(std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeInfo){
        // TargetGroupRealTimeInfoTestUtil::areEqual(expectedTgRealTimeInfo, testService->tgRealTimeInfo);

}

void BidderPacingTests::whenCampaignRealTimeInfoIsAskedFromPacing(){
        testService->campaignRealTimeInfo = bidderTester->sendCampaignRealtimeFetchRequestToPacer(testService->campaign->getId());
}
void BidderPacingTests::thenCampaignRealTimeInfoIsAsExpected(std::shared_ptr<CampaignRealTimeInfo> expectedCmpRealTimeInfo){
        CampaignRealTimeInfoTestUtil::areEqual(expectedCmpRealTimeInfo, testService->campaignRealTimeInfo);
}


void BidderPacingTests::whenTgRealTimeInfoIsReadFromCassandra() {

}
void BidderPacingTests::whenCampaignRealTimeInfoIsReadFromCassandra() {
}

void BidderPacingTests::givenPacerIsUnhealthy() {
        Pacer::setPacerHealth(false);
}




/*
   in this test we send a bid request suited for a target group with a pacing plan
   then we see that after adserver shows the creative for target group
   we see that the campaign and target group real time infos have been
   updated in Pacer and in cassandra
 */
TEST_F(BidderPacingTests, sendABidRequestAndTestPacingByDollar) {
        MLOG(3)<<"starting sendABidRequestGetNoBid test";


        /*
            preparing the data in mysql
         */
        testFixture->setupTargetGroupAndCreativeForBidRequest();
        testFixture->setupGeoSegments();
        /*
            preparing the bid request and fixing the data to target it
         */
        testFixture->givenABidRequest();
        std::shared_ptr<PacingPlan> pacingPlan(new PacingPlan());
        pacingPlan->set(10, DateTimeUtil::getCurrentHourAsIntegerInUTC());

        BidderPacingTests::givenTargetGroupWithUnMetPacingPlan(pacingPlan);


        testFixture->persistTheDataInMySql();
        /*
         * we need to make sure that we get the proper bid
         */
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> bidResponse(new OpenRtbBidResponse());
        testFixture->whenRequestIsSentToBidder();
        BidderPacingTests::thenBidderResponseIs(bidResponse);

        std::shared_ptr<OpportunityContext> expectedContext(std::make_shared<OpportunityContext>());
        BidderPacingTests::whenContextIsAskedFor();
        BidderPacingTests::thenContextIs(expectedContext);

        /*
           calling the adserver , playing the role of exchange
         */
        BidderTestsHelper::hitAdServer(bidResponse->toJson());
        BidderPacingTests::whenContextIsAskedForFromAdServer();

        std::shared_ptr<OpportunityContext> expectedAdServerContext(std::make_shared<OpportunityContext>());
        BidderPacingTests::whenAdServerContextIs(expectedAdServerContext);

        /*
           asking the pacer to force flush and merge all the changes with cassandra
         */
        BidderPacingTests::whenRequestForForceFlushIsSentToPacing();

        /*
            calling the pacer to give us the real time infos,
         */
        BidderPacingTests::whenTgRealTimeInfoIsAskedFromPacing();
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgExpected = std::make_shared<EntityRealTimeDeliveryInfo>();
        BidderPacingTests::thenTgRealTimeInfoIsAsExpected(tgExpected);

        BidderPacingTests::whenCampaignRealTimeInfoIsAskedFromPacing();

        std::shared_ptr<CampaignRealTimeInfo> expectedCmpRealTimeInfo = std::make_shared<CampaignRealTimeInfo>();
        BidderPacingTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);


        /*
            reading the real time info from cassandra to make sure that the changes are persisted
            correctly
         */
        BidderPacingTests::whenTgRealTimeInfoIsReadFromCassandra();

        BidderPacingTests::thenTgRealTimeInfoIsAsExpected(tgExpected);

        BidderPacingTests::whenCampaignRealTimeInfoIsReadFromCassandra();

        BidderPacingTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);

        MLOG(3)<<"end of sendABidRequestGetNoBid test!";
}


/*
   in this test, we send n request and see that the pacing of the target group
   becomes exhausted and after that , when we send a bid request, we get a no bid response
 */
TEST_F(BidderPacingTests, sendNBidRequestToSatisfyPacingLimit) {

        testFixture->setupTargetGroupAndCreativeForBidRequest();
        testFixture->setupGeoSegments();

        testFixture->givenABidRequest();
        std::shared_ptr<PacingPlan> pacingPlan(new PacingPlan());
        pacingPlan->set(10, DateTimeUtil::getCurrentHourAsIntegerInUTC());

        BidderPacingTests::givenTargetGroupWithUnMetPacingPlan(pacingPlan);

        testFixture->persistTheDataInMySql();

        //send 10 bidRequest with bid floor 1.50
        for(int i = 0; i < 10; i++) {
                /*
                 * we need to make sure that we get the proper bid
                 */
                std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> bidResponse(new OpenRtbBidResponse());
                testFixture->whenRequestIsSentToBidder();
                BidderPacingTests::thenBidderResponseIs(bidResponse);

                BidderTestsHelper::hitAdServer(bidResponse->toJson());
        }

        /*
         *   make sure the real time info captures 10 bid request with price of 1.50
         */
        BidderPacingTests::whenTgRealTimeInfoIsAskedFromPacing();
        std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeInfo(new EntityRealTimeDeliveryInfo());
        BidderPacingTests::thenTgRealTimeInfoIsAsExpected(expectedTgRealTimeInfo);

        std::shared_ptr<CampaignRealTimeInfo> expectedCmpRealTimeInfo(new CampaignRealTimeInfo());
        BidderPacingTests::whenCampaignRealTimeInfoIsAskedFromPacing();
        BidderPacingTests::thenCampaignRealTimeInfoIsAsExpected(expectedCmpRealTimeInfo);


        /*
            the pacing plan is exhausted, we should get a no bid
            and verify that the last module run was pacingByBudget
         */
        testFixture->whenRequestIsSentToBidder();
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> nodBidResponse(new OpenRtbBidResponse());
        BidderPacingTests::thenBidderResponseIs(nodBidResponse);

        BidderPacingTests::whenContextIsAskedFor();
        std::shared_ptr<OpportunityContext> expectedContext(std::make_shared<OpportunityContext>());
        BidderPacingTests::thenContextIs(expectedContext);

}


TEST_F(BidderPacingTests, bidderGoesToNoBidModeWhenPacerIsDown) {

        BidderPacingTests::givenPacerIsUnhealthy();
        testFixture->givenABidRequest();

        testFixture->whenRequestIsSentToBidder();
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> nodBidResponse(new OpenRtbBidResponse());
        BidderPacingTests::thenBidderResponseIs(nodBidResponse);

        /*
           we should verify that bidder was in a no bid mode and that's
           why we got a no bid
         */
        BidderPacingTests::whenContextIsAskedFor();
        std::shared_ptr<OpportunityContext> expectedContext(std::make_shared<OpportunityContext>());
        BidderPacingTests::thenContextIs(expectedContext);
}

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef BidderPacingTests_H
#define BidderPacingTests_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
class OpportunityContext;
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;

#include "CampaignRealTimeInfo.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "BidderTestFixture.h"

class BidderPacingTests;

class BidderPacingTests : public ::testing::Test {
    public :

    BidderTestServicePtr testService;
    BidderTestFixturePtr testFixture;
    BidderTester* bidderTester;
    BidderPacingTests();


    void givenTargetGroupWithUnMetPacingPlan(std::shared_ptr<PacingPlan> pacingPlan);

	   void givenTgWithImpressionLimit(int limit);

	   void whenContextIsAskedFor();

	   void thenContextIsAsExpected();

     virtual ~BidderPacingTests();

     void thenBidderResponseIs(std::shared_ptr<OpenRtbBidResponse> bidResponse);

     void SetUp();

     void TearDown();

     void whenContextIsAskedForFromAdServer();

     void whenAdServerContextIs(std::shared_ptr<OpportunityContext> expectedAdServerContext);

    void whenRequestForForceFlushIsSentToPacing();

    void	whenTgRealTimeInfoIsAskedFromPacing();
    void	thenTgRealTimeInfoIsAsExpected(std::shared_ptr<EntityRealTimeDeliveryInfo> tg);

    void	whenCampaignRealTimeInfoIsAskedFromPacing();
    void	thenCampaignRealTimeInfoIsAsExpected(std::shared_ptr<CampaignRealTimeInfo> cmp);


    void whenTgRealTimeInfoIsReadFromCassandra();
    void whenCampaignRealTimeInfoIsReadFromCassandra();

    void thenContextIs(std::shared_ptr<OpportunityContext> expectedContext);
    void givenPacerIsUnhealthy();
};
#endif //BidderPacingTests_H

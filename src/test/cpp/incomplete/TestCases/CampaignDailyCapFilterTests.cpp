#include "CampaignDailyCapFilterTests.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "CacheManagerServiceTestHelper.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderTester.h"
#include "BidderApplicationContext.h"
#include "MySqlCampaignService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "OpportunityContextTestUtil.h"
#include "BidderResponseTestUtil.h"

CampaignDailyCapFilterTests::CampaignDailyCapFilterTests() {
}

CampaignDailyCapFilterTests::~CampaignDailyCapFilterTests() {

}

void CampaignDailyCapFilterTests::SetUp() {

}

void CampaignDailyCapFilterTests::TearDown() {

}

void  CampaignDailyCapFilterTests::givenBidRequestHasDailyCampaignCapOf1() {
    testFixture->campaign->getDailyMaxImpression (1);

}

TEST_F(CampaignDailyCapFilterTests , sendBidRequestAndGetNoBidDueToCampaignCap) {
    MLOG(3) << "starting sendABidRequestGetNoBid test";
    /*
          preparing the data in mysql
      */
    testFixture->setupTargetGroupAndCreativeForBidRequest ();
    testFixture->setupGeoSegments ();
    /*
        preparing the bid request and fixing the data to target it
    */
    testFixture->givenABidRequest ();
//         CampaignDailyCapFilterTests::givenBidRequestHasDailyCampaignCapOf1();

    testFixture->persistTheDataInMySql ();
   std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> expectedBidderResponse (new OpenRtbBidResponse ());
    testFixture->whenRequestIsSentToBidder ();
    MLOG(3) << "actual bidder response is " << testFixture->actualOpenRtbBidResponse->toJson ();
    BidderResponseTestUtil::areEqual (expectedBidderResponse , testFixture->actualOpenRtbBidResponse);

    MLOG(3) << "end of sendABidRequestGetNoBid test!";
}

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef OpenRtbProcessingTest_H
#define OpenRtbProcessingTest_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
class OpportunityContext;
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "BidderTestService.h"

class OpenRtbProcessingTest;

class OpenRtbProcessingTest : public ::testing::Test {
public:

BidderTestServicePtr testService;
std::shared_ptr<BidRequestCreator> bidRequestCreator;
OpenRtbProcessingTest();

void setupTargetGroupAndCreativeForBidRequest();

void givenABidRequest();

void setupGeoSegments();

void givenTgWithImpressionLimit(int limit);

void whenContextIsAskedFor();

void thenContextIsAsExpected();

void whenRequestIsSentToBidder();

void thenBidderResponseIsAsExpected();

virtual ~OpenRtbProcessingTest();
void SetUp();
void TearDown();

};
#endif //OpenRtbProcessingTest_H

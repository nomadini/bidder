/*
 * BidderTester.h
 *
 *  Created on: Aug 8, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_LOADTESTER_BIDDERTESTER_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_LOADTESTER_BIDDERTESTER_H_
#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequest.h"


namespace gicapods { class ConfigService; }
class StringUtil;
#include "HttpUtil.h"

#include "CreativeTypeDefs.h"
class Creative;
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class CassandraDriverInterface;
#include "BidderTestService.h"
class OpportunityContext;
#include "BiddingMode.h"
#include "Bidder.h"
class CampaignCacheService;
class TargetGroupCacheService;
class BidRequestCreator;
#include "EntityRealTimeDeliveryInfo.h"
#include "CampaignRealTimeInfo.h"
#include <memory>
#include <string>
#include <boost/exception/all.hpp>
namespace gicapods { class ConfigService; }
class BidderTester {

public:
gicapods::ConfigService configService;
BidderTester();
void deleteAllDataReadFromMySql();

void sendRandomBidRequest();

std::shared_ptr<OpportunityContext> sendRequestToBidderForContext(std::string transactionId);
std::shared_ptr<OpportunityContext> sendRequestToAdServerForContext(std::string impressionId);
std::string sendRequestToBidder(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest);
std::string sendForceFlushRequestToPacer();
std::shared_ptr<EntityRealTimeDeliveryInfo> sendTgRealtimeFetchRequestToPacer(int targetGroupId);
std::shared_ptr<BidRequestCreator> bidRequestCreator;
std::shared_ptr<CampaignRealTimeInfo> sendCampaignRealtimeFetchRequestToPacer(int campaignId);
void sendRequestToAdServer(std::string url);

void populateDomainList();


};

#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_LOADTESTER_BIDDERTESTER_H_ */

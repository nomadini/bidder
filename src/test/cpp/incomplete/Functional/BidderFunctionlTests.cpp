/*
 * Bidder.cpp
 *
 *  Created on: Sep 7, 2015
 *      Author: mtaabodi
 */

#include "GUtil.h"


#include "CassandraDriverInterface.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "ConfigService.h"
#include "Bidder.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "TestsCommon.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "AdServer.h"
#include "AdServerFunctionalTestsHelper.h"
#include "Pacer.h"
#include "PacerFunctionalTestsHelper.h"
#include "PacingProcessor.h"
#include "TempUtil.h"
#include "HttpUtil.h"
#include <thread>
int runGoogleTests(int argc, char** argv) {
  // The following line must be executed to initialize Google Mock
  // (and Google Test) before running the tests.
    ::testing::InitGoogleMock(&argc, argv);
    ::testing::FLAGS_gmock_verbose ="info";

    return RUN_ALL_TESTS();
}

void runAdServer(){
    std::shared_ptr<AdServer> adserver = std::make_shared<AdServer>("adserver-test.properties");
    AdServerFunctionalTestsHelper::adserver = adserver;
    adserver->run(AdServerFunctionalTestsHelper::argc, AdServerFunctionalTestsHelper::argv);
}

 void runPacer(){
    PacerPtr pacer = std::make_shared<Pacer>("pacer-test11111.properties");
    PacerFunctionalTestsHelper::pacer = pacer;
    pacer->run(PacerFunctionalTestsHelper::argc, PacerFunctionalTestsHelper::argv);
}

 void runBidder(){
    BidderTestsHelper::getBidder()->run(CommonTestsHelper::argc, CommonTestsHelper::argv);
}

 void givenBidderRunning() {

     std::thread everyMinuteThread(runBidder);
     gicapods::Util::sleepViaBoost(_L_, 4);//wait for bidder to start up!!
     everyMinuteThread.detach();
}

void waitUntilServerIsUp(int port) {

    Poco::Net::HTTPResponse::HTTPStatus status;
    do {
        Poco::Net::HTTPResponse response;
        std::string url = "http://localhost:" + StringUtil::toStr (port) + StringUtil::toStr("/heartbeat");
        MLOG(3) << "hitting server to check if its up : "<< url;
        HttpUtil::sendGetRequestAndGetFullResponse (url,
                                                    response ,
                                                    1000);
        status = response.getStatus ();
//        sleep(2000);
    } while (status != Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);
}

void givenAdServerRunning() {
     std::thread everyMinuteThread(runAdServer);
     everyMinuteThread.detach();
//     while(true) //todo : we should wait until adServer is on
    int port = AdServerFunctionalTestsHelper::adserver->getPort();
    waitUntilServerIsUp(port);
}

void givenPacerRunning() {
  std::thread everyMinuteThread(runPacer);
  everyMinuteThread.detach();
    //gicapods::Util::sleepViaBoost(_L_, 4);//todo : wait for pacer to start up!!
    int port = PacerFunctionalTestsHelper::pacer->getPort();
    waitUntilServerIsUp(port);
}


int main(int argc, char** argv) {
    SignalHandler::installHandlers();

    TempUtil::configureLogging("BidderFunctionalTests", argv);
    MLOG(3)<<" argc :  "<<argc<<" , argv : "<<StringUtil::toStr(*argv);

    BidderTestsHelper::init(argc,argv);

    MLOG(3)<<" BidderFunctionalTests::argc :  "<<BidderTestsHelper::argc<<" , "
                    "BidderTestsHelper::argv : "<<StringUtil::toStr(*BidderTestsHelper::argv);

    CassandraDriver::startCassandraCluster();

    givenBidderRunning();
    givenAdServerRunning();
    givenPacerRunning();

    runGoogleTests(argc, argv);
    CassandraDriver::closeSessionAndCluster();
}

#include "BidderTestFixture.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "JsonMapUtil.h"
#include "StringUtil.h"
#include <string>
#include <memory>
#include "MySqlOfferService.h"
#include "DateTimeUtil.h"
#include "TargetGroupTestHelper.h"
#include "CampaignTestHelper.h"
#include "CreativeTestHelper.h"
#include "BidderTestService.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
#include "CacheManagerServiceTestHelper.h"
#include "BidRequestCreator.h"
#include "OpportunityContext.h"
#include "StringUtil.h"
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TestUtil.h"
#include "BidderApplicationContext.h"
#include "MySqlCampaignService.h"
#include "MySqlTargetGroupService.h"
#include "MySqlCreativeService.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"
#include "OpportunityContextTestUtil.h"
#include "BidderResponseTestUtil.h"

#include "CampaignRealTimeInfoTestUtil.h"

#include "BidderTestService.h"
#include "MySqlGeoSegmentListService.h"
#include "MySqlInventoryService.h"
#include "BidderApplicationContext.h"
#include "OpenRtbBidResponseJsonConverter.h"
BidderTestFixture::BidderTestFixture() {

        cacheManagerServiceTestHelper = std::make_shared<CacheManagerServiceTestHelper> (
                beanFactory->targetGroupCacheService,
                beanFactory->campaignCacheService,
                beanFactory->creativeCacheService
                );

        bidRequestCreator = std::make_shared<BidRequestCreator> ();
}

BidderTestFixture::~BidderTestFixture() {

}

void BidderTestFixture::whenRequestIsSentToBidder() {
        auto resp = BidderTester::sendRequestToBidder (this->openRtbBidRequest);

        if (resp.empty ()) {
                MLOG(3) << "response from bidder is empty.";
                throwEx(
                        "test failed because of no response from bidder");
        }

        this->actualOpenRtbBidResponse = OpenRtbBidResponseJsonConverter::fromJson (resp);
        MLOG(3) << "openRtbBidResponse : " << this->actualOpenRtbBidResponse;

}

void BidderTestFixture::tearDown() {
        MLOG(3) << "going to sleep to  let the threads break out of their loops....";
        gicapods::Util::sleepViaBoost (_L_, 5);

}

void BidderTestFixture::setUp() {

        beanFactory->mySqlCampaignService->deleteAll ();

        beanFactory->mySqlTargetGroupService ()->deleteAll;

        beanFactory->mySqlCreativeService ()->deleteAll;

        beanFactory->deviceFeatureHistoryCassandraService ()->deleteAll;
        beanFactory->featureDeviceHistoryCassandraService ()->deleteAll;
}


void BidderTestFixture::givenABidRequest() {

        this->openRtbBidRequest = bidRequestCreator->createOpenRtbBidRequest ();
        std::shared_ptr<Inventory> inv (new Inventory ());
        inv->name = HttpUtil::getDomainFromUrl (this->openRtbBidRequest->site->domain);
        inv->id = RandomUtil::sudoRandomNumber (1000);
        inv->status = "active";
        beanFactory->mySqlInventoryService->addInventory (inv);
}


void BidderTestFixture::setupGeoSegments() {

        std::shared_ptr<GeoSegment> geoSegment (new GeoSegment ());
        geoSegment->lat = 100;
        geoSegment->lon = 100;
        geoSegment->segmentRadiusInMile = 2;
        geoSegment->description = "macDonald's nyc";
        geoSegment->id = RandomUtil::sudoRandomNumber (1000);

        std::shared_ptr<GeoSegmentList> geoSegmentList = std::make_shared<GeoSegmentList> ();
        geoSegmentList->id = RandomUtil::sudoRandomNumber (1000);
        geoSegmentList->geoSegments.push_back (geoSegment);
        beanFactory->mySqlGeoSegmentListService ()->getAllGeoSegmentLists->push_back (geoSegmentList);

        beanFactory->mySqlGeoSegmentListService ()->getAllGeoSegmentListMap->insert (
                std::make_pair (geoSegmentList->id, geoSegmentList));

        beanFactory->targetGroupCacheService ()->getAllTargetGroupGeoSegmentListIdMap->insert (
                std::make_pair (this->targetGroup->getId (), geoSegmentList->id));
}


void BidderTestFixture::persistTheDataInMySql() {
        beanFactory->mySqlCampaignService->insert (this->campaign);
        beanFactory->mySqlTargetGroupService->insert (this->targetGroup);
        beanFactory->mySqlCreativeService->insert (this->creative);
}


void BidderTestFixture::setupTargetGroupAndCreativeForBidRequest() {

        this->targetGroup = TargetGroupTestHelper::createSampleTargetGroup ();
        this->campaign = CampaignTestHelper::createSampleCampaign ();
        this->targetGroup->campaignId = this->campaign->getId ();
        this->creative = CreativeTestHelper::createSampleCreative ();
        this->creative->setSize ("180x150");


        cacheManagerServiceTestHelper->putTgInRelativeMaps (this->targetGroup, this->campaign,
                                                            this->creative);

}

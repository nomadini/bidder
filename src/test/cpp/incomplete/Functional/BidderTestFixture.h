#ifndef BidderTestFixture_H
#define BidderTestFixture_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include <gtest/gtest.h>
#include "TestsCommon.h"
class StringUtil;
class BidRequestCreator;
#include "BidderTestService.h"

#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequest.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "CampaignRealTimeInfo.h"
#include "CacheManagerServiceTestHelper.h"

class BidderTestFixture;

class BidderTestFixture {

public:
BidderTestFixture();
virtual ~BidderTestFixture();
void setUp();
void tearDown();
void givenABidRequest();
void setupGeoSegments();
void persistTheDataInMySql();
void setupTargetGroupAndCreativeForBidRequest();
void whenRequestIsSentToBidder();

CacheManagerServiceTestHelperPtr cacheManagerServiceTestHelper;
std::shared_ptr<TargetGroup> targetGroup;
std::shared_ptr<OpenRtbBidRequest> openRtbBidRequest;
std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> actualOpenRtbBidResponse;
std::shared_ptr<OpportunityContext> context;
std::shared_ptr<Creative> creative;
std::shared_ptr<Campaign> campaign;
std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo;
std::shared_ptr<BidRequestCreator> bidRequestCreator;
std::shared_ptr<CampaignRealTimeInfo> campaignRealTimeInfo;
};

#endif

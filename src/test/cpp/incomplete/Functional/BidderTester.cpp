
#include "BidderTestsHelper.h"
 #include "BeanFactory.h"

#include "OpenRtbBidResponse.h"
#include "OpenRtbBidRequest.h"

#include "GUtil.h"
#include "ConfigService.h"
#include "StringUtil.h"
#include "HttpUtil.h"

#include "Creative.h"
#include "TargetGroup.h"
#include "CassandraDriverInterface.h"
#include "BidderTestService.h"
#include "OpportunityContext.h"
#include "BiddingMode.h"
#include "Bidder.h"
#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include <memory>
#include <string>
#include <boost/exception/all.hpp>
#include "BidderTester.h"
#include "PacerCommandDto.h"
#include "SiteTestHelper.h"
#include "BidRequestCreator.h"


BidderTester::BidderTester() {
        configService = std::make_shared<gicapods::ConfigService>(
                "bidder-test.properties", "common-test.properties");
        bidRequestCreator = std::make_shared<BidRequestCreator>();
}
void BidderTester::deleteAllDataReadFromMySql() {

        beanFactory->targetGroupCacheService()->clearCaches;
        beanFactory->campaignCacheService()->clearCaches;
}

void BidderTester::sendRandomBidRequest() {

        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest =
                bidRequestCreator->createOpenRtbBidRequest();
        bidRequest->site->domain = SiteTestHelper::getRandomDomainFromFiniteSet();
        bidRequest->site->page = SiteTestHelper::getRandomPageFromDomain();
        bidRequest->site->cat.push_back("IAB3-1");

        bidRequest->site->publisher->id = StringUtil::random_string(6);
        bidRequest->site->publisher->domain = SiteTestHelper::getRandomDomainFromFiniteSet();
        bidRequest->site->publisher->cat.push_back("IAB3-1");


        bidRequest->imp->banner->w = 180;
        bidRequest->imp->banner->h = 150;
        bidRequest->deviceOpenRtb->ip = StringUtil::toStr("10.10.10.1");
        bidRequest->deviceOpenRtb->ua = StringUtil::toStr(
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) ");
        bidRequest->deviceOpenRtb->deviceType = 1;
        bidRequest->deviceOpenRtb->geo->lat = 37.23;
        bidRequest->deviceOpenRtb->geo->lon = 121.02;
        bidRequest->deviceOpenRtb->geo->country = StringUtil::toStr("US");
        bidRequest->deviceOpenRtb->geo->city = StringUtil::toStr("NEW YORK");
        bidRequest->deviceOpenRtb->geo->region = StringUtil::toStr("NEW YORK");
        bidRequest->deviceOpenRtb->geo->zip = StringUtil::toStr("11102");
        MLOG(1)<<"hitting bidder with request : "<< bidRequest->toJson();
        auto openRtbBidResponse = BidderTester::sendRequestToBidder(bidRequest);
        BidderTestsHelper::hitAdServer(openRtbBidResponse);

}

std::shared_ptr<OpportunityContext> BidderTester::sendRequestToBidderForContext(std::string transactionId) {

        MLOG(3)<<"sending some random context request";

        std::string url = StringUtil::toStr("__BIDDER_URL__") + StringUtil::toStr("/context");
        url = StringUtil::replaceString(url, StringUtil::toStr("__BIDDER_URL__"),
                                        configService->get("bidderHostUrl"));
        LOG(INFO)<<"sending context request, url "<<url;
        std::string contextResponse = HttpUtil::sendPostRequest(url,
                                                                transactionId);
        auto contextPtr = OpportunityContext::fromJson(contextResponse);
        return contextPtr;
}

std::shared_ptr<OpportunityContext> BidderTester::sendRequestToAdServerForContext(std::string impressionId) {
        MLOG(3)<<"sending some random context request";

        std::string url = StringUtil::toStr("__ADSERVER_URL__") + StringUtil::toStr("/context");
        url = StringUtil::replaceString(url, StringUtil::toStr("__ADSERVER_URL__"),
                                        configService->get("adservHostUrl"));
        LOG(INFO)<<"sending context request, url "<<url;
        std::string contextResponse = HttpUtil::sendPostRequest(url, impressionId);
        auto contextPtr = OpportunityContext::fromJson(contextResponse);
        return contextPtr;


}

std::shared_ptr<EntityRealTimeDeliveryInfo> BidderTester::sendTgRealtimeFetchRequestToPacer(int targetGroupId) {
        MLOG(3)<<"sendTgRealtimeFetchRequestToPacer to pacer , targetGroupId : "<<targetGroupId;
        std::string url = StringUtil::toStr("__PACER_URL__") + StringUtil::toStr("/command-center");
        url = StringUtil::replaceString(url, StringUtil::toStr("__PACER_URL__"),
                                        configService->get("pacingHostUrl"));
        LOG(INFO)<<"sending sendTgRealtimeFetchRequestToPacer request, url "<<url;
        auto commandDto = PacerCommandDto::createCommandDtoFrom("tgRealTimeFetch",targetGroupId);

        std::string resp = HttpUtil::sendPostRequest(url, commandDto->toJson());
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeFetch =
                EntityRealTimeDeliveryInfo::fromJson(resp);
        return tgRealTimeFetch;

}
std::shared_ptr<CampaignRealTimeInfo> BidderTester::sendCampaignRealtimeFetchRequestToPacer(int campaignId) {
        MLOG(3)<<"sendCampaignRealtimeFetchRequestToPacer to pacer, "<<campaignId;
        std::string url = StringUtil::toStr("__PACER_URL__") + StringUtil::toStr("/command-center");
        url = StringUtil::replaceString(url, StringUtil::toStr("__PACER_URL__"),
                                        configService->get("pacingHostUrl"));
        LOG(INFO)<<"sending sendCampaignRealtimeFetchRequestToPacer request, url "<<url;

        auto commandDto = PacerCommandDto::createCommandDtoFrom("campaignRealTimeFetch",campaignId);
        std::string resp = HttpUtil::sendPostRequest(url, commandDto->toJson());

        std::shared_ptr<CampaignRealTimeInfo> cmp =
                CampaignRealTimeInfo::fromJson(resp);
        return cmp;


}


std::string BidderTester::sendForceFlushRequestToPacer() {
        MLOG(3)<<"sending forceFlush to pacer";
        std::string url = StringUtil::toStr("__PACER_URL__") + StringUtil::toStr("/command-center");
        url = StringUtil::replaceString(url, StringUtil::toStr("__PACER_URL__"),
                                        configService->get("pacingHostUrl"));
        LOG(INFO)<<"sending force flush request, url "<<url;
        std::string forceFlushResponse = HttpUtil::sendPostRequest(url, "forceFlushCommand");

        return forceFlushResponse;

}

std::string BidderTester::sendRequestToBidder(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> bidRequest) {

        MLOG(3)<<"sending some random bid request";
        std::string url = StringUtil::toStr("__BIDDER_URL__") + StringUtil::toStr("/bid/openrtb2.3");
        url = StringUtil::replaceString(url, StringUtil::toStr("__BIDDER_URL__"),
                                        configService->get("bidderHostUrl"));
        LOG(INFO)<<"sending bid request, url "<<url;
        std::string openRtbBidResponse = HttpUtil::sendPostRequest(url,
                                                                   bidRequest->toJson());

        return openRtbBidResponse;
}


void BidderTester::sendRequestToAdServer(std::string url) {
        std::string adResponse = HttpUtil::sendPostRequest(url, "");
        MLOG(3)<<"this is the response from adserver : "<<adResponse;
}

void BidderTester::populateDomainList() {
        for (int i = 0; i < 10; i++) {
                SiteTestHelper::getSampleDomains()->push_back(StringUtil::toStr("http://bbcpersian")+ StringUtil::toStr(i)+StringUtil::toStr(".com"));
        }
        SiteTestHelper::getSampleDomains()->push_back(StringUtil::toStr("http://www.goldenPROP100offer01.com"));
        LOG(INFO)<<"SiteTestHelper::getSampleDomains() was populated , size is"<<
                SiteTestHelper::getSampleDomains()->size();
}

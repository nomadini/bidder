//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef OpportunityContextTestUtil_H
#define OpportunityContextTestUtil_H
#include "BidderTestService.h"
#include <gtest/gtest.h>
#include "TestsCommon.h"
class OpportunityContext;
class StringUtil;

class OpportunityContextTestUtil;

class OpportunityContextTestUtil {
    public :
    static void areEqual(std::shared_ptr<OpportunityContext> context1, std::shared_ptr<OpportunityContext> context2);
    static void areEqual(BidderTestServicePtr testService);
};
#endif //OpportunityContextTestUtil_H

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef TargetGroupRealTimeInfoTestUtil_H
#define TargetGroupRealTimeInfoTestUtil_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "EntityRealTimeDeliveryInfo.h"
class StringUtil;

class TargetGroupRealTimeInfoTestUtil;

class TargetGroupRealTimeInfoTestUtil : public ::testing::Test {
    public :
    static bool areEqual(std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeDeliveryInfo1, std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeDeliveryInfo2);

};
#endif //TargetGroupRealTimeInfoTestUtil_H

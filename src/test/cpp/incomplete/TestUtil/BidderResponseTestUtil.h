//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef BidderResponseTestUtil_H
#define BidderResponseTestUtil_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidResponse.h"
class StringUtil;

class BidderResponseTestUtil;

class BidderResponseTestUtil : public ::testing::Test {
public:
static bool areEqual( std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> expectedBidderResponse1, std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> expectedBidderResponse2);

};
#endif //BidderResponseTestUtil_H

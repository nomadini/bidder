

#include "BidderResponseTestUtil.h"

bool BidderResponseTestUtil::areEqual( std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> expectedBidderResponse1,std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> expectedBidderResponse2) {
								EXPECT_THAT(expectedBidderResponse1->id,
																				testing::Eq(expectedBidderResponse2->id));
								EXPECT_THAT(expectedBidderResponse1->bidId,
																				testing::Eq(expectedBidderResponse2->bidId));
								EXPECT_THAT(expectedBidderResponse1->cur,
																				testing::Eq(expectedBidderResponse2->cur));
								EXPECT_THAT(expectedBidderResponse1->seatbid->seat,
																				testing::Eq(expectedBidderResponse2->seatbid->seat));
								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->id,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->id));
								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->impid,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->impid));
								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->price,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->price));
								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->nurl,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->nurl));
								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->iurl,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->iurl));

//      EXPECT_THAT(expectedBidderResponse1->seatbid->bid->adomain),
//                  testing::Eq(expectedBidderResponse2->seatbid->bid->adomain));
//      EXPECT_THAT(expectedBidderResponse1->seatbid->bid->attr),
//                  testing::Eq(expectedBidderResponse2->seatbid->bid->attr));

								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->cid,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->cid));

								EXPECT_THAT(expectedBidderResponse1->seatbid->bid->crid,
																				testing::Eq(expectedBidderResponse2->seatbid->bid->crid));


}

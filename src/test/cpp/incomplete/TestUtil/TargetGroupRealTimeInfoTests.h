//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef TgRealTimeDeliveryInfoTests_H
#define TgRealTimeDeliveryInfoTests_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidResponse.h"
class OpportunityContext;
class StringUtil;
#include "GeoSegment.h"
#include "GeoLocation.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CampaignTypeDefs.h"
#include "CreativeTypeDefs.h"
class Creative;
#include "BidRequestTestHelper.h"
#include "CampaignRealTimeInfo.h"
#include "EntityRealTimeDeliveryInfo.h"

class TgRealTimeDeliveryInfoTests;

class TgRealTimeDeliveryInfoTests : public ::testing::Test {
    public :

//    BidRequestTestHelperPtr testService;
//    TgRealTimeDeliveryInfoTests();

//     virtual ~TgRealTimeDeliveryInfoTests();
////
//     void SetUp();
//
//     void TearDown();
//
};
#endif //TgRealTimeDeliveryInfoTests_H

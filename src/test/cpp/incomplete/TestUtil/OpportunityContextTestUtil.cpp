

#include "OpportunityContextTestUtil.h"
#include "OpportunityContext.h"
#include "CollectionUtil.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "FeatureDeviceHistoryCassandraService.h"

void OpportunityContextTestUtil::areEqual(std::shared_ptr<OpportunityContext> context1, std::shared_ptr<OpportunityContext> context2){


								EXPECT_THAT(context1->transactionId,
																				testing::Eq(context2->transactionId));

								EXPECT_THAT(context1->id,
																				testing::Eq(context2->id));


								EXPECT_THAT(context1->chosenTargetGroupId,
																				testing::Eq(context2->chosenTargetGroupId));
								EXPECT_THAT(context1->chosenCreativeId,
																				testing::Eq(context2->chosenCreativeId));

								EXPECT_THAT(context1->campaignId,
																				testing::Eq(context2->campaignId));
								EXPECT_THAT(context1->advertiserId,
																				testing::Eq(context2->advertiserId));

								EXPECT_THAT(context1->publisherId,
																				testing::Eq(context2->publisherId));
								EXPECT_THAT(context1->chosenSegmentId,
																				testing::Eq(context2->chosenSegmentId));

								EXPECT_THAT(context1->winBidPrice,
																				testing::Eq(context2->winBidPrice));
								EXPECT_THAT(context1->bidPrice,
																				testing::Eq(context2->bidPrice));


								EXPECT_THAT(context1->deviceLat,
																				testing::Eq(context2->deviceLat));
								EXPECT_THAT(context1->deviceLon,
																				testing::Eq(context2->deviceLon));



								EXPECT_THAT(context1->userTimeZone,
																				testing::Eq(context2->userTimeZone));
								EXPECT_THAT(context1->userTimeZonDifferenceWithUTC,
																				testing::Eq(context2->userTimeZonDifferenceWithUTC));

}

void OpportunityContextTestUtil::areEqual(BidderTestServicePtr testService) {
								EXPECT_THAT(testService->context->chosenTargetGroupId,
																				testing::Eq(testService->targetGroup->getId()));

								EXPECT_THAT(testService->context->chosenCreativeId,
																				testing::Eq(testService->creative->getId()));

								EXPECT_THAT(testService->context->campaignId,
																				testing::Eq(testService->campaign->getId()));

								EXPECT_THAT(testService->context->advertiserId,
																				testing::Eq(testService->campaign->getAdvertiserId()));

								EXPECT_THAT(testService->context->bidPrice, testing::Eq(testService->openRtbBidRequest->imp->bidFloor));


								auto device = std::make_shared<Device>(
																testService->openRtbBidRequest->user->buyeruid,
																"");
								//soosan fix these!!!!!!!!!!
//           std::vector <std::shared_ptr<DeviceFeatureHistory>> deviceFeatureHistory = beanFactory->deviceFeatureHistoryCassandraService->readFeatureHistoryOfDevice(device , 300);
//           bool value = CollectionUtil::valueExistInMap<TimeType, std::string>(deviceFeatureHistory.at(0)->timeFeatureMap,
//           testService->openRtbBidRequest->site->domain);
//           EXPECT_THAT(value, testing::Eq(true));
//
//
//           auto featureDeviceHistory = beanFactory->featureDeviceHistoryCassandraService->readDeviceHistoryOfFeature(
//           testService->openRtbBidRequest->site->publisher->domain, (TimeType) 1000, 10);
//
//
//           EXPECT_THAT(featureDeviceHistory->devicehistories.size(), testing::Eq(1));
//           auto deviceHistory = featureDeviceHistory->devicehistories.at(0);
//           EXPECT_THAT(deviceHistory->device->getDeviceId(), testing::Eq(testService->openRtbBidRequest->user->buyeruid));
//           EXPECT_THAT(deviceHistory->getDeviceType(), testing::Eq("DESKTOP"));


}

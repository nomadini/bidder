//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef CampaignRealTimeInfoTestUtil_H
#define CampaignRealTimeInfoTestUtil_H

#include <gtest/gtest.h>
#include "TestsCommon.h"
#include "OpenRtbBidResponse.h"
class StringUtil;
#include "CampaignRealTimeInfo.h"
class CampaignRealTimeInfoTestUtil;

class CampaignRealTimeInfoTestUtil : public ::testing::Test {
    public :
    static bool areEqual(std::shared_ptr<CampaignRealTimeInfo> expectedCampaignRealTimeInfo1,
                         std::shared_ptr<CampaignRealTimeInfo> expectedCampaignRealTimeInfo2);

};
#endif //CampaignRealTimeInfoTestUtil_H

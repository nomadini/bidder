

#include "TargetGroupRealTimeInfoTestUtil.h"

bool TargetGroupRealTimeInfoTestUtil::areEqual(std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeDeliveryInfo1, std::shared_ptr<EntityRealTimeDeliveryInfo> expectedTgRealTimeDeliveryInfo2) {

								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->timeReported,
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->timeReported));
								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->targetGroupId,
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->targetGroupId));

								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->numOfImpressionsServedInCurrentDateUpToNow->getValue(),
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->numOfImpressionsServedInCurrentDateUpToNow->getValue()));

								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->numOfImpressionsServedOverallUpToNow->getValue(),
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->numOfImpressionsServedOverallUpToNow->getValue()));


								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->platformCostSpentInCurrentDateUpToNow->getValue(),
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->platformCostSpentInCurrentDateUpToNow->getValue()));

								EXPECT_THAT(expectedTgRealTimeDeliveryInfo1->platformCostSpentOverallUpToNow->getValue(),
																				testing::Eq(expectedTgRealTimeDeliveryInfo2->platformCostSpentOverallUpToNow->getValue()));

}

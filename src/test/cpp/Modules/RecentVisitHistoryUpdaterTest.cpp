#include "RecentVisitHistoryUpdaterTest.h"
#include "RecentVisitHistoryUpdater.h"
#include "OpportunityContext.h"
#include "BeanFactory.h"
#include "ModuleContainer.h"
#include "TargetGroupCacheService.h"
#include "RecentVisitHistoryScoreCard.h"
#include "TargetGroup.h"
#include "RecencyModel.h"
#include "TgScoreEligibilityRecord.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "DeviceRecentVisitHistory.h"
#include "RecencyModel.h"
#include "RecentVisitHistory.h"
#include "RecentVisitHistory.h"
#include "RecentVisitHistoryScorer.h"
#include "TargetGroupTestHelper.h"
#include "RecentVisitHistoryScoreReader.h"

RecentVisitHistoryUpdaterTest::RecentVisitHistoryUpdaterTest() {

        recentVisitHistoryScorer = moduleContainer->recentVisitHistoryScorer.get();
        recentVisitHistoryUpdater = moduleContainer->recentVisitHistoryUpdater.get();
        recentVisitHistoryScoreReader = moduleContainer->recentVisitHistoryScoreReader.get();
        targetGroupUsedInTest = TargetGroupTestHelper::createSampleTargetGroup();
        beanFactory->targetGroupCacheService->addTargetGroupToCache
                (targetGroupUsedInTest);

        modelUsedInTest = RecencyModel::fromJson(
                "{\"id\":11, \"scoreBase\":12.0243, \"featureScores\": [{\"name\" : \"feature_1.com\" , \"score\": 2.32}, {\"name\" : \"feature_2.com\", \"score\" : 2.43}]}"
                );

}

RecentVisitHistoryUpdaterTest::~RecentVisitHistoryUpdaterTest() {

}

void RecentVisitHistoryUpdaterTest::SetUp() {

}

void RecentVisitHistoryUpdaterTest::TearDown() {

}

TEST_F(RecentVisitHistoryUpdaterTest, testWritingVisitHistoryForADifferentDevice) {
        context->siteDomain = "testDomain1.com";
        beanFactory->targetGroupRecencyModelMapCacheService->
        addRecencyModelToCache(targetGroupUsedInTest->getId(), modelUsedInTest);
        recentVisitHistoryUpdater->process(context);

        std::shared_ptr<DeviceRecentVisitHistory> newAdHistory = recentVisitHistoryScoreReader->readRecentVisitHistory(context->device);
        std::shared_ptr<DeviceRecentVisitHistory> expectedAdHistory = std::make_shared<DeviceRecentVisitHistory>(context->device);

        LOG(INFO)<<"newAdHistory is "<<newAdHistory->toJson();
        LOG(INFO)<<"expectedAdHistory is "<<expectedAdHistory->toJson();

        EXPECT_THAT(newAdHistory->toJson(), testing::Eq(expectedAdHistory->toJson()));
}

TEST_F(RecentVisitHistoryUpdaterTest, testWritingVisitHistoryForTheSameDevice) {
        std::vector<std::string> domains = {"feature_1.com", "feature_2.com", "test3.com"};
        //getting a single device id that we process the module for 3 times on it
        context->device =  std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP");

        for(int i = 0; i < 3; i++) {
                context->siteDomain = domains.at(i);
                recentVisitHistoryUpdater->process(context);
                gicapods::Util::sleepViaBoost(_L_, 1);
        }

        std::shared_ptr<DeviceRecentVisitHistory> newAdHistory = recentVisitHistoryScoreReader->readRecentVisitHistory(context->device);

        std::shared_ptr<DeviceRecentVisitHistory> expectedAdHistory = std::make_shared<DeviceRecentVisitHistory>(context->device);

        LOG(INFO)<<"newAdHistory is "<<newAdHistory->toJson();
        LOG(INFO)<<"expectedAdHistory is "<<expectedAdHistory->toJson();
        EXPECT_THAT(newAdHistory->toJson(), testing::Eq(expectedAdHistory->toJson()));


}

#include "RecentVisitHistoryScorerTest.h"
#include "RecentVisitHistoryUpdater.h"
#include "OpportunityContext.h"
#include "BeanFactory.h"
#include "ModuleContainer.h"
#include "TargetGroupCacheService.h"
#include "RecentVisitHistoryScoreCard.h"
#include "TargetGroup.h"
#include "RecencyModel.h"
#include "TgScoreEligibilityRecord.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "DeviceRecentVisitHistory.h"
#include "RecencyModel.h"
#include "RecentVisitHistory.h"
#include "RecentVisitHistory.h"
#include "RecentVisitHistoryScorer.h"
#include "TargetGroupTestHelper.h"
#include "RecentVisitHistoryScoreReader.h"

RecentVisitHistoryScorerTest::RecentVisitHistoryScorerTest() {


        recentVisitHistoryScorer = moduleContainer->recentVisitHistoryScorer.get();

        recentVisitHistoryUpdater = moduleContainer->recentVisitHistoryUpdater.get();
        recentVisitHistoryScoreReader = moduleContainer->recentVisitHistoryScoreReader.get();
        targetGroupUsedInTest = TargetGroupTestHelper::createSampleTargetGroup();
        beanFactory->targetGroupCacheService->addTargetGroupToCache
                (targetGroupUsedInTest);
        modelUsedInTest = RecencyModel::fromJson(
                "{\"id\":11, \"scoreBase\":12.0243, \"featureScores\": [{\"name\" : \"feature_1.com\" , \"score\": 2.32}, {\"name\" : \"feature_2.com\", \"score\" : 2.43}]}"
                );

}

RecentVisitHistoryScorerTest::~RecentVisitHistoryScorerTest() {

}

void RecentVisitHistoryScorerTest::SetUp() {

}

void RecentVisitHistoryScorerTest::TearDown() {

}


TEST_F(RecentVisitHistoryScorerTest, testWritingVisitHistoryAndScoringAsNotQualified) {
        beanFactory->targetGroupRecencyModelMapCacheService->
        addRecencyModelToCache(targetGroupUsedInTest->getId(), modelUsedInTest);
        std::vector<std::string> domains = {"feature_1.com", "feature_1.com", "test3.com"};
        //getting a single device id that we process the module for 3 times on it
        context->device =  std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP");

        for(int i = 0; i < 3; i++) {
                context->siteDomain = domains.at(i);
                recentVisitHistoryUpdater->process(context);
                //we sleep here to give time to aerospike to record all histories
                gicapods::Util::sleepViaBoost(_L_, 1);
        }

        std::shared_ptr<DeviceRecentVisitHistory> newAdHistory = recentVisitHistoryScoreReader->readRecentVisitHistory(context->device);

        LOG(INFO)<<"newAdHistory is "<<newAdHistory->toJson();

        recentVisitHistoryScorer->scoreAllTgsForDevice(context);

        std::shared_ptr<RecentVisitHistoryScoreCard> newScoreCard =
                recentVisitHistoryScoreReader->readTheScoreCardForDevice(context->device);

        LOG(INFO)<<"newScoreCard is "<<newScoreCard->toJson();
        LOG(INFO)<<"targetGroupUsedInTest->getId() :  "<<targetGroupUsedInTest->getId();

        auto pairPtr1 = newScoreCard->targetGroupIdToQualification->find(targetGroupUsedInTest->getId());
        assertAndThrow(pairPtr1 != newScoreCard->targetGroupIdToQualification->end());
        std::shared_ptr<TgScoreEligibilityRecord> tgScoreEligibilityRecord = pairPtr1->second;

        auto pairPtr2 = tgScoreEligibilityRecord->modelIdToEligibilityRecords->
                        find(modelUsedInTest->id);

        assertAndThrow(pairPtr2 != tgScoreEligibilityRecord->modelIdToEligibilityRecords->end());
        auto newRecencyModel = pairPtr2->second;


        EXPECT_THAT(newRecencyModel->targetGroupId, testing::Eq(targetGroupUsedInTest->getId()));
        EXPECT_THAT(newRecencyModel->id, testing::Eq(modelUsedInTest->id));
        EXPECT_THAT(newRecencyModel->score, testing::Eq(4.64));
        EXPECT_THAT(newRecencyModel->eligibile, testing::Eq("no"));
        EXPECT_THAT(newRecencyModel->scoreDiffComparedToScoreBase, testing::Eq(-7.3843));
        EXPECT_THAT(tgScoreEligibilityRecord->targetGroupId, testing::Eq(targetGroupUsedInTest->getId()));
}

TEST_F(RecentVisitHistoryScorerTest, testWritingVisitHistoryAndScoringAsQualified) {

        modelUsedInTest = RecencyModel::fromJson(
                "{\"id\":11, \"scoreBase\":1.000, \"featureScores\": [{\"name\" : \"feature_1.com\" , \"score\": 2.32}, {\"name\" : \"feature_2.com\", \"score\" : 2.43}]}"
                );
        beanFactory->targetGroupRecencyModelMapCacheService->getAllEntitiesMap()->clear();

        beanFactory->targetGroupRecencyModelMapCacheService->
        addRecencyModelToCache(targetGroupUsedInTest->getId(), modelUsedInTest);

        std::vector<std::string> domains = {"feature_1.com", "feature_2.com", "test3.com"};
        //getting a single device id that we process the module for 3 times on it
        context->device =  std::make_shared<Device>(Device::createStandardDeviceId(), "DESKTOP");

        for(int i = 0; i < 2; i++) {
                context->siteDomain = domains.at(i);
                recentVisitHistoryUpdater->process(context);
                //we sleep here to give time to aerospike to record all histories
                gicapods::Util::sleepViaBoost(_L_, 1);
        }

        std::shared_ptr<DeviceRecentVisitHistory> newAdHistory = recentVisitHistoryScoreReader->readRecentVisitHistory(context->device);

        LOG(INFO)<<"newAdHistory is "<<newAdHistory->toJson();

        recentVisitHistoryScorer->scoreAllTgsForDevice(context);

        std::shared_ptr<RecentVisitHistoryScoreCard> newScoreCard =
                recentVisitHistoryScoreReader->readTheScoreCardForDevice(context->device);

        LOG(INFO)<<"newScoreCard is "<<newScoreCard->toJson();
        LOG(INFO)<<"targetGroupUsedInTest->getId() :  "<<targetGroupUsedInTest->getId();

        auto pairPtr1 = newScoreCard->targetGroupIdToQualification->find(targetGroupUsedInTest->getId());
        assertAndThrow(pairPtr1 != newScoreCard->targetGroupIdToQualification->end());
        std::shared_ptr<TgScoreEligibilityRecord> tgScoreEligibilityRecord = pairPtr1->second;

        auto pairPtr2 = tgScoreEligibilityRecord->modelIdToEligibilityRecords->
                        find(modelUsedInTest->id);

        assertAndThrow(pairPtr2 != tgScoreEligibilityRecord->modelIdToEligibilityRecords->end());
        auto newRecencyModel = pairPtr2->second;

        EXPECT_THAT(newRecencyModel->targetGroupId, testing::Eq(targetGroupUsedInTest->getId()));
        EXPECT_THAT(newRecencyModel->id, testing::Eq(modelUsedInTest->id));
        EXPECT_THAT(newRecencyModel->score, testing::Eq(4.75));
        EXPECT_THAT(newRecencyModel->eligibile, testing::Eq("yes"));
        EXPECT_THAT(newRecencyModel->scoreDiffComparedToScoreBase, testing::Eq(3.75));
        EXPECT_THAT(tgScoreEligibilityRecord->targetGroupId, testing::Eq(targetGroupUsedInTest->getId()));
}

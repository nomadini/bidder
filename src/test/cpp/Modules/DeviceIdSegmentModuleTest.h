//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef DeviceIdSegmentModuleTest_H
#define DeviceIdSegmentModuleTest_H

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include "TestsCommon.h"
#include "BidderTestBase.h"
class BeanFactory;
class DeviceIdSegmentModule;
class OpportunityContext;
class RecencyModel;
class RecentVisitHistoryScoreReader;
class TargetGroup;
class ModuleContainer;
class RecentVisitHistoryScorer;


class DeviceIdSegmentModuleTest : public BidderTestBase {
public:

DeviceIdSegmentModuleTest();

std::shared_ptr<TargetGroup> targetGroupUsedInTest;
std::shared_ptr<RecencyModel> modelUsedInTest;
DeviceIdSegmentModule* deviceIdSegmentModule;
RecentVisitHistoryScoreReader* recentVisitHistoryScoreReader;
RecentVisitHistoryScorer* recentVisitHistoryScorer;

virtual ~DeviceIdSegmentModuleTest();

void SetUp();

void TearDown();

};
#endif //DeviceIdSegmentModuleTest_H

//
// Created by Mahmoud Taabodi on 11/27/15.
//

#ifndef RecentVisitHistoryScorerTest_H
#define RecentVisitHistoryScorerTest_H

#include <gtest/gtest.h>
#include <memory>
#include <string>
#include "TestsCommon.h"
#include "BidderTestBase.h"
class BeanFactory;
class RecentVisitHistoryUpdater;
class OpportunityContext;
class RecencyModel;
class RecentVisitHistoryScoreReader;
class TargetGroup;
class ModuleContainer;
class RecentVisitHistoryScorer;


class RecentVisitHistoryScorerTest : public BidderTestBase {
public:

RecentVisitHistoryScorerTest();

std::shared_ptr<TargetGroup> targetGroupUsedInTest;
std::shared_ptr<RecencyModel> modelUsedInTest;
RecentVisitHistoryUpdater* recentVisitHistoryUpdater;
RecentVisitHistoryScoreReader* recentVisitHistoryScoreReader;
RecentVisitHistoryScorer* recentVisitHistoryScorer;

virtual ~RecentVisitHistoryScorerTest();

void SetUp();

void TearDown();

};
#endif //RecentVisitHistoryScorerTest_H

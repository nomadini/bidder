#ifndef TargetGroupFrequencyCapModule_H
#define TargetGroupFrequencyCapModule_H


#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "Object.h"
#include "CollectionUtil.h"
class OpportunityContext;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"

template<typename Type>
class RealTimeEntityCacheService;


class TargetGroupFrequencyCapModule : public BidderModule, public Object {

private:
RealTimeEntityCacheService<AdHistory>* adHistoryCassandraService;
public:

virtual std::string getName();

TargetGroupFrequencyCapModule(RealTimeEntityCacheService<AdHistory>* adHistoryCassandraService,
                              EntityToModuleStateStats* entityToModuleStateStats,
                              std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

void filterTargetGroupsBasedOnFrequencyCap(std::shared_ptr<OpportunityContext> context);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
virtual ~TargetGroupFrequencyCapModule();
};



#endif

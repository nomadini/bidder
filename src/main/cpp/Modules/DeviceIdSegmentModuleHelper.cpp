#include "DeviceIdSegmentModuleHelper.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"

#include "GUtil.h"
#include <boost/foreach.hpp>
#include "DeviceSegmentHistoryCassandraService.h"
#include "TargetGroupSegmentCacheService.h"
#include "TargetGroupCacheService.h"
#include "SegmentCacheService.h"
#include "ConverterUtil.h"
#include "JsonArrayUtil.h"
#include "TargetGroupWithoutSegmentFilterModule.h"
#include "EntityToModuleStateStats.h"
#include "DeviceSegmentHistory.h"
#include "ConcurrentHashMap.h"

DeviceIdSegmentModuleHelper::DeviceIdSegmentModuleHelper(
        TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
        SegmentCacheService* segmentCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->targetGroupSegmentCacheService = targetGroupSegmentCacheService;
        this->segmentCacheService = segmentCacheService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

DeviceIdSegmentModuleHelper::~DeviceIdSegmentModuleHelper() {

}

void DeviceIdSegmentModuleHelper::recordTgHavingSpecificSegments(
        std::string& uniqueNameOfSegment,
        std::vector<std::shared_ptr<TargetGroup> >& targetGroupsHavingSegments) {
        if (!targetGroupsHavingSegments.empty ()) {

//                                if (ONLY_RUN_IN_TEST) { //soosan , fix the only run in test
                for (std::shared_ptr<TargetGroup> tg :  targetGroupsHavingSegments) {
                        entityToModuleStateStats->addStateModuleForEntity (
                                "SOME_TG_MATCH_DEVICE_SEGMENT",
                                "DeviceIdSegmentModuleHelper",
                                uniqueNameOfSegment + StringUtil::toStr ("-tg-") +
                                StringUtil::toStr (tg->getId ()));
                }
//  }

        }
}

void DeviceIdSegmentModuleHelper::recordDeviceHavingSegments(
        std::shared_ptr<OpportunityContext> context,
        std::vector<std::string>& uniqueNameOfSegmentForDevice) {
        if (!uniqueNameOfSegmentForDevice.empty ()) {
                entityToModuleStateStats->addStateModuleForEntity ("FOUND_DEVICE_SEGMENT_FOR_USER",
                                                                   "DeviceIdSegmentModuleHelper",
                                                                   EntityToModuleStateStats::all,
                                                                   EntityToModuleStateStats::important);
                MLOG(10) << "read these deviceSegments for nomadiniDeviceId "
                         << context->device->getDeviceId() << " , deviceType"
                         << context->device->getDeviceType() << " ,segments : "
                         << JsonArrayUtil::convertListToJson (uniqueNameOfSegmentForDevice);

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("NO_DEVICE_SEGMENT_FOUND_FOR_USER",
                                                                   "DeviceIdSegmentModuleHelper",
                                                                   EntityToModuleStateStats::all,
                                                                   EntityToModuleStateStats::important);
        }

}

void DeviceIdSegmentModuleHelper::recordTgNotHavingSegments(std::shared_ptr<TargetGroup>& tg) {

        bool tgHasSegmentsAssigned = targetGroupSegmentCacheService->
                                     getAllTargetGroupSegmentListMap ()
                                     ->exists(tg->getId ());

        if (tgHasSegmentsAssigned) {
                MLOG (2) << " tg " << tg->getId () << " doesn't have the right uniqueNameOfSegment for the user ";
                entityToModuleStateStats->addStateModuleForEntity (
                        "FAIL_TG_HAS_NOT_RIGHT_DEVICE_SEGMENT",
                        "DeviceIdSegmentModuleHelper",
                        StringUtil::toStr ("tg") +
                        StringUtil::toStr (tg->getId ()));
        }
}

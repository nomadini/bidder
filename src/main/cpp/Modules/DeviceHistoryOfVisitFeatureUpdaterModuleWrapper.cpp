#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "OpportunityContext.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "FeatureDeviceHistory.h"
DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureCategory = featureCategory;
}

std::string DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::getName() {
        return "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper";
}



void DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device == nullptr || context->siteDomain.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                                                                   "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                                                                   "ALL");

                throwEx("unknown siteDomain or device");
        }

        featureDeviceHistoryUpdaterModule->process(featureCategory,
                                                   context->siteDomain,
                                                   context->device);
        entityToModuleStateStats->addStateModuleForEntity ("adding_gtld_feature_device_history",
                                                           "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                                                           "ALL");

        if (context->device == nullptr || context->siteCategory.empty()) {
                throwEx("unknown siteCategory or deviceType");
        }

        entityToModuleStateStats->addStateModuleForEntity ("adding_iabcat_feature_device_history",
                                                           "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper",
                                                           "ALL");

        //we save iabcat to device here which is used by clustering app
        for (auto cat : context->siteCategory) {
                featureDeviceHistoryUpdaterModule->process(Feature::IAB_CAT,
                                                           cat,
                                                           context->device);
        }

}


DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::~DeviceHistoryOfVisitFeatureUpdaterModuleWrapper() {

}
bool DeviceHistoryOfVisitFeatureUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

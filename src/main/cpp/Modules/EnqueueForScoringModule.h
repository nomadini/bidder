#ifndef EnqueueForScoringModule_h
#define EnqueueForScoringModule_h


#include "Status.h"

#include "KafkaProducer.h"
#include "Sampler.h"
#include "TransAppMessage.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <string>
class OpportunityContext;
#include "BidderModule.h"
#include "Poco/NotificationQueue.h"
#include "AtomicLong.h"
#include "BidderApplicationContext.h"
#include <tbb/concurrent_queue.h>

class HttpUtilService;
class KafkaProxyPopulator;
#include "BiddingMonitor.h"
#include "LoadPercentageBasedSampler.h"
#include "Object.h"
class EnqueueForScoringModule :  public BidderModule, public Object {


public:
KafkaProxyPopulator* kafkaProxyPopulator;
std::unique_ptr<LoadPercentageBasedSampler> sampler;
std::string kafkaEnqueueProcessIntervalInSeconds;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingMessagesInKafka;

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<TransAppMessage> > > transAppQueue;
std::shared_ptr<gicapods::AtomicLong> sizeOfKafkaScoringQueue;
std::shared_ptr<gicapods::AtomicBoolean> enqueueForScoringModuleIsDisabled;


EnqueueForScoringModule(EntityToModuleStateStats* entityToModuleStateStats,
                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                        KafkaProxyPopulator* kafkaProxyPopulator);

virtual std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

static std::shared_ptr<TransAppMessage> constructTransAppMessage(std::shared_ptr<OpportunityContext> context);

void sendOneMessageQueue();

void processManyMessages();

virtual ~EnqueueForScoringModule();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

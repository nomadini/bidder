#include "GlobalWhiteListModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "GlobalWhiteListedCacheService.h"
#include <boost/foreach.hpp>
#include "CollectionUtil.h"

GlobalWhiteListModule::GlobalWhiteListModule(GlobalWhiteListedCacheService* globalWhiteListedCacheService,
                                             EntityToModuleStateStats* entityToModuleStateStats,
                                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->globalWhiteListedCacheService = globalWhiteListedCacheService;
}

GlobalWhiteListModule::~GlobalWhiteListModule() {

}

void GlobalWhiteListModule::process(std::shared_ptr<OpportunityContext> context) {

        auto map =
                globalWhiteListedCacheService->getAllWhiteListedDomains();

        if (map->size() <= 100) {
                LOG_EVERY_N(ERROR, 100)<< google::COUNTER
                                       << "th, loaded whiteListedDomains are too small,"
                                       <<map->size();
        }

        auto found = map->exists(StringUtil::toLowerCase (context->siteDomain));
        if (found) {
                //these states are too many!!!
                // entityToModuleStateStats->addStateModuleForEntity ("PASSED:"+ StringUtil::toLowerCase (context->siteDomain),
                //                                                    "GlobalWhiteListModule",
                //                                                    "ALL");
                context->status->value = CONTINUE_PROCESSING;
        } else {
                // entityToModuleStateStats->addStateModuleForEntity ("FAILED:"+  StringUtil::toLowerCase (context->siteDomain),
                //                                                    "GlobalWhiteListModule",
                //                                                    "ALL");
                context->status->value = STOP_PROCESSING;
        }
}

std::string GlobalWhiteListModule::getName() {
        return "GlobalWhiteListModule";
}

bool GlobalWhiteListModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

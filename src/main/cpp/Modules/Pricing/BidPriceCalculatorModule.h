#ifndef BidPriceCalculatorModule_H
#define BidPriceCalculatorModule_H


#include "Status.h"
#include <memory>
#include <string>
class OpportunityContext;
#include "BidderModule.h"
#include "Object.h"
class BidPriceCalculatorModule : public BidderModule, public Object {

private:

public:

BidPriceCalculatorModule(EntityToModuleStateStats* entityToModuleStateStats,
                         std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

virtual ~BidPriceCalculatorModule();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};



#endif

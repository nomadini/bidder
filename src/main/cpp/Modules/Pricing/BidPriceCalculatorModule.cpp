
#include "BidPriceCalculatorModule.h"
#include "OpportunityContext.h"
#include "NumberUtil.h"
BidPriceCalculatorModule::BidPriceCalculatorModule(EntityToModuleStateStats* entityToModuleStateStats,
                                                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string BidPriceCalculatorModule::getName() {
        return "BidPriceCalculatorModule";
}

void BidPriceCalculatorModule::process(std::shared_ptr<OpportunityContext> context) {
        //improve the logic based on different strategies later
        context->bidPrice = NumberUtil::roundDouble(context->bidFloor * 1.1, 4);

}
bool BidPriceCalculatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}
BidPriceCalculatorModule::~BidPriceCalculatorModule() {

}

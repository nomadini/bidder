#ifndef VisitFeatureHistoryOfDeviceUpdaterModuleWrapper_H
#define VisitFeatureHistoryOfDeviceUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
class DeviceFeatureHistory;
class Feature;
#include "DeviceHistoryUpdaterModule.h"
#include "Object.h"
class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper : public BidderModule, public Object {

private:

public:
DeviceHistoryUpdaterModule* visitFeatureDeviceHistoryUpdaterModule;
VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~VisitFeatureHistoryOfDeviceUpdaterModuleWrapper();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

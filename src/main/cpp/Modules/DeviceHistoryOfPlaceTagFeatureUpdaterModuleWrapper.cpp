#include "DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "StringUtil.h"
#include "OpportunityContext.h"

DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper::DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureCategory = featureCategory;
}

std::string DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper::getName() {
        return "DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper ";
}



void DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {

        if (context->device == nullptr) {
                throwEx("unknown device");
        }
        if(context->tagIds.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_TAG",
                                                                   "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                                   "ALL");
                return;
        }

        for(auto tagId : context->tagIds) {
                featureDeviceHistoryUpdaterModule->process(featureCategory,
                                                           StringUtil::toStr(tagId),
                                                           context->device );

        }

        entityToModuleStateStats->addStateModuleForEntity ("ADDING_FEATURE_FEATURE_HISTORY ",
                                                           "DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper ",
                                                           "ALL ");

}

bool DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}
DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper::~DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper() {

}

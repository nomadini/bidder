#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupPacingByBudgetModule.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "DateTimeUtil.h"

#include "DateTimeService.h"
#include "ConcurrentHashMap.h"

std::string TargetGroupPacingByBudgetModule::getName() {
        return "TargetGroupPacingByBudgetModule";
}


TargetGroupPacingByBudgetModule::TargetGroupPacingByBudgetModule(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        DateTimeService* dateTimeService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        NULL_CHECK(this->entityDeliveryInfoCacheService);
        this->dateTimeService = dateTimeService;
}


std::shared_ptr<EntityRealTimeDeliveryInfo> TargetGroupPacingByBudgetModule::getTgRealTimeDeliveryInfo(std::shared_ptr<TargetGroup> tg) {
        return entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ()->getOptional(tg->getId());
}

/**
 * it will filter out target groups that have spent more than
 * their hourly budget
 */
bool TargetGroupPacingByBudgetModule::paceByDailyBudget(std::shared_ptr<TargetGroup> tg,
                                                        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr,
                                                        int percentageLimitByHour) {
        // MLOG(10) << "maxHourIndex : "<< maxHourIndex;
        auto limitByNow = percentageLimitByHour;
        auto percentOfDailyBudget = tg->getDailyMaxBudget() / 100;
        long budgetLimitByHour = (percentOfDailyBudget) * limitByNow;

        MLOG(10) <<
                "going to process pacing for this tg :  "
                 << tg->toJson () << " based on this realtime info :  " << tgRealTimePtr->toJson ()
                 << " percentOfDailyBudget :  "
                 << percentOfDailyBudget << " , limitByNow :" << limitByNow;

        if (tgRealTimePtr->platformCostSpentInCurrentDateUpToNow->getValue() < budgetLimitByHour) {
                MLOG(10) << " tg is still under limit : id :   " << tg->getId()
                         << " budgetLimitByHour :  " << budgetLimitByHour <<
                        " platformCostSpentInCurrentDateUpToNow : " << tgRealTimePtr->platformCostSpentInCurrentDateUpToNow->getValue();
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_MONEY_TO_SPEND",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return true;
        } else {
                MLOG(10) << "pacing this tg  " << tg->getId() << " ,"
                        "budgetLimitByHour :  " << budgetLimitByHour << ""
                        " platformCostSpentInCurrentDateUpToNow : " << tgRealTimePtr->platformCostSpentInCurrentDateUpToNow->getValue();
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_MONEY_TO_SPEND",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        }
        return false;
}
//PacingPlan is based on
int TargetGroupPacingByBudgetModule::getLimitByHour(std::shared_ptr<TargetGroup> tg,
                                                    DateTimeService* dateTimeService,
                                                    EntityToModuleStateStats* entityToModuleStateStats,
                                                    std::string nameOfModule) {
        NULL_CHECK(tg);
        MLOG(10)<<"tg->getPacingPlan() : " << tg->getPacingPlan()->toJson();
        int timeOffSet = dateTimeService->getUtcOffsetBasedOnTimeZone(tg->getPacingPlan()->timeZone);

        MLOG(10)<<"timeOffSet  : " << timeOffSet;

        int hourWanted = DateTimeUtil::getCurrentHourAsIntegerInUTC () + timeOffSet;
        MLOG(10)<<"hourWanted before change  : " << hourWanted <<
                "dateTimeService->getCurrentHourAsIntegerInUTC () : "<< dateTimeService->getCurrentHourAsIntegerInUTC ();
        if(hourWanted < 0) {
                hourWanted = 24 + hourWanted;
        } else if (hourWanted > 24) {
                hourWanted = hourWanted -24;
        }

        MLOG(10)<<" tg->getPacingPlan()->timeZone : " << tg->getPacingPlan()->timeZone
                <<" , timeOffSet : " << timeOffSet<< " hourWanted : "<< hourWanted;

        int index = 0;
        if (tg->getPacingPlan() == NULL) {
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION:NULL_PACING_PLAN",
                                                                   nameOfModule,
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                throwEx("tg with id  doesn't have a pacing plan " + tg->idAsString);
        }
        auto pairPtr = tg->getPacingPlan()->hourToCumulativePercentageLimit.find(hourWanted);
        if(pairPtr == tg->getPacingPlan()->hourToCumulativePercentageLimit.end()) {
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION:HOUR_WANTED_NOT_FOUND:HOUR:" + StringUtil::toStr(hourWanted),
                                                                   nameOfModule,
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                throwEx("wrong pacing plan, hourWanted was not found in plan, hour wanted :  " +
                        StringUtil::toStr(hourWanted));

        }
        auto limitByNow= pairPtr->second;
        MLOG(10) << " limit selected is " << limitByNow;

        return limitByNow;
}

bool TargetGroupPacingByBudgetModule::paceTargetGroup(std::shared_ptr<TargetGroup> tg) {
        int maxHourIndex = getLimitByHour (tg, dateTimeService, entityToModuleStateStats, "TargetGroupPacingByBudgetModule");
        //we should follow this plan now
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr = getTgRealTimeDeliveryInfo (tg);
        if (tg->getPacingPlan() == NULL) {
                MLOG(2) << "tg with id  " << tg->getId() << " doesnt have a pacing plan ";
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_PACING_PLAN",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return false;
        }

        if (tgRealTimePtr == NULL) {
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_REAL_TIME_INFO",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return false;
        }

        if (paceByDailyBudget (tg, tgRealTimePtr, maxHourIndex)) {
                return true;//we are still allowed to spend
        }
        return false;
}

void TargetGroupPacingByBudgetModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupPacingByBudgetModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_IMPRESSION) ||
            paceTargetGroup (tg)) {
                entityToModuleStateStats->addStateModuleForEntity ("TG_PASSING_BUDGET_PACING",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        } else {

                entityToModuleStateStats->addStateModuleForEntity ("TG_FAILING_BUDGET_PACING",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        }

        return filterTg;
}

#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupPacingByImpressionModule.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "TargetGroupPacingByBudgetModule.h"
#include "ConcurrentHashMap.h"
std::string TargetGroupPacingByImpressionModule::getName() {
        return "TargetGroupPacingByImpressionModule";
}


TargetGroupPacingByImpressionModule::TargetGroupPacingByImpressionModule(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        DateTimeService* dateTimeService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        this->dateTimeService = dateTimeService;
}

/**
 * this will filter out the target groups that have shown impressions
 * more than their hourly limit
 */
bool TargetGroupPacingByImpressionModule::paceByDailyImpression(std::shared_ptr<TargetGroup> tg,
                                                                std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr,
                                                                int limitOfHourInPercentage) {
        //lets say the daily max impression is 10000
        //we want to pace it. we want to show have shown 10% by 9am
        //20% by 12am , 50% by 5am , 100% by 10pm
        // so we will have a json array that
        //tells us how many percentage we should have used by an hour
        //the json will look like this
        //[ {"8":0} , {"12":30 }, {"16":50},{"20":100}]
        //this means that from hour 8.00 to 20.59th of the day
        //we should spend the daily impression count

        long impLimitByHour = (tg->getDailyMaxImpression() / 100)
                              * limitOfHourInPercentage;

        MLOG(10) << "tg->getDailyMaxImpression() : " << tg->getDailyMaxImpression()
                 << "tgRealTimePtr->numOfImpressionsServedInCurrentDateUpToNow :  " <<
                tgRealTimePtr->numOfImpressionsServedInCurrentDateUpToNow->getValue()
                 << "impLimitByHour : " << impLimitByHour;

        if (tgRealTimePtr->numOfImpressionsServedInCurrentDateUpToNow->getValue() <= impLimitByHour) {
                MLOG(10) << "we still have impression to buy according to plan";
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_IMPRESSION_TO_SPEND",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return true;
        } else {
                MLOG(10) << "we can't buy impression anymore according to plan";
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_IMPRESSION_TO_SPEND",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        }

        return false;
}

std::shared_ptr<EntityRealTimeDeliveryInfo> TargetGroupPacingByImpressionModule::getTgRealTimeDeliveryInfo(
        std::shared_ptr<TargetGroup> tg) {
        return entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ()->getOptional(tg->getId());
}


bool TargetGroupPacingByImpressionModule::paceTargetGroup(std::shared_ptr<TargetGroup> tg) {
        int hourlyLimit = TargetGroupPacingByBudgetModule::getLimitByHour (tg, dateTimeService, entityToModuleStateStats, "TargetGroupPacingByImpressionModule");
        //we should follow this plan now
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr = getTgRealTimeDeliveryInfo (
                tg);
        if (tg->getPacingPlan() == NULL) {
                MLOG(10) << "tg with id " << tg->getId() << " doesnt have a pacing plan ";
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_PACING_PLAN",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return false;
        }

        if (tgRealTimePtr == NULL) {
                entityToModuleStateStats->addStateModuleForEntity ("TG_HAS_NO_REAL_TIME_INFO",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                return false;
        }

        return paceByDailyImpression (tg, tgRealTimePtr, hourlyLimit);

}

void TargetGroupPacingByImpressionModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupPacingByImpressionModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                                            std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_BUDGET) ||
            paceTargetGroup (tg)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSING",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        } else {

                entityToModuleStateStats->addStateModuleForEntity ("FAILING",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));

        }

        return filterTg;
}

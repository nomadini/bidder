
#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "HttpUtil.h"
#include "EntityToModuleStateStats.h"
#include "Entity.h"
#include "HttpUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "EntityDeliveryInfoCacheService.h"
#include "HttpUtilService.h"
#include "EntityDeliveryInfoFetcher.h"
#include "JsonUtil.h"
#include "ConfigService.h"
#include "TargetGroupCacheService.h"
#include "BidderApplicationContext.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "JsonArrayUtil.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "MySqlEntityRealTimeDeliveryInfoService.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "ConcurrentHashMap.h"
#include "Campaign.h"
/***
 * This class is responsible for updating the EntityDeliveryInfoCacheService with the most up-to-date information
 * from pacing Engine
 */

EntityDeliveryInfoFetcher::EntityDeliveryInfoFetcher(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void EntityDeliveryInfoFetcher::getLatestRealTimeInfo(std::string entityName, std::vector<int> entityIds) {

        try {
                refreshRealTimeInfos (entityName, entityIds);
                isFetchingDeliveryInfoFromPacerHealthy->setValue(true);
        } catch (std::exception const &e) {
                entityToModuleStateStats->addStateModuleForEntity
                        ("EXCEPTION_WHILE_GETTING_LATEST_REAL_TIME_INFO",
                        getName (),
                        "ALL",
                        EntityToModuleStateStats::exception);
                gicapods::Util::showStackTrace(&e);
                isFetchingDeliveryInfoFromPacerHealthy->setValue(false);

        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("UNKNOWN_EXCEPTION_WHILE_GETTING_LATEST_REAL_TIME_INFO",
                                                                   getName (),
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                isFetchingDeliveryInfoFromPacerHealthy->setValue(false);
        }


}

void EntityDeliveryInfoFetcher::refreshRealTimeInfos(std::string entityName, std::vector<int> entityIds) {

        std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos =
                mySqlEntityRealTimeDeliveryInfoService->
                getTgRealTimeDeliveryInfos(entityName, entityIds);
        if (gicapods::Util::allowedToCall(_FL_, 10)) {
                LOG(INFO) << "entityName : "<<entityName
                          <<",latest delivery Infos " << JsonArrayUtil::convertListToJson(allDeliveryInfos);
        }
        populateDeliveryInfoCache(entityName, allDeliveryInfos);
}

void EntityDeliveryInfoFetcher::populateDeliveryInfoCache(
        std::string entityName,
        std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos) {

        if (StringUtil::equalsIgnoreCase(entityName, TargetGroup::getEntityName())) {
                auto deliveryCacheMap = entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ();
                deliveryCacheMap->clear();
                for (auto deliveryInfo : allDeliveryInfos) {
                        deliveryCacheMap->put(deliveryInfo->entityId, deliveryInfo);
                }
                recordExchangeCostsAsStates(entityName, deliveryCacheMap);

        } else if (StringUtil::equalsIgnoreCase(entityName, Campaign::getEntityName())) {
                auto deliveryCacheMap = entityDeliveryInfoCacheService->getAllCampaignRealTimeDeliveryInfoMap ();
                deliveryCacheMap->clear();
                for (auto deliveryInfo : allDeliveryInfos) {
                        deliveryCacheMap->put(deliveryInfo->entityId, deliveryInfo);
                }
                recordExchangeCostsAsStates(entityName, deliveryCacheMap);
        }
}

void EntityDeliveryInfoFetcher::recordExchangeCostsAsStates(
        std::string entityName,
        std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > allDeliveryInfoMap) {
        auto map = allDeliveryInfoMap->getCopyOfMap();
        for (auto iter = map->begin ();
             iter != map->end ();
             iter++) {
                entityToModuleStateStats->addStateModuleForEntityWithValuePerMinute (
                        entityName+"_total_imps_"+_toStr(iter->first),
                        iter->second->numOfImpressionsServedOverallUpToNow->getValue(),
                        getName() + "totalImpsMonitor",
                        "ALL");
                entityToModuleStateStats->addStateModuleForEntityWithValuePerMinute (
                        entityName+"_total_platformCost_"+_toStr(iter->first),
                        iter->second->platformCostSpentOverallUpToNow->getValue(),
                        getName() + "PlatFormCostsMonitor",
                        "ALL");
        }

}
std::string EntityDeliveryInfoFetcher::getName() {
        return "EntityDeliveryInfoFetcher";
}

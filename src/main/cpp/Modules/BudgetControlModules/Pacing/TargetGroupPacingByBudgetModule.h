#ifndef TargetGroupPacingByBudgetModule_H
#define TargetGroupPacingByBudgetModule_H


#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;
#include "DateTimeService.h"
#include "Object.h"
#include "EntityRealTimeDeliveryInfo.h"
/**
   this module filters out the target groups that have gone above their pacing limits
   in terms of budget at some point.
 */
class TargetGroupPacingByBudgetModule : public BidderModule, public Object {

private:

public:

EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
DateTimeService* dateTimeService;

TargetGroupPacingByBudgetModule(EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
                                DateTimeService* dateTimeService,
                                EntityToModuleStateStats* entityToModuleStateStats,
                                std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

static int getLimitByHour(std::shared_ptr<TargetGroup> tg,
                          DateTimeService* dateTimeService,
                          EntityToModuleStateStats* entityToModuleStateStats,
                          std::string nameOfModule);

virtual std::string getName();

std::shared_ptr<EntityRealTimeDeliveryInfo> getTgRealTimeDeliveryInfo(std::shared_ptr<TargetGroup> tg);

/**
 * it will filter out target groups that have spent more than
 * their hourly budget
 */
bool paceByDailyBudget(std::shared_ptr<TargetGroup> tg, std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr,
                       int maxHourIndex);

bool paceTargetGroup(std::shared_ptr<TargetGroup> tg);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
virtual void process(std::shared_ptr<OpportunityContext> context);
};



#endif

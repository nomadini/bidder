#ifndef EntityDeliveryInfoFetcher_H
#define EntityDeliveryInfoFetcher_H


#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "AtomicBoolean.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
class EntityDeliveryInfoCacheService;
class TargetGroupCacheService;
class EntityToModuleStateStats;
class EntityRealTimeDeliveryInfo;
class BidderApplicationContext;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class TgFilterMeasuresService;
class EntityRealTimeDeliveryInfo;
class HttpUtilService;
class MySqlEntityRealTimeDeliveryInfoService;

/**
 * this class is now responsible for hitting the pacing engine
 * and populating all the tg realtime info in Cache Manager
 *
 */
class EntityDeliveryInfoFetcher : public Object {

public:

EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
MySqlEntityRealTimeDeliveryInfoService* mySqlEntityRealTimeDeliveryInfoService;
int updateRealTimeInfoInterval;
bool inTestMode;

std::shared_ptr<gicapods::AtomicBoolean> isFetchingDeliveryInfoFromPacerHealthy;

EntityToModuleStateStats* entityToModuleStateStats;

void getLatestRealTimeInfo(std::string entityName, std::vector<int> tgIds);

void refreshRealTimeInfos(std::string entityName, std::vector<int> tgIds);

EntityDeliveryInfoFetcher(EntityDeliveryInfoCacheService* EntityDeliveryInfoCacheService,
                          EntityToModuleStateStats* entityToModuleStateStats);

void recordExchangeCostsAsStates(
        std::string entityName,
        std::shared_ptr<gicapods::ConcurrentHashMap<int, EntityRealTimeDeliveryInfo > > allDeliveryInfoMap);
void populateDeliveryInfoCache(
        std::string entityName,
        std::vector<std::shared_ptr<EntityRealTimeDeliveryInfo> > allDeliveryInfos);

virtual std::string getName();

private:

};

#endif

#ifndef TargetGroupPacingByImpressionModule_H
#define TargetGroupPacingByImpressionModule_H


#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;
class EntityRealTimeDeliveryInfo;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;
#include "DateTimeService.h"
#include "Object.h"
/**
   this module filters out the target groups that have gone above their pacing limits
   in terms of impressions at some point.
 */
class TargetGroupPacingByImpressionModule : public BidderModule, public Object {

private:

public:


EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
TargetGroupPacingByImpressionModule(EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
                                    DateTimeService* dateTimeService,
                                    EntityToModuleStateStats* entityToModuleStateStats,
                                    std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
DateTimeService* dateTimeService;
virtual std::string getName();

/**
 * this will filter out the target groups that have shown impressions
 * more than their hourly limit
 */
bool paceByDailyImpression(std::shared_ptr<TargetGroup> tg,
                           std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimePtr, int maxHourIndex);

std::shared_ptr<EntityRealTimeDeliveryInfo> getTgRealTimeDeliveryInfo(std::shared_ptr<TargetGroup> tg);

bool paceTargetGroup(std::shared_ptr<TargetGroup> tg);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
virtual void process(std::shared_ptr<OpportunityContext> context);
};



#endif

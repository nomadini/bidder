//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef PacingBasedBugdetLimitEnforcerFilter_H
#define PacingBasedBugdetLimitEnforcerFilter_H


#include "PacingBasedBugdetLimitEnforcerFilter.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "Object.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
/*
 *
 * this filter makes sure that we dont bid more than the pacing per hour limit on a target group
 */
class PacingBasedBugdetLimitEnforcerFilter : public TgMarker, public Object {

private:

public:
TgFilterMeasuresService* tgFilterMeasuresService;

PacingBasedBugdetLimitEnforcerFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual ~PacingBasedBugdetLimitEnforcerFilter();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif //BIDDER_CAPPEDBIDDINGPERHOURFILTER_H

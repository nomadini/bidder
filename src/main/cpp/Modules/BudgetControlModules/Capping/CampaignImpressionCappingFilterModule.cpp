#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "CampaignCacheService.h"
#include "CampaignImpressionCappingFilterModule.h"
#include "JsonUtil.h"
#include "Campaign.h"
#include "OpportunityContext.h"
#include "EntityDeliveryInfoCacheService.h"
#include "CacheService.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "CampaignRealTimeInfo.h"

std::string CampaignImpressionCappingFilterModule::getName() {
        return "CampaignImpressionCappingFilterModule";
}

CampaignImpressionCappingFilterModule::CampaignImpressionCappingFilterModule(
        CampaignCacheService* campaignCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        NULL_CHECK(entityDeliveryInfoCacheService);
        NULL_CHECK(campaignCacheService);
        this->campaignCacheService = campaignCacheService;
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}

void CampaignImpressionCappingFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool CampaignImpressionCappingFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        auto campaign = campaignCacheService->findCampaignByTgId (tg->getId());
        bool filterTg = true;
        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        auto cmpRealTimeInfo =
                entityDeliveryInfoCacheService->getAllCampaignRealTimeDeliveryInfoMap ()
                ->getOptional(tg->getCampaignId ());

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_BUDGET)) {
                if (cmpRealTimeInfo != nullptr) {
                        //no impression capping for budget pacing tgs
                        if (
                                (cmpRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->getValue() < campaign->getDailyMaxImpression () &&
                                 cmpRealTimeInfo->numOfImpressionsServedOverallUpToNow->getValue() < campaign->getMaxImpression ())) {

                                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                                   "CampaignImpressionCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                                filterTg = false;
                        } else {
                                entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                                   "CampaignImpressionCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                        }
                } else {

                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_REAL_TIME_INFO",
                                                                           "CampaignImpressionCappingFilterModule",
                                                                           StringUtil::toStr (tg->getId ()));
                }
        } else {
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOT_BEING_BUDGET_PACED_TG",
                                                                   "CampaignImpressionCappingFilterModule",
                                                                   StringUtil::toStr (tg->getId ()));
        }
        return filterTg;
}

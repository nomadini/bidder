/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef CampaignImpressionCappingFilterModule_H_
#define CampaignImpressionCappingFilterModule_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;
class EntityToModuleStateStats;
class CampaignCacheService;
class CampaignRealTimeInfo;
#include "Object.h"
template <class T>
class CacheService;

class CampaignImpressionCappingFilterModule : public BidderModule, public Object {

private:

public:

virtual std::string getName();

CampaignCacheService* campaignCacheService;
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
CampaignImpressionCappingFilterModule(CampaignCacheService* campaignCacheService,
																																						EntityToModuleStateStats* entityToModuleStateStats,
																																						EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
																																						std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > filteringTgsWithDailyCampaignMaxImpressionMet(
								std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPDAILYCAPSFILTERMODULE_H_ */

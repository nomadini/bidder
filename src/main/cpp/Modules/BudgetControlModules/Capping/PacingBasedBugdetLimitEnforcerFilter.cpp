//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "Status.h"
#include "TargetGroupTypeDefs.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "PacingBasedBugdetLimitEnforcerFilter.h"
#include "DateTimeUtil.h"

PacingBasedBugdetLimitEnforcerFilter::PacingBasedBugdetLimitEnforcerFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts), Object(__FILE__) {

}


std::string PacingBasedBugdetLimitEnforcerFilter::getName() {
        return "PacingBasedBugdetLimitEnforcerFilter";
}

PacingBasedBugdetLimitEnforcerFilter::~PacingBasedBugdetLimitEnforcerFilter() {

}


bool PacingBasedBugdetLimitEnforcerFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;


        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_IMPRESSION)) {
                //we don't pace by hourl budget limit if tg is paced based on imps
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_PACED_BY_IMPRESSION",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;

        } else {
                if (tgFilterMeasuresService->getSumOfOfPlatformCostsInLastHourForTg(tg->getId()) <=
                    tg->pacingBasedHourlyCalculatedBudgetLimit->getValue()) {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);
                        filterTg = false;

                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                }
        }

        return filterTg;
}

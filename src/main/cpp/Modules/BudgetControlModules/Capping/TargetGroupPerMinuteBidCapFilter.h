/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupPerMinuteBidCapFilter_H_
#define TargetGroupPerMinuteBidCapFilter_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"

#include "CollectionUtil.h"
class OpportunityContext;
#include "Object.h"
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
class TargetGroupPerMinuteBidCapFilter : public BidderModule, public Object {

private:

public:

TgFilterMeasuresService* tgFilterMeasuresService;

virtual std::string getName();

TargetGroupPerMinuteBidCapFilter(EntityToModuleStateStats* entityToModuleStateStats,
                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif /* TargetGroupPerMinuteBidCapFilter_H_ */

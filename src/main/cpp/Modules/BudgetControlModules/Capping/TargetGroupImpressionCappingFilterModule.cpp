#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupImpressionCappingFilterModule.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "DateTimeUtil.h"
#include "ConcurrentHashMap.h"

#include "Status.h"


std::string TargetGroupImpressionCappingFilterModule::getName() {
        return "TargetGroupImpressionCappingFilterModule";
}

TargetGroupImpressionCappingFilterModule::TargetGroupImpressionCappingFilterModule(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}

void TargetGroupImpressionCappingFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupImpressionCappingFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_BUDGET)) {
                filterTg = false;
        } else {

                if (tg->getDailyMaxImpression () >= 0) {
                        auto tgRealTimeInfo =
                                entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ()
                                ->getOptional(tg->getId ());
                        if (tgRealTimeInfo != nullptr) {
                                if (tgRealTimeInfo->numOfImpressionsServedInCurrentDateUpToNow->getValue() < tg->getDailyMaxImpression () &&
                                    tgRealTimeInfo->numOfImpressionsServedOverallUpToNow->getValue() < tg->getMaxImpression ()) {

                                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                                           "TargetGroupImpressionCappingFilterModule",
                                                                                           StringUtil::toStr (tg->getId ()));
                                        filterTg = false;
                                } else {

                                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                                           "TargetGroupImpressionCappingFilterModule",
                                                                                           StringUtil::toStr (tg->getId ()));
                                }
                        } else {

                                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_REAL_TIME_INFO",
                                                                                   "TargetGroupImpressionCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                        }
                } else {

                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_MINUS_DAILY_MAX",
                                                                           "TargetGroupImpressionCappingFilterModule",
                                                                           StringUtil::toStr (tg->getId ()));
                }
        }

        return filterTg;
}

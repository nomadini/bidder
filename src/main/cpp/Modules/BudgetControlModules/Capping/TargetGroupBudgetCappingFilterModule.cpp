#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupBudgetCappingFilterModule.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashMap.h"

std::string TargetGroupBudgetCappingFilterModule::getName() {
        return "TargetGroupBudgetCappingFilterModule";
}

TargetGroupBudgetCappingFilterModule::TargetGroupBudgetCappingFilterModule(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}

void TargetGroupBudgetCappingFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupBudgetCappingFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_IMPRESSION)) {
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_IMP_PACED_TG",
                                                                   "TargetGroupBudgetCappingFilterModule",
                                                                   StringUtil::toStr (tg->getId ()));
        } else {
                auto tgRealTimeInfo =
                        entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ()->getOptional (tg->getId ());
                if (tgRealTimeInfo != nullptr) {
                        if(tgRealTimeInfo->platformCostSpentInCurrentDateUpToNow->getValue() < tg->getDailyMaxBudget () &&
                           tgRealTimeInfo->platformCostSpentInCurrentDateUpToNow->getValue() < tg->getMaxBudget ()) {
                                //we are under budget still for today
                                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                                   "TargetGroupBudgetCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                                filterTg = false;
                        } else {

                                entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                                   "TargetGroupBudgetCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                        }
                } else {

                        entityToModuleStateStats->addStateModuleForEntity ("IGNORED_FOR_NO_REAL_TIME_INFO",
                                                                           "TargetGroupBudgetCappingFilterModule",
                                                                           StringUtil::toStr (tg->getId ()));
                }
        }

        return filterTg;
}

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetingTypeLessTargetGroupFilter_H_
#define TargetingTypeLessTargetGroupFilter_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"

#include "CollectionUtil.h"
class OpportunityContext;
#include "Object.h"
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
class TargetingTypeLessTargetGroupFilter : public BidderModule, public Object {

private:

public:

TgFilterMeasuresService* tgFilterMeasuresService;

virtual std::string getName();

TargetingTypeLessTargetGroupFilter(EntityToModuleStateStats* entityToModuleStateStats,
                                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif /* TargetingTypeLessTargetGroupFilter_H_ */

//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef CappedBiddingPerHourFilter_H
#define CappedBiddingPerHourFilter_H


#include "CappedBiddingPerHourFilter.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
/*
 *
 * this filter makes sure that we dont bid unlimitedly on a target group or campaign if something
 * goes wrong. there is a cap per hour bidding that should be saved in campaign or target group table
 * and we need to be able to update that over http  in every bidder
 */
class CappedBiddingPerHourFilter : public TgMarker, public Object {

private:

public:
TgFilterMeasuresService* tgFilterMeasuresService;
CappedBiddingPerHourFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        );

std::string getName();


virtual ~CappedBiddingPerHourFilter();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif //BIDDER_CAPPEDBIDDINGPERHOURFILTER_H

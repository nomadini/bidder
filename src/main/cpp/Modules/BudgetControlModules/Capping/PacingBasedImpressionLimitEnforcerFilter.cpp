//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "Status.h"
#include "TargetGroupTypeDefs.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "PacingBasedImpressionLimitEnforcerFilter.h"
#include "DateTimeUtil.h"

PacingBasedImpressionLimitEnforcerFilter::PacingBasedImpressionLimitEnforcerFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts), Object(__FILE__) {

}


std::string PacingBasedImpressionLimitEnforcerFilter::getName() {
        return "PacingBasedImpressionLimitEnforcerFilter";
}

PacingBasedImpressionLimitEnforcerFilter::~PacingBasedImpressionLimitEnforcerFilter() {

}


bool PacingBasedImpressionLimitEnforcerFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;


        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_BUDGET)) {
                //we don't pace by hourl imps limit if tg is paced based on budget
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_BASED_ON_BUDGET",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
        } else {
                auto lastHourWins = tgFilterMeasuresService->getNumberOfWinsInLastHourForTg(tg->getId());
                if (lastHourWins <=
                    tg->pacingBasedHourlyCalculatedImpressionLimit->getValue()) {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);
                        filterTg = false;

                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NOT_HITTING_HOUR_WIN_LIMIT",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                        LOG_EVERY_N(INFO, 1000)
                                <<"tg is paced id : "<<tg->idAsString
                                <<" , lastHourWins : "<< lastHourWins
                                << " ,limit : "<<tg->pacingBasedHourlyCalculatedImpressionLimit->getValue();

                }
        }

        return filterTg;
}

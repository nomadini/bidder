/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupImpressionCappingFilterModule_H_
#define TargetGroupImpressionCappingFilterModule_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"
#include "Object.h"
#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;

class TargetGroupImpressionCappingFilterModule : public BidderModule, public Object {

private:

public:

EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;

virtual std::string getName();

TargetGroupImpressionCappingFilterModule(
        EntityDeliveryInfoCacheService* EntityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};




#endif /* TargetGroupImpressionCappingFilterModule_H_ */

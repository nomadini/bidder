/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule_H_
#define TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"

#include "CollectionUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
class OpportunityContext;
#include "Object.h"
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityDeliveryInfoCacheService;

class TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule : public BidderModule, public Object {

private:

public:

EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;

virtual std::string getName();

TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule(
        EntityDeliveryInfoCacheService* EntityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

void calculateHourlyLimitBasedOnPacing(std::shared_ptr<TargetGroup> tg);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
void setBudgetLimits(std::shared_ptr<TargetGroup> tg,
                     long numberOfHoursLeftTillEndOfTg,
                     double timeOfDayAdjuster,
                     std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo);
void setImpressionLimits(std::shared_ptr<TargetGroup> tg,
                         long numberOfHoursLeftTillEndOfTg,
                         double timeOfDayAdjuster,
                         int hourNowInEst,
                         std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo);
};




#endif

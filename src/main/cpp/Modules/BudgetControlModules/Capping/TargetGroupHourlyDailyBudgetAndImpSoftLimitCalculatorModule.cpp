#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "DateTimeUtil.h"
#include "ConcurrentHashMap.h"

#include "Status.h"


std::string TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::getName() {
        return "TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule";
}

TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}
bool TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}
void TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::process(std::shared_ptr<OpportunityContext> context) {

        for(auto tg : *context->getAllAvailableTgs()) {
                if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_EVEN)) {
                        calculateHourlyLimitBasedOnPacing(tg);
                }
        }
}

void TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::calculateHourlyLimitBasedOnPacing(std::shared_ptr<TargetGroup> tg) {
        Poco::DateTime now;

        MLOG(3) << "tg->endDate : "<< DateTimeUtil::dateTimeToStr(tg->endDate);
        long numberOfHoursLeftTillEndOfTg = (tg->endDate.timestamp().epochMicroseconds() / 1000000 - now.timestamp().epochMicroseconds() / 1000000) / (3600);
        MLOG(3)<< "numberOfHoursLeftTillEndOfTg : "<< numberOfHoursLeftTillEndOfTg;
        if (numberOfHoursLeftTillEndOfTg <= 0) {
                return;
        }

        //TODO : change the 5 from a function that gets the timzeZoneDiff based on IP of server that app is on
        //TODO : look at the OpportunityContextBuilderOpenRtb2_3 class
        int hourNowInEst = DateTimeUtil::getCurrentHourAsIntegerInUTC () - 5;

        double timeOfDayAdjuster = 1.00;
        if (hourNowInEst > 8 && hourNowInEst < 20) {
                timeOfDayAdjuster = 2.00;
        }

        auto tgRealTimeInfo =
                entityDeliveryInfoCacheService->getAllTgRealTimeDeliveryInfoMap ()
                ->getOptional(tg->getId ());
        if (tgRealTimeInfo == nullptr) {
                LOG(ERROR)<< "no realtime info for tg "<< tg->getId ()<< " found, setting pacing limits to 0";
                entityToModuleStateStats->addStateModuleForEntity("NO REALTIME INFO FOUND",
                                                                  "TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);
                return;
        }

        if (!StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_BUDGET)) {
                setImpressionLimits(tg,
                                    numberOfHoursLeftTillEndOfTg,
                                    timeOfDayAdjuster,
                                    hourNowInEst,
                                    tgRealTimeInfo);
        }

        if (!StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_IMPRESSION)) {
                setBudgetLimits(tg, numberOfHoursLeftTillEndOfTg, timeOfDayAdjuster,
                                tgRealTimeInfo);
        }
}

void TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::setImpressionLimits(
        std::shared_ptr<TargetGroup> tg,
        long numberOfHoursLeftTillEndOfTg,
        double timeOfDayAdjuster,
        int hourNowInEst,
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo) {
        long numberOfImpressionsLeft = tg->maxImpression - tgRealTimeInfo->numOfImpressionsServedOverallUpToNow->getValue();
        long numberOfImpressionPerHour = numberOfImpressionsLeft / numberOfHoursLeftTillEndOfTg;

        long realLimitPerHour = numberOfImpressionPerHour * timeOfDayAdjuster;

        MLOG(3)<< "numberOfImpressionsLeft : "<< numberOfImpressionsLeft
               <<" numberOfImpressionPerHour : "<< numberOfImpressionPerHour
               <<" hourNowInEst : "<< hourNowInEst
               <<" timeOfDayAdjuster : "<< timeOfDayAdjuster
               <<" realLimitPerHour : "<< realLimitPerHour;
        if (numberOfImpressionPerHour > 0) {
                assertAndThrow(realLimitPerHour > 0);
        }

        tg->pacingBasedHourlyCalculatedImpressionLimit->setValue(realLimitPerHour);

}

void TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule::setBudgetLimits(
        std::shared_ptr<TargetGroup> tg,
        long numberOfHoursLeftTillEndOfTg,
        double timeOfDayAdjuster,
        std::shared_ptr<EntityRealTimeDeliveryInfo> tgRealTimeInfo) {
        double budgetLeft = tg->maxBudget - tgRealTimeInfo->platformCostSpentOverallUpToNow->getValue();
        double budgetLeftPerHour = budgetLeft / numberOfHoursLeftTillEndOfTg;
        double realBudgetLimitPerHour = budgetLeftPerHour * timeOfDayAdjuster;
        MLOG(3)<< "budgetLeft : "<< budgetLeft
               <<" budgetLeftPerHour : "<< budgetLeftPerHour
               <<" timeOfDayAdjuster : "<< timeOfDayAdjuster
               <<" realBudgetLimitPerHour : "<< realBudgetLimitPerHour;

        if (budgetLeftPerHour > 0) {
                assertAndThrow(realBudgetLimitPerHour > 0);
        }

        tg->pacingBasedHourlyCalculatedBudgetLimit->setValue(realBudgetLimitPerHour);
}

#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetingTypeLessTargetGroupFilter.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "Status.h"


std::string TargetingTypeLessTargetGroupFilter::getName() {
        return "TargetingTypeLessTargetGroupFilter";
}

TargetingTypeLessTargetGroupFilter::TargetingTypeLessTargetGroupFilter
        (EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

void TargetingTypeLessTargetGroupFilter::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

//for some weird reasons like sampling, throttling, we might end up with
//target groups that have passed all the filtering without any targeting type
//we must run this filter before target group selector module
bool TargetingTypeLessTargetGroupFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        auto typesOfTargetingChosen =
                context->getTargetingTypesForTg(tg->getId());

        if (typesOfTargetingChosen->empty()) {
                entityToModuleStateStats->
                addStateModuleForEntity("TG_WITHOUT_TARGETTING_TYPE",
                                        "TargetingTypeLessTargetGroupFilter",
                                        "tg" + _toStr(tg->getId()));
        } else {
                filterTg = false;
        }

        return filterTg;
}

#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupPerMinuteBidCapFilter.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "Status.h"


std::string TargetGroupPerMinuteBidCapFilter::getName() {
        return "TargetGroupPerMinuteBidCapFilter";
}

TargetGroupPerMinuteBidCapFilter::TargetGroupPerMinuteBidCapFilter
        (EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

void TargetGroupPerMinuteBidCapFilter::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupPerMinuteBidCapFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (tgFilterMeasuresService->getNumberOfBidsInLastMinuteByThisBidderForTg(tg->getId())
            <=
            tg->getNumberOfBidsAllowedPerMinute()) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   "TargetGroupPerMinuteBidCapFilter",
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        } else {

                entityToModuleStateStats->addStateModuleForEntity ("OVER_DELIVERY_WARNING",
                                                                   "TargetGroupPerMinuteBidCapFilter",
                                                                   StringUtil::toStr (tg->getId ()),
                                                                   "WARNING");


        }

        return filterTg;
}

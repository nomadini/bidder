//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef PacingBasedImpressionLimitEnforcerFilter_H
#define PacingBasedImpressionLimitEnforcerFilter_H


#include "PacingBasedImpressionLimitEnforcerFilter.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "Object.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
/*
 *
 * this filter makes sure that we dont bid more than the pacing per hour limit on a target group
 */
class PacingBasedImpressionLimitEnforcerFilter : public TgMarker, public Object {

private:

public:
TgFilterMeasuresService* tgFilterMeasuresService;

PacingBasedImpressionLimitEnforcerFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual ~PacingBasedImpressionLimitEnforcerFilter();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif //BIDDER_CAPPEDBIDDINGPERHOURFILTER_H

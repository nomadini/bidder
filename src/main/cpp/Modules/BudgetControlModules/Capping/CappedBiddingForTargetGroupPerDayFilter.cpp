//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "Status.h"
#include "TargetGroupTypeDefs.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CappedBiddingForTargetGroupPerDayFilter.h"
#include "DateTimeUtil.h"

CappedBiddingForTargetGroupPerDayFilter::CappedBiddingForTargetGroupPerDayFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : TgMarker(filterNameToFailureCounts),  Object(__FILE__) {

}


std::string CappedBiddingForTargetGroupPerDayFilter::getName() {
        return "CappedBiddingForTargetGroupPerDayFilter";
}

CappedBiddingForTargetGroupPerDayFilter::~CappedBiddingForTargetGroupPerDayFilter() {

}

bool CappedBiddingForTargetGroupPerDayFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        bool condition = tg->getNumberOfBidsLimitInInADayPerBidder ()->getValue() > 0;
        if(!condition) {
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION:FAILED_FOR_NOT_HITTING_DAY_BID_LIMIT",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                //this will lead to a throw exception in the logs and
                assertAndThrow(tg->getNumberOfBidsLimitInInADayPerBidder ()->getValue() > 0);
        }


        if (tgFilterMeasuresService->getNumberOfBidsMadeTodayByThisBidderForTg(tg->getId()) <=
            tg->getNumberOfBidsLimitInInADayPerBidder ()->getValue()) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOT_HITTING_DAY_BID_LIMIT",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NOT_HITTING_DAY_BID_LIMIT",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

        }

        return filterTg;
}

#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "Campaign.h"
#include "CampaignCacheService.h"
#include "CampaignBudgetCappingFilterModule.h"
#include "JsonUtil.h"
#include <functional>
#include "OpportunityContext.h"
#include "CampaignRealTimeInfo.h"
#include "CacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "EntityRealTimeDeliveryInfo.h"
std::string CampaignBudgetCappingFilterModule::getName() {
        return "CampaignBudgetCappingFilterModule";
}

CampaignBudgetCappingFilterModule::CampaignBudgetCappingFilterModule(
        CampaignCacheService* campaignCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,

        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {


        this->campaignCacheService = campaignCacheService;
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}

void CampaignBudgetCappingFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}


bool CampaignBudgetCappingFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (StringUtil::equalsIgnoreCase(tg->pacingSpeed, TargetGroup::PACING_SPEED_ASAP)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_ASAP",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
                return filterTg;
        }

        auto campaign = campaignCacheService->findCampaignByTgId (tg->getId());

        auto cmpRealTimeInfo =
                entityDeliveryInfoCacheService->getAllCampaignRealTimeDeliveryInfoMap ()->
                getOptional (tg->getCampaignId ());

        //no budget capping for impression pacing campaign

        if (StringUtil::equalsIgnoreCase(tg->pacingMethod, TargetGroup::PACING_METHOD_IMPRESSION)) {
                if (cmpRealTimeInfo != nullptr) {

                        if (cmpRealTimeInfo->platformCostSpentInCurrentDateUpToNow->getValue() < campaign->getDailyMaxBudget () &&
                            cmpRealTimeInfo->platformCostSpentInCurrentDateUpToNow->getValue() < campaign->getMaxBudget ()) {
                                //we are under budget still for today
                                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                                   "CampaignBudgetCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                                filterTg = false;
                        } else {

                                entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                                   "CampaignBudgetCappingFilterModule",
                                                                                   StringUtil::toStr (tg->getId ()));
                        }
                } else {

                        entityToModuleStateStats->addStateModuleForEntity ("IGNORED_FOR_NO_REAL_TIME_INFO",
                                                                           "CampaignBudgetCappingFilterModule",
                                                                           StringUtil::toStr (tg->getId ()));
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_BEING_IMP_PACED_TG",
                                                                   "CampaignBudgetCappingFilterModule",
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        }

        return filterTg;

}

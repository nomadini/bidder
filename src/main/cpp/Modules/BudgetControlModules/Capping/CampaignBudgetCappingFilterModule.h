/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef CampaignBudgetCappingFilterModule_H_
#define CampaignBudgetCappingFilterModule_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
class EntityToModuleStateStats;
class Campaign;
template <class T>
class CacheService;

class EntityDeliveryInfoCacheService;
class CampaignRealTimeInfo;
class CampaignCacheService;
#include "Object.h"
class CampaignBudgetCappingFilterModule : public BidderModule, public Object {

private:

public:

virtual std::string getName();

CampaignCacheService* campaignCacheService;
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
CampaignBudgetCappingFilterModule(
								CampaignCacheService* campaignCacheService,
								EntityToModuleStateStats* entityToModuleStateStats,
								EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
								std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif

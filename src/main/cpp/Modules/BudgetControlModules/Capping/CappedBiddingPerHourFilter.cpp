//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CappedBiddingPerHourFilter.h"
#include "DateTimeUtil.h"

CappedBiddingPerHourFilter::CappedBiddingPerHourFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : TgMarker(filterNameToFailureCounts), Object(__FILE__) {

}


std::string CappedBiddingPerHourFilter::getName() {
        return "CappedBiddingPerHourFilter";
}


CappedBiddingPerHourFilter::~CappedBiddingPerHourFilter() {

}

bool CappedBiddingPerHourFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        bool condition = tg->getNumberOfBidsLimitInLastHourPerBidder ()->getValue() > 0;
        if(!condition) {
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION:NumberOfBidsLimitInLastHourPerBidderIsLessThan0",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

                LOG(ERROR) << "bad tg loaded : "<<tg->toJson();
                //this will lead to a throw exception in the logs and
                assertAndThrow(tg->getNumberOfBidsLimitInLastHourPerBidder ()->getValue() > 0);
        }


        if ( tgFilterMeasuresService->getNumberOfBidsInLastHourByThisBidderForTg(tg->getId()) <=
             tg->getNumberOfBidsLimitInLastHourPerBidder ()->getValue()) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOT_HITTING_HOUR_BID_LIMIT",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
        } else {

                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NOT_HITTING_HOUR_BID_LIMIT",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

        }

        return filterTg;
}

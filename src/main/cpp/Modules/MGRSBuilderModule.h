#ifndef MGRSBuilderModule_h
#define MGRSBuilderModule_h


#include "Status.h"
#include <memory>
#include <string>
class OpportunityContext;
class EventLog;
#include "BidderModule.h"
#include "Object.h"
class EventLogCassandraService;
class LatLonToMGRSConverter;
class MGRSBuilderModule : public BidderModule, public Object {

public:
LatLonToMGRSConverter* latLonToMGRSConverter;
MGRSBuilderModule(EntityToModuleStateStats* entityToModuleStateStats,
																		std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};


#endif

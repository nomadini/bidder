#ifndef TargetGroupTargetingTypeValidatorModule_H
#define TargetGroupTargetingTypeValidatorModule_H

class OpportunityContext;
#include "Status.h"
#include <memory>
#include <string>
#include "EntityProviderService.h"
#include "Object.h"
class TargetGroupCacheService;
class EntityToModuleStateStats;
class TargetGroupTargetingTypeValidatorModule : public Object {

private:

public:

TargetGroupCacheService* targetGroupCacheService;
EntityToModuleStateStats* entityToModuleStateStats;

TargetGroupTargetingTypeValidatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        TargetGroupCacheService* targetGroupCacheService);

std::string getName();

void validateTargetingType(std::shared_ptr<OpportunityContext> context);

virtual ~TargetGroupTargetingTypeValidatorModule();

};



#endif

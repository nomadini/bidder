#include "RecentVisitHistoryUpdater.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "AerospikeDriver.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "RecentVisitHistoryScoreCard.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "Device.h"
#include "TgModelEligibilityRecord.h"
#include "DeviceRecentVisitHistory.h"
#include "RecentVisitHistory.h"

#include "RecentVisitHistoryScoreReader.h"

RecentVisitHistoryScoreReader::RecentVisitHistoryScoreReader()  : Object(__FILE__) {

}

RecentVisitHistoryScoreReader::~RecentVisitHistoryScoreReader() {

}

std::shared_ptr<RecentVisitHistoryScoreCard>
RecentVisitHistoryScoreReader::readTheScoreCardForDevice(std::shared_ptr<Device> device) {

        std::string binContent =  aeroSpikeDriver->readTheBin(
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_NAMESPACE,
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SET,
                device->getDeviceId(),
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SCORE_BIN_NAME);
        MLOG(10) << "scoreCard bin content : "<< binContent;
        auto currentDeviceScoreCard = RecentVisitHistoryScoreCard::fromJson(binContent);
        return currentDeviceScoreCard;
}

std::shared_ptr<DeviceRecentVisitHistory>  RecentVisitHistoryScoreReader::readRecentVisitHistory(std::shared_ptr<Device> device) {

        std::string allRecentHistories =  aeroSpikeDriver->readTheBin(
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_NAMESPACE,
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SET,
                device->getDeviceId(),
                RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_BIN_NAME);

        std::vector<std::string> jsonRepresentation =
                StringUtil::split(allRecentHistories, aeroSpikeDriver->stringValueSeperator);

        std::shared_ptr<DeviceRecentVisitHistory> deviceRecentVisitHistory =
                std::make_shared<DeviceRecentVisitHistory>(device);

        float sumOfScores = 0.00;
        //we are parsing all the objects that we wrote
        for (auto json : jsonRepresentation) {
                if (json.empty()) { continue; }
                // LOG(ERROR) << "json : "<< json;
                deviceRecentVisitHistory->
                allRecentHistories.push_back(RecentVisitHistory::fromJson(json));
        }

        return deviceRecentVisitHistory;
}


std::vector<std::shared_ptr<RecencyModel> > RecentVisitHistoryScoreReader::getAllRecentModelsOfTg(int targetGroupId) {
        return targetGroupRecencyModelMapCacheService->getAllRecentModelsOfTg(targetGroupId);
}

#include "BidderModule.h"
#include "Status.h"
#include "TargetGroupSelectorModule.h"
#include "TargetGroupCacheService.h"
#include "CreativeCacheService.h"
#include "TargetGroup.h"
#include "JsonUtil.h"
#include "CampaignCacheService.h"
#include "Campaign.h"
#include "ConverterUtil.h"
#include "CreativeSizeFilter.h"
#include "Creative.h"
#include "OpportunityContext.h"
#include "BlockedCreativeAttributeFilter.h"
#include "EventLog.h"

#include "Advertiser.h"
#include "CreativeSizeFilter.h"
#include "RandomUtil.h"
#include "BlockedBannerAdTypeFilter.h"
#include "CreativeContentTypeFilter.h"
#include "CreativeBannerApiFilter.h"
#include "TargetGroupTargetingTypeValidatorModule.h"
TargetGroupSelectorModule::TargetGroupSelectorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

bool TargetGroupSelectorModule::
checkNextTargetGroupIfCurrentSelectionHasNoCreative(std::shared_ptr<TargetGroup> tgCandidate) {
        auto allCreatives = creativeSizeFilter->getCreativesAssignedToTg (tgCandidate->getId());
        if (allCreatives.size () == 0) {
                return false;
        }

        return true;
}

void TargetGroupSelectorModule::setRequiredContextVariablesBasedOnChosenTg(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<TargetGroup>& tgCandidate) {
        MLOG(2) << "tgCandidate : " << tgCandidate->toJson ();
        context->chosenTargetGroupId = tgCandidate->getId();
        context->campaignId = tgCandidate->getCampaignId();
        std::shared_ptr<Campaign> cmp = campaignCacheService->findByEntityId (tgCandidate->campaignId);
        context->advertiserId = cmp->getAdvertiserId();
        auto advertiser = advertiserCacheService->findByEntityId(context->advertiserId);
        context->clientId = advertiser->clientId;
        //		context->bidPrice =
}

void TargetGroupSelectorModule::selectTheOptimalTargetGroup(std::shared_ptr<OpportunityContext> context,
                                                            std::vector<std::shared_ptr<TargetGroup> > rawOrdered) {
        //make this more intelligent later

        //TODO : this is a random selection strategy...add more strategy later

        auto initialCandidateTargetGroups = CollectionUtil::shuffleTheList(rawOrdered);


        //this is just to show how much are target groups , pass all the filtering
        for(auto tg : initialCandidateTargetGroups) {
                entityToModuleStateStats->
                addStateModuleForEntity("PASSED_ALL_FILTERING",
                                        "TargetGroupSelectorModule",
                                        tg->idAsString);
        }

        entityToModuleStateStats->
        addStateModuleForEntityWithValue("GOOD_CANDIDATE_TARGETGROUP_SIZE",
                                         initialCandidateTargetGroups.size (),
                                         "TargetGroupSelectorModule",
                                         "ALL");


        int randomIndex = 0;

        if (initialCandidateTargetGroups.size () > 1) {
                randomIndex = RandomUtil::sudoRandomNumber(initialCandidateTargetGroups.size () - 1);
        }


        auto tgCandidate = initialCandidateTargetGroups.at (randomIndex);
        bool result = checkNextTargetGroupIfCurrentSelectionHasNoCreative(tgCandidate);

        if (result) {
                //now we choose the candidate tg
                setRequiredContextVariablesBasedOnChosenTg(context, tgCandidate);
        }
}

bool TargetGroupSelectorModule::creativeIsValidInTermsOfSSL(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative> crt) {
        if (context->secureCreativeRequired && !crt->IsSecure()) {
                MLOG(10) << "crt id : " << crt->getId() << " is not secure, this is a secure impression, skipping";

                return false;
        }
        return true;
}

void TargetGroupSelectorModule::selectTheBestCreative(std::shared_ptr<OpportunityContext> context) {
        //make this more intelligent later

        //we should filter for size and type of creative too here, this should be done in tg filter module
        auto allCreatives = creativeSizeFilter->getCreativesAssignedToTg (context->chosenTargetGroupId);
        MLOG(2) << "number of creatives : "
                << allCreatives.size () << " for chosen tg id : "
                << context->chosenTargetGroupId;

        if (allCreatives.size () > 0) {

                for (auto crt : allCreatives) {

                        MLOG(2) << "considering crt id : " << crt->getId();
                        //TODO : add rigourous testing for this module,
                        //by having a target group with multiple different creatives and
                        //making sure we pick the right ones
                        if (!creativeIsValidInTermsOfSSL(context, crt)) {
                                continue;
                        }

                        if (BlockedCreativeAttributeFilter::isCreativeBlockedByAttributeInContext(crt, context)) {
                                continue;
                        }

                        if (!CreativeSizeFilter::hasCreativeTheRequestedSize(context,crt)) {
                                continue;
                        }

                        if (BlockedBannerAdTypeFilter::doesCreativeHaveBlockedBannerType(context, crt)) {
                                continue;
                        }

                        if (!CreativeBannerApiFilter::doesCreativeHaveRightApi(context, crt)) {
                                continue;
                        }

                        if (!CreativeContentTypeFilter::doesCreativeHaveRightContentType(context, crt)) {
                                continue;
                        }

                        context->chosenCreativeId = crt->getId();
                        MLOG(2) << "this creative was chosen, id : " << context->chosenCreativeId;
                        entityToModuleStateStats->addStateModuleForEntity ("CHOSEN_CREATIVE_ID",
                                                                           "TargetGroupSelectorModule",
                                                                           "cr" + StringUtil::toStr(context->chosenCreativeId));


                        break;
                }
        } else {
                LOG(WARNING) << "The chosen target group , id :  " << context->chosenTargetGroupId <<
                        " had no creative assigned";
                entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION: NO_CREATIVE_WAS_FOUND_FOR_CHOSEN_TG",
                                                                   "TargetGroupSelectorModule",
                                                                   "ALL");

                throwEx("The chosen target group , id :  " + StringUtil::toStr(context->chosenTargetGroupId) +
                        " had no creative assigned");
        }
}

void TargetGroupSelectorModule::process(std::shared_ptr<OpportunityContext> context) {

        auto rawOrdered  = *TgMarker::getPassingTargetGroups(context);
        if (rawOrdered.empty()) {
                context->status->value = STOP_PROCESSING;
                return;
        }

        selectTheOptimalTargetGroup (context, rawOrdered);

        if (context->chosenTargetGroupId == 0) {
                //NO GOOD TG was chosen
                context->status->value = STOP_PROCESSING;
                return;
        }
        //now that we have the chosen target group, we must validate the type of targeting
        //this step is very important
        targetGroupTargetingTypeValidatorModule->validateTargetingType(context);
        selectTheBestCreative (context);

        context->chosenTargetGroup = targetGroupCacheService->findByEntityId(context->chosenTargetGroupId);

        if (context->chosenCreativeId == 0) {
                LOG_EVERY_N(ERROR, 100)<<google::COUNTER<<"th unable to select good creative. chosenTargetGroupId : "<<context->chosenTargetGroupId;
                entityToModuleStateStats->
                addStateModuleForEntity("NO_GOOD_CREATIVE_SIZE",
                                        "TargetGroupSelectorModule",
                                        "ALL",
                                        EntityToModuleStateStats::exception);
                context->status->value = STOP_PROCESSING;
        } else {
                context->chosenCreative =
                        creativeCacheService->findByEntityId(context->chosenCreativeId);

                //this is very important, it shows us how many times we have
                //the qualified candidate
                entityToModuleStateStats->
                addStateModuleForEntity("find_perfect_candidate_for_bidding",
                                        "TargetGroupSelectorModule",
                                        "ALL",
                                        EntityToModuleStateStats::important);
                //this is the only place in bidder that
                // we decide to bid
                context->bidDecisionResult = EventLog::EVENT_TYPE_BID;
                assertAndThrow(context->chosenTargetGroupId > 0 && context->chosenCreativeId > 0);
        }



}

std::string TargetGroupSelectorModule::getName() {
        return "TargetGroupSelectorModule";
}
bool TargetGroupSelectorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}
TargetGroupSelectorModule::~TargetGroupSelectorModule() {

}

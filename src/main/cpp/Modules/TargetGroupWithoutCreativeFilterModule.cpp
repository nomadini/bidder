#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupWithoutCreativeFilterModule.h"
#include "TargetGroupCreativeCacheService.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"

std::string TargetGroupWithoutCreativeFilterModule::getName() {
        return "TargetGroupWithoutCreativeFilterModule";
}

TargetGroupWithoutCreativeFilterModule::TargetGroupWithoutCreativeFilterModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

void TargetGroupWithoutCreativeFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupWithoutCreativeFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        auto tgCreativeEntry = targetGroupCreativeCacheService->getAllTargetGroupCreativesMap ()->getOptional (
                tg->getId());

        if (tgCreativeEntry != nullptr) {

                auto creativeMap = tgCreativeEntry;
                if (creativeMap->empty()) {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                                                                           "TargetGroupWithoutCreativeFilterModule",
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                } else {
                        //targetGroup has some creatives assigned to it
                        filterTg = false;
                }

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "TargetGroupWithoutCreativeFilterModule",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        return filterTg;
}

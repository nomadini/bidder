#ifndef PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper_H
#define PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include "DeviceHistoryUpdaterModule.h"
#include "Object.h"
class PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper : public BidderModule, public Object {

private:
std::string featureType;
public:
DeviceHistoryUpdaterModule* devicePlaceTagFeatureUpdaterModule;
PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper(
        std::string featureType,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

#include "BidEventRecorderModule.h"

#include <boost/exception/all.hpp>
#include "GUtil.h"

#include "ConverterUtil.h"
#include "EventLog.h"
#include "BidderApplicationContext.h"
#include "CassandraManagedType.h"
#include "EventLogCreatorModule.h"
#include <thread>
#include "ExceptionUtil.h"
#include "OpportunityContext.h"

BidEventRecorderModule::BidEventRecorderModule(EntityToModuleStateStats* entityToModuleStateStats,
                                               std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string BidEventRecorderModule::getName() {
        return "BidEventRecorderModule";
}

void BidEventRecorderModule::process(std::shared_ptr<OpportunityContext> context) {

        if (!context->eventLog) {
                context->strictEventLogChecking = false;
                context->eventLog = eventLogCreatorModule->createEventLogFrom(context);
        }

        recordBidEvent(context);
        recordNoBidEvent(context);

        context->status->value = CONTINUE_PROCESSING;

}

void BidEventRecorderModule::recordBidEvent(std::shared_ptr<OpportunityContext> context) {
        auto isInSample = bidEventRecorderModuleSampler->isPartOfSample ();
        if (isInSample) {
                if(StringUtil::equalsIgnoreCase(context->bidDecisionResult, EventLog::EVENT_TYPE_BID)) {

                        entityToModuleStateStats->addStateModuleForEntity ("bid_writing_to_queue",
                                                                           "BidEventRecorderModule",
                                                                           "ALL");

                        assertAndThrow(StringUtil::equalsIgnoreCase(context->eventLog->eventType,
                                                                    EventLog::EVENT_TYPE_BID));
                        assertAndThrow(context->chosenTargetGroupId > 0 && context->chosenCreativeId > 0);
                        bidEventLogCassandraService->pushToWriteBatchQueue(
                                std::static_pointer_cast<CassandraManagedType>(context->eventLog));
                }

        }
}
void BidEventRecorderModule::recordNoBidEvent(std::shared_ptr<OpportunityContext> context) {
        auto isInSample = noBidEventRecorderModuleSampler->isPartOfSample ();
        if (isInSample) {

                if (StringUtil::equalsIgnoreCase(context->bidDecisionResult, EventLog::EVENT_TYPE_NO_BID)) {

                        entityToModuleStateStats->addStateModuleForEntity ("no_bid_writing_to_queue",
                                                                           "BidEventRecorderModule",
                                                                           "ALL");
                        if(!StringUtil::equalsIgnoreCase(context->eventLog->eventType,
                                                         EventLog::EVENT_TYPE_NO_BID)) {
                                EXIT("eventLog for a no bid event is " +  context->eventLog->toJson());
                        }

                        bidEventLogCassandraService->pushToWriteBatchQueue(
                                std::static_pointer_cast<CassandraManagedType>(context->eventLog));
                }
        }
}

bool BidEventRecorderModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

BidEventRecorderModule::~BidEventRecorderModule() {
}

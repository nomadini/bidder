#include "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "OpportunityContext.h"

DeviceHistoryOfGeoFeatureUpdaterModuleWrapper::DeviceHistoryOfGeoFeatureUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureCategory = featureCategory;
}

std::string DeviceHistoryOfGeoFeatureUpdaterModuleWrapper::getName() {
        return "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper ";
}



void DeviceHistoryOfGeoFeatureUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device == nullptr || context->siteDomain.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER ",
                                                                   "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper ",
                                                                   "ALL ");

                throwEx("unknown siteDomain or device ");
        }

        featureDeviceHistoryUpdaterModule->process(featureCategory,
                                                   context->mgrs100m,
                                                   context->device );
        entityToModuleStateStats->addStateModuleForEntity ("ADDING_FEATURE_FEATURE_HISTORY ",
                                                           "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper ",
                                                           "ALL ");

}

DeviceHistoryOfGeoFeatureUpdaterModuleWrapper::~DeviceHistoryOfGeoFeatureUpdaterModuleWrapper() {

}
bool DeviceHistoryOfGeoFeatureUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

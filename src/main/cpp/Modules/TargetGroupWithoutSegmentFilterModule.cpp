#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "TargetGroupWithoutSegmentFilterModule.h"
#include "JsonUtil.h"
#include "Segment.h"
#include "TargetGroupSegmentCacheService.h"
#include "TargetGroupCacheService.h"

#include "Status.h"
#include "ConcurrentHashSet.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "ConcurrentHashSet.h"
#include "ConcurrentHashMap.h"

std::string TargetGroupWithoutSegmentFilterModule::getName() {
        return "TargetGroupWithoutSegmentFilterModule";
}

TargetGroupWithoutSegmentFilterModule::TargetGroupWithoutSegmentFilterModule(
        TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

        this->targetGroupSegmentCacheService =  targetGroupSegmentCacheService;
        this->targetGroupsWithoutSegments = std::make_unique<gicapods::ConcurrentHashSet<int> > ();
}

void TargetGroupWithoutSegmentFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        targetGroupsWithoutSegments->clear();//we clear the set before each data reload pipeline run
        markFilteredTgs(context);
}

bool TargetGroupWithoutSegmentFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                                              std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        // targetGroupSegmentCacheService->printCacheContent();
        bool tgHasSegmentsAssigned = targetGroupSegmentCacheService->
                                     getAllTargetGroupSegmentListMap ()
                                     ->exists (tg->getId ());

        if (!tgHasSegmentsAssigned) {
                LOG(ERROR) << " tg " << tg->getId ()
                           << " doesn't have the any assignment segmentId, but we pass for now, for RON and other targeting methods";
                filterTg = false;
                //we save these targetgroups so that they can pass the DeviceIdSegmentModule
                targetGroupsWithoutSegments->put(tg->getId());
                entityToModuleStateStats->addStateModuleForEntity(
                        "NO_SEGMENT_TG_QUALIFIED",
                        "TargetGroupWithoutSegmentFilterModule",
                        "tg" + tg->idAsString);
        } else {
                filterTg = false;
        }

        return filterTg;
}

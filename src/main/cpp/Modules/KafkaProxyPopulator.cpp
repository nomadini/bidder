#include <boost/exception/all.hpp>
#include "GUtil.h"

#include "ConverterUtil.h"
#include "BidderApplicationContext.h"
#include <thread>
#include "ExceptionUtil.h"
#include "OpportunityContext.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "KafkaProxyPopulator.h"

#include "ConfigService.h"
#include "PocoSession.h"
KafkaProxyPopulator::KafkaProxyPopulator(EntityToModuleStateStats* entityToModuleStateStats,
                                         std::string kafkaProxyServiceHostUrl,
                                         gicapods::ConfigService* configService,
                                         int failureThresholdPercentageArg)
        : BiddingMonitor(entityToModuleStateStats,
                         failureThresholdPercentageArg,
                         "KafkaProxyPopulatorMonitor"),
        Object(__FILE__) {
        this->kafkaProxyServiceHostUrl = kafkaProxyServiceHostUrl;
        session = std::make_shared<PocoSession>(
                "KafkaProxyPopulator",
                configService->getAsBooleanFromString("kafkaProxyPopulatorCallKeepAliveMode"),
                configService->getAsInt("kafkaProxyPopulatorKeepAliveTimeoutInMillis"),
                configService->getAsInt("kafkaProxyPopulatorCallTimeoutInMillis"),
                configService->get("kafkaProxyPopulatorAppHostUrl"),
                entityToModuleStateStats);
}


KafkaProxyPopulator::~KafkaProxyPopulator() {
}

void KafkaProxyPopulator::sendMessageToKafkaOverHttp(std::string requestBody) {

        try {
                numberOfTotal->increment();

                auto response = session->sendRequest(
                        *PocoHttpRequest::createSimplePostRequest(kafkaProxyServiceHostUrl,
                                                                  requestBody));

                if (gicapods::Util::allowedToCall(_FL_, 1000)) {
                        LOG(ERROR)<<" unable to call kafka proxy at "<<kafkaProxyServiceHostUrl;
                        assertAndWarn(response->statusCode == 204);
                }
        } catch (...) {
                numberOfFailures->increment();
        }

}

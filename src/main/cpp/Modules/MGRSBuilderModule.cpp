
#include "MGRSBuilderModule.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include <tbb/concurrent_queue.h>
#include <thread>
#include "Encoder.h"
#include "OpportunityContext.h"
#include "EventLog.h"
#include "LatLonToMGRSConverter.h"
MGRSBuilderModule::MGRSBuilderModule(EntityToModuleStateStats* entityToModuleStateStats,
																																					std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
								: BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

}

std::string MGRSBuilderModule::getName() {
								return "MGRSBuilderModule";
}

void MGRSBuilderModule::process(std::shared_ptr<OpportunityContext> context) {
								std::tuple<std::string, std::string, std::string> allMGrsValues =
																latLonToMGRSConverter->convertToAllMGPRSValues(
																								context->deviceLat,
																								context->deviceLon);

								context->mgrs1km = std::get<0>(allMGrsValues);
								context->mgrs100m = std::get<1>(allMGrsValues);
								context->mgrs10m = std::get<2>(allMGrsValues);

								context->status->value = CONTINUE_PROCESSING;
}
bool MGRSBuilderModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
								throwEx("should not be used");
}

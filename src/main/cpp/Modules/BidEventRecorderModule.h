#ifndef BidEventRecorderModule_h
#define BidEventRecorderModule_h


#include "Status.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <string>
class OpportunityContext;
#include "BidderModule.h"
#include "CassandraServiceInterface.h"
#include "AtomicLong.h"
#include <tbb/concurrent_queue.h>

class EventLogCreatorModule;
#include "LoadPercentageBasedSampler.h"
#include "Object.h"
class BidEventRecorderModule : public BidderModule, public Object {


public:

std::unique_ptr<LoadPercentageBasedSampler>  bidEventRecorderModuleSampler;
std::unique_ptr<LoadPercentageBasedSampler>  noBidEventRecorderModuleSampler;
CassandraServiceInterface* bidEventLogCassandraService;
EventLogCreatorModule* eventLogCreatorModule;
BidEventRecorderModule(EntityToModuleStateStats* entityToModuleStateStats,
                       std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void recordNoBidEvent(std::shared_ptr<OpportunityContext> context);
void recordBidEvent(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);


virtual ~BidEventRecorderModule();

};


#endif

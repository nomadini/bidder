#include "AsyncPipelineFeederModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "OpportunityContext.h"
#include "Device.h"
AsyncPipelineFeederModule::AsyncPipelineFeederModule(EntityToModuleStateStats* entityToModuleStateStats,
                                                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

AsyncPipelineFeederModule::~AsyncPipelineFeederModule() {

}

std::string AsyncPipelineFeederModule::getName() {
        return "AsyncPipelineFeederModule";
}

void AsyncPipelineFeederModule::process(std::shared_ptr<OpportunityContext> context) {
        if (queueOfContexts->unsafe_size() <= 1000 ) {
                queueOfContexts->push(context);
        } else {
                LOG_EVERY_N(ERROR, 10000)<< google::COUNTER<< " th cannot put message in async queue, queue is too large "<< queueOfContexts->unsafe_size();
                entityToModuleStateStats->addStateModuleForEntity("CANT_ENQUEUE_MESSAGE_FOR_KAFKA",
                                                                  "AsyncPipelineFeederModule",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::exception);
        }
}


bool AsyncPipelineFeederModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

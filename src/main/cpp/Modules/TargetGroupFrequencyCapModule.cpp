#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "AdHistoryCassandraService.h"
#include "RealTimeEntityCacheService.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroupFrequencyCapModule.h"
#include "DateTimeUtil.h"
#include "JsonUtil.h"


std::string TargetGroupFrequencyCapModule::getName() {
        return "TargetGroupFrequencyCapModule";
}

TargetGroupFrequencyCapModule::TargetGroupFrequencyCapModule(
        RealTimeEntityCacheService<AdHistory>* adHistoryCassandraService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

        assertAndThrow(adHistoryCassandraService != NULL);
        this->adHistoryCassandraService = adHistoryCassandraService;
}


void TargetGroupFrequencyCapModule::filterTargetGroupsBasedOnFrequencyCap(std::shared_ptr<OpportunityContext> context) {

        auto keyOfAdHistory = std::make_shared<AdHistory>(context->device);
        context->adHistory = adHistoryCassandraService->readDataOptional(keyOfAdHistory);
        if (context->adHistory == nullptr) {
                context->adHistory = keyOfAdHistory;
        }

        markFilteredTgs(context);
}

void TargetGroupFrequencyCapModule::process(std::shared_ptr<OpportunityContext> context) {
        filterTargetGroupsBasedOnFrequencyCap (context);
}

bool TargetGroupFrequencyCapModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        std::vector<std::shared_ptr<AdEntry> > setOfAdEntriesForTg = context->adHistory->getAdEntriesByTgId (tg->getId ());
        MLOG(2) << "FrequencyCap module  setOfAdEntriesForTg.size() : " << setOfAdEntriesForTg.size ();
        if (setOfAdEntriesForTg.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSING_FOR_NO_AD_HISTORY",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        }

        long lastAdShownInSeconds = 0;
        for (auto adEntry : setOfAdEntriesForTg) {
                if (!adEntry->adInteractionType.empty()  //this is for backward compbatility, remove in 1 month
                    && !StringUtil::equalsIgnoreCase(adEntry->adInteractionType, EventLog::EVENT_TYPE_IMPRESSION)) {
                        //ignoring non impression ad interaction types
                        continue;
                }

                long currentAdShownInSecond = adEntry->timeAdShownInMillis / 1000;
                if (currentAdShownInSecond >= lastAdShownInSeconds) {
                        lastAdShownInSeconds = currentAdShownInSecond;
                }
        }

        TimeType secondHasPassedSinceAdShown = DateTimeUtil::getNowInSecond () - lastAdShownInSeconds;
        MLOG(2) << "secondHasPassedSinceAdShown :  " << secondHasPassedSinceAdShown << " , tg->getFrequencyInSec() " << tg->getFrequencyInSec ();
        if (secondHasPassedSinceAdShown >= tg->getFrequencyInSec ()) {
                //ad has been shown longer seconds than enough ago
                filterTg = false;
                MLOG(2) << "FrequencyCap module is filtering out this tg  "
                        << tg->getId () << " because tg->freq :  " << tg->getFrequencyInSec () << ""
                        " now-AdEntry-timeAdShownInMillis =" << secondHasPassedSinceAdShown;
                entityToModuleStateStats->addStateModuleForEntity ("PASSING_FOR_NOT_MEETING_FREQ_LIMIT",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));


        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_MEETING_FRQ_LIMIT",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        }

        return filterTg;
}

TargetGroupFrequencyCapModule::~TargetGroupFrequencyCapModule() {

}

#ifndef ActiveFilterModule_H
#define ActiveFilterModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class Campaign;
class Advertiser;
class Client;
class TargetGroupCacheService;
class CampaignCacheService;

#include "Object.h"
#include "CacheService.h"
#include "BidderModule.h"
class ActiveFilterModule;


class ActiveFilterModule : public BidderModule, public Object {

public:
TargetGroupCacheService* targetGroupCacheService;
CampaignCacheService* campaignCacheService;
CacheService<Advertiser>* advertiserCacheService;
CacheService<Client>* clientCacheService;

ActiveFilterModule(EntityToModuleStateStats* entityToModuleStateStats,
                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~ActiveFilterModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

bool isStatusActive(std::string statusOfEntity,
                    int entityId,
                    std::string entityType);
bool isTargetGroupStatusActive(std::shared_ptr<TargetGroup> tg);
bool isCampignStatusActive(std::shared_ptr<TargetGroup> tg);
bool isAdvertiserStatusActive(std::shared_ptr<TargetGroup> tg);
bool isClientStatusActive(std::shared_ptr<TargetGroup> tg);
bool isCurrent(Poco::DateTime startDate, Poco::DateTime endDate);
};

#endif

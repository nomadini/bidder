#include "FeatureToFeatureHistoryUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "StringUtil.h"
#include "Feature.h"
#include "OpportunityContext.h"

FeatureToFeatureHistoryUpdaterModuleWrapper::FeatureToFeatureHistoryUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureCategory = featureCategory;
}

std::string FeatureToFeatureHistoryUpdaterModuleWrapper::getName() {
        return "FeatureToFeatureHistoryUpdaterModuleWrapper ";
}



void FeatureToFeatureHistoryUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device == nullptr || context->siteDomain.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                                                                   "FeatureToFeatureHistoryUpdaterModuleWrapper",
                                                                   "ALL");

                throwEx("unknown siteDomain or device");
        }
        //this will save domain -> mgrs100m pair
        featureToFeatureHistoryUpdaterModule->process(
                Feature::generalTopLevelDomain,
                context->siteDomain,
                Feature::MGRS100,
                context->mgrs100m);

        //this will save mgrs100m->domain   pair
        featureToFeatureHistoryUpdaterModule->process(
                Feature::MGRS100,
                context->mgrs100m,
                Feature::generalTopLevelDomain,
                context->siteDomain);

        //very important to check we have siteCategory
        assertAndThrow(!context->siteCategory.empty());
        //this will save iab category -> mgrs100m pair
        //this will save iab category -> domain pair
        for(std::string siteCat : context->siteCategory) {
                featureToFeatureHistoryUpdaterModule->process(
                        Feature::IAB_CAT,
                        siteCat,
                        Feature::MGRS100,
                        context->mgrs100m);

                featureToFeatureHistoryUpdaterModule->process(
                        Feature::IAB_CAT,
                        siteCat,
                        Feature::generalTopLevelDomain,
                        context->siteDomain);
        }



}

bool FeatureToFeatureHistoryUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}
FeatureToFeatureHistoryUpdaterModuleWrapper::~FeatureToFeatureHistoryUpdaterModuleWrapper() {

}

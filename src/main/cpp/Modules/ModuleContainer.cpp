#include "GUtil.h"
#include "ModuleContainer.h"
#include "BidderRequestHandlerFactory.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include <thread>
#include "IpInfoFetcherModule.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "RecentVisitHistoryScoreReader.h"
#include "RecentVisitHistoryScorer.h"
#include "RecentVisitHistoryUpdater.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "GUtil.h"
#include "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "CreativeContentTypeFilter.h"
#include "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "BidderResponseModule.h"
#include "TargetingTypeLessTargetGroupFilter.h"
#include "KafkaProxyPopulator.h"
#include "TargetGroupSelectorModule.h"
#include "ConcurrentHashMap.h"
#include "FeatureRecorderPipeline.h"
#include "RecencyVisitScoreFilter.h"
#include "RecentVisitHistoryScoreReader.h"

#include "BidPriceCalculatorModule.h"
#include "TargetGroupPacingByBudgetModule.h"
#include "TargetGroupPacingByImpressionModule.h"
#include "TargetGroupFilterChainModule.h"
#include "TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule.h"
#include "BidderMainPipelineProcessor.h"
#include "CampaignImpressionCappingFilterModule.h"
#include "CampaignBudgetCappingFilterModule.h"
#include "MatchingIabCategoryFilter.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "DataReloadPipeline.h"
#include "OpenRtb2_3_0BidResponseBuilder.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "EventLog.h"
#include "BidEventRecorderModule.h"
#include "LastTimeIpSeenSource.h"
#include "Histogram.h"
#include "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper.h"
#include "DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper.h"
#include "BidResponseCreatorModule.h"
#include "ContextPopulatorModule.h"
#include "OpportunityContextBuilder.h"
#include "OpportunityContextBuilderOpenRtb2_3.h"
#include "NetworkUtil.h"

#include "ContextPopulatorModule.h"
#include "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "TargetGroupImpressionCappingFilterModule.h"
#include "TargetGroupBudgetCappingFilterModule.h"
#include "TargetGroupBudgetFilterChainModule.h"
#include "AsyncPipelineFeederModule.h"
#include "ProcessorInvokerModule.h"
#include "LastXSecondSeenVisitorModule.h"
#include "LastXSecondSeenIpVisitorModule.h"
#include "TargetGroupFrequencyCapModule.h"
#include "GlobalWhiteListModule.h"
#include "EventLogCreatorModule.h"
#include "DeviceSegmentHistoryCassandraService.h"
#include "LastTimeSeenSource.h"
#include "TargetGroup.h"
#include "BidderModule.h"
#include "TargetGroupFrequencyCapModule.h"
#include <tbb/concurrent_hash_map.h>
#include "AdHistoryCassandraService.h"
#include "TargetGroupFilterStatistic.h"
#include "EntityToModuleStateStats.h"
#include "EventLogCreatorModule.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DeviceHistoryOfVisitFeatureUpdaterModuleWrapper.h"

#include "TgBiddingPerformanceFilterMeasures.h"
#include "TargetGroupSelectorModule.h"
#include "GlobalWhiteListModule.h"
#include "AsyncPipelineFeederModule.h"
#include "GicapodsIdToExchangeIdMapModule.h"
#include "DeviceIdSegmentModule.h"
#include "TargetGroupGeoSegmentFilter.h"
#include "CrossWalkUpdaterModuleWrapper.h"
#include "TargetGroupBudgetFilterChainModule.h"
#include "BidPriceCalculatorModule.h"
#include "BidderResponseModule.h"
#include "LastXSecondSeenVisitorModule.h"
#include "DeviceHistoryOfGeoFeatureUpdaterModuleWrapper.h"
#include "CassandraDriverInterface.h"
#include "HttpUtilService.h"
#include "FeatureDeviceHistoryCassandraService.h"
#include "BeanFactory.h"
#include "BiddingMode.h"
#include "TargetGroupFilterChainModule.h"
#include "RealTimeEntityCacheService.h"
#include "TargetGroupSelectorModule.h"
#include "BidderLatencyRecorder.h"
#include "RecentVisitHistoryUpdater.h"
#include "EventLogCreatorModule.h"
#include "GlobalWhiteListModule.h"
#include "GicapodsIdToExchangeIdMapModule.h"
#include "CrossWalkUpdaterModuleWrapper.h"
#include "DeviceIdSegmentModule.h"
#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "FeatureToFeatureHistoryUpdaterModule.h"
#include "BidPriceCalculatorModule.h"
#include "TargetGroupPerMinuteBidCapFilter.h"
#include "EnqueueForScoringModule.h"
#include "FeatureToFeatureHistoryUpdaterModuleWrapper.h"
#include "AtomicLong.h"
#include "BannedDevicesFilterModule.h"
#include "TgFilterMeasuresService.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "ConcurrentHashMap.h"
#include "ActiveFilterModule.h"
#include "TargetGroupTargetingTypeValidatorModule.h"
#include "TargetGroupWithoutSegmentFilterModule.h"
#include "MGRSBuilderModule.h"
#include "EventLog.h"
#include "DeviceFeatureHistory.h"
#include "Feature.h"
#include "LastTimeIpSeenSource.h"
#include "BadIpByCountFilterModule.h"
#include "BidModeControllerService.h"
#include "BadDeviceByCountFilterModule.h"
#include "BadIpByCountPopulatorModule.h"
#include "BadDeviceByCountPopulatorModule.h"
#include "LastXSecondSeenIpVisitorModule.h"
#include "NoBidModeEnforcerModule.h"
#include "BidProbabilityEnforcerModule.h"
#include "ThrottlerModule.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "BidderApplicationContext.h"
#include "AdServerStatusChecker.h"
#include "ThrottlerModule.h"
#include "ConcurrentHashMap.h"
#include "DomainBlackListedFilter.h"
#include "BlockedAdvertiserDomainFilter.h"
#include "DomainWhiteListedFilter.h"
#include "CreativeBannerApiFilter.h"
#include "CreativeContentTypeFilter.h"
#include "AdPositionFilter.h"
#include "CreativeSizeFilter.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BlockedIabCategoryFilter.h"
#include "BlockedCreativeAttributeFilter.h"
#include "DeviceTypeFilter.h"
#include "GeoLocationFilter.h"
#include "TopMgrsFilter.h"
#include "OsTypeFilter.h"
#include "BidderWiretapModule.h"
#include "DayPartFilter.h"
#include "ServiceFactory.h"
#include "DataReloadService.h"

ModuleContainer::ModuleContainer() {
        enbaleSendingScoringMessageToKafka = true; //in tests..we might want to disable this
}

ModuleContainer::~ModuleContainer() {

}

void ModuleContainer::initializeModules() {
        sizeOfKafkaScoringQueue = std::make_unique<gicapods::AtomicLong>();
        allMarkersMap = std::make_shared<std::unordered_map<std::string, TgMarker*> >();

        filterNameToFailureCounts = std::make_shared<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic > >();

        numberOfBidRequestRecievedPerMinute = std::make_shared<gicapods::AtomicLong>(0);
        bidPercentage = std::make_shared<gicapods::AtomicLong>(0);
        std::string allAdServerHostUrls = beanFactory->configService->get("allAdServerHostUrls");
        LOG(INFO) << "allAdServerHostUrls : "<< allAdServerHostUrls;
        isNoBidModeIsTurnedOn = std::make_shared<gicapods::AtomicBoolean>(false);
        isReloadingDataInProgress = std::make_shared<gicapods::AtomicBoolean>(false);
        isMetricPersistenceInProgress = std::make_shared<gicapods::AtomicBoolean>(false);
        isDeliveryInfoSnapshotPersistenceInProgress = std::make_shared<gicapods::AtomicBoolean>(false);

        isFetchingDeliveryInfoFromPacerHealthy = std::make_shared<gicapods::AtomicBoolean>(true);
        int bidPercentagePropValue;
        beanFactory->configService->get("bidPercentage", bidPercentagePropValue);
        this->bidPercentage->setValue(bidPercentagePropValue);
        assertAndThrow(this->bidPercentage->getValue() > 0);
        totalNumberOfRequestsProcessedInFilterCountPeriod = std::make_shared<gicapods::AtomicLong>();

        recentVisitHistoryScoreReader =  std::make_unique<RecentVisitHistoryScoreReader> ();
        recentVisitHistoryScoreReader->aeroSpikeDriver = beanFactory->aeroSpikeDriver.get();
        recentVisitHistoryScoreReader->targetGroupRecencyModelMapCacheService = beanFactory->targetGroupRecencyModelMapCacheService.get();

        recencyVisitScoreFilter = std::make_unique<RecencyVisitScoreFilter>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        recencyVisitScoreFilter->aeroSpikeDriver = beanFactory->aeroSpikeDriver.get();
        recencyVisitScoreFilter->targetGroupCacheService = beanFactory->targetGroupCacheService;
        recencyVisitScoreFilter->recentVisitHistoryScoreReader = recentVisitHistoryScoreReader.get();


        //////////
        numberOfExceptionsInWritingEvents = std::make_shared<gicapods::AtomicLong>();
        numberOfBidsForPixelMatchingPurposePerHour = std::make_shared<gicapods::AtomicLong>();
        numberOfExceptionsInWritingMessagesInKafka = std::make_shared<gicapods::AtomicLong>();
        queueOfContexts = std::make_shared<tbb::concurrent_queue<std::shared_ptr<OpportunityContext> > >();
        tbb::concurrent_queue<std::shared_ptr<OpportunityContext> >::size_type size = 10000;


        targetGroupIdToFilterMeasuresMap =
                std::make_shared<tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> > >();

        tgFilterMeasuresService = std::make_shared<TgFilterMeasuresService>();
        bidderLatencyRecorder = std::make_unique<BidderLatencyRecorder>();
        bidderLatencyRecorder->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidderLatencyRecorder->entityToModuleStateStatsPersistenceService = beanFactory->entityToModuleStateStatsPersistenceService.get();
        bidderLatencyRecorder->latencyMinToReportToInfluxDb = beanFactory->configService->getAsInt("latencyInMillisMinToReportToInfluxDb");
        std::thread recordLatencyToInfluxThread (&BidderLatencyRecorder::recordLatencyToInfluxThread, bidderLatencyRecorder.get());
        recordLatencyToInfluxThread.detach ();



        tgBiddingPerformanceMetricInBiddingPeriodService = std::make_shared<TgBiddingPerformanceMetricInBiddingPeriodService>();
        tgBiddingPerformanceMetricInBiddingPeriodService->mySqlTgBiddingPerformanceMetricDtoService = beanFactory->mySqlTgBiddingPerformanceMetricDtoService.get();

        tgBiddingPerformanceMap =
                std::make_shared<gicapods::ConcurrentHashMap<int, TgBiddingPerformanceMetricInBiddingPeriod> >(
                        beanFactory->entityToModuleStateStats.get()
                        );
        tgBiddingPerformanceMetricInBiddingPeriodService->tgBiddingPerformanceMap = tgBiddingPerformanceMap;
        tgFilterMeasuresService->targetGroupIdToFilterMeasuresMap = targetGroupIdToFilterMeasuresMap.get();
        tgFilterMeasuresService->mySqlTgBiddingPerformanceMetricDtoService = beanFactory->mySqlTgBiddingPerformanceMetricDtoService.get();




        mapOfInterestingFeatures = std::make_shared<gicapods::ConcurrentHashMap<std::string, FeatureDeviceHistory> > (
                beanFactory->entityToModuleStateStats.get()
                );



        targetGroupFrequencyCapModule = std::make_unique<TargetGroupFrequencyCapModule> (
                beanFactory->adhistoryCacheService.get(),
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);


        throttlerModule = std::make_unique<ThrottlerModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        throttlerModule->numberOfBidRequestRecievedPerMinute = numberOfBidRequestRecievedPerMinute;
        int numberOfbidsRequestAllowedToProcessPerSecond = 0;
        beanFactory->configService->get("numberOfbidsRequestAllowedToProcessPerSecond",
                                        numberOfbidsRequestAllowedToProcessPerSecond);
        throttlerModule->numberOfbidsRequestAllowedToProcessPerSecond->setValue(numberOfbidsRequestAllowedToProcessPerSecond);


        adServerStatusChecker = std::make_unique<AdServerStatusChecker> (
                allAdServerHostUrls);

        adServerStatusChecker->
        adServerStatusCheckerIsDisabled->setValue(
                beanFactory->configService->getAsBooleanFromString("adServerStatusCheckerIsDisabled"));

        int adservStatusCheckIntervalInSecond = 0;
        beanFactory->configService->get("adservStatusCheckIntervalInSecond", adservStatusCheckIntervalInSecond);
        adServerStatusChecker->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        numberOfRequestsProcessingNow = std::make_shared<gicapods::AtomicLong>();

        bidProbabilityEnforcerModule = std::make_unique<BidProbabilityEnforcerModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        bidProbabilityEnforcerModule->bidPercentage = bidPercentage;


        targetGroupTargetingTypeValidatorModule = std::make_unique<TargetGroupTargetingTypeValidatorModule>(
                beanFactory->entityToModuleStateStats.get(),
                beanFactory->targetGroupCacheService);

        creativeSizeFilter = std::make_unique<CreativeSizeFilter> (beanFactory->targetGroupCreativeCacheService.get(),
                                                                   beanFactory->creativeCacheService.get(),
                                                                   filterNameToFailureCounts);

        targetGroupSelectorModule = std::make_unique<TargetGroupSelectorModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        targetGroupSelectorModule->campaignCacheService= beanFactory->campaignCacheService.get();
        targetGroupSelectorModule->targetGroupCacheService = beanFactory->targetGroupCacheService;
        targetGroupSelectorModule->creativeCacheService= beanFactory->creativeCacheService.get();
        targetGroupSelectorModule->advertiserCacheService = beanFactory->advertiserCacheService.get();
        targetGroupSelectorModule->creativeSizeFilter = creativeSizeFilter.get();
        targetGroupSelectorModule->targetGroupTargetingTypeValidatorModule = targetGroupTargetingTypeValidatorModule.get();




        globalWhiteListModule = std::make_unique<GlobalWhiteListModule> (
                beanFactory->globalWhiteListedCacheService.get(),
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);

        gicapodsIdToExchangeIdMapModule = std::make_unique<GicapodsIdToExchangeIdMapModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );
        targetGroupsEligibleForBidding =
                std::make_shared<std::vector<std::shared_ptr<TargetGroup> > >();

        auto uniqueNumberOfContext = std::make_shared<gicapods::AtomicLong>();
        auto openRtbBidRequestParser = std::make_shared<OpenRtb2_3_0::OpenRtbBidRequestParser>();
        openRtbBidRequestParser->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        ipInfoFetcherModule =
                std::make_unique<IpInfoFetcherModule>(
                        beanFactory->entityToModuleStateStats.get(),
                        beanFactory->configService.get(),
                        beanFactory->configService->getAsInt("failureThresholdPercentageForMaxMindCalls"));

        opportunityContextBuilderOpenRtb2_3 =
                std::make_shared<OpportunityContextBuilderOpenRtb2_3> (
                        beanFactory->entityToModuleStateStats.get());

        opportunityContextBuilderOpenRtb2_3->dateTimeService =   beanFactory->dateTimeService.get();
        opportunityContextBuilderOpenRtb2_3->ipInfoFetcherModule = ipInfoFetcherModule.get();
        opportunityContextBuilderOpenRtb2_3->openRtbBidRequestParser = openRtbBidRequestParser.get();
        opportunityContextBuilderOpenRtb2_3->uniqueNumberOfContext = uniqueNumberOfContext;

        opportunityContextBuilder = std::make_unique<OpportunityContextBuilder>();
        opportunityContextBuilder->opportunityContextBuilderOpenRtb2_3 = opportunityContextBuilderOpenRtb2_3.get();
        opportunityContextBuilder->uniqueNumberOfContext = uniqueNumberOfContext;
        opportunityContextBuilder->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        contextPopulatorModule = std::make_unique<ContextPopulatorModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );
        contextPopulatorModule->opportunityContextBuilder = opportunityContextBuilder.get();

        mgrsBuilderModule = std::make_unique<MGRSBuilderModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );
        mgrsBuilderModule->latLonToMGRSConverter = beanFactory->latLonToMGRSConverter.get();




        std::unique_ptr<LoadPercentageBasedSampler> bidEventRecorderModuleSampler =
                std::make_unique<LoadPercentageBasedSampler> (
                        beanFactory->configService->getAsInt("percentOfBidsToRecord"),
                        "percentOfBidsToRecord"
                        );

        std::unique_ptr<LoadPercentageBasedSampler> noBidEventRecorderModuleSampler
                = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentOfNoBidsToRecord"),
                "percentOfNoBidsToRecord"
                );

        auto wiretapBidRequestResponseSampler = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentOfBidsToBeWiretapped"),
                "percentOfBidsToBeWiretapped"
                );

        bidderWiretapModule = std::make_unique<BidderWiretapModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        bidderWiretapModule->wiretapBidRequestResponseSampler =
                std::move(wiretapBidRequestResponseSampler);

        bidderWiretapModule->wiretapCassandraService =
                beanFactory->wiretapCassandraService.get();

        eventLogCreatorModule = std::make_unique<EventLogCreatorModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts,
                NetworkUtil::getHostName(),
                beanFactory->appVersion);

        bidEventRecorderModule = std::make_unique<BidEventRecorderModule> (beanFactory->entityToModuleStateStats.get(),
                                                                           filterNameToFailureCounts);
        bidEventRecorderModule->eventLogCreatorModule = eventLogCreatorModule.get();
        bidEventRecorderModule->bidEventLogCassandraService = beanFactory->bidEventLogCassandraService.get();
        bidEventRecorderModule->bidEventRecorderModuleSampler = std::move(bidEventRecorderModuleSampler);
        bidEventRecorderModule->noBidEventRecorderModuleSampler = std::move(noBidEventRecorderModuleSampler);

        auto numberOfBidsForPixelMatchingPurposePerHourLimit =
                std::make_shared<gicapods::AtomicLong>();

        long numberOfBidsForPixelMatchingPurposePerHourLimitLong = 0;

        beanFactory->configService->get("numberOfBidsForPixelMatchingPurposePerHourLimit",
                                        numberOfBidsForPixelMatchingPurposePerHourLimitLong);

        numberOfBidsForPixelMatchingPurposePerHourLimit
        ->setValue(numberOfBidsForPixelMatchingPurposePerHourLimitLong);

        gicapodsIdToExchangeIdMapModule->numberOfBidsForPixelMatchingPurposePerHourLimit =
                numberOfBidsForPixelMatchingPurposePerHourLimit;

        gicapodsIdToExchangeIdMapModule->numberOfBidsForPixelMatchingPurposePerHour =
                numberOfBidsForPixelMatchingPurposePerHour;

        gicapodsIdToExchangeIdMapModule->gicapodsIdToExchangeIdsMapCassandraService =
                beanFactory->gicapodsIdToExchangeIdsMapCassandraService.get();

        gicapodsIdToExchangeIdMapModule->numberOfBidsForPixelMatchingPurposePerHour =
                numberOfBidsForPixelMatchingPurposePerHour;

        targetGroupFilterChainModule = std::make_unique<TargetGroupFilterChainModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );
        targetGroupFilterChainModule->bidderLatencyRecorder = bidderLatencyRecorder.get();
        targetGroupFilterChainModule->configService = beanFactory->configService.get();

        addFilters();
        targetGroupFilterChainModule->addFilters();


        deviceFeatureHistoryUpdaterModule =
                std::make_unique<DeviceHistoryUpdaterModule>(
                        beanFactory->deviceFeatureHistoryCassandraService.get()
                        );
        deviceFeatureHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();
        deviceFeatureHistoryUpdaterModule->deviceIdService = beanFactory->deviceIdService.get();


        deviceFeatureHistoryUpdaterModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        deviceFeatureHistoryUpdaterModule->deviceHistorySizeSampler =
                std::make_unique<LoadPercentageBasedSampler>(
                        beanFactory->configService->getAsInt("percentageOfRecordingDeviceWebVisitDataFeatures"),
                        "percentageOfRecordingDeviceWebVisitDataFeatures"
                        );

        deviceGeoFeatureUpdaterModule =
                std::make_unique<DeviceHistoryUpdaterModule >(
                        beanFactory->deviceFeatureHistoryCassandraService.get()
                        );
        deviceGeoFeatureUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();
        deviceGeoFeatureUpdaterModule->deviceIdService = beanFactory->deviceIdService.get();


        deviceGeoFeatureUpdaterModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        deviceGeoFeatureUpdaterModule->deviceHistorySizeSampler =
                std::make_unique<LoadPercentageBasedSampler>(
                        beanFactory->configService->getAsInt("percentageOfRecordingDeviceGeoVisitDataFeatures"),
                        "percentageOfRecordingDeviceGeoVisitDataFeatures"
                        );

        devicePlaceTagFeatureUpdaterModule =
                std::make_unique<DeviceHistoryUpdaterModule>(
                        beanFactory->deviceFeatureHistoryCassandraService.get()
                        );
        devicePlaceTagFeatureUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();
        devicePlaceTagFeatureUpdaterModule->deviceIdService = beanFactory->deviceIdService.get();


        devicePlaceTagFeatureUpdaterModule->entityToModuleStateStats =
                beanFactory->entityToModuleStateStats.get();

        devicePlaceTagFeatureUpdaterModule->deviceHistorySizeSampler =
                std::make_unique<LoadPercentageBasedSampler>(
                        beanFactory->configService->getAsInt("percentageOfRecordingDevicePlaceTagFeatures"),
                        "percentageOfRecordingDevicePlaceTagFeatures"
                        );

        visitFeatureHistoryOfDeviceUpdaterModuleWrapper = std::make_unique<VisitFeatureHistoryOfDeviceUpdaterModuleWrapper>
                                                                  (beanFactory->entityToModuleStateStats.get(),
                                                                  filterNameToFailureCounts);
        visitFeatureHistoryOfDeviceUpdaterModuleWrapper->visitFeatureDeviceHistoryUpdaterModule = deviceFeatureHistoryUpdaterModule.get();
        visitFeatureHistoryOfDeviceUpdaterModuleWrapper->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();

        geoFeatureHistoryOfDeviceUpdaterModuleWrapper =
                std::make_unique<GeoFeatureHistoryOfDeviceUpdaterModuleWrapper>(
                        Feature::MGRS100,
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);
        geoFeatureHistoryOfDeviceUpdaterModuleWrapper->deviceGeoFeatureUpdaterModule = deviceGeoFeatureUpdaterModule.get();
        geoFeatureHistoryOfDeviceUpdaterModuleWrapper->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();


        placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper =
                std::make_unique<PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper>(
                        Feature::PLACE_TAG,
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);
        placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper->devicePlaceTagFeatureUpdaterModule = devicePlaceTagFeatureUpdaterModule.get();
        placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();

        createVisitFeatureDeviceHistoryUpdaterModule();
        createGeoFeatureDeviceHistoryUpdaterModule();
        createPlaceTagFeatureDeviceHistoryUpdaterModule();

        createDomainFeatureToFeatureHistoryUpdaterModule();

        deviceHistoryOfVisitFeatureUpdaterModuleWrapper =
                std::make_unique<DeviceHistoryOfVisitFeatureUpdaterModuleWrapper>(
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        Feature::generalTopLevelDomain);
        deviceHistoryOfVisitFeatureUpdaterModuleWrapper->featureDeviceHistoryUpdaterModule = featureDeviceHistoryUpdaterModule.get();


        deviceHistoryOfGeoFeatureUpdaterModuleWrapper =
                std::make_unique<DeviceHistoryOfGeoFeatureUpdaterModuleWrapper>(
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        Feature::MGRS100);
        deviceHistoryOfGeoFeatureUpdaterModuleWrapper->featureDeviceHistoryUpdaterModule = geoFeatureDeviceHistoryUpdaterModule.get();


        deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper =
                std::make_unique<DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper>(
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        Feature::PLACE_TAG);
        deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper->featureDeviceHistoryUpdaterModule = placeTagFeatureDeviceHistoryUpdaterModule.get();

        domainFeatureToFeatureHistoryUpdaterModuleWrapper =
                std::make_unique<FeatureToFeatureHistoryUpdaterModuleWrapper>(
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        Feature::generalTopLevelDomain);
        domainFeatureToFeatureHistoryUpdaterModuleWrapper->featureToFeatureHistoryUpdaterModule = domainFeatureToFeatureHistoryUpdaterModule.get();





        crossWalkUpdaterModuleWrapper = std::make_unique<CrossWalkUpdaterModuleWrapper>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );


        bannedDevicesFilterModule = std::make_unique<BannedDevicesFilterModule>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );

        bannedDevicesFilterModule->bannedIps = beanFactory->bannedIps;
        bannedDevicesFilterModule->bannedDevices = beanFactory->bannedDevices;
        bannedDevicesFilterModule->bannedMgrs10ms = beanFactory->bannedMgrs10ms;

        crossWalkUpdaterModuleWrapper->crossWalkUpdaterModule = beanFactory->crossWalkUpdaterModule.get();

        this->activeFilterModule =
                std::make_unique<ActiveFilterModule> (beanFactory->entityToModuleStateStats.get(),
                                                      filterNameToFailureCounts);

        activeFilterModule->clientCacheService =
                beanFactory->clientCacheService.get();
        activeFilterModule->advertiserCacheService =
                beanFactory->advertiserCacheService.get();
        activeFilterModule->campaignCacheService =
                beanFactory->campaignCacheService.get();
        activeFilterModule->targetGroupCacheService =
                beanFactory->targetGroupCacheService;

        activeFilterModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        activeFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(activeFilterModule->getName(), (TgMarker*) activeFilterModule.get()));

        targetGroupWithoutSegmentFilterModule = std::make_unique<TargetGroupWithoutSegmentFilterModule> (
                beanFactory->targetGroupSegmentCacheService.get(),
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);




        deviceIdSegmentModule = std::make_unique<DeviceIdSegmentModule>(
                beanFactory->targetGroupSegmentCacheService.get(),
                beanFactory->segmentCacheService,
                filterNameToFailureCounts,
                beanFactory->entityToModuleStateStats.get(),
                beanFactory->realTimeEntityCacheServiceDeviceSegmentHistory.get(),
                beanFactory->configService->getAsLong("ignoreSegmentsOlderThanXMilliSeconds"));

        std::unique_ptr<LoadPercentageBasedSampler> segmentFetchThrottler = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentageOfRequestsToConsiderForSegmentTargeting"),
                "percentageOfRequestsToConsiderForSegmentTargeting"
                );

        deviceIdSegmentModule->segmentFetchThrottler = std::move(segmentFetchThrottler);
        deviceIdSegmentModule->targetGroupCacheService = beanFactory->targetGroupCacheService;
        deviceIdSegmentModule->targetGroupWithoutSegmentFilterModule = targetGroupWithoutSegmentFilterModule.get();


        targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule =
                std::make_unique<TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule>(
                        beanFactory->entityDeliveryInfoCacheService.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts
                        );
        targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule->getName(), (TgMarker*) targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule.get()));


        dayPartFilter = std::make_unique<DayPartFilter> (
                beanFactory->targetGroupDayPartCacheService.get(),
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        dayPartFilter->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(dayPartFilter->getName(), (TgMarker*) dayPartFilter.get()));

        dataReloadPipeline = std::make_unique<DataReloadPipeline>(
                beanFactory->entityToModuleStateStats.get());
        dataReloadPipeline->allMarkersMap = allMarkersMap;
        dataReloadPipeline->beanFactory = beanFactory;
        dataReloadPipeline->filterNameToFailureCounts = filterNameToFailureCounts;
        dataReloadPipeline->dateTimeService = beanFactory->dateTimeService.get();
        dataReloadPipeline->activeFilterModule = activeFilterModule.get();
        dataReloadPipeline->targetGroupWithoutSegmentFilterModule = targetGroupWithoutSegmentFilterModule.get();
        dataReloadPipeline->targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule = targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule.get();
        dataReloadPipeline->dayPartFilter = (BidderModule*) dayPartFilter.get();
        dataReloadPipeline->setTheModuleList();



        targetGroupGeoSegmentFilter = std::make_unique<TargetGroupGeoSegmentFilter>
                                              (beanFactory->targetGroupGeoSegmentCacheService.get(),
                                              beanFactory->entityToModuleStateStats.get(),
                                              filterNameToFailureCounts);

        targetGroupBudgetFilterChainModule = std::make_unique<TargetGroupBudgetFilterChainModule>
                                                     (beanFactory->entityToModuleStateStats.get(),
                                                     filterNameToFailureCounts);
        targetGroupBudgetFilterChainModule->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();
        targetGroupBudgetFilterChainModule->targetGroupCacheService= beanFactory->targetGroupCacheService;
        targetGroupBudgetFilterChainModule->bidderLatencyRecorder= bidderLatencyRecorder.get();

        targetGroupBudgetFilterChainModule->tgFilterMeasuresService = tgFilterMeasuresService.get();
        targetGroupBudgetFilterChainModule->entityDeliveryInfoCacheService= beanFactory->entityDeliveryInfoCacheService.get();

        //this filter chain is called in the main Pipeline
        targetGroupBudgetFilterChainModule->setUpFilters();

        bidPriceCalculatorModule = std::make_unique<BidPriceCalculatorModule> (
                beanFactory->entityToModuleStateStats.get(), filterNameToFailureCounts);

        auto enqueueForScoringModuleIsDisabled  =  std::make_shared<gicapods::AtomicBoolean>(false);
        enqueueForScoringModuleIsDisabled->setValue(beanFactory->configService->getAsBooleanFromString("enqueueForScoringModuleIsDisabled"));

        std::string kafkaProxyPopulatorAppHostUrl = beanFactory->configService->get ("kafkaProxyPopulatorAppHostUrl");

        std::unique_ptr<LoadPercentageBasedSampler> randSamplerForScroing = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentOfRequestsToSendToScoring"),
                "percentOfRequestsToSendToScoring"
                );

        kafkaProxyPopulator =
                std::make_unique<KafkaProxyPopulator>(
                        beanFactory->entityToModuleStateStats.get(),
                        kafkaProxyPopulatorAppHostUrl,
                        beanFactory->configService.get(),
                        beanFactory->configService->getAsInt("failureThresholdPercentageForKafkaProxyCalls")
                        );

        enqueueForScoringModule = std::make_unique<EnqueueForScoringModule>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts,
                kafkaProxyPopulator.get()
                );

        enqueueForScoringModule->enqueueForScoringModuleIsDisabled = enqueueForScoringModuleIsDisabled;
        enqueueForScoringModule->sampler = std::move(randSamplerForScroing);

        std::string kafkaEnqueueProcessIntervalInSeconds = beanFactory->configService->get("kafkaEnqueueProcessIntervalInSeconds");
        MLOG(10)<< "kafkaEnqueueProcessIntervalInSeconds : "<<kafkaEnqueueProcessIntervalInSeconds;
        enqueueForScoringModule->kafkaEnqueueProcessIntervalInSeconds= kafkaEnqueueProcessIntervalInSeconds;
        auto transAppQueue  = std::make_shared<tbb::concurrent_queue<std::shared_ptr<TransAppMessage> > > ();
        enqueueForScoringModule->transAppQueue = transAppQueue;
        enqueueForScoringModule->sizeOfKafkaScoringQueue = sizeOfKafkaScoringQueue;
        enqueueForScoringModule->numberOfExceptionsInWritingMessagesInKafka = numberOfExceptionsInWritingMessagesInKafka;

        int acceptableIntervalToBidOnAUserInSeconds;
        beanFactory->
        configService->get("acceptableIntervalToBidOnAUserInSeconds",
                           acceptableIntervalToBidOnAUserInSeconds);

        directLastTimeSeenSource = std::make_unique<LastTimeSeenSource>(
                beanFactory->aeroSpikeDriver.get(),
                "rtb2",
                "usersSeenSet",
                "bidInLastN"
                );
        lastTimeIpSeenSource = std::make_unique<LastTimeIpSeenSource>(
                beanFactory->aeroSpikeDriver.get(),
                "rtb2",
                "ipsSeenSet",
                "bidInLastN"
                );


        realTimeEventLogCacheService = beanFactory->realTimeEventLogCacheService.get();




        bidderResponseModule = std::make_unique<BidderResponseModule> (directLastTimeSeenSource.get(),
                                                                       acceptableIntervalToBidOnAUserInSeconds,
                                                                       beanFactory->entityToModuleStateStats.get(),
                                                                       filterNameToFailureCounts
                                                                       );

        bidderResponseModule->numberOfBidsForPixelMatchingPurposePerHour = numberOfBidsForPixelMatchingPurposePerHour;
        bidderResponseModule->realTimeEventLogCacheService = realTimeEventLogCacheService;

        bidderResponseModule->tgFilterMeasuresService = tgFilterMeasuresService.get();
        bidderResponseModule->tgBiddingPerformanceMetricInBiddingPeriodService = tgBiddingPerformanceMetricInBiddingPeriodService.get();

        lastXSecondSeenVisitorModule =
                std::make_unique<LastXSecondSeenVisitorModule>(
                        directLastTimeSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);

        lastXSecondSeenIpVisitorModule =
                std::make_unique<LastXSecondSeenIpVisitorModule>(
                        lastTimeIpSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);

        auto histoOfIpCounts = std::make_shared<Histogram>(0, 1000, 50);
        auto histoOfDeviceCounts = std::make_shared<Histogram>(0, 1000, 50);
        badIpByCountFilterModule =
                std::make_unique<BadIpByCountFilterModule>(
                        lastTimeIpSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        histoOfIpCounts);
        badDeviceByCountFilterModule =
                std::make_unique<BadDeviceByCountFilterModule>(
                        directLastTimeSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        histoOfDeviceCounts);
        badIpByCountPopulatorModule =
                std::make_unique<BadIpByCountPopulatorModule>(
                        lastTimeIpSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        histoOfIpCounts);
        badDeviceByCountPopulatorModule =
                std::make_unique<BadDeviceByCountPopulatorModule>(
                        directLastTimeSeenSource.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts,
                        histoOfDeviceCounts,
                        beanFactory->configService->getAsLong("maxAcceptableTimeSeenInLastTenMinute"));

        asyncPipelineFeederModule =
                std::make_unique<AsyncPipelineFeederModule>(
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);
        asyncPipelineFeederModule->queueOfContexts = queueOfContexts;


        targetGroupPerMinuteBidCapFilter = std::make_unique<TargetGroupPerMinuteBidCapFilter>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        targetGroupPerMinuteBidCapFilter->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();
        targetGroupPerMinuteBidCapFilter->tgFilterMeasuresService = tgFilterMeasuresService.get();

        targetingTypeLessTargetGroupFilter = std::make_unique<TargetingTypeLessTargetGroupFilter>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        targetingTypeLessTargetGroupFilter->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();



        std::string allAdserverDomainsInOneString;
        beanFactory->configService->get("nomadiniAdServerDomains", allAdserverDomainsInOneString);
        auto nomadiniAdServerDomains = StringUtil::split(allAdserverDomainsInOneString, ",");
        assertAndThrow(!nomadiniAdServerDomains.empty());



        openRtb2_3_0BidResponseBuilder = std::make_shared<OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder>(
                nomadiniAdServerDomains,
                beanFactory->entityToModuleStateStats.get(),
                beanFactory->advertiserCacheService.get(),
                beanFactory->creativeCacheService.get(),
                beanFactory->campaignCacheService.get());
        auto openRtbBidResponseJsonConverter = std::make_shared<OpenRtb2_3_0::OpenRtbBidResponseJsonConverter>();
        openRtb2_3_0BidResponseBuilder->openRtbBidResponseJsonConverter = openRtbBidResponseJsonConverter.get();


        bidResponseCreatorModule =  std::make_unique<BidResponseCreatorModule>(
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);

        bidResponseCreatorModule->openRtb2_3_0BidResponseBuilder = openRtb2_3_0BidResponseBuilder;





        recentVisitHistoryScorer =  std::make_unique<RecentVisitHistoryScorer> (beanFactory->entityToModuleStateStats.get(),
                                                                                filterNameToFailureCounts);

        recentVisitHistoryScorer->aeroSpikeDriver = beanFactory->aeroSpikeDriver.get();
        recentVisitHistoryScorer->targetGroupCacheService = beanFactory->targetGroupCacheService;
        recentVisitHistoryScorer->recentVisitHistoryScoreReader = recentVisitHistoryScoreReader.get();

        recentVisitHistoryUpdater
                =  std::make_unique<RecentVisitHistoryUpdater> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );

        recentVisitHistoryUpdater->aeroSpikeDriver = beanFactory->aeroSpikeDriver.get();
        recentVisitHistoryUpdater->targetGroupCacheService = beanFactory->targetGroupCacheService;

        processorInvokerModule = std::make_unique<ProcessorInvokerModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts
                );
        processorInvokerModule->bidderLatencyRecorder = bidderLatencyRecorder.get();


        bidderMainPipelineProcessor = std::make_unique<BidderMainPipelineProcessor> ();
        bidderMainPipelineProcessor->moduleContainer = this;
        bidderMainPipelineProcessor->bidderLatencyRecorder = bidderLatencyRecorder.get();
        bidderMainPipelineProcessor->tgFilterMeasuresService = tgFilterMeasuresService.get();
        bidderMainPipelineProcessor->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidderMainPipelineProcessor->configService = beanFactory->configService.get();
        bidderMainPipelineProcessor->acceptableModuleLatencyInMillis = beanFactory->configService->getAsInt("acceptableModuleLatencyInMillis");
        bidderMainPipelineProcessor->setTheModuleList();



        processorInvokerModule->targetGroupCacheService = beanFactory->targetGroupCacheService;
        processorInvokerModule->targetGroupsEligibleForBidding = targetGroupsEligibleForBidding;
        processorInvokerModule->bidderMainPipelineProcessor = bidderMainPipelineProcessor.get();

        featureRecorderPipeline = std::make_unique<FeatureRecorderPipeline> (
                beanFactory->entityToModuleStateStats.get(),
                this
                );

        featureRecorderPipeline->setTheModuleList();

        bidModeControllerService = std::make_unique<BidModeControllerService>();
        bidModeControllerService->sizeOfKafkaScoringQueue = sizeOfKafkaScoringQueue;
        bidModeControllerService->queueSizeWaitingForAck = beanFactory->queueSizeWaitingForAck;
        bidModeControllerService->configService = beanFactory->configService.get();
        bidModeControllerService->adServerStatusChecker = adServerStatusChecker.get();
        bidModeControllerService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidModeControllerService->isNoBidModeIsTurnedOn = isNoBidModeIsTurnedOn;
        bidModeControllerService->isReloadingDataInProgress = isReloadingDataInProgress;
        bidModeControllerService->isMetricPersistenceInProgress = isMetricPersistenceInProgress;
        NULL_CHECK(serviceFactory);
        NULL_CHECK(serviceFactory->dataReloadService);
        bidModeControllerService->isReloadProcessHealthy = serviceFactory->dataReloadService->isReloadProcessHealthy;
        bidModeControllerService->isDeliveryInfoSnapshotPersistenceInProgress = isDeliveryInfoSnapshotPersistenceInProgress;
        bidModeControllerService->isFetchingDeliveryInfoFromPacerHealthy = isFetchingDeliveryInfoFromPacerHealthy;
        bidModeControllerService->numberOfExceptionsInWritingEvents = numberOfExceptionsInWritingEvents;
        bidModeControllerService->numberOfExceptionsInWritingMessagesInKafka = numberOfExceptionsInWritingMessagesInKafka;
        bidModeControllerService->numberOfCuncurrentReuqestsNow = beanFactory->cassandraDriver->numberOfCuncurrentReuqestsNow;
        bidModeControllerService->queueOfContexts = queueOfContexts;
        bidModeControllerService->init();

        bidModeControllerService->allBiddingMonitors->push_back(geoFeatureFilter.get());
        bidModeControllerService->allBiddingMonitors->push_back(ipInfoFetcherModule.get());
        bidModeControllerService->allBiddingMonitors->push_back(kafkaProxyPopulator.get());

        noBidModeEnforcerModule = std::make_unique<NoBidModeEnforcerModule> (
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts);
        noBidModeEnforcerModule->bidModeControllerService = bidModeControllerService.get();



        bidRequestHandlerPipelineProcessor = std::make_unique<BidRequestHandlerPipelineProcessor> ();
        bidRequestHandlerPipelineProcessor->maxAllowedTimeToProcessARequestInMillis->setValue(
                beanFactory->configService->getAsInt("maxAllowedTimeToProcessARequestInMillis")
                );

        bidRequestHandlerPipelineProcessor->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidRequestHandlerPipelineProcessor->bidderLatencyRecorder =  this->bidderLatencyRecorder.get();
        bidRequestHandlerPipelineProcessor->throttlerModule =  throttlerModule.get();
        bidRequestHandlerPipelineProcessor->noBidModeEnforcerModule = noBidModeEnforcerModule.get();
        bidRequestHandlerPipelineProcessor->bidProbabilityEnforcerModule = bidProbabilityEnforcerModule.get();
        bidRequestHandlerPipelineProcessor->contextPopulatorModule = contextPopulatorModule.get();
        bidRequestHandlerPipelineProcessor->processorInvokerModule = processorInvokerModule.get();
        bidRequestHandlerPipelineProcessor->bidResponseCreatorModule = bidResponseCreatorModule.get();
        bidRequestHandlerPipelineProcessor->bidderWiretapModule = bidderWiretapModule.get();
        bidRequestHandlerPipelineProcessor->setTheModuleList();

}

void ModuleContainer::addFilters() {


        std::unique_ptr<LoadPercentageBasedSampler> geoFeatureFilterRemoteCallThrottler = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentageOfRequestsToConsiderForGeoTargeting"),
                "percentageOfRequestsToConsiderForGeoTargeting"
                );

        geoFeatureFilter = std::make_shared<GeoFeatureFilter>(
                beanFactory->placesCachedResultAeroCacheService.get(),
                beanFactory->configService.get(),
                beanFactory->entityToModuleStateStats.get(),
                filterNameToFailureCounts,
                beanFactory->configService->getAsInt("failureThresholdPercentageForPlaceFinderCalls"));
        geoFeatureFilter->geoFeatureFilterRemoteCallThrottler = std::move(geoFeatureFilterRemoteCallThrottler);
        geoFeatureFilter->targetGroupToGeoCollectionKeys= beanFactory->targetGroupToGeoCollectionKeys.get();
        geoFeatureFilter->targetGroupIdsWithGeoCollectionAssignments= beanFactory->targetGroupIdsWithGeoCollectionAssignments.get();

        geoFeatureFilter->placeFinderAppHostUrl = beanFactory->configService->get ("placeFinderAppHostUrl");
        targetGroupFilterChainModule->geoFeatureFilter = geoFeatureFilter;


        domainBlackListedFilter =
                std::make_shared<DomainBlackListedFilter> (
                        beanFactory->targetGroupBWListCacheService.get(), filterNameToFailureCounts);

        targetGroupFilterChainModule->domainBlackListedFilter = domainBlackListedFilter;
        matchingIabCategoryFilter =
                std::make_shared<MatchingIabCategoryFilter> (filterNameToFailureCounts);

        targetGroupFilterChainModule->matchingIabCategoryFilter = matchingIabCategoryFilter;


        domainWhiteListedFilter = std::make_shared<DomainWhiteListedFilter> (
                beanFactory->targetGroupBWListCacheService.get(), filterNameToFailureCounts);
        targetGroupFilterChainModule->domainWhiteListedFilter = domainWhiteListedFilter;

        creativeBannerApiFilter = std::make_shared<CreativeBannerApiFilter> (
                beanFactory->targetGroupCreativeCacheService.get(),
                beanFactory->creativeCacheService.get(),
                filterNameToFailureCounts);

        targetGroupFilterChainModule->creativeBannerApiFilter = creativeBannerApiFilter;

        creativeContentTypeFilter = std::make_shared<CreativeContentTypeFilter> (
                beanFactory->targetGroupCreativeCacheService.get(),
                beanFactory->creativeCacheService.get(),
                filterNameToFailureCounts);

        targetGroupFilterChainModule->creativeContentTypeFilter = creativeContentTypeFilter;


        adPositionFilter = std::make_shared<AdPositionFilter> (filterNameToFailureCounts);
        targetGroupFilterChainModule->adPositionFilter = adPositionFilter;


        blockedAdvertiserDomainFilter = std::make_shared<BlockedAdvertiserDomainFilter> (
                beanFactory->advertiserCacheService.get(),
                beanFactory->campaignCacheService.get(),
                filterNameToFailureCounts
                );
        targetGroupFilterChainModule->blockedAdvertiserDomainFilter =
                blockedAdvertiserDomainFilter;



        blockedIabCategoryFilter = std::make_shared<BlockedIabCategoryFilter> (filterNameToFailureCounts);
        targetGroupFilterChainModule->blockedIabCategoryFilter = blockedIabCategoryFilter;



        targetGroupFilterChainModule->creativeSizeFilter = creativeSizeFilter;

        blockedBannerAdTypeFilter = std::make_shared<BlockedBannerAdTypeFilter> (
                (CreativeSizeFilter*) creativeSizeFilter.get(),
                filterNameToFailureCounts);
        targetGroupFilterChainModule->blockedBannerAdTypeFilter = blockedBannerAdTypeFilter;

        blockedCreativeAttributeFilter = std::make_shared<BlockedCreativeAttributeFilter>
                                                 ((CreativeSizeFilter*)creativeSizeFilter.get(),
                                                 filterNameToFailureCounts);

        targetGroupFilterChainModule->blockedCreativeAttributeFilter =
                blockedCreativeAttributeFilter;

        deviceTypeFilter = std::make_shared<DeviceTypeFilter> (
                filterNameToFailureCounts);
        targetGroupFilterChainModule->deviceTypeFilter = deviceTypeFilter;

        osTypeFilter = std::make_shared<OsTypeFilter> (
                filterNameToFailureCounts);
        targetGroupFilterChainModule->osTypeFilter = osTypeFilter;

        geoLocationFilter = std::make_shared<GeoLocationFilter> (
                beanFactory->targetGroupGeoLocationCacheService.get(),
                beanFactory->geoLocationCacheService.get(),
                filterNameToFailureCounts);
        targetGroupFilterChainModule->geoLocationFilter = geoLocationFilter;

        topMgrsFilter = std::make_shared<TopMgrsFilter> (
                beanFactory->targetGroupMgrsSegmentCacheService.get(),
                filterNameToFailureCounts);
        targetGroupFilterChainModule->topMgrsFilter = topMgrsFilter;
}


void ModuleContainer::createGeoFeatureDeviceHistoryUpdaterModule() {
        geoFeatureDeviceHistoryUpdaterModule =
                std::make_unique<FeatureDeviceHistoryUpdaterModule>(
                        beanFactory->featureDeviceHistoryCassandraService.get());

        geoFeatureDeviceHistoryUpdaterModule->mapOfInterestingFeatures = mapOfInterestingFeatures;
        geoFeatureDeviceHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        long negativeFeatureToStoreSamplerPercentage = 10;

        beanFactory->configService->get("negativeGeoFeatureToStoreSamplerPercentage",
                                        negativeFeatureToStoreSamplerPercentage);
        std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler =
                std::make_unique<LoadPercentageBasedSampler>(negativeFeatureToStoreSamplerPercentage,
                                                             "negativeGeoFeatureToStoreSamplerPercentage");
        LOG(ERROR)<< "negativeFeatureToStoreSamplerPercentage : "<< negativeFeatureToStoreSamplerPercentage;

        geoFeatureDeviceHistoryUpdaterModule->randomFeatureSampler = std::move(randomFeatureSampler);



        geoFeatureDeviceHistoryUpdaterModule->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();
}

void ModuleContainer::createPlaceTagFeatureDeviceHistoryUpdaterModule() {
        placeTagFeatureDeviceHistoryUpdaterModule =
                std::make_unique<FeatureDeviceHistoryUpdaterModule>(
                        beanFactory->featureDeviceHistoryCassandraService.get());

        placeTagFeatureDeviceHistoryUpdaterModule->mapOfInterestingFeatures = mapOfInterestingFeatures;
        placeTagFeatureDeviceHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        int negativeFeatureToStoreSamplerPercentage = beanFactory->configService->getAsInt("negativePlaceFeatureToStoreSamplerPercentage");
        std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler =
                std::make_unique<LoadPercentageBasedSampler>(negativeFeatureToStoreSamplerPercentage,
                                                             "negativeFeatureToStore");
        LOG(ERROR)<< "negativeFeatureToStoreSamplerPercentage : "<< negativeFeatureToStoreSamplerPercentage;

        placeTagFeatureDeviceHistoryUpdaterModule->randomFeatureSampler = std::move(randomFeatureSampler);

        placeTagFeatureDeviceHistoryUpdaterModule->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();

}

void ModuleContainer::createDomainFeatureToFeatureHistoryUpdaterModule() {
        domainFeatureToFeatureHistoryUpdaterModule =
                std::make_unique<FeatureToFeatureHistoryUpdaterModule>(
                        beanFactory->featureToFeatureHistoryCassandraService.get());

        auto mapOfInterestingFeatures = std::make_shared<gicapods::ConcurrentHashMap<std::string, Feature> > ();
        domainFeatureToFeatureHistoryUpdaterModule->mapOfInterestingFeatures = mapOfInterestingFeatures;
        domainFeatureToFeatureHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        int negativeFeatureToStoreSamplerPercentage =
                beanFactory->configService->getAsInt("negativeDomainFeatureToFeatureHistoryToSaveSamplerPercentage");
        std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler =
                std::make_unique<LoadPercentageBasedSampler>(negativeFeatureToStoreSamplerPercentage,
                                                             "negativeFeatureToStore");
        LOG(ERROR)<< "negativeFeatureToStoreSamplerPercentage : "<< negativeFeatureToStoreSamplerPercentage;

        domainFeatureToFeatureHistoryUpdaterModule->randomFeatureSampler = std::move(randomFeatureSampler);

        domainFeatureToFeatureHistoryUpdaterModule->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();

}

void ModuleContainer::createVisitFeatureDeviceHistoryUpdaterModule() {
        featureDeviceHistoryUpdaterModule =
                std::make_unique<FeatureDeviceHistoryUpdaterModule>(
                        beanFactory->featureDeviceHistoryCassandraService.get());

        featureDeviceHistoryUpdaterModule->mapOfInterestingFeatures = mapOfInterestingFeatures;
        featureDeviceHistoryUpdaterModule->featureGuardService = beanFactory->featureGuardService.get();

        long negativeFeatureToStoreSamplerPercentage = 10;

        beanFactory->configService->get("negativeFeatureToStoreSamplerPercentage",
                                        negativeFeatureToStoreSamplerPercentage);
        std::unique_ptr<LoadPercentageBasedSampler> randomFeatureSampler =
                std::make_unique<LoadPercentageBasedSampler>(negativeFeatureToStoreSamplerPercentage, "randomFeature");
        LOG(ERROR)<< "negativeFeatureToStoreSamplerPercentage : "<< negativeFeatureToStoreSamplerPercentage;

        featureDeviceHistoryUpdaterModule->randomFeatureSampler = std::move(randomFeatureSampler);



        featureDeviceHistoryUpdaterModule->entityToModuleStateStats= beanFactory->entityToModuleStateStats.get();

}

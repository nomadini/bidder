#ifndef DeviceIdSegmentModule_H
#define DeviceIdSegmentModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "BidderModule.h"
class TargetGroupSegmentCacheService;
class TargetGroupCacheService;
class SegmentCacheService;
class DeviceIdSegmentModuleHelper;
#include <tbb/concurrent_hash_map.h>
#include "RealTimeEntityCacheService.h"
#include "EntityProviderService.h"
#include "Object.h"
class TargetGroupWithoutSegmentFilterModule;
class DeviceSegmentHistory;
class DeviceIdSegmentModule;
class DeviceIdTargetGroupWithSegmentProvider;

#include "LoadPercentageBasedSampler.h"
class DeviceIdSegmentModule : public BidderModule, public Object {

public:
std::unique_ptr<LoadPercentageBasedSampler> segmentFetchThrottler;
TargetGroupSegmentCacheService* targetGroupSegmentCacheService;
TargetGroupCacheService* targetGroupCacheService;
TargetGroupWithoutSegmentFilterModule* targetGroupWithoutSegmentFilterModule;
//this service uses both aerospike and cassandra to read the segemnts as fast as it can
// it uses the aerospike as the caching layer
RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory;
SegmentCacheService* segmentCacheService;
std::unique_ptr<DeviceIdSegmentModuleHelper> deviceIdSegmentModuleHelper;
std::unique_ptr<DeviceIdTargetGroupWithSegmentProvider> deviceIdTargetGroupWithSegmentProvider;

DeviceIdSegmentModule(TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
                      SegmentCacheService* segmentCacheService,
                      std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                      EntityToModuleStateStats* entityToModuleStateStats,
                      RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory,
                      long ignoreSegmentsOlderThanXMilliSeconds);


// void passTargetGroupsWithoutSegment(std::shared_ptr<OpportunityContext> context);
void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(
        std::shared_ptr<TargetGroup> tg,
        std::shared_ptr<OpportunityContext> context);
std::string getName();

virtual ~DeviceIdSegmentModule();

};

#endif


#include "TargetGroupTargetingTypeValidatorModule.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "JsonArrayUtil.h"
#include "EntityToModuleStateStats.h"
#include "EventLog.h"

TargetGroupTargetingTypeValidatorModule::TargetGroupTargetingTypeValidatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        TargetGroupCacheService* targetGroupCacheService) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->targetGroupCacheService = targetGroupCacheService;

}


void TargetGroupTargetingTypeValidatorModule::validateTargetingType(
        std::shared_ptr<OpportunityContext> context) {

        auto typesOfTargetingChosen =
                context->getTargetingTypesForTg(context->chosenTargetGroupId);

        auto tgLoadedFromDb = targetGroupCacheService->findByEntityId(context->chosenTargetGroupId);

        if (tgLoadedFromDb->typesOfTargeting->empty()) {
                entityToModuleStateStats->
                addStateModuleForEntity("CHOSEN_TG_NOT_HAVING_ANY_LOADED_TYPES_OF_TARGETING",
                                        "TargetGroupTargetingTypeValidatorModule",
                                        "tg" + _toStr(context->chosenTargetGroupId),
                                        EntityToModuleStateStats::exception);
                EXIT("NO_TYPES_OF_TARGETING_LOADED_FOR_CHOSEN_TG: tg" + _toStr(context->chosenTargetGroupId));
        }

        if (typesOfTargetingChosen->empty()) {
                context->bidDecisionResult = EventLog::EVENT_TYPE_NO_BID;
                return;
                LOG_EVERY_N(ERROR, 1) <<
                        "NO_TYPES_OF_TARGETING_WAS_SET_IN_BIDDING_PROCESS: tg" + _toStr(context->chosenTargetGroupId);
        }

        bool validBasedOnMatchingTargeting = false;
        for (std::unordered_set<std::string>::iterator iter = typesOfTargetingChosen->begin();
             iter != typesOfTargetingChosen->end();
             iter++) {
                if (tgLoadedFromDb->typesOfTargeting->exists(*iter)) {
                        validBasedOnMatchingTargeting = true;
                        break;
                }
        }


        if (!validBasedOnMatchingTargeting) {

                std::vector<std::string> allKeys;
                for (std::unordered_set<std::string>::iterator iter = typesOfTargetingChosen->begin();
                     iter != typesOfTargetingChosen->end();
                     iter++) {
                        allKeys.push_back(*iter);
                }


                auto typesOfTargetingChosenStr = JsonArrayUtil::convertListToJson(allKeys);
                entityToModuleStateStats->
                addStateModuleForEntity("BAD_TYPES_OF_TARGETING_CHOSEN_" +typesOfTargetingChosenStr,
                                        "TargetGroupTargetingTypeValidatorModule",
                                        "tg" + _toStr(context->chosenTargetGroupId),
                                        EntityToModuleStateStats::exception);
                LOG_EVERY_N(ERROR, 1)<<"not matching type : typesOfTargetingChosen : "<<
                        typesOfTargetingChosenStr << " vs tgLoadedFromDb->typesOfTargeting "<<
                        JsonArrayUtil::convertListToJson(tgLoadedFromDb->typesOfTargeting->getKeys());
                throwEx("BAD_TYPES_OF_TARGETING_CHOSEN_tg" + _toStr(context->chosenTargetGroupId) + "_type : "+ typesOfTargetingChosenStr);
        }

}
std::string TargetGroupTargetingTypeValidatorModule::getName() {
        return "TargetGroupTargetingTypeValidatorModule";
}

TargetGroupTargetingTypeValidatorModule::~TargetGroupTargetingTypeValidatorModule() {

}

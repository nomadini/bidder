#include "RecentVisitHistoryUpdater.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "AerospikeDriver.h"
#include "Feature.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "RecentVisitHistoryScoreCard.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "Device.h"
#include "TgScoreEligibilityRecord.h"
#include "TgModelEligibilityRecord.h"
#include "RecencyModelCacheService.h"
#include "DeviceRecentVisitHistory.h"
#include "RecentVisitHistoryScoreReader.h"
#include "RecentVisitHistory.h"

std::string RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_BIN_NAME = "s1";
std::string RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SCORE_BIN_NAME = "s2";
std::string RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_NAMESPACE = "test";
std::string RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SET = "visitSet";

RecentVisitHistoryUpdater::RecentVisitHistoryUpdater(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string RecentVisitHistoryUpdater::getName() {
        return "RecentVisitHistoryUpdater";
}

void RecentVisitHistoryUpdater::process(std::shared_ptr<OpportunityContext> context) {
        std::string allRecentHistories = writeAdHistory(Feature::generalTopLevelDomain,
                                                        context->siteDomain,
                                                        context->device);
        MLOG(3) << "allRecentHistories read : "<< allRecentHistories;
}

std::string RecentVisitHistoryUpdater::writeAdHistory(
        std::string featureType,
        std::string featureValue,
        std::shared_ptr<Device> device) {
        assertAndThrow(!featureValue.empty());

        auto recentVisitHistory = std::make_shared<RecentVisitHistory>();
        recentVisitHistory->featureName = featureValue;
        recentVisitHistory->featureType = featureType;
        recentVisitHistory->timeSeenInMillis = DateTimeUtil::getNowInMilliSecond();
        std::string jsonFeatureObject  = recentVisitHistory->toJson();

        MLOG(10)<< "jsonFeatureObject : "<< jsonFeatureObject;

        std::string valueWrittenAfterCleanUp = aeroSpikeDriver->addValueAndRead(RECENT_VISIT_HISTORY_NAMESPACE,
                                                                                RECENT_VISIT_HISTORY_SET,
                                                                                device->getDeviceId(),
                                                                                RECENT_VISIT_HISTORY_BIN_NAME,
                                                                                jsonFeatureObject,
                                                                                10000);
        entityToModuleStateStats->addStateModuleForEntity("pushed_to_queue_for_write",
                                                          "RecentVisitHistoryUpdater",
                                                          EntityToModuleStateStats::all);

        return valueWrittenAfterCleanUp;
}
bool RecentVisitHistoryUpdater::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}
RecentVisitHistoryUpdater::~RecentVisitHistoryUpdater() {

}

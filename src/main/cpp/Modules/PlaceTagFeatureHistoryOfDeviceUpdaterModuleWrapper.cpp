#include "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "OpportunityContext.h"


PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper::PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper(
        std::string featureType,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureType = featureType;
}

std::string PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper::getName() {
        return "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper";
}

void PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {

        if (context->device == nullptr) {
                throwEx("unknown device");
        }

        if(context->tagIds.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_TAG",
                                                                   "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                                   "ALL");
                return;
        }
        for(auto tagId : context->tagIds) {
                devicePlaceTagFeatureUpdaterModule->process(featureType,
                                                            StringUtil::toStr(tagId),
                                                            context->device);
        }

        entityToModuleStateStats->addStateModuleForEntity ("ADDING_DEVICE_FEATURE_HISTORY",
                                                           "PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                           "ALL");

}
bool PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper::~PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper() {

}

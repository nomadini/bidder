#ifndef FeatureToFeatureHistoryUpdaterModuleWrapper_H
#define FeatureToFeatureHistoryUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
class Feature;
class FeatureDeviceHistory;
#include <memory>
#include <string>
#include "FeatureToFeatureHistoryUpdaterModule.h"
#include "Object.h"
class FeatureToFeatureHistoryUpdaterModuleWrapper : public BidderModule, public Object {

private:

public:
std::string featureCategory;
FeatureToFeatureHistoryUpdaterModule* featureToFeatureHistoryUpdaterModule;

FeatureToFeatureHistoryUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory);

std::string getName();

std::string toString();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~FeatureToFeatureHistoryUpdaterModuleWrapper();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);


};

#endif

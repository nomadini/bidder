#include "DeviceIdSegmentModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"

#include "GUtil.h"
#include <boost/foreach.hpp>
#include "DeviceSegmentHistoryCassandraService.h"
#include "TargetGroupSegmentCacheService.h"
#include "TargetGroupCacheService.h"
#include "SegmentCacheService.h"
#include "ConverterUtil.h"
#include "DeviceIdSegmentModuleHelper.h"
#include "DeviceIdTargetGroupWithSegmentProvider.h"
#include "TargetGroupWithoutSegmentFilterModule.h"
#include "DeviceSegmentHistory.h"

DeviceIdSegmentModule::DeviceIdSegmentModule(TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
                                             SegmentCacheService* segmentCacheService,
                                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                             EntityToModuleStateStats* entityToModuleStateStats,
                                             RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory,
                                             long ignoreSegmentsOlderThanXMilliSeconds)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->targetGroupSegmentCacheService = targetGroupSegmentCacheService;
        this->segmentCacheService = segmentCacheService;
        deviceIdSegmentModuleHelper = std::make_unique<DeviceIdSegmentModuleHelper> (
                targetGroupSegmentCacheService,
                segmentCacheService,
                filterNameToFailureCounts,
                entityToModuleStateStats);

        deviceIdTargetGroupWithSegmentProvider = std::make_unique<DeviceIdTargetGroupWithSegmentProvider> (
                targetGroupSegmentCacheService,
                segmentCacheService,
                filterNameToFailureCounts,
                entityToModuleStateStats,
                deviceIdSegmentModuleHelper.get(),
                realTimeEntityCacheServiceDeviceSegmentHistory,
                ignoreSegmentsOlderThanXMilliSeconds);

}

DeviceIdSegmentModule::~DeviceIdSegmentModule() {

}

/**
 *
 * reads the uniqueNameOfSegment list for the device and filter out all the
 * target groups that don't have the user segments, in case the target groups
 * have assigned segments .
 */
void DeviceIdSegmentModule::process(
        std::shared_ptr<OpportunityContext> context) {

        if (segmentFetchThrottler->isPartOfSample()) {
                //this is a very expensive call that we want to avoid based on a sampler
                //there might be some targetgroups that are not doing segment targeting
                //and  dont get enough bids because of slowness of bid processing
                //as a direct result of this call
                context->allTgsWithCorrectSegmentsForDeviceAsMap  =
                        deviceIdTargetGroupWithSegmentProvider->readSegmentsAndFetchAllTgsAttached(context);
        }

        markFilteredTgs(context);

}

bool DeviceIdSegmentModule::filterTargetGroup(
        std::shared_ptr<TargetGroup> tg,
        std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> >::accessor a;
        if (context->allTgsWithCorrectSegmentsForDeviceAsMap.find (a, tg->getId ())) {

                entityToModuleStateStats->addStateModuleForEntity (
                        "PASS_TG_MATCH_DEVICE_SEGMENT",
                        getName (),
                        StringUtil::toStr ("tg") +
                        StringUtil::toStr (tg->getId ()));
                context->addTargetingTypeForTg(tg->getId(),
                                               TargetGroup::TARGETTING_TYPE_SEGMENT_TARGETING);
                filterTg = false;
        } else {

                //we check if tg belongs to segmentLess tgs , we pass it
                if (targetGroupWithoutSegmentFilterModule->targetGroupsWithoutSegments
                    ->exists(tg->getId())) {
                        filterTg = false;
                        entityToModuleStateStats->addStateModuleForEntity (
                                "PASS_TG_HAS_NO_SEGMENT",
                                getName (),
                                StringUtil::toStr ("tg") +
                                StringUtil::toStr (tg->getId())
                                );
                        context->addTargetingTypeForTg(tg->getId(), TargetGroup::TARGETTING_TYPE_SEGMENTLESS_TARGETING);
                } else {
                        /*
                           this is only for testing and debugging purpose
                           we don't need to run it in prod all the time
                         */
                        deviceIdSegmentModuleHelper->recordTgNotHavingSegments(tg);
                }


        }

        return filterTg;
}



std::string DeviceIdSegmentModule::getName() {
        return "DeviceIdSegmentModule";
}

#include "RecentVisitHistoryScorer.h"
#include "RecentVisitHistory.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "AerospikeDriver.h"
#include "Feature.h"
#include "TargetGroupCacheService.h"
#include "TargetGroup.h"
#include "RecentVisitHistoryScoreCard.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "Device.h"
#include "TgScoreEligibilityRecord.h"
#include "TgModelEligibilityRecord.h"
#include "RecencyModelCacheService.h"
#include "DeviceFeatureHistory.h"
#include "RecentVisitHistoryScoreReader.h"
#include "RecentVisitHistoryUpdater.h"
#include "RecentVisitHistoryScorer.h"
#include "DeviceRecentVisitHistory.h"

RecentVisitHistoryScorer::RecentVisitHistoryScorer(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string RecentVisitHistoryScorer::getName() {
        return "RecentVisitHistoryScorer";
}

void RecentVisitHistoryScorer::process(std::shared_ptr<OpportunityContext> context) {

        scoreAllTgsForDevice(context);

}

RecentVisitHistoryScorer::~RecentVisitHistoryScorer() {

}

void RecentVisitHistoryScorer::scoreAllTgsForDevice(std::shared_ptr<OpportunityContext> context) {

        //read the current Score Card
        std::shared_ptr<RecentVisitHistoryScoreCard> currentDeviceScoreCard =
                recentVisitHistoryScoreReader->readTheScoreCardForDevice(context->device);
        MLOG(4)<<"reading current device Score Card : "<<currentDeviceScoreCard->toJson();

        auto targetgroups = targetGroupCacheService->getAllCurrentTargetGroups();
        for (auto tg : *targetgroups) {
                auto pairPtr = currentDeviceScoreCard->targetGroupIdToQualification->find(tg->getId());
                if (pairPtr == currentDeviceScoreCard->targetGroupIdToQualification->end()) {
                        //insert tg pair into map
                        std::shared_ptr<TgScoreEligibilityRecord> value =
                                std::make_shared<TgScoreEligibilityRecord>();
                        value->targetGroupId = tg->getId();

                        currentDeviceScoreCard->targetGroupIdToQualification->insert(
                                std::make_pair(tg->getId(), value));

                        pairPtr = currentDeviceScoreCard->targetGroupIdToQualification->find(tg->getId());
                }

                std::shared_ptr<TgScoreEligibilityRecord>  tgScoreEligibilityRecord = pairPtr->second;
                auto allModelScoresInfoOfTg = tgScoreEligibilityRecord->modelIdToEligibilityRecords;

                std::vector<std::shared_ptr<RecencyModel> > recentModels =
                        recentVisitHistoryScoreReader->getAllRecentModelsOfTg(tg->getId());
                for (auto model : recentModels) {
                        // std::shared_ptr<TgModelEligibilityRecord> > modelIdToEligibilityRecords;

                        auto modelScorePair = allModelScoresInfoOfTg->find(model->id);
                        if (modelScorePair == allModelScoresInfoOfTg->end()) {
                                //we insert the model score entry
                                std::shared_ptr<TgModelEligibilityRecord> modelScore =
                                        std::make_shared<TgModelEligibilityRecord> ();
                                allModelScoresInfoOfTg->insert(std::make_pair(model->id, modelScore));

                                modelScorePair = allModelScoresInfoOfTg->find(model->id);
                        }

                        std::shared_ptr<TgModelEligibilityRecord>
                        tgModelEligibilityRecord = modelScorePair->second;
                        //now we change the tgModelEligibilityRecord that holds info about if a
                        //device has been qualifed for a model and tg

                        float scoreForTg = scoreDeviceForATgAndModel(context,
                                                                     tg->getId(),
                                                                     model);
                        tgModelEligibilityRecord->eligibile = "no";
                        assertAndThrow(model->scoreBase !=0);
                        if (scoreForTg > 0 && scoreForTg >= model->scoreBase ) {
                                //targetgroup is eligibile for bidding
                                tgModelEligibilityRecord->eligibile = "yes";
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "elgibile",
                                        "RecentVisitHistoryScorer",
                                        "ALL",
                                        EntityToModuleStateStats::important);
                        }
                        tgModelEligibilityRecord->targetGroupId = tg->getId();
                        tgModelEligibilityRecord->id =  model->id;
                        tgModelEligibilityRecord->score = scoreForTg;
                        tgModelEligibilityRecord->scoreDiffComparedToScoreBase = scoreForTg - model->scoreBase;
                        std::shared_ptr<ScoreRecord> scoreRecord = std::make_shared<ScoreRecord>();
                        scoreRecord->scoreRecorded = scoreForTg;
                        scoreRecord->scoreBase =  model->scoreBase;
                        scoreRecord->timeScoredInUtc= DateTimeUtil::getNowInMilliSecond();
                        NULL_CHECK(tgModelEligibilityRecord->scoreHistory);
                        tgModelEligibilityRecord->scoreHistory->push_back(scoreRecord);


                        MLOG(10)<<"updating tgModelEligibilityRecord to : "<<
                                modelScorePair->second->toJson();
                }

        }

        if (!targetgroups->empty()) {
                MLOG(3) << "new scoreCard to save : " << currentDeviceScoreCard->toJson();
                saveScoreCard(currentDeviceScoreCard, context->device);
        }
}

void RecentVisitHistoryScorer::saveScoreCard(
        std::shared_ptr<RecentVisitHistoryScoreCard> currentDeviceScoreCard,
        std::shared_ptr<Device> device) {

        std::string jsonFeatureObject = currentDeviceScoreCard->toJson();
        aeroSpikeDriver->setValueAndRead(RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_NAMESPACE,
                                         RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SET,
                                         device->getDeviceId(),
                                         RecentVisitHistoryUpdater::RECENT_VISIT_HISTORY_SCORE_BIN_NAME,
                                         jsonFeatureObject,
                                         10000);
}


/**
   calculates score of a device for a tg
 */
float RecentVisitHistoryScorer::scoreDeviceForATgAndModel(std::shared_ptr<OpportunityContext> context,
                                                          int tgId,
                                                          std::shared_ptr<RecencyModel> model) {

        auto deviceRecentVisitHistory = recentVisitHistoryScoreReader->readRecentVisitHistory(context->device);

        float sumOfScores = 0.00;
        //we are parsing all the objects that we wrote
        for (auto readRecentVisitHistory : deviceRecentVisitHistory->allRecentHistories) {
                auto pairPtr = model->featureScoreMap.find(readRecentVisitHistory->featureName);
                if(pairPtr == model->featureScoreMap.end()) {
                        continue;
                }
                float scoreOfFeature = pairPtr->second;
                sumOfScores += scoreOfFeature;
        }


        MLOG(2)<<"sumOfScores : "<<sumOfScores;

        return sumOfScores;
}

bool RecentVisitHistoryScorer::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

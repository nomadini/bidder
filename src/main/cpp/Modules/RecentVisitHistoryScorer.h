#ifndef RecentVisitHistoryScorer_H
#define RecentVisitHistoryScorer_H


#include "EntityProviderService.h"
#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include <unordered_map>
#include "Object.h"
class RecentVisitHistoryScoreCard;
class AerospikeDriverInterface;
class Device;
class TargetGroupCacheService;
class RecencyModel;
class TargetGroupRecencyModelMapCacheService;
class RecencyModel;
class TgModelEligibilityRecord;
class TargetGroup;
class RecentVisitHistoryScoreReader;

class RecentVisitHistoryScorer : public BidderModule, public Object {
public:
RecentVisitHistoryScorer(EntityToModuleStateStats* entityToModuleStateStats,
                         std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~RecentVisitHistoryScorer();
AerospikeDriverInterface* aeroSpikeDriver;
TargetGroupCacheService* targetGroupCacheService;
RecentVisitHistoryScoreReader* recentVisitHistoryScoreReader;

float scoreDeviceForATgAndModel(std::shared_ptr<OpportunityContext> context,
                                int tgId,
                                std::shared_ptr<RecencyModel> model);



void scoreAllTgsForDevice(std::shared_ptr<OpportunityContext> context);

void saveScoreCard(std::shared_ptr<RecentVisitHistoryScoreCard> currentDeviceScoreCard,
                   std::shared_ptr<Device> device);


std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};


#endif

#include "EnqueueForScoringModule.h"

#include <boost/exception/all.hpp>
#include "GUtil.h"

#include "ConverterUtil.h"
#include "BidderApplicationContext.h"
#include <thread>
#include "ExceptionUtil.h"
#include "OpportunityContext.h"
#include "Poco/URI.h"
#include "Poco/Net/HTTPClientSession.h"
#include "HttpUtilService.h"
#include "KafkaProxyPopulator.h"
EnqueueForScoringModule::EnqueueForScoringModule(EntityToModuleStateStats* entityToModuleStateStats,
                                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                                 KafkaProxyPopulator* kafkaProxyPopulator)
        :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->kafkaProxyPopulator = kafkaProxyPopulator;
}

std::string EnqueueForScoringModule::getName() {
        return "EnqueueForScoringModule";
}

void EnqueueForScoringModule::process(std::shared_ptr<OpportunityContext> context) {
        if (enqueueForScoringModuleIsDisabled->getValue() == true) {
                BidderModule::entityToModuleStateStats->addStateModuleForEntity ("DISABLED",
                                                                                 "EnqueueForScoringModule",
                                                                                 "ALL",
                                                                                 EntityToModuleStateStats::exception);
                LOG_EVERY_N(ERROR, 1000) << "enqueue is disabled";
                return;
        }

        auto isInSample = sampler->isPartOfSample ();
        if (isInSample) {
                NULL_CHECK(BidderModule::entityToModuleStateStats);
                BidderModule::entityToModuleStateStats->addStateModuleForEntity ("REQUEST_BELONG_TO_SAMPLE",
                                                                                 "EnqueueForScoringModule",
                                                                                 "ALL");
                std::shared_ptr<TransAppMessage> msg = constructTransAppMessage (context);
                // Enqueues the given notification by adding it to the end of the queue (FIFO).
                // The queue takes ownership of the notification, thus a call like
                transAppQueue->push (msg);

                sizeOfKafkaScoringQueue->setValue(transAppQueue->unsafe_size());

        } else {
                BidderModule::entityToModuleStateStats->addStateModuleForEntity ("REQUEST_DOESNT_BELONG_TO_SAMPLE",
                                                                                 "EnqueueForScoringModule",
                                                                                 "ALL");

        }
        context->status->value = CONTINUE_PROCESSING;

}





std::shared_ptr<TransAppMessage> EnqueueForScoringModule::constructTransAppMessage(std::shared_ptr<OpportunityContext> context) {
        MLOG(2)<<"constructing trans app message";
        std::shared_ptr<TransAppMessage> msg = std::make_shared<TransAppMessage>();
        msg->transactionId = context->transactionId;
        msg->chosenTargetGroupId = context->chosenTargetGroupId;
        msg->chosenCreativeId = context->chosenCreativeId;
        msg->adHistory = context->adHistory;

        msg->bidPrice = context->bidPrice;

        msg->userTimeZone = context->userTimeZone;
        msg->userTimeZonDifferenceWithUTC = context->userTimeZonDifferenceWithUTC;

        msg->deviceUserAgent = context->deviceUserAgent;
        msg->deviceIp = context->deviceIp;
        msg->device= context->device;

        msg->deviceLat = context->deviceLat;
        msg->deviceLon = context->deviceLon;
        msg->deviceCountry = context->deviceCountry;
        msg->deviceCity = context->deviceCity;
        msg->deviceState = context->deviceState;
        msg->deviceZipcode = context->deviceZipcode;
        msg->siteCategory = context->siteCategory;
        msg->siteDomain = context->siteDomain;
        msg->sitePage = context->sitePage;
        msg->sitePublisherDomain = context->sitePublisherDomain;
        msg->sitePublisherName = context->sitePublisherName;

        return msg;
}
void EnqueueForScoringModule::processManyMessages() {
        int numberOfMessageToReadInOnGo = 10000;

        for (int i=0; i< numberOfMessageToReadInOnGo; i++) {
                sendOneMessageQueue();
        }

}

void EnqueueForScoringModule::sendOneMessageQueue() {

        try {
                std::shared_ptr<TransAppMessage> message;
                if (transAppQueue->try_pop(message)) {
                        auto msgToKafka = message->toJson();
                        LOG_EVERY_N(INFO, 1000000) <<google::COUNTER<<"th writing message to kafka " << msgToKafka;
                        LOG_EVERY_N(INFO, 100000) << "wrote "<<google::COUNTER << " th" <<" kafka";

                        //kafkaProducer->produceMessage (msgToKafka);

                        kafkaProxyPopulator->sendMessageToKafkaOverHttp(msgToKafka);

                        NULL_CHECK(BidderModule::entityToModuleStateStats);
                        BidderModule::entityToModuleStateStats->addStateModuleForEntity ("MSG_PUT_TO_KAFKA_QUEUE",
                                                                                         "EnqueueForScoringModule",
                                                                                         "ALL",
                                                                                         EntityToModuleStateStats::important);

                }

        } catch (...) {
                BidderModule::entityToModuleStateStats->addStateModuleForEntity ("EXCEPTION_IN_PUTTING_MSG_TO_KAFKA_QUEUE",
                                                                                 "EnqueueForScoringModule",
                                                                                 "ALL",
                                                                                 EntityToModuleStateStats::exception);

                numberOfExceptionsInWritingMessagesInKafka->increment();
                gicapods::Util::showStackTrace();

        }
}

bool EnqueueForScoringModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

EnqueueForScoringModule::~EnqueueForScoringModule() {
}

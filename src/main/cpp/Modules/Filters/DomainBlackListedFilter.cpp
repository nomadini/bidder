#include "DomainBlackListedFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"

#include <boost/foreach.hpp>
#include "MySqlBWListService.h"
#include "TargetGroupBWListCacheService.h"
#include "CollectionUtil.h"

DomainBlackListedFilter::DomainBlackListedFilter
        (TargetGroupBWListCacheService* targetGroupBWListCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts) {
        this->targetGroupBWListCacheService = targetGroupBWListCacheService;

        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}
void DomainBlackListedFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "DomainBlackListedFilter cache cleared";
        attributesToGoodTgs->clear();
}

DomainBlackListedFilter::~DomainBlackListedFilter() {

}

void DomainBlackListedFilter::buildMapOfTgsBlockingThisDomain(std::shared_ptr<OpportunityContext> context) {
        auto map = targetGroupBWListCacheService->getBlackListedDomainToListOfTargetGroups ();
        context->mapOfDomainBlockingTgs =
                std::make_shared<std::unordered_map<int, std::shared_ptr<TargetGroup> > >();

        std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > passingTargetGroups;
        auto tgVector = map->getOptional (StringUtil::toLowerCase (context->siteDomain));

        if (tgVector != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity ("SOME_HAVE_BLACK_LISTS",
                                                                   "DomainBlackListedFilter",
                                                                   "someTargetGroups");

                for (auto value : *tgVector->values) {
                        context->mapOfDomainBlockingTgs->insert(std::make_pair(value->getId(), nullptr));
                }

        } else {

                entityToModuleStateStats->addStateModuleForEntity ("NONE_HAVE_BLACK_LISTS",
                                                                   "DomainBlackListedFilter",
                                                                   "NoTargetGroups");
        }
}

std::string DomainBlackListedFilter::convertMapToJson(
        std::shared_ptr<OpportunityContext> context) {
        std::string allTgsIds = " blockingTgIds:";
        for (auto pair : *context->mapOfDomainBlockingTgs) {
                allTgsIds.append(_toStr(pair.first));
        }
        return "blackListedDomain:" + context->siteDomain + allTgsIds;
}

bool DomainBlackListedFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                                std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (context->mapOfDomainBlockingTgs == nullptr) {
                buildMapOfTgsBlockingThisDomain(context);
        }

        MLOG(2)<<"considering tg "<<tg->getId()<<" with mapOfDomainBlockingTgs : "<<convertMapToJson(context);
        auto pairPtr = context->mapOfDomainBlockingTgs->find (tg->getId ());
        if (pairPtr != context->mapOfDomainBlockingTgs->end ()) {

                MLOG(2) << "targetgroup id : " << tg->getId () <<
                        "has blacklist."<<
                        " disqualified siteDomain '"<< context->siteDomain<<"'";
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_BLACK_LIST",
                                                                   "DomainBlackListedFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

        } else {
                MLOG(2) << "targetgroup id : " << tg->getId () <<
                        "has no blacklist."<<
                        " qualifying siteDomain '"<< context->siteDomain<<"'";
                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   "DomainBlackListedFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

                filterTg = false;
        }
        return filterTg;
}
std::string DomainBlackListedFilter::getName() {
        return "DomainBlackListedFilter";
}

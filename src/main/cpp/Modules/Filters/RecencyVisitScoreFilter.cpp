#include "RecencyVisitScoreFilter.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "AerospikeDriver.h"
#include "Feature.h"
#include "TargetGroupCacheService.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroup.h"
#include "RecentVisitHistoryScoreCard.h"
#include "RecencyModel.h"
#include "TargetGroupRecencyModelMapCacheService.h"
#include "Device.h"
#include "TgScoreEligibilityRecord.h"
#include "TgModelEligibilityRecord.h"
#include "RecencyModelCacheService.h"
#include "DeviceFeatureHistory.h"
#include "RecentVisitHistoryScoreReader.h"

std::string RecencyVisitScoreFilter::RECENT_VISIT_HISTORY_BIN_NAME = "s1";
std::string RecencyVisitScoreFilter::RECENT_VISIT_HISTORY_NAMESPACE = "test";
std::string RecencyVisitScoreFilter::RECENT_VISIT_HISTORY_SET = "visitSet";

RecencyVisitScoreFilter::RecencyVisitScoreFilter(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string RecencyVisitScoreFilter::getName() {
        return "RecencyVisitScoreFilter";
}


void RecencyVisitScoreFilter::process(std::shared_ptr<OpportunityContext> context) {

        context->recentVisitHistoryScoreCardOfDevice =
                recentVisitHistoryScoreReader->readTheScoreCardForDevice(context->device);
        MLOG(3) << "read score card : " << context->recentVisitHistoryScoreCardOfDevice->toJson();

        markFilteredTgs(context);
}

bool RecencyVisitScoreFilter::filterTargetGroup(
        std::shared_ptr<TargetGroup> tg,
        std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;


        if (!tg->typesOfTargeting->exists(TargetGroup::TARGETTING_TYPE_RECENCY_TARGETING)) {
                return false;
        }

        std::vector<std::shared_ptr<RecencyModel> > recentModels =
                recentVisitHistoryScoreReader->getAllRecentModelsOfTg(tg->getId());
        if (recentModels.empty()) {
                MLOG(2)<< "no recency model for tg "<<tg->getId();
                return true;
        }

        auto pairPtr = context->recentVisitHistoryScoreCardOfDevice->
                       targetGroupIdToQualification->find(tg->getId());
        if (pairPtr != context->recentVisitHistoryScoreCardOfDevice->
            targetGroupIdToQualification->end()) {
                std::shared_ptr<TgScoreEligibilityRecord>  tgScoreEligibilityRecord =
                        pairPtr->second;

                MLOG(4)<<"evaluating tgScoreEligibilityRecord  "<<
                        tgScoreEligibilityRecord->toJson();

                auto allModelScoresInfoOfTg = tgScoreEligibilityRecord->modelIdToEligibilityRecords;

                for (auto model : recentModels) {
                        auto modelScorePair = allModelScoresInfoOfTg->find(model->id);
                        if (modelScorePair != allModelScoresInfoOfTg->end()) {
                                std::shared_ptr<TgModelEligibilityRecord>
                                tgModelEligibilityRecord = modelScorePair->second;
                                if (tgModelEligibilityRecord->score > model->scoreBase) {
                                        MLOG(10)<<" tg "<< tg->getId() <<", is qualified for model" << model->id;
                                        entityToModuleStateStats->addStateModuleForEntity(
                                                "PASSING_TG_BECAUSE_OF_GOOD_SCORE",
                                                "RecencyVisitScoreFilter",
                                                "ALL",
                                                tg->idAsString);
                                        context->addTargetingTypeForTg(tg->getId(), TargetGroup::TARGETTING_TYPE_RECENCY_TARGETING);
                                        filterTg = false;
                                        break;
                                }
                        }
                }
                if (recentModels.empty()) {
                        //we pass tgs that don't have any recent models
                        filterTg = false;
                        entityToModuleStateStats->addStateModuleForEntity(
                                "PASSING_TG_NOT_HAVING_RECENCY_MODELS",
                                "RecencyVisitScoreFilter",
                                "ALL",
                                tg->idAsString);
                }

        } else {
                //there is no score model for this tg
                if (!tg->typesOfTargeting->exists(TargetGroup::TARGETTING_TYPE_RECENCY_TARGETING)) {
                        //this tg is not supposed to be strictly targeted for recency...
                        //so we pass it
                        filterTg=false;
                        entityToModuleStateStats->addStateModuleForEntity("PASSING_TG_NOT_STRICT_WITH_RECENCY_TARGETING",
                                                                          "RecencyVisitScoreFilter",
                                                                          "ALL",
                                                                          tg->idAsString);
                }
        }

        return filterTg;
}

RecencyVisitScoreFilter::~RecencyVisitScoreFilter() {

}

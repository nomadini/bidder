#include "BlockedBannerAdTypeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CreativeSizeFilter.h"
#include "JsonArrayUtil.h"
#include "Creative.h"
#include <boost/foreach.hpp>

BlockedBannerAdTypeFilter::BlockedBannerAdTypeFilter(CreativeSizeFilter* creativeSizeFilter,
                                                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        this->creativeSizeFilter = creativeSizeFilter;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

BlockedBannerAdTypeFilter::~BlockedBannerAdTypeFilter() {

}

std::string BlockedBannerAdTypeFilter::createElementId(int targetGroupId, int crtId) {
        std::string stateId = StringUtil::toStr ("tg") +
                              StringUtil::toStr (targetGroupId) +
                              StringUtil::toStr ("crt") +
                              StringUtil::toStr (crtId);
        return stateId;
}
bool BlockedBannerAdTypeFilter::doesCreativeHaveBlockedBannerType(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative> crt) {
        if (context->getBlockedBannerTypesMap()->exists (crt->getAdType())) {
                return true;
        } else {
                return false;
        }
}

void BlockedBannerAdTypeFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "BlockedBannerAdTypeFilter cache cleared";

        attributesToGoodTgs->clear();
}

bool BlockedBannerAdTypeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        auto allCreatives = creativeSizeFilter->getCreativesAssignedToTg (tg->getId());

        if (allCreatives.empty ()) {

                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "BlockedBannerAdTypeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        for(std::shared_ptr<Creative> crt :  allCreatives) {

                std::string stateId = createElementId(tg->getId(), crt->getId());

                MLOG(10) << " stateId is : "<< stateId;

                if (doesCreativeHaveBlockedBannerType(context, crt)) {
                        MLOG(10) << "crt id : " << crt->getId() << " is blocked by bid request because of adType ";
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           "BlockedBannerAdTypeFilter",
                                                                           stateId);
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                           "BlockedBannerAdTypeFilter",
                                                                           stateId);
                        MLOG(10) << "targetgroup id : "
                                 << tg->getId()
                                 << " has at least one good creative in terms of adType, passing the filter "
                                 << " crt->getId : " << crt->getId ()
                                 << "crt->getAdType() : " << crt->getAdType();
                        filterTg = false;
                        recordPassingValue(context->allBlockedBannerTypesAsStr,
                                           tg->getId (),
                                           "tg",
                                           getName ());
                        //TODO : we shouldn't break here..
                        //we should add this tg+crt combo to the tgCreativeCandidates map that
                        //is mapped by tg -> tg+crt ids
                        //so we don't have to rerun this logic in selectOptimalCreative filter
                        break;
                }
        }

        return filterTg;
}
std::string BlockedBannerAdTypeFilter::getName() {
        return "BlockedBannerAdTypeFilter";
}

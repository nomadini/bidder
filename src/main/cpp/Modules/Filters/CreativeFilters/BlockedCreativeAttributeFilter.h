#ifndef BlockedCreativeAttributeFilter_H
#define BlockedCreativeAttributeFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include "TgMarker.h"
#include "CreativeSizeFilter.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "CacheUpdateWatcher.h"
class BlockedCreativeAttributeFilter;

class BlockedCreativeAttributeFilter : public TgMarker, public CacheUpdateWatcher {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

CreativeSizeFilter* creativeSizeFilter;
BlockedCreativeAttributeFilter(CreativeSizeFilter* creativeSizeFilter,
                               std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~BlockedCreativeAttributeFilter();

static bool isCreativeBlockedByAttributeInContext(std::shared_ptr<Creative> creative, std::shared_ptr<OpportunityContext> context);
std::string getName();
void cacheWasUpdatedEvent();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);


};

#endif

#include "BlockedCreativeAttributeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CreativeSizeFilter.h"
#include "ConverterUtil.h"
#include <boost/foreach.hpp>
#include "Creative.h"
#include "JsonArrayUtil.h"
#include "BlockedBannerAdTypeFilter.h"

BlockedCreativeAttributeFilter::BlockedCreativeAttributeFilter(CreativeSizeFilter* creativeSizeFilter,
                                                               std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        this->creativeSizeFilter = creativeSizeFilter;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();
}

BlockedCreativeAttributeFilter::~BlockedCreativeAttributeFilter() {

}

bool BlockedCreativeAttributeFilter::isCreativeBlockedByAttributeInContext(
        std::shared_ptr<Creative> crt, std::shared_ptr<OpportunityContext> context) {
        bool blockCreative = false;
        for (auto contextAttribute : context->blockedAttributes) {
                std::string attributesLowerCased = StringUtil::toLowerCase(contextAttribute);
                auto pair = crt->attributes->find(attributesLowerCased);
                if (pair != crt->attributes->end()) {
                        //found a blocking attribute
                        blockCreative = true;
                        MLOG(2) << "crt id : " << crt->getId() <<
                                " is blocked by bid request, skipping, commonAttributes : "
                                << attributesLowerCased;
                        break;
                }
        }
        return blockCreative;
}

void BlockedCreativeAttributeFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "BlockedCreativeAttributeFilter cache cleared";
        attributesToGoodTgs->clear();
}

bool BlockedCreativeAttributeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                                       std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        auto allCreatives = creativeSizeFilter->getCreativesAssignedToTg (tg->getId());
        MLOG(2) << "number of creatives : " << allCreatives.size () << " for chosen tg id : " << tg->getId();

        if (allCreatives.empty ()) {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "BlockedCreativeAttributeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }
        for(std::shared_ptr<Creative> crt :  allCreatives) {

                std::string stateId = BlockedBannerAdTypeFilter::createElementId(tg->getId(), crt->getId());

                MLOG(2) << " stateId is : "<< stateId;



                if (isCreativeBlockedByAttributeInContext(crt, context)) {

                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           "BlockedCreativeAttributeFilter",
                                                                           stateId);

                        continue;
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                           "BlockedCreativeAttributeFilter",
                                                                           stateId);
                        filterTg = false;
                }
        }

        return filterTg;
}
std::string BlockedCreativeAttributeFilter::getName() {
        return "BlockedCreativeAttributeFilter";
}

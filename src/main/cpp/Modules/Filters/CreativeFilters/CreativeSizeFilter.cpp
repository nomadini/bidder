#include "CreativeSizeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include <boost/exception/all.hpp>
#include "CreativeCacheService.h"
#include "TargetGroupCreativeCacheService.h"
#include "Creative.h"
CreativeSizeFilter::CreativeSizeFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                                       CreativeCacheService* creativeCacheService,
                                       std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        this->targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this->creativeCacheService = creativeCacheService;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

CreativeSizeFilter::~CreativeSizeFilter() {

}

void CreativeSizeFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "BlockedCreativeAttributeFilter cache cleared";
        attributesToGoodTgs->clear();
}

bool CreativeSizeFilter::rightSizeCreativeExist(std::string requstedCreativeSize,
                                                std::vector<std::shared_ptr<Creative> >& crtList) {
        for (auto &crt : crtList) {
                if (StringUtil::equalsIgnoreCase (crt->getSize(), requstedCreativeSize)) {
                        return true;
                } else {
                        MLOG (10) << "requstedCreativeSize " << requstedCreativeSize << " vs crtSize " << crt->getSize();
                }

        }

        return false;
}

std::vector<std::shared_ptr<Creative> > CreativeSizeFilter::getCreativesAssignedToTg(int targetGroupId) {

        auto tgCreativeEntry = targetGroupCreativeCacheService
                               ->getAllTargetGroupCreativesMap ()
                               ->getOptional (targetGroupId);
        std::vector<std::shared_ptr<Creative> > allAssignedCreatives;

        if (tgCreativeEntry != nullptr) {

                auto map = tgCreativeEntry->getCopyOfMap();
                for (auto entity : *map) {
                        auto crtId = entity.first;
                        try {
                                auto crt = creativeCacheService->findByEntityId (crtId);
                                allAssignedCreatives.push_back (crt);
                        } catch (std::exception const &e) {

                                entityToModuleStateStats->addStateModuleForEntity ("ERROR_NO_CREATIVE_IN_MAP_FOR_ID",
                                                                                   "CreativeSizeFilter",
                                                                                   StringUtil::toStr ("crt") +
                                                                                   StringUtil::toStr (crtId));
                        }
                }
        }
        return allAssignedCreatives;
}

bool CreativeSizeFilter::hasCreativeTheRequestedSize(std::shared_ptr<OpportunityContext> context,
                                                     std::shared_ptr<Creative> crt) {
        if (StringUtil::equalsIgnoreCase (crt->getSize(), context->adSize)) {
                return true;
        }

        return false;
}

bool CreativeSizeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        std::vector<std::shared_ptr<Creative> > creatives = getCreativesAssignedToTg (tg->getId());
        if (creatives.size () == 0) {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "CreativeSizeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                return filterTg;
        }
        if (rightSizeCreativeExist (context->adSize, creatives)) {
                filterTg = false;

                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   "CreativeSizeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_MISMATCHING_SIZE",
                                                                   "CreativeSizeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        return filterTg;
}
std::string CreativeSizeFilter::getName() {
        return "CreativeSizeFilter";
}

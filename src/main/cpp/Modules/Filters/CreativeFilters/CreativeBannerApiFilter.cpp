#include "CreativeBannerApiFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"

#include "CreativeCacheService.h"
#include "Creative.h"
#include "JsonArrayUtil.h"
#include "TargetGroupCreativeCacheService.h"
#include "BlockedBannerAdTypeFilter.h"
/*From OpenRtb 2.3
   /List of supported API frameworks for this impression.
   /Refer to List 5.6. If an API is not explicitly listed, it is assumed not to be supported.
   5.6 API Frameworks
   The following table is a list of API frameworks supported by the publisher. Note that MRAID-1 is a subset of MRAID-2. In OpenRTB 2.1 and prior, value “3” was “MRAID”. However, not all MRAID capable APIs understand MRAID-2 features and as such the only safe interpretation of value “3” is MRAID-1. In OpenRTB 2.2, this was made explicit and MRAID-2 has been added as value “5”.
   Value Description
   1       VPAID 1.0
   2       VPAID 2.0
   3       MRAID-1
   4       ORMMA
   5       MRAID-2
 */

CreativeBannerApiFilter::CreativeBannerApiFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                                                 CreativeCacheService* creativeCacheService,
                                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        this->targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this->creativeCacheService = creativeCacheService;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}
void CreativeBannerApiFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "CreativeBannerApiFilter cache cleared";
        attributesToGoodTgs->clear();
}

CreativeBannerApiFilter::~CreativeBannerApiFilter() {

}

bool CreativeBannerApiFilter::doesCreativeHaveRightApi(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative>& crt) {

        if (StringUtil::equalsIgnoreCase(context->impressionType, "banner" )) {
                //request that come from banner . AKA desktop requests, don't need to have an pi
                //TODO : this is important, test it and have metrics for it
                return true;
        }
        for(std::string apiFromRequest : context->creativeAPI) {
                if(crt->apis->find(apiFromRequest) != crt->apis->end()) {
                        return true;
                }
        }
        return false;
}
bool CreativeBannerApiFilter::findAtLeastOneCreativeWithMatchingApi(std::shared_ptr<gicapods::ConcurrentHashMap<int, Creative> > allCreativeListForThisTargetGroup,
                                                                    std::shared_ptr<TargetGroup> tg,
                                                                    std::string protocolVersion,
                                                                    std::shared_ptr<OpportunityContext> context) {


        auto map = allCreativeListForThisTargetGroup->getCopyOfMap();
        for (auto crtIdPair : *map) {
                auto crt = crtIdPair.second;
                if (doesCreativeHaveRightApi(context, crt)) {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                           "CreativeBannerApiFilter",
                                                                           BlockedBannerAdTypeFilter::createElementId (tg->getId(),
                                                                                                                       crt->getId()));

                        return true;
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           "CreativeBannerApiFilter",
                                                                           BlockedBannerAdTypeFilter::createElementId (tg->getId(),
                                                                                                                       crt->getId()));
                }
        }

        return false;
}
bool CreativeBannerApiFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {


        if (context->uniqueNumberOfContext->getValue() % 100 == 1) {
                //we do this every 100 times
                for (auto creativeApi : context->creativeAPI) {
                        entityToModuleStateStats->addStateModuleForEntity (creativeApi,
                                                                           "CreativeBannerApiFilter-DataFromRequests"
                                                                           ,"ALL");
                }
        }

        bool filterTg = true;

        auto tgCreativeEntry = targetGroupCreativeCacheService->getAllTargetGroupCreativesMap ()->getOptional (tg->getId());

        if (tgCreativeEntry != nullptr) {

                auto creativeMap = tgCreativeEntry;
                if (creativeMap->empty()) {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                                                                           "CreativeBannerApiFilter",
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                }
                if (findAtLeastOneCreativeWithMatchingApi (creativeMap,
                                                           tg,
                                                           context->protocolVersion,
                                                           context)) {
                        filterTg = false;
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "CreativeBannerApiFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        return filterTg;
}
std::string CreativeBannerApiFilter::getName() {
        return "CreativeBannerApiFilter";
}

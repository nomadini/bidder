#ifndef CreativeBannerApiFilter_H
#define CreativeBannerApiFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class Creative;
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
class TargetGroupCreativeCacheService;
class CreativeCacheService;
#include "tbb/concurrent_hash_map.h"
#include <boost/foreach.hpp>
#include "CacheUpdateWatcher.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "EntityProviderService.h"
#include "ObjectVectorHolder.h"
class CreativeBannerApiFilter;

class CreativeBannerApiFilter : public TgMarker, public CacheUpdateWatcher {

public:
TargetGroupCreativeCacheService* targetGroupCreativeCacheService;
CreativeCacheService* creativeCacheService;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;


CreativeBannerApiFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                        CreativeCacheService* creativeCacheService,
                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

bool findAtLeastOneCreativeWithMatchingApi (std::shared_ptr<gicapods::ConcurrentHashMap<int, Creative > > allCreativeListForThisTargetGroup,
                                            std::shared_ptr<TargetGroup> tg,
                                            std::string protocolVersion,
                                            std::shared_ptr<OpportunityContext> context);

virtual ~CreativeBannerApiFilter();
void cacheWasUpdatedEvent();
static bool doesCreativeHaveRightApi(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative>& crt);


std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

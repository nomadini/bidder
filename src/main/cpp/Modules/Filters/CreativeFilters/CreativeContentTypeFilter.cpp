#include "CreativeContentTypeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"

#include "CreativeCacheService.h"
#include "Creative.h"
#include "JsonArrayUtil.h"
#include "TargetGroupCreativeCacheService.h"
#include "BlockedBannerAdTypeFilter.h"

CreativeContentTypeFilter::CreativeContentTypeFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                                                     CreativeCacheService* creativeCacheService,
                                                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        this->targetGroupCreativeCacheService = targetGroupCreativeCacheService;
        this->creativeCacheService = creativeCacheService;

}

CreativeContentTypeFilter::~CreativeContentTypeFilter() {

}

bool CreativeContentTypeFilter::doesCreativeHaveRightContentType(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative>& crt) {

        if (StringUtil::equalsIgnoreCase(context->creativeContentType,
                                         crt->creativeContentType )) {
                return true;
        }

        return false;
}

bool CreativeContentTypeFilter::findAtLeastOneCreativeWithMatchingContentType(std::shared_ptr<gicapods::ConcurrentHashMap<int, Creative> > allCreativeListForThisTargetGroup,
                                                                              std::shared_ptr<TargetGroup> tg,
                                                                              std::string protocolVersion,
                                                                              std::shared_ptr<OpportunityContext> context) {


        auto map = allCreativeListForThisTargetGroup->getCopyOfMap();
        for (auto crtIdPair : *map) {
                auto crt = crtIdPair.second;
                if (doesCreativeHaveRightContentType(context, crt)) {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                           "CreativeContentTypeFilter",
                                                                           BlockedBannerAdTypeFilter::createElementId (tg->getId(),
                                                                                                                       crt->getId()));

                        return true;
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           "CreativeContentTypeFilter",
                                                                           BlockedBannerAdTypeFilter::createElementId (tg->getId(),
                                                                                                                       crt->getId()));
                }
        }

        return false;
}
bool CreativeContentTypeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                                  std::shared_ptr<OpportunityContext> context) {

        bool filterTg = true;

        auto tgCreativeEntry = targetGroupCreativeCacheService->getAllTargetGroupCreativesMap ()->getOptional (tg->getId());

        if (tgCreativeEntry != nullptr) {

                auto creativeMap = tgCreativeEntry;
                if (creativeMap->empty()) {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_ZERO_CREATIVES_IN_MAP",
                                                                           "CreativeContentTypeFilter",
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                }
                if (findAtLeastOneCreativeWithMatchingContentType (creativeMap,
                                                                   tg,
                                                                   context->protocolVersion,
                                                                   context)) {
                        filterTg = false;
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NO_CREATIVES",
                                                                   "CreativeContentTypeFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        return filterTg;
}

std::string CreativeContentTypeFilter::getName() {
        return "CreativeContentTypeFilter";
}

#ifndef BlockedBannerAdTypeFilter_H
#define BlockedBannerAdTypeFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "CreativeSizeFilter.h"
#include "ObjectVectorHolder.h"
#include "CacheUpdateWatcher.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class BlockedBannerAdTypeFilter;

class BlockedBannerAdTypeFilter : public TgMarker, public CacheUpdateWatcher {

public:

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

CreativeSizeFilter* creativeSizeFilter;

static std::string createElementId(int targetGroupId, int crtId);

BlockedBannerAdTypeFilter(CreativeSizeFilter* creativeSizeFilter,
                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);


static bool doesCreativeHaveBlockedBannerType(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative> crt);
void cacheWasUpdatedEvent();
virtual ~BlockedBannerAdTypeFilter();

void markGoodTgsAndCacheThem(std::shared_ptr<OpportunityContext>context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext>context);
std::string getName();
};

#endif

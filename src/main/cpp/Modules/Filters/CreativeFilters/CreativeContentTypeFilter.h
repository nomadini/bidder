#ifndef CreativeContentTypeFilter_H
#define CreativeContentTypeFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class Creative;
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
class TargetGroupCreativeCacheService;
class CreativeCacheService;
#include "tbb/concurrent_hash_map.h"
#include <boost/foreach.hpp>
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "EntityProviderService.h"
#include "ObjectVectorHolder.h"
class CreativeContentTypeFilter;

class CreativeContentTypeFilter : public TgMarker {

public:
TargetGroupCreativeCacheService* targetGroupCreativeCacheService;
CreativeCacheService* creativeCacheService;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;


CreativeContentTypeFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                          CreativeCacheService* creativeCacheService,
                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

bool findAtLeastOneCreativeWithMatchingContentType (std::shared_ptr<gicapods::ConcurrentHashMap<int, Creative > > allCreativeListForThisTargetGroup,
                                                    std::shared_ptr<TargetGroup> tg,
                                                    std::string protocolVersion,
                                                    std::shared_ptr<OpportunityContext> context);

virtual ~CreativeContentTypeFilter();
static bool doesCreativeHaveRightContentType(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative>& crt);


std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

#ifndef CreativeSizeFilter_H
#define CreativeSizeFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "CreativeTypeDefs.h"
#include "Object.h"
#include "EntityProviderService.h"
#include "CacheUpdateWatcher.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
class Creative;
class TargetGroupCreativeCacheService;
class EntityToModuleStateStats;
class TargetGroupCacheService;
class CreativeCacheService;

class CreativeSizeFilter;



class CreativeSizeFilter : public TgMarker, public CacheUpdateWatcher {

public:

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

CreativeCacheService* creativeCacheService;
TargetGroupCreativeCacheService* targetGroupCreativeCacheService;

CreativeSizeFilter(TargetGroupCreativeCacheService* targetGroupCreativeCacheService,
                   CreativeCacheService* creativeCacheService,
                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~CreativeSizeFilter();
void cacheWasUpdatedEvent();


std::vector<std::shared_ptr<Creative> > getCreativesAssignedToTg(int targetGroupId);

bool rightSizeCreativeExist(std::string requstedCreativeSize, std::vector<std::shared_ptr<Creative> >& crtList);

static bool hasCreativeTheRequestedSize(std::shared_ptr<OpportunityContext> context,
                                        std::shared_ptr<Creative> crt);
std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg,  std::shared_ptr<OpportunityContext> context);
};

#endif

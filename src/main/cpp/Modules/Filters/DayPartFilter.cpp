#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "DayPartFilter.h"
#include "DateTimeUtil.h"
#include "TargetGroupDayPartCacheService.h"
#include "JsonArrayUtil.h"
#include "EntityToModuleStateStats.h"

DayPartFilter::DayPartFilter(TargetGroupDayPartCacheService* targetGroupDayPartCacheService,
                             EntityToModuleStateStats* entityToModuleStateStats,
                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->targetGroupDayPartCacheService = targetGroupDayPartCacheService;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

void DayPartFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "DayPartFilter cache cleared";
        attributesToGoodTgs->clear();
}

int DayPartFilter::getUserTimeZoneDifferenceWithUTC(std::shared_ptr<OpportunityContext> context) {
        //to be implemented later //read this from the IPInfo that comes from maxmind
        return context->userTimeZonDifferenceWithUTC;
}


std::string DayPartFilter::getName() {
        return "DayPartFilter";
}


void DayPartFilter::process(std::shared_ptr<OpportunityContext> context) {
        //this filter is a bidder module so we call this function here
        markFilteredTgs (context);
}

DayPartFilter::~DayPartFilter() {

}

bool DayPartFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        auto tgToHourMap = targetGroupDayPartCacheService->mapOfTargetGroupsToMapOfHoursInWeek->getOptional (tg->getId());
        if (tgToHourMap != nullptr) {
                int userActiveInLocalTime = DateTimeUtil::getCurrentHourAsIntegerInUTC ()
                                            + DayPartFilter::getUserTimeZoneDifferenceWithUTC (context);

                MLOG(2)<<" userActiveInLocalTime : "<< userActiveInLocalTime <<
                        " DayPartFilter::getUserTimeZoneDifferenceWithUTC (context) : "
                       <<DayPartFilter::getUserTimeZoneDifferenceWithUTC (context)
                       <<" DateTimeUtil::getCurrentHourAsIntegerInUTC () : " <<DateTimeUtil::getCurrentHourAsIntegerInUTC ();



                int userActiveInWeekHourNumber = DateTimeUtil::getHourNumberInWeekBasedOnUserOffset(DayPartFilter::getUserTimeZoneDifferenceWithUTC (context));
                MLOG(2) << " looking for key in hoursMap : " <<userActiveInWeekHourNumber;

                auto hoursMap = tgToHourMap;
                auto hourInDayPairPtr = hoursMap->map->getOptional(userActiveInWeekHourNumber);

                if(hourInDayPairPtr != nullptr) {
                        {
                                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_RIGHT_HOUR",
                                                                                   getName (),
                                                                                   StringUtil::toStr ("tg") +
                                                                                   StringUtil::toStr (tg->getId ()));
                                filterTg = false;
                        }
                } else {
                        MLOG(2) << "hoursMap content for tg : "<<userActiveInWeekHourNumber << " : " <<
                                JsonArrayUtil::convertListToJson(hoursMap->map->getKeys());

                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_INACTIVE_HOUR",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                        filterOutTgBasedOnDayparting (tg, userActiveInLocalTime, context);
                }
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NO_HOUR_SETTING",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                MLOG(2) << " PASSED_FOR_NO_HOUR_SETTING ";
                filterTg = false; //there is no day part for this target group
        }
        return filterTg;
}

void DayPartFilter::filterOutTgBasedOnDayparting(std::shared_ptr<TargetGroup> tg,
                                                 int userActiveInUTC,
                                                 std::shared_ptr<OpportunityContext> context) {
        MLOG (3) << " DayPartFilter::getUserTimeZoneDifferenceWithUTC (context) " <<
                DayPartFilter::getUserTimeZoneDifferenceWithUTC (context)
                 << " DateTimeUtil::getCurrentHourAsIntegerInUTC() " << DateTimeUtil::getCurrentHourAsIntegerInUTC ()
                 << "filtering tg based on dayparting userActiveInUTC is " << userActiveInUTC;

}

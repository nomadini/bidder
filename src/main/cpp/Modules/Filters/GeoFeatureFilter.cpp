#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "TargetGroupCacheService.h"
#include "GeoFeatureFilter.h"
#include "Place.h"
#include "JsonArrayUtil.h"
#include "Tag.h"
#include "PlacesCachedResult.h"
#include "ConfigService.h"
#include <boost/foreach.hpp>
#include "Poco/URI.h"

#include "PocoSession.h"
GeoFeatureFilter::GeoFeatureFilter(
        AeroCacheService<PlacesCachedResult>* aerospikePlaceCache,
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        int failureThresholdPercentageArg)
        : BiddingMonitor(entityToModuleStateStats,
                         failureThresholdPercentageArg,
                         "GeoFeatureFilterMonitor"),
        TgMarker(filterNameToFailureCounts),
        Object(__FILE__) {

        this->entityToModuleStateStats = entityToModuleStateStats;
        this->aerospikePlaceCache = aerospikePlaceCache;
        NULL_CHECK(this->aerospikePlaceCache);

        session = std::make_shared<PocoSession>(
                "GeoFeatureFilter",
                configService->getAsBooleanFromString("placeFinderCallKeepAliveMode"),
                configService->getAsInt("placeFinderKeepAliveTimeoutInMillis"),
                configService->getAsInt("placeFinderCallTimeoutInMillis"),
                configService->get("placeFinderAppHostUrl"),
                entityToModuleStateStats);

}

std::string GeoFeatureFilter::getName() {
        return "GeoFeatureFilter";
}

void GeoFeatureFilter::fetchPlacesAndTags(std::shared_ptr<OpportunityContext> context) {

        int maxNumebrOfWantedFeatures = 5;
        context->allPlaces = getPlacesFromCache(
                context->mgrs10m,
                context->deviceLat,
                context->deviceLon,
                maxNumebrOfWantedFeatures);

        //we set this to true, in order no to repeat this call in AsyncPipelineFeederModule
        //and only call this based on throttler, becauses this is an expensive call
        context->fetchingPlacesFromRemoteExecuted = true;
        context->tagIds = getTagIdsOfFeatures(context->allPlaces);

}
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > GeoFeatureFilter::filterTargetGroups(std::shared_ptr<OpportunityContext> context) {

        if (context->fetchingPlacesFromRemoteExecuted == false) {
                if (geoFeatureFilterRemoteCallThrottler->isPartOfSample()) {
                        //this is a very expensive call that we want to avoid based on a sampler
                        fetchPlacesAndTags(context);
                }
        }
        markFilteredTgs(context);
}

bool GeoFeatureFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                                         std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;


        if (targetGroupIdsWithGeoCollectionAssignments->find(tg->getId()) == targetGroupIdsWithGeoCollectionAssignments->end()) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSING_NO_GEO_COLLECITON_ASSIGNMENT_FOUND",
                                                                   "GeoFeatureFilter",
                                                                   "tg" + tg->idAsString);
                return false;
        }



        for(auto tagId : context->tagIds) {
                std::string key = tg->idAsString + "g" + _toStr(tagId);

                if (targetGroupToGeoCollectionKeys->find(key) != targetGroupToGeoCollectionKeys->end()) {
                        filterTg = false;
                        entityToModuleStateStats->addStateModuleForEntity ("PASSING_FOUND_NEAR_FEATURES",
                                                                           "GeoFeatureFilter",
                                                                           "tg" + tg->idAsString);

                        context->addTargetingTypeForTg(tg->getId(),
                                                       TargetGroup::TARGETTING_TYPE_GEO_FEATURE_TARGETING);
                        break;
                }
        }

        return filterTg;
}

std::vector<std::shared_ptr<Place> > GeoFeatureFilter::getPlacesFromCache(
        std::string mgrs10m,
        double deviceLat,
        double deviceLon,
        int maxNumebrOfWantedFeatures) {

        auto cacheKey = std::make_shared<PlacesCachedResult>(mgrs10m, maxNumebrOfWantedFeatures);
        int cachedResult = 0;

        auto inMemoryValue = aerospikePlaceCache->readDataOptional(cacheKey, cachedResult);
        if (cachedResult != AerospikeDriver::CACHE_MISS) {
                entityToModuleStateStats->
                addStateModuleForEntity("IN_MEMORY_CACHE_HIT",
                                        "GeoFeatureFilter",
                                        "ALL",
                                        EntityToModuleStateStats::important);

                return inMemoryValue->places;
        }

        std::vector<std::shared_ptr<Place> > allPlaces;
        try {
                numberOfTotal->increment();
                allPlaces = getPlacesFromRemote(
                        deviceLat,
                        deviceLon,
                        maxNumebrOfWantedFeatures);
                entityToModuleStateStats->
                addStateModuleForEntity("IN_MEMORY_CACHE_MISS",
                                        "GeoFeatureFilter",
                                        "ALL");

                cacheKey->places = allPlaces;
                aerospikePlaceCache->putDataInCache(cacheKey);
        } catch(std::exception& e) {
                entityToModuleStateStats->addStateModuleForEntity("EXCEPTION_IN_PLACES_FETCH",
                                                                  "GeoFeatureFilter",
                                                                  EntityToModuleStateStats::all,
                                                                  EntityToModuleStateStats::warning);

                auto diagnosticInformation = boost::diagnostic_information (e);
                LOG_EVERY_N(ERROR, 1000) << "Got the " << google::COUNTER << "th exception "
                                         <<" while handling request : "<<diagnosticInformation;

                numberOfFailures->increment();
        }

        return allPlaces;
}

std::vector<std::shared_ptr<Place> > GeoFeatureFilter::getPlacesFromRemote(
        double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures) {


        int numberOfTries = 1;
        auto docResponse = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(docResponse.get(), "lat", deviceLat, value);
        JsonUtil::addMemberToValue_FromPair(docResponse.get(), "lon", deviceLon, value);
        JsonUtil::addMemberToValue_FromPair(docResponse.get(), "maxNumebrOfWantedFeatures", maxNumebrOfWantedFeatures, value);

        std::string requestBody = JsonUtil::valueToString (value);


        auto response = session->sendRequest(
                *PocoHttpRequest::createSimplePostRequest(placeFinderAppHostUrl + "/maxmind-data-fetch/",
                                                          requestBody));

        std::string placesInJson = response->body;
        LOG_EVERY_N(INFO, 1000)<< google::COUNTER<<"th response->body : "<< placesInJson;

        return parsePlacesFromJson(placesInJson);
}

std::vector<std::shared_ptr<Place> >
GeoFeatureFilter::parsePlacesFromJson(std::string placesInJson) {

        std::vector<std::shared_ptr<Place> > allPlaces;
        if (!placesInJson.empty()) {
                auto doc = parseJsonSafely(placesInJson);
                JsonArrayUtil::getArrayFromValueMemeber(*doc,"places", allPlaces);
        }

        return allPlaces;

}
std::vector<int> GeoFeatureFilter::getTagIdsOfFeatures(
        std::vector<std::shared_ptr<Place> > allPlaces) {
        std::set<int> tagIdsSet;
        for(auto place :  allPlaces) {
                std::vector<int> tagIds = getTagsOfPlace(place);
                for (auto tagId : tagIds) {
                        tagIdsSet.insert(tagId);
                }

        }

        return CollectionUtil::convertSetToList(tagIdsSet);
}

std::vector<int> GeoFeatureFilter::getTagsOfPlace(std::shared_ptr<Place> place) {
        std::vector<int> tagIds;
        for (auto tag : place->tags) {
                tagIds.push_back(tag->id);
        }

        return tagIds;
}

GeoFeatureFilter::~GeoFeatureFilter() {

}


#include "Status.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"
#include "Creative.h"

#include "BidderMainPipelineProcessor.h"
#include "CollectionUtil.h"
#include "GeoLocation.h"
#include "BidderLatencyRecorder.h"
#include "GeoSegment.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "BidderModule.h"
#include "BidderApplicationContext.h"
#include "TargetGroupFilterChainModule.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"

#include "GeoLocationFilter.h"
#include "TopMgrsFilter.h"

#include "CreativeSizeFilter.h"

#include "MatchingIabCategoryFilter.h"
#include "DomainBlackListedFilter.h"
#include "DomainWhiteListedFilter.h"
#include "ActiveFilterModule.h"
#include "BlockedAdvertiserDomainFilter.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BlockedIabCategoryFilter.h"
#include "AdPositionFilter.h"
#include "CreativeBannerApiFilter.h"
#include "OldRealTimeInfoFilter.h"
#include "TgMarker.h"
#include "CappedBiddingPerHourFilter.h"
#include <boost/foreach.hpp>
#include "MySqlBWListService.h"
#include "CappedBiddingForTargetGroupPerDayFilter.h"
#include "GeoFeatureFilter.h"
#include "BeanFactory.h"
#include "MySqlBWListService.h"
#include "CappedBiddingForTargetGroupPerDayFilter.h"

#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "CreativeCacheService.h"
#include "MySqlBWListService.h"
#include "TargetGroupBWListCacheService.h"

#include "TargetGroupCreativeCacheService.h"
#include "TargetGroupGeoSegmentCacheService.h"
#include "TargetGroupDayPartCacheService.h"
#include "TargetGroupGeoLocationCacheService.h"
#include "TgFilterMeasuresService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "DeviceTypeFilter.h"
#include "OsTypeFilter.h"
#include "BlockedCreativeAttributeFilter.h"

TargetGroupFilterChainModule::TargetGroupFilterChainModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts) {

        orderedListOfFiltersToRun = std::make_shared<std::vector<std::shared_ptr<TgMarker> > >();
        this->entityToModuleStateStats = entityToModuleStateStats;

        originalName  = "TargetGroupFilterChainModule";
        filtersAreReady = std::make_shared<gicapods::AtomicBoolean>(false);

}
void TargetGroupFilterChainModule::addFilters() {

        LOG(INFO) << "initializing the filter map";
        //we start by adding the cheapest filters operationally
        initFilter(matchingIabCategoryFilter);
        initFilter(blockedAdvertiserDomainFilter);
        initFilter(blockedIabCategoryFilter);
        initFilter(adPositionFilter);
        initFilter(creativeSizeFilter);
        initFilter(blockedBannerAdTypeFilter);
        initFilter(blockedCreativeAttributeFilter);
        initFilter(deviceTypeFilter);
        initFilter(osTypeFilter);

        initFilter(domainBlackListedFilter);
        initFilter(domainWhiteListedFilter);
        initFilter(creativeBannerApiFilter);
        initFilter(creativeContentTypeFilter);

        initFilter (geoLocationFilter);
        initFilter (topMgrsFilter);
        initFilter(geoFeatureFilter);
        filtersAreReady->setValue(true);
        /**not implemented yet***/

}
void TargetGroupFilterChainModule::initFilter(std::shared_ptr<TgMarker> filter) {
        NULL_CHECK(filter);
        std::string filterName = filter->getName();
        std::string propertyValue = configService->get(filterName + "IsEnabled");
        if (StringUtil::equalsIgnoreCase(propertyValue, "true")) {
                filter->entityToModuleStateStats = this->entityToModuleStateStats;
                orderedListOfFiltersToRun->push_back (filter);
        } else if (StringUtil::equalsIgnoreCase(propertyValue, "false")) {
                BidderMainPipelineProcessor::logExclusion(filterName, entityToModuleStateStats);
        } else {
                LOG(ERROR) << "property is filterName : "
                           <<filterName<<", wrong : "<< propertyValue;
                EXIT("");
        }
}


void TargetGroupFilterChainModule::process(std::shared_ptr<OpportunityContext> context) {
        if (filtersAreReady->getValue() == false) {
                entityToModuleStateStats->addStateModuleForEntity ("FILTERS_ARE_NOT_READY",
                                                                   "TargetGroupFilterChainModule",
                                                                   "ALL");

                return;
        }
        entityToModuleStateStats->addStateModuleForEntity ("sizeOfTargetGroup_StartOfFilterChain"  + StringUtil::toStr(context->getSizeOfUnmarkedTargetGroups()),
                                                           "TargetGroupFilterChainModule",
                                                           "ALL");



        //make all these filters use tgs in the context. you don't need to pass any tgs around
        for(auto filter : *orderedListOfFiltersToRun) {

                //I am adding try catch only around filterTargetGroups method invocation since I don't want
                //any exception thrown in that function , winds up the stack and I dont get the correct lastFilterName
                try {

                        int beforeTgSize = context->getSizeOfUnmarkedTargetGroups();
                        if (beforeTgSize > 0) {

                                runFilter(filter.get(),
                                          context,
                                          entityToModuleStateStats,
                                          bidderLatencyRecorder);

                                int afterTgSize = context->getSizeOfUnmarkedTargetGroups();

                                int filterPercentage = 0;
                                if (beforeTgSize != 0) {
                                        filterPercentage  = (beforeTgSize - afterTgSize) * 100 / beforeTgSize;
                                }
                                entityToModuleStateStats->addStateModuleForEntityWithValue((filter)->getName(),
                                                                                           filterPercentage,
                                                                                           "TargetGroupFilterChainModule-PercentOfFilteringTg",
                                                                                           "ALL");

                                if (filterPercentage >= 100) {
                                        LOG_EVERY_N(ERROR, 10000)<< google::COUNTER<<"th percenteOfFilteringTg for filter "<< filter->getName()<< " is "<< filterPercentage;
                                }
                                if (afterTgSize == 0) {
                                        entityToModuleStateStats->addStateModuleForEntity(
                                                (filter)->getName(),
                                                "TargetGroupFilterChainModule-LastChainFilter",
                                                "ALL"
                                                );
                                }
                                MLOG(3)<< "percenteOfFilteringTg for filter "<< filter->getName()<< " is "<< filterPercentage;
                        }
                } catch (std::exception const &e) {
                        entityToModuleStateStats->addStateModuleForEntity ("Exception : " + StringUtil::toStr(e.what()),
                                                                           "TargetGroupFilterChainModule",
                                                                           "ALL");
                } catch (...) {
                        entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                           "TargetGroupFilterChainModule",
                                                                           "ALL");
                }



                if (context->getSizeOfUnmarkedTargetGroups() == 0) {
                        break;
                }
        }


        //http://69.200.247.201:9980/pages/filtermap
        //printFilterNameToTargetGroupCounterMap();
        //this line should be commented out later, this line is for disabling the target group filter!

        if (context->getSizeOfUnmarkedTargetGroups() == 0) {
                entityToModuleStateStats->addStateModuleForEntity ("AllTargetGroupsWereFiltered",
                                                                   "TargetGroupFilterChainModule",
                                                                   "ALL");

                context->status->value = STOP_PROCESSING;
        } else {
                context->status->value = CONTINUE_PROCESSING;
        }


}

void TargetGroupFilterChainModule::runFilter(TgMarker* filter,
                                             std::shared_ptr<OpportunityContext> context,
                                             EntityToModuleStateStats* entityToModuleStateStats,
                                             BidderLatencyRecorder* bidderLatencyRecorder) {

        Poco::Timestamp moduleStartTime;

        filter->beforeMarking(context);
        filter->markFilteredTgs(context);
        filter->afterMarking(context);

        BidderLatencyRecorder::logTimeTaken(filter->getName(), moduleStartTime, 10000, 2);

        bidderLatencyRecorder->recordLatency(moduleStartTime,
                                             100
                                             , filter->getName());

}

std::string TargetGroupFilterChainModule::getName() {
        return originalName;
}

TargetGroupFilterChainModule::~TargetGroupFilterChainModule() {

}
bool TargetGroupFilterChainModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

/**************************************/
/**************************************/
/************ helper methods***********/
/**************************************/
/**************************************/


//<bean id="evalTargetFilterChain" class="org.apache.commons.chain.impl.ChainBase">
//	<constructor-arg type="java.util.Collection">
//		<!-- order by filter % - heavy-duty filtering first -->
//		<list>
//			<ref bean="filterBySpendSegmentTargetCommand" />
//			<!-- geo filters a lot -->
//			<ref bean="filterBySpendGeoTargetCommand" />
//			<ref bean="filterByCreativeGeoTargetCommand" />
//
//			<!-- pacing filters need to run after spend-segment & geo (as per DS) -->
//			<ref bean="filterByCampaignPacingTargetCommand" />
//			<ref bean="filterBySpendPacingTargetCommand" />
//			<ref bean="filterByCreativePacingTargetCommand" />
//
//			<ref bean="filterByATExclusionCommand" />
//			<ref bean="filterByHostIdsTargetCommand" />
//            <ref bean="filterByAppIdsTargetCommand" />
//			<ref bean="filterByInventoryMarketerTargetCommand" />
//			<ref bean="filterBySpendDeviceClassTargetCommand" />
//			<ref bean="filterBySpendEnvTypeTargetCommand" />
//
//			<ref bean="filterByBmsExtCommand" />
//			<ref bean="filterByBmsIntCommand" />
//
//			<ref bean="filterByCreativeVideoDataCommand" />
//			<ref bean="filterBySpendSectionTargetCommand" />
//			<ref bean="filterSpendByRemoteAgencyCommand"/>
//			<ref bean="filterBySpendInventoryTargetCommand" />
//			<ref bean="filterBySpendDayPartCommand" />
//			<ref bean="filterByDelayFreqRecencyTargetsCommand" />
//			<ref bean="filterByIndustryCategoriesCommand" />
//			<ref bean="filterByIabCategoriesCommand" />
//			<ref bean="filterByTopLevelDomainCommand" />
//			<ref bean="filterByTechSpecTargetCommand" />
//			<ref bean="filterBySpendAdPositionTargetCommand" />
//			<ref bean="filterBySpendPublisherCategoryTargetCommand" />
//			<ref bean="filterByCreativeRemoteStatusCommand" />
//			<ref bean="filterByCreativeVendorCommand" />
//			<ref bean="filterByCreativeAdAttributeCommand" />
//			<ref bean="filterInventoryByEnvTypeCommand" />

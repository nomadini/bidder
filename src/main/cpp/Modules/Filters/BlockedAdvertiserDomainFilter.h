#ifndef BlockedAdvertiserDomainFilter_H
#define BlockedAdvertiserDomainFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "ObjectVectorHolder.h"
#include "CacheUpdateWatcher.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
#include "CacheService.h"
#include "Advertiser.h"
class CampaignCacheService;


class BlockedAdvertiserDomainFilter;



class BlockedAdvertiserDomainFilter : public TgMarker, public CacheUpdateWatcher {

public:

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

CampaignCacheService* campaignCacheService;
CacheService<Advertiser>* advertiserCacheService;

BlockedAdvertiserDomainFilter(CacheService<Advertiser>* advertiserCacheService,
                              CampaignCacheService* campaignCacheService,
                              std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~BlockedAdvertiserDomainFilter();



void cacheWasUpdatedEvent();
bool hasTargetGroupAnyBlockingAdvertiserDomain(std::shared_ptr<OpportunityContext> context, std::shared_ptr<TargetGroup> tg);

std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > getAdvertiserDomainForTargetGroup(std::shared_ptr<TargetGroup> tg);

std::string getName();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

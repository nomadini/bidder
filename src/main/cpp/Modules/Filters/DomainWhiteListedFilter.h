#ifndef DomainWhiteListedFilter_H
#define DomainWhiteListedFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "ConcurrentHashMap.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
class TargetGroupBWListCacheService;
#include "CacheUpdateWatcher.h"
#include "Object.h"
#include "ObjectVectorHolder.h"
#include "CacheUpdateWatcher.h"
class DomainWhiteListedFilter;

class DomainWhiteListedFilter : public TgMarker, public CacheUpdateWatcher {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

TargetGroupBWListCacheService* targetGroupBWListCacheService;

DomainWhiteListedFilter(TargetGroupBWListCacheService* targetGroupBWListCacheService,
                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~DomainWhiteListedFilter();

void cacheWasUpdatedEvent();



bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

std::string getName();
};

#endif

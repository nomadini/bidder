#ifndef TargetGroupFilterChainModule_H
#define TargetGroupFilterChainModule_H



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class TargetGroupCacheService;
#include "CreativeTypeDefs.h"
class Creative;
#include "Object.h"
#include "CollectionUtil.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
class OpportunityContext;
class EntityToModuleStateStats;
#include "BidderModule.h"
#include "BidderApplicationContext.h"
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TgMarker.h"
#include "Object.h"
#include <unordered_set>
#include "ConfigService.h"
#include "TopMgrsFilter.h"
#include "GeoFeatureFilter.h"

class BidderLatencyRecorder;

class CampaignCacheService;
class TargetGroupCacheService;
class CreativeCacheService;
class MySqlBWListService;
class TargetGroupBWListCacheService;
class TargetGroupCreativeCacheService;
class TargetGroupGeoSegmentCacheService;
class TargetGroupDayPartCacheService;
class TargetGroupGeoLocationCacheService;
class TgFilterMeasuresService;
class EntityDeliveryInfoCacheService;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;

class BeanFactory;

class TargetGroupFilterChainModule : public BidderModule {

public:
EntityToModuleStateStats* entityToModuleStateStats;
BidderLatencyRecorder* bidderLatencyRecorder;

std::shared_ptr<TgMarker> geoFeatureFilter;
std::shared_ptr<TgMarker> matchingIabCategoryFilter;
std::shared_ptr<TgMarker> domainBlackListedFilter;
std::shared_ptr<TgMarker> domainWhiteListedFilter;
std::shared_ptr<TgMarker> creativeBannerApiFilter;

std::shared_ptr<TgMarker> adPositionFilter;
std::shared_ptr<TgMarker> blockedAdvertiserDomainFilter;
std::shared_ptr<TgMarker> blockedIabCategoryFilter;
std::shared_ptr<TgMarker> creativeSizeFilter;

std::shared_ptr<TgMarker> creativeContentTypeFilter;
std::shared_ptr<TgMarker> blockedBannerAdTypeFilter;
std::shared_ptr<TgMarker> blockedCreativeAttributeFilter;
std::shared_ptr<TgMarker> deviceTypeFilter;
std::shared_ptr<TgMarker> osTypeFilter;
std::shared_ptr<TgMarker> geoLocationFilter;
std::shared_ptr<TgMarker> topMgrsFilter;

std::shared_ptr<gicapods::AtomicBoolean> filtersAreReady;
gicapods::ConfigService* configService;

TargetGroupFilterChainModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::shared_ptr<std::vector<std::shared_ptr<TgMarker> > > orderedListOfFiltersToRun;

std::string originalName;

void initFilter(std::shared_ptr<TgMarker> filter);

void addFilters();

public:

static void runFilter(TgMarker* filter,
                      std::shared_ptr<OpportunityContext> context,
                      EntityToModuleStateStats* entityToModuleStateStats,
                      BidderLatencyRecorder* bidderLatencyRecorder);

virtual void process(std::shared_ptr<OpportunityContext> context);

virtual std::string getName();

virtual ~TargetGroupFilterChainModule();


bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};



#endif

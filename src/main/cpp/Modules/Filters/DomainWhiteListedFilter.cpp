#include "DomainWhiteListedFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupBWListCacheService.h"

#include <boost/foreach.hpp>
#include "CollectionUtil.h"
#include "ConcurrentHashMap.h"

DomainWhiteListedFilter::DomainWhiteListedFilter(TargetGroupBWListCacheService* targetGroupBWListCacheService,
                                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts) {
        this->targetGroupBWListCacheService = targetGroupBWListCacheService;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}
void DomainWhiteListedFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "DomainWhiteListedFilter cache cleared";
        attributesToGoodTgs->clear();
}

DomainWhiteListedFilter::~DomainWhiteListedFilter() {

}

bool DomainWhiteListedFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        auto map = targetGroupBWListCacheService->getTargetGroupIdToWhiteListsMap();

        bool filterTg = true;
        //this is map of target gruop ids to map of whitelisted domains
        auto domainMapContainer = map->getOptional (tg->getId ());
        if ( domainMapContainer != nullptr) {
                MLOG(2) << "targetgroup id : " << tg->getId () <<
                        "has whitelist in the map "<<
                        " cosidering siteDomain '"<< context->siteDomain<<"'";
                auto domainMap =  domainMapContainer->map;
                if (domainMap->exists (StringUtil::toLowerCase (context->siteDomain))) {
                        MLOG(2) << "targetgroup id : " << tg->getId () <<
                                "qualified for siteDomain '"<< context->siteDomain<<"'";
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                           "DomainWhiteListedFilter",
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                        filterTg = false;
                } else {
                        MLOG(2) << "targetgroup id : " << tg->getId () <<
                                "disqualified for siteDomain '"<< context->siteDomain<<"'";
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           "DomainWhiteListedFilter",
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);

                }

        } else {
                MLOG(2) << "targetgroup id : " << tg->getId () <<
                        "qualified for siteDomain '"<< context->siteDomain<<"' for not having whitelist";

                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NO_WHITE_LIST",
                                                                   "DomainWhiteListedFilter",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

                filterTg = false;
        }

        return filterTg;
}

std::string DomainWhiteListedFilter::getName() {
        return "DomainWhiteListedFilter";
}

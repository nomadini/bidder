#ifndef MatchingIabCategoryFilter_H
#define MatchingIabCategoryFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include "TgMarker.h"
#include "Object.h"
class MatchingIabCategoryFilter;

class MatchingIabCategoryFilter : public TgMarker, public Object {

public:
MatchingIabCategoryFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~MatchingIabCategoryFilter();

std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

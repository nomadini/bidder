/*
 * TopMgrsFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef TopMgrsSegment_H_
#define TopMgrsSegment_H_


#include "Object.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
#include "TopMgrsSegment.h"
class OpportunityContext;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TgMarker.h"
class TargetGroupMgrsSegmentCacheService;

class TopMgrsFilter : public TgMarker, public Object {

private:
TargetGroupMgrsSegmentCacheService* targetGroupMgrsSegmentCacheService;
public:

TopMgrsFilter(TargetGroupMgrsSegmentCacheService* targetGroupMgrsSegmentCacheService,
              std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

bool topMgrsSegmentMatch(std::shared_ptr<OpportunityContext> context,
                         std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> > topMgrsSegment);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > filterTargetGroups(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

virtual ~TopMgrsFilter();
};



#endif /* TopMgrsSegment_H_ */

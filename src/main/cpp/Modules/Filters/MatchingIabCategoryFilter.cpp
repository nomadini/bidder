#include "MatchingIabCategoryFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "TargetGroupCacheService.h"
#include <boost/foreach.hpp>

MatchingIabCategoryFilter::MatchingIabCategoryFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts), Object(__FILE__) {
}

MatchingIabCategoryFilter::~MatchingIabCategoryFilter() {

}

std::string MatchingIabCategoryFilter::getName() {
        return "MatchingIabCategoryFilter";
}

bool MatchingIabCategoryFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        /**
         * this filter cares about subCategory and Not IAB Category
         * This filter checks a site category agains a tg iab category, to make sure that tg is showing
         * impressions on the site with the exact site category, I think we can have an option for tgs like
         * show on matching iab category site , to make this filter relax in case we are not showing enough impressions
         *
         */
        MLOG (10) << "target group id : " << tg->getId()
                  << " tg iabCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabCategory())
                  << " tg iabSubCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabSubCategory());

        if (tg->getIabSubCategory()->empty() && tg->getIabCategory()->empty()) {

                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NO_IAB_TARGETING",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;

        } else {
                bool failed = true;

                for(std::string siteCat :  context->siteCategory) {
                        for(std::string tgSubCat :  *tg->getIabSubCategory()) {
                                if (StringUtil::equalsIgnoreCase (tgSubCat, siteCat)) {
                                        entityToModuleStateStats->addStateModuleForEntity (
                                                "PASSED_FOR_MATCHING_IAB_SUB_CATEGORY",
                                                getName (),
                                                StringUtil::toStr ("tg") +
                                                StringUtil::toStr (tg->getId ()));
                                        filterTg = false;

                                        failed = false;
                                        break;
                                }
                        }
                }
                for(std::string siteCat :  context->siteCategory) {
                        for(std::string tgCat :  *tg->getIabCategory()) {
                                if (StringUtil::equalsIgnoreCase (tgCat, siteCat)) {
                                        entityToModuleStateStats->addStateModuleForEntity (
                                                "PASSED_FOR_MATCHING_IAB_CATEGORY",
                                                getName (),
                                                StringUtil::toStr ("tg") +
                                                StringUtil::toStr (tg->getId ()));
                                        filterTg = false;
                                        failed = false;
                                        break;
                                }
                        }
                }

                if (failed) {
                        MLOG(2) << "not matching categories :   " <<  JsonArrayUtil::convertListToJson (*tg->getIabSubCategory ()) << " , "
                                << " tg iabCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabCategory())
                                <<" context->siteCategory : " << JsonArrayUtil::convertListToJson (context->siteCategory);

                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_UN_MATCHING_IAB_SUB_CATEGORY",
                                                                           getName (),
                                                                           StringUtil::toStr ("tg") +
                                                                           StringUtil::toStr (tg->getId ()));
                }
        }

        return filterTg;
}

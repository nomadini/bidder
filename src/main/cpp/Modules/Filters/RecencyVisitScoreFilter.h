#ifndef RecencyVisitScoreFilter_H
#define RecencyVisitScoreFilter_H


#include "BidderModule.h"
#include "EntityProviderService.h"
#include "Object.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include <unordered_map>
class RecentVisitHistoryScoreCard;
class AerospikeDriverInterface;
class Device;
class TargetGroupCacheService;
class RecentVisitHistoryScoreReader;
class RecencyModel;
class TargetGroupRecencyModelMapCacheService;
class RecencyModel;
class TgModelEligibilityRecord;
class TargetGroup;
class EntityToModuleStateStats;

class RecencyVisitScoreFilter : public BidderModule, public Object {

public:
static std::string RECENT_VISIT_HISTORY_BIN_NAME;
static std::string RECENT_VISIT_HISTORY_NAMESPACE;
static std::string RECENT_VISIT_HISTORY_SET;
AerospikeDriverInterface* aeroSpikeDriver;
TargetGroupCacheService* targetGroupCacheService;
RecentVisitHistoryScoreReader* recentVisitHistoryScoreReader;

RecencyVisitScoreFilter(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

virtual ~RecencyVisitScoreFilter();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};


#endif

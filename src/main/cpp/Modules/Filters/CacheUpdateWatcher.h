#ifndef COMMON_CacheUpdateWatcher_H
#define COMMON_CacheUpdateWatcher_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include <tbb/concurrent_hash_map.h>
#include "OpportunityContext.h"
#include "TgMarker.h"
class EntityToModuleStateStats;

/*
   you need to add the instance of CacheUpdateWatcher to cacheUpdateWatcherModules in bidderAsyncJobsService

   like this bidderAsyncJobsService->cacheUpdateWatcherModules.push_back(moduleContainer->deviceTypeFilter.get());
 */
class CacheUpdateWatcher {

public:
CacheUpdateWatcher();

virtual ~CacheUpdateWatcher();

virtual void cacheWasUpdatedEvent() = 0;

};



#endif //COMMON_CacheUpdateWatcher_H

#include "BlockedAdvertiserDomainFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"

#include "JsonArrayUtil.h"
#include "Advertiser.h"
#include "Campaign.h"
BlockedAdvertiserDomainFilter::BlockedAdvertiserDomainFilter(CacheService<Advertiser>* advertiserCacheService,
                                                             CampaignCacheService* campaignCacheService,
                                                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker(filterNameToFailureCounts) {
        this->campaignCacheService =  campaignCacheService;
        this->advertiserCacheService = advertiserCacheService;
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

BlockedAdvertiserDomainFilter::~BlockedAdvertiserDomainFilter() {

}

void BlockedAdvertiserDomainFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "BlockedAdvertiserDomainFilter cache cleared";
        attributesToGoodTgs->clear();
}

bool BlockedAdvertiserDomainFilter::hasTargetGroupAnyBlockingAdvertiserDomain(std::shared_ptr<OpportunityContext> context, std::shared_ptr<TargetGroup> tg) {

        std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > advertiserDomains = getAdvertiserDomainForTargetGroup(tg);
        std::string allAdvDomains;
        tbb::concurrent_hash_map<std::string, int>::iterator it;
        for (it = advertiserDomains->begin (); it != advertiserDomains->end (); ++it) {
                auto domainName  = it->first;
                allAdvDomains += domainName + " ,";
                if (context->getBlockedAdvertiserMap()->exists(StringUtil::toLowerCase (domainName))) {
                        MLOG (3) << "tg id : " << tg->getId() << "has is blocked by bid request domain name : " << domainName;
                        return true;
                }

                /*
                 * TODO : later , add a flag to addStateModuleForEntity to have this
                 * be registered only in tests , we don't need this in prod
                 */
                entityToModuleStateStats->addStateModuleForEntity ("TG_WAS_EVALUATED",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        std::string blockedAdvertiserInRequest;
        auto map = context->getBlockedAdvertiserMap()->getCopyOfMap();
        typename tbb::concurrent_hash_map<std::string, gicapods::AtomicLong>::iterator iter;
        for (auto iter = map->begin ();
             iter != map->end ();
             iter++) {
                blockedAdvertiserInRequest += iter->first + " , ";
        }

        MLOG (3) << "tg id : " << tg->getId() << "with adv domains " << allAdvDomains << " is not blocked by bid request blocking domains : "<<blockedAdvertiserInRequest;

        return false;
}

std::shared_ptr<tbb::concurrent_hash_map<std::string, int> > BlockedAdvertiserDomainFilter::getAdvertiserDomainForTargetGroup(std::shared_ptr<TargetGroup> tg) {
        auto campaign = this->campaignCacheService->findByEntityId(tg->getCampaignId());
        assertAndThrow(campaign !=NULL);

        assertAndThrow(this->advertiserCacheService !=NULL);
        assertAndThrow(this->advertiserCacheService->getAllEntitiesMap() != NULL);

        auto advertiser = this->advertiserCacheService->findByEntityId(campaign->getAdvertiserId());
        assertAndThrow(advertiser !=NULL);
        return advertiser->domainNames;
}

std::string BlockedAdvertiserDomainFilter::getName() {
        return "BlockedAdvertiserDomainFilter";
}

bool BlockedAdvertiserDomainFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        auto found = hasTargetGroupAnyBlockingAdvertiserDomain(context, tg);

        if (!found) {
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_BLOCKED_ADVERTISER",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        return filterTg;
}

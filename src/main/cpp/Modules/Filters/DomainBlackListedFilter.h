#ifndef DomainBlackListedFilter_H
#define DomainBlackListedFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "Object.h"
#include "MySqlBWListService.h"
class TargetGroupBWListCacheService;
#include <tbb/concurrent_hash_map.h>
#include "ConcurrentHashMap.h"
#include "CacheUpdateWatcher.h"
#include "ObjectVectorHolder.h"

class DomainBlackListedFilter : public TgMarker, public CacheUpdateWatcher {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

TargetGroupBWListCacheService* targetGroupBWListCacheService;

DomainBlackListedFilter(TargetGroupBWListCacheService* targetGroupBWListCacheService,
                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~DomainBlackListedFilter();

void cacheWasUpdatedEvent();
MySqlBWListService* mySqlBWListService;
std::string convertMapToJson(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg,
                       std::shared_ptr<OpportunityContext> context);
void buildMapOfTgsBlockingThisDomain(std::shared_ptr<OpportunityContext> context);
std::string getName();
};

#endif

#include "Status.h"
#include "TargetGroup.h"
#include "Creative.h"

#include "CollectionUtil.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroupGeoSegmentFilter.h"
#include "JsonUtil.h"
#include "TargetGroupGeoSegmentCacheService.h"

TargetGroupGeoSegmentFilter::TargetGroupGeoSegmentFilter(
        TargetGroupGeoSegmentCacheService* targetGroupGeoSegmentCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->targetGroupGeoSegmentCacheService = targetGroupGeoSegmentCacheService;

}

std::string TargetGroupGeoSegmentFilter::getName() {
        return "TargetGroupGeoSegmentFilter";
}


bool TargetGroupGeoSegmentFilter::processGeoSegmentValidationForTg(
        std::shared_ptr<TargetGroup> tg, std::vector<std::shared_ptr<GeoSegmentList> >& geoSegmentListForTg,
        std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        for (std::vector<std::shared_ptr<GeoSegmentList> >::iterator it =
                     geoSegmentListForTg.begin ();
             it != geoSegmentListForTg.end (); ++it) {

                std::shared_ptr<GeoSegmentList> oneGeoSegmentList = *it;
                MLOG(2) << "tg id :" << tg->getId() << " is evaluating this geosegment list " << oneGeoSegmentList->id;
                for (std::vector<std::shared_ptr<GeoSegment> >::iterator it =
                             oneGeoSegmentList->geoSegments.begin ();
                     it != oneGeoSegmentList->geoSegments.end (); ++it) {

                        if (context->deviceLat == 0 || context->deviceLon == 0) {
                                MLOG(2) << "this user doesnt have lat lon info needed, so filtering target group for this user";
                                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_USE_NOT_HAVING_GEO_INFO",
                                                                                   getName (),
                                                                                   StringUtil::toStr ("tg") +
                                                                                   StringUtil::toStr (tg->getId ()));
                        } else {
                                double distanceWithGeoSegment =
                                        gicapods::Util::getLatLonDistanceInMile (
                                                context->deviceLat,
                                                context->deviceLon, (*it)->lat,
                                                (*it)->lon);
                                MLOG(2) << "distanceWithGeoSegment :" << distanceWithGeoSegment <<
                                        " (*it)->segmentRadiusInMile : " <<
                                (*it)->segmentRadiusInMile;
                                if (distanceWithGeoSegment
                                    < (*it)->segmentRadiusInMile) {
                                        //this user is in the segment right now.
                                        //add him to the passing filters;
                                        filterTg = false;
                                        entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_GOOD_GEO_SEGMENT",
                                                                                           getName (),
                                                                                           StringUtil::toStr ("tg") +
                                                                                           StringUtil::toStr (tg->getId ()));

                                        MLOG(2) << " user is withing geoSegment "
                                                << " distanceWithGeoSegment :"
                                                << distanceWithGeoSegment
                                                << " (*it)->segmentRadiusInMile : "
                                                << (*it)->segmentRadiusInMile;

                                } else {
                                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_NOT_MATCHING_GEO_SEGMENT",
                                                                                           getName (),
                                                                                           StringUtil::toStr ("tg") +
                                                                                           StringUtil::toStr (tg->getId ()));

                                        MLOG(2) << " user is not withing geoSegment ";

                                }
                        }
                }
        }


        return filterTg;
}

void TargetGroupGeoSegmentFilter::process(std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool TargetGroupGeoSegmentFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        auto tgGeoSegmentList =
                targetGroupGeoSegmentCacheService->getAllTgGeoSegmentsListMap ()->getOptional (tg->getId());


        if (tgGeoSegmentList
            != nullptr) {
                std::vector<std::shared_ptr<GeoSegmentList> > geoSegmentListForTg = *tgGeoSegmentList->values;
                MLOG(2) << "tg id :" << tg->getId() << " has " << geoSegmentListForTg.size () << "geosegment lists.";

                entityToModuleStateStats->addStateModuleForEntity ("FOUND_GEO_SEGMENT_FOR_TG",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));

                auto found = processGeoSegmentValidationForTg(tg, geoSegmentListForTg, context);
                if(found) {
                        filterTg = false;
                }
        } else {
                MLOG(2) << "tg id :" << tg->getId() << " didn't have any assigned geosegment lists, so passing filter.";
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NOGEO_SEGMENT_FOUND_FOR_TG",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
                filterTg = false;
        }

        return filterTg;
}

TargetGroupGeoSegmentFilter::~TargetGroupGeoSegmentFilter() {


}

//The last example in the documented "User-defined queries" shows how to use a lambda in the predicate. This lambda can bind other variables in the scope, for instance, the point whose neighbors you are looking for.

//Here is a small example. The example looks for points that are closer to (5, 5) than 2 units, for a collection of points that lie on the straight y = x. It should be easy to modify in order to check first if the sought point is in the R-tree, or get it directly out of the R-tree.

#include <iostream>

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/index/rtree.hpp>


namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

typedef bg::model::point<float, 2, bg::cs::cartesian> point;
typedef std::pair<point, unsigned> value;

int mainUseMeLater2(int argc, char *argv[])
{
        bgi::rtree< value, bgi::quadratic<16> > rtree;

        // create some values
        for ( unsigned i = 0; i < 10; ++i )
        {
                point p = point(i, i);
                rtree.insert(std::make_pair(p, i));
        }

        // search for nearest neighbours
        std::vector<value> returned_values;
        point sought = point(5, 5);
        rtree.query(bgi::satisfies([&](value const& v) {
                return bg::distance(v.first, sought) < 2;
        }),
                    std::back_inserter(returned_values));

        // print returned values
        value to_print_out;
        for (size_t i = 0; i < returned_values.size(); i++) {
                to_print_out = returned_values[i];
                float x = to_print_out.first.get<0>();
                float y = to_print_out.first.get<1>();
                std::cout << "Select point: " << to_print_out.second << std::endl;
                std::cout << "x: " << x << ", y: " << y << std::endl;
        }

        return 0;
}














#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point.hpp>
#include <boost/geometry/geometries/box.hpp>
#include <boost/geometry/geometries/polygon.hpp>

#include <boost/geometry/index/rtree.hpp>

#include <cmath>
#include <vector>
#include <iostream>
#include <boost/foreach.hpp>

namespace bg = boost::geometry;
namespace bgi = boost::geometry::index;

int mainUseMeLater()
{
        typedef bg::model::point<float, 2, bg::cs::cartesian> point;
        typedef bg::model::box<point> box;
        typedef bg::model::polygon<point, false, false> polygon; // ccw, open polygon
        typedef std::pair<box, unsigned> value;

        // polygons
        std::vector<polygon> polygons;

        // create some polygons
        for ( unsigned i = 0; i < 10; ++i )
        {
                // create a polygon
                polygon p;
                for ( float a = 0; a < 6.28316f; a += 1.04720f )
                {
                        float x = i + int(10*::cos(a))*0.1f;
                        float y = i + int(10*::sin(a))*0.1f;
                        p.outer().push_back(point(x, y));
                }

                // add polygon
                polygons.push_back(p);
        }

        // display polygons
        std::cout << "generated polygons:" << std::endl;
        for(polygon const& p :  polygons)
                std::cout << bg::wkt<polygon>(p) << std::endl;

        // create the rtree using default constructor
        bgi::rtree< value, bgi::rstar<16, 4> > rtree;

        // fill the spatial index
        for ( unsigned i = 0; i < polygons.size(); ++i )
        {
                // calculate polygon bounding box
                box b = bg::return_envelope<box>(polygons[i]);
                // insert new value
                rtree.insert(std::make_pair(b, i));
        }

        // find values intersecting some area defined by a box
        box query_box(point(0, 0), point(5, 5));
        std::vector<value> result_s;
        rtree.query(bgi::intersects(query_box), std::back_inserter(result_s));

        // find 5 nearest values to a point
        std::vector<value> result_n;
        rtree.query(bgi::nearest(point(0, 0), 5), std::back_inserter(result_n));

        // note: in Boost.Geometry the WKT representation of a box is polygon

        // note: the values store the bounding boxes of polygons
        // the polygons aren't used for querying but are printed

        // display results
        std::cout << "spatial query box:" << std::endl;
        std::cout << bg::wkt<box>(query_box) << std::endl;
        std::cout << "spatial query result:" << std::endl;
        for(value const& v :  result_s)
                std::cout << bg::wkt<polygon>(polygons[v.second]) << std::endl;

        std::cout << "knn query point:" << std::endl;
        std::cout << bg::wkt<point>(point(0, 0)) << std::endl;
        std::cout << "knn query result:" << std::endl;
        for(value const& v :  result_n)
                std::cout << bg::wkt<polygon>(polygons[v.second]) << std::endl;

        return 0;
}

#ifndef DealFilter_H
#define DealFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include "TgMarker.h"
class DealFilter;
#include "Object.h"

class DealFilter : public TgMarker, public Object {

public:
DealFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~DealFilter();

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup>>> filterTargetGroups(
        std::shared_ptr<OpportunityContext> context);

std::string getName();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

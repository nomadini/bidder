#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GeoLocationFilter.h"
#include "TargetGroupGeoLocationCacheService.h"

#include "GeoLocationCacheService.h"
#include <boost/foreach.hpp>
/*
   We support several different geo targeting methods. This page exists to give brief overviews of their intended functionalities from a business perspective and how they're implemented within our systems.
   Methods of determining location
   Locale
   Locale data included in a request may contain a country, city, zip code (5 digit), region (state), and metro code. A request with locale data will not necessarily contain every piece of locale data.
   Lat-Long
   Lat long data will include a latitude and a longitude. These can be used to derive MGRS coordinates, ZIP (+4) codes, etc.

   A request that has locale data will not necessarily have lat-long data and vice versa. Additionally, our methods of mapping location data to segments tend to work on either locale data or lat-long data, but not both.
   Methods of mapping location data to segments
   Place Still
   This Place Still is designed to allow marketers to target bid requests from a certain type of place (e.g. a college campus, stores of a certain type, stadiums, etc.). Geographies in the Place Still contain sets of places rather than individual ones - it is not intended to allow targeting a specific area.
   Here Ever
   Bid requests are placed in Here Ever segments when the device making the request has been in a location at least once before.
   Here Now
   Bid requests are placed in Here Now segments when they are from a location being targeted.
   Home
   Devices are placed in Home segments when they have:
   Appeared in a location at least three times
   Not appeared anywhere else more often
   There aren't too many other devices sharing the exact same home
   Home segments can target MGRS, ZIP+4s, or ATZs. In the adserv code, there are two lookups:
   A lookup to determine the home geographies of the device. There may be up to three: an MGRS-100, a ZIP+4, and an ATZ.
   A lookup to determine whether any home segments are targeting those geos. This lookup happens at the same time as the second Visitor lookup to map geos to segments (see below).
   Code involved: GeoPairSchemeV2 does this lookup.
   This lookup is throttled for performance reasons - it will not be done for every request.
   Visitor
   Bid requests are placed in Visitor segments when they are from a location being targeted. Visitor segments can target MGRS, ZIP+4s, or ATZs. In the adserv code, there are two lookups:
   A lookup to determine the geographies the device is currently present in. There may be several: an MGRS-100, an MGRS-1000, and one or more ZIP+4s and ATZs.
   A lookup to determine whether any visitor segments are targeting those geos. This lookup happens at the same time as the second Home lookup to map geos to segments (see above).
   Code involved: GeoPairSchemeV2 does this lookup.
   This lookup is throttled for performance reasons - it will not be done for every request.
   There is a limit to the total number of geos looked up. At the moment, this limit is 8 home and visitor geos combined. Home geos are preferred. Additionally, we currently try to make sure geos of each type are represented in the lookup when they are available (see PreferDiversityGeoPairLookupLimit). Our goal is to ensure ATZs (and especially ZIP+4s) do not crowd other types of geos out of the lookup.
 */
GeoLocationFilter::GeoLocationFilter(
        TargetGroupGeoLocationCacheService* targetGroupGeoLocationCacheService,
        GeoLocationCacheService* geoLocationCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker(filterNameToFailureCounts),  Object(__FILE__) {
        this->targetGroupGeoLocationCacheService =  targetGroupGeoLocationCacheService;
        this->geoLocationCacheService =  geoLocationCacheService;
}

std::string GeoLocationFilter::getName() {
        return "GeoLocationFilter";
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > GeoLocationFilter::filterTargetGroups(std::shared_ptr<OpportunityContext> context) {

        markFilteredTgs(context);

}

bool GeoLocationFilter::geoLocationMatch(std::shared_ptr<OpportunityContext> context,
                                         std::shared_ptr<GeoLocation> geoLocation) {
        //we don't compare city for geoLocation Filter
        NULL_CHECK(geoLocation);

        if(!geoLocationCacheService->mapOfStateNamesToGeoLocations->exists(
                   StringUtil::toLowerCase(context->deviceState))) {
                LOG_EVERY_N(ERROR, 1) <<"couldn't find state in loaded predefined map of states : " <<context->deviceState;
                throwEx("couldn't find state in loaded predefined map of states : " + context->deviceState);
        }

        if(!geoLocationCacheService->mapOfCountryNamesToGeoLocations->exists(
                   StringUtil::toLowerCase(context->deviceCountry))) {
                LOG_EVERY_N(ERROR, 1) <<"couldn't find state in loaded predefined map of countries : " << context->deviceCountry;
                throwEx("couldn't find state in loaded predefined map of countries : " + context->deviceCountry);
        }

        MLOG(2)<<" checking geoLocation : "<<geoLocation->toJson();

        if (StringUtil::equalsIgnoreCase(context->deviceCountry, geoLocation->getCountry()) &&
            StringUtil::equalsIgnoreCase(context->deviceState, geoLocation->getState())) {
                entityToModuleStateStats->addStateModuleForEntity("LOCATION_MATCH_FOUND",
                                                                  getName(),
                                                                  "ALL");
                return true;
        } else {

                if (!StringUtil::equalsIgnoreCase(context->deviceCountry, geoLocation->getCountry())) {
                        MLOG(2) << " country different  : context->deviceCountry " << context->deviceCountry
                                << " geoLocation->getCountry() " << geoLocation->getCountry();
                }

                if (!StringUtil::equalsIgnoreCase(context->deviceState, geoLocation->getState())) {
                        MLOG(2) << " state different  : context->deviceState " << context->deviceState
                                << " geoLocation->getState() " << geoLocation->getState();
                }
        }

        return false;
}

bool GeoLocationFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        auto tgPairPtr = targetGroupGeoLocationCacheService->
                         getMapOfAllTargetGroupToGeoLocationList()->
                         getOptional(tg->getId());

        if (tgPairPtr != nullptr) {
                bool failed = true;
                for(std::shared_ptr<GeoLocation> geoLocation :  *tgPairPtr->values) {
                        if (geoLocationMatch(context, geoLocation)) {
                                entityToModuleStateStats->addStateModuleForEntity("PASSED_FOR_MATCHING_GEO_LOCAITON",
                                                                                  getName(),
                                                                                  StringUtil::toStr("tg") +
                                                                                  StringUtil::toStr(tg->getId()));
                                filterTg = false;
                                failed = false;
                                break;

                        }
                }
                if (failed) {

                        entityToModuleStateStats->addStateModuleForEntity("FAILED_FOR_UN_MATCHING_GEO_LOCAITON",
                                                                          getName(),
                                                                          StringUtil::toStr("tg") +
                                                                          StringUtil::toStr(tg->getId()));
                }
        } else {
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity("PASSED_NO_GEO_LOCAITON_FOR_TG",
                                                                  getName(),
                                                                  StringUtil::toStr("tg") +
                                                                  StringUtil::toStr(tg->getId()));
        }

        return filterTg;
}

GeoLocationFilter::~GeoLocationFilter() {

}

//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef BidderLatencyRecorder_H
#define BidderLatencyRecorder_H


#include <memory>
#include <string>
class EntityToModuleStateStats;
class EntityToModuleStateStatsPersistenceService;
#include "AtomicLong.h"
#include "Object.h"
#include "AtomicBoolean.h"
#include "DateTimeUtil.h"
#include "CallStats.h"
#include "ConcurrentHashMap.h"
#include "EntityToModuleStateStats.h"

#include "Poco/Timestamp.h"

class BidderLatencyRecorder :  public Object {

private:

public:
TimeType lastTimeThreadRan;
EntityToModuleStateStats* entityToModuleStateStats;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;

//we have a special entityToModuleStateStats that will be written to influx
//every minute.
std::unique_ptr<EntityToModuleStateStats> entityToModuleStateStatsPerMinute;

std::shared_ptr<gicapods::AtomicBoolean> isAllowedToRun;
std::shared_ptr<gicapods::AtomicBoolean> threadIsFinished;
int latencyMinToReportToInfluxDb;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, CallStats> > modulesToCallStats;
BidderLatencyRecorder();
void recordLatency(Poco::Timestamp& startTime,
                   int freq,
                   std::string nameOfModule);

static void logTimeTaken(std::string moduleName,
                         Poco::Timestamp& startTime,
                         int freq,
                         int limitInMillis);

void recordLatencyToInfluxThread();
void recordLatencyToInflux();
void recordOneMinuteEntityStatesModules();

void addStateModuleForEntityPerMinute(
        std::string state, std::string module, std::string entityId);
void addStateModuleForEntityWithValuePerMinute(
        std::string state, double value,
        std::string module,
        std::string entityId);

virtual ~BidderLatencyRecorder();
};

#endif //BIDDER_FILTER_H

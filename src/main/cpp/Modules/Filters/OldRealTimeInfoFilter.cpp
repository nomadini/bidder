#include "OldRealTimeInfoFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoCacheService.h"
#include "BidderLatencyRecorder.h"


OldRealTimeInfoFilter::OldRealTimeInfoFilter(
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        TgMarker(filterNameToFailureCounts), Object(__FILE__) {
        this->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
}


OldRealTimeInfoFilter::~OldRealTimeInfoFilter() {

}

std::string OldRealTimeInfoFilter::getName() {
        return "OldRealTimeInfoFilter";
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > OldRealTimeInfoFilter::filterTargetGroups(
        std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool OldRealTimeInfoFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        if (this->entityDeliveryInfoCacheService->isRealTimeInfoOldForTg (tg->getId())) {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_OLD_REAL_TIME_INFO",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        } else {
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity ("PASSING_FOR_FRESH_REAL_TIME_INFO",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));
        }

        return filterTg;
}

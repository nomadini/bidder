#include "BlockedIabCategoryFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "JsonArrayUtil.h"
#include "CreativeSizeFilter.h"
#include <boost/foreach.hpp>

BlockedIabCategoryFilter::BlockedIabCategoryFilter(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker(filterNameToFailureCounts) {
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

BlockedIabCategoryFilter::~BlockedIabCategoryFilter() {

}

void BlockedIabCategoryFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "BlockedIabCategoryFilter cache cleared";

        attributesToGoodTgs->clear();
}

bool BlockedIabCategoryFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        bool foundCat = false;
        for(std::string cat :  *tg->getIabCategory()) {
                foundCat = context->getBlockedIabCategoriesMap()->exists (StringUtil::toLowerCase (cat));
                if (foundCat) {
                        MLOG(10)<<"found blocking category : "<< StringUtil::toLowerCase (cat);
                        break;
                }
        }


        bool foundSubCat = false;
        for(std::string subCat :  *tg->getIabSubCategory()) {
                foundSubCat = context->getBlockedIabCategoriesMap ()->exists(StringUtil::toLowerCase (subCat));
                if(foundSubCat) {
                        MLOG(10)<<"found blocking subCategory : "<< StringUtil::toLowerCase (subCat);
                        break;
                }
        }

        if (foundCat || foundSubCat) {

                std::string blcokedCats;
                auto map = context->getBlockedIabCategoriesMap()->getCopyOfMap();
                typename tbb::concurrent_hash_map<std::string, gicapods::AtomicLong>::iterator iter;
                for (auto iter = map->begin ();
                     iter != map->end ();
                     iter++) {
                        blcokedCats += iter->first + " , ";
                }

                MLOG (10) << "target group cat or sub cat is blocked by user : "
                          << "foundCat : " << foundCat
                          << "foundSubCat : " << foundSubCat
                          << "blcokedCats : " << blcokedCats
                          << " for chosen tg id : " << tg->getId()
                          << " tg iabCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabCategory())
                          << " tg iabSubCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabSubCategory());
        } else {
                MLOG (10) << "target group cat or sub cat is not blocked by user : "
                          << " for chosen tg id : " << tg->getId()
                          << " tg iabCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabCategory())
                          << " tg iabSubCategories : " << JsonArrayUtil::convertListToJson(*tg->getIabSubCategory());
        }

        if (foundCat) {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_BLOCKED_CATEGORY",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
        }

        if (foundSubCat) {
                entityToModuleStateStats->addStateModuleForEntity("FAILED_FOR_BLOCKED_SUB_CATEGORY",
                                                                  getName(),
                                                                  StringUtil::toStr("tg") +
                                                                  StringUtil::toStr(tg->getId()));

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;
        }

        return filterTg;
}
std::string BlockedIabCategoryFilter::getName() {
        return "BlockedIabCategoryFilter";
}

#include "DeviceTypeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "Device.h"
#include "TargetGroupCacheService.h"

DeviceTypeFilter::DeviceTypeFilter
        (std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts) {
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

DeviceTypeFilter::~DeviceTypeFilter() {

}
void DeviceTypeFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "DeviceTypeFilter cache cleared";
        attributesToGoodTgs->clear();
}

bool DeviceTypeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        assertAndThrow(tg!=NULL);

        entityToModuleStateStats->addStateModuleForEntity (context->device->getDeviceType (),
                                                           getName () + "-DeviceTypeFromRequests",
                                                           "ALL");

        auto typePairPtr = tg->deviceTypeTargets->getOptional (context->device->getDeviceType ());
        if (typePairPtr != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_MATCHING_DEVICE_TYPE",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));


                recordPassingValue(context->device->getDeviceType (),
                                   tg->getId (),
                                   "tg",
                                   getName ());
                filterTg = false;
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_UNMATCHING_DEVICE_TYPE",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));

        }
        return filterTg;
}


std::string DeviceTypeFilter::getName() {
        return "DeviceTypeFilter";
}

/*
 * GeoFeatureFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef GeoFeatureFilter_H_
#define GeoFeatureFilter_H_


#include "Poco/Net/HTTPClientSession.h"
#include "Object.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
#include "GeoLocation.h"
class OpportunityContext;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TgMarker.h"
class HttpUtilService;
class TargetGroupGeoLocationCacheService;
#include <unordered_set>
#include "AtomicBoolean.h"
#include "ConfigService.h"

#include "SizeBasedLRUCache.h"
#include "LoadPercentageBasedSampler.h"
#include "BiddingMonitor.h"
#include "TgMarker.h"
#include "AeroCacheService.h"

class PlacesCachedResult;
class Place;
class PocoSession;

class GeoFeatureFilter : public TgMarker, public BiddingMonitor, public Object {

private:


public:
std::unique_ptr<LoadPercentageBasedSampler> geoFeatureFilterRemoteCallThrottler;
std::shared_ptr<PocoSession> session;
std::unordered_set<std::string>* targetGroupToGeoCollectionKeys;
std::unordered_set<int>* targetGroupIdsWithGeoCollectionAssignments;

AeroCacheService<PlacesCachedResult>* aerospikePlaceCache;

EntityToModuleStateStats* entityToModuleStateStats;

std::string placeFinderAppHostUrl;


GeoFeatureFilter(
        AeroCacheService<PlacesCachedResult>* aerospikePlaceCache,
        gicapods::ConfigService* configService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        int failureThresholdPercentageArg);
std::shared_ptr<Poco::Net::HTTPClientSession> createSession();
std::string getName();

void fetchPlacesAndTags(std::shared_ptr<OpportunityContext> context);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > filterTargetGroups(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

std::vector<int> getTagIdsOfFeatures(
        std::vector<std::shared_ptr<Place> > allPlaces
        );

std::vector<int> getTagsOfPlace(std::shared_ptr<Place> place);

std::vector<std::shared_ptr<Place> > parsePlacesFromJson(std::string placesInJson);

std::vector<std::shared_ptr<Place> > getPlacesFromCache(
        std::string mgrs10m,
        double deviceLat,
        double deviceLon,
        int maxNumebrOfWantedFeatures);
std::vector<std::shared_ptr<Place> > getPlacesFromRemote(
        double deviceLat, double deviceLon, int maxNumebrOfWantedFeatures);

virtual ~GeoFeatureFilter();
};



#endif /* GeoLocation_H_ */

#ifndef OsTypeFilter_H
#define OsTypeFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "CacheUpdateWatcher.h"
#include "Object.h"
#include "ObjectVectorHolder.h"

class OsTypeFilter;

class OsTypeFilter : public TgMarker, public CacheUpdateWatcher {

private:
public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

OsTypeFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~OsTypeFilter();

void cacheWasUpdatedEvent();
void markGoodTgsAndCacheThem(
        std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
std::string getName();

};

#endif

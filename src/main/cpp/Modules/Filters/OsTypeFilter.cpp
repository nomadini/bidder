#include "OsTypeFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "Device.h"
#include "TargetGroupCacheService.h"

OsTypeFilter::OsTypeFilter
        (std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        :  TgMarker(filterNameToFailureCounts) {
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

OsTypeFilter::~OsTypeFilter() {

}
void OsTypeFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "OsTypeFilter cache cleared";
        attributesToGoodTgs->clear();
}

bool OsTypeFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        assertAndThrow(tg!=NULL);

        auto typePairPtr = tg->osTypeTargets->getOptional (context->deviceOsFamily);
        if (typePairPtr != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_MATCHING_DEVICE_TYPE",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));


                recordPassingValue(context->deviceOsFamily,
                                   tg->getId (),
                                   "tg",
                                   getName ());
                filterTg = false;
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_UNMATCHING_DEVICE_TYPE",
                                                                   getName (),
                                                                   StringUtil::toStr ("tg") +
                                                                   StringUtil::toStr (tg->getId ()));

        }

        MLOG(3)<<"osFamily is : "<<context->deviceOsFamily<< ", tg id : "<< tg->getId()<<", result : "<< filterTg;
        return filterTg;
}


std::string OsTypeFilter::getName() {
        return "OsTypeFilter";
}

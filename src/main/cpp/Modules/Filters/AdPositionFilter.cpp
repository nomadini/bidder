#include "AdPositionFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "JsonArrayUtil.h"
#include "TargetGroupCacheService.h"

#include "EntityToModuleStateStats.h"

AdPositionFilter::AdPositionFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts) {
        attributesToGoodTgs = std::make_shared<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > >();

}

AdPositionFilter::~AdPositionFilter() {

}

void AdPositionFilter::cacheWasUpdatedEvent() {
        LOG_EVERY_N(INFO, 1)<< "AdPositionFilter cache cleared";
        attributesToGoodTgs->clear();
}

std::string AdPositionFilter::getName() {
        return "AdPositionFilter";
}

bool AdPositionFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        entityToModuleStateStats->addStateModuleForEntity (context->adPosition,
                                                           getName() + "-AdpositionsFromRequest",
                                                           "ALL");
        if (tg->getAdPositions()->size () == 0) {
                //user has no preference in ad positions , so we pass
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_NO_PREFERENCE",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;

        }

        if (tg->getAdPositions()->exists("ANY")) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_MATCHING_ANY",
                                                                   getName(),
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);
                filterTg = false;

        } else {
                if (!tg->getAdPositions()->exists(context->adPosition)) {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);
                        MLOG(2) << "Ad position filter not matching the tg, filtered out, adPosistion from context"
                                << context->adPosition << " , tg adpositions : "
                                << JsonArrayUtil::convertListToJson (tg->getAdPositions()->getKeys());
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("PASSED_FOR_MATCHING_AD_POSITIONS",
                                                                           getName(),
                                                                           StringUtil::toStr ("tg") +
                                                                           tg->idAsString);
                        filterTg = false;
                }

        }

        return filterTg;
}

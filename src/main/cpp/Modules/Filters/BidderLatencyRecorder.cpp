//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "BidderLatencyRecorder.h"
#include "EntityToModuleStateStats.h"
#include "Object.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "EntityToModuleStateStatsPersistenceService.h"
BidderLatencyRecorder::BidderLatencyRecorder()
        : Object(__FILE__) {
        this->modulesToCallStats =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, CallStats> > ();

        isAllowedToRun = std::make_shared<gicapods::AtomicBoolean>(true); //this is the flag that thread waits for
        threadIsFinished = std::make_shared<gicapods::AtomicBoolean>(false);
        lastTimeThreadRan = DateTimeUtil::getNowInSecond();
        latencyMinToReportToInfluxDb = 2;
        entityToModuleStateStatsPerMinute = std::make_unique<EntityToModuleStateStats>();
}

void BidderLatencyRecorder::logTimeTaken(
        std::string moduleName, Poco::Timestamp& startTime, int freq, int limitInMillis) {
        Poco::Timestamp::TimeDiff diff = startTime.elapsed ();
        double time = (double) diff / 1000;
        if(time >= limitInMillis) {
                LOG_EVERY_N(INFO, freq) << moduleName <<" time took in millis : "<< time;
        }
}

void BidderLatencyRecorder::recordLatency(Poco::Timestamp& startTime,
                                          int freq,
                                          std::string nameOfModule) {

        Poco::Timestamp::TimeDiff diff = startTime.elapsed ();
        double filterLatencyInLastRun = (double) diff;
        freq = 1;


        if (gicapods::Util::allowedToCall(_FL_, freq)) {

                auto pair = modulesToCallStats->getOptional(nameOfModule);
                if (pair != nullptr) {
                        pair->totalCalls->increment();
                        pair->totalMicroseconds->addValue(filterLatencyInLastRun);
                } else {
                        auto callStats  = std::make_shared<CallStats>();
                        callStats->totalCalls->increment();
                        callStats->totalMicroseconds->addValue(filterLatencyInLastRun);
                        modulesToCallStats->put(nameOfModule, callStats);
                }

        }
}


void BidderLatencyRecorder::recordLatencyToInfluxThread() {
        while(isAllowedToRun->getValue()) {
                if (DateTimeUtil::getNowInSecond() - lastTimeThreadRan < 58) {
                        //sleep for one second
                        gicapods::Util::sleepViaBoost(_L_, 1);
                        continue;
                }

                recordLatencyToInflux();
                recordOneMinuteEntityStatesModules();
                lastTimeThreadRan = DateTimeUtil::getNowInSecond();
        }

        threadIsFinished->setValue(true);
}

void BidderLatencyRecorder::addStateModuleForEntityPerMinute(
        std::string state, std::string module, std::string entityId) {
        //we do a check if the combo is in the map,
        //it means it has not been persisted yet. in that minute.
        //we log error in that case
        if (entityToModuleStateStatsPerMinute->
            hasStateModuleEntity(state, module, entityId)) {
                LOG_EVERY_N(INFO, 1) << "state combo already recorded for the minute "
                                     << state + "-"+module + "-" + entityId;
                return;
        }

        entityToModuleStateStatsPerMinute->addStateModuleForEntity
                (state, module, entityId);


}

void BidderLatencyRecorder::addStateModuleForEntityWithValuePerMinute(
        std::string state, double value,
        std::string module, std::string entityId) {
        //we do a check if the combo is in the map,
        //it means it has not been persisted yet. in that minute.
        //we log error in that case
        if (entityToModuleStateStatsPerMinute->
            hasStateModuleEntity(state, module, entityId)) {
                LOG_EVERY_N(INFO, 1) << "state combo already recorded for the minute "
                                     << state + "-"+module + "-" + entityId;
                return;
        }

        entityToModuleStateStatsPerMinute->addStateModuleForEntityWithValue(state, value, module, entityId);
}

void BidderLatencyRecorder::recordOneMinuteEntityStatesModules() {
        //we record this special per minute metric and clear it
        entityToModuleStateStatsPersistenceService->persistStats(entityToModuleStateStatsPerMinute.get());
        entityToModuleStateStatsPerMinute->clearAllMetrics();
}

void BidderLatencyRecorder::recordLatencyToInflux() {

        auto map = modulesToCallStats->moveMap();
        for (auto&& pair : *map) {
                auto nameOfModule = pair.first;
                auto callStats = pair.second;

                auto latencyInMillis = (callStats->totalMicroseconds->getValue()/ 1000) /
                                       callStats->totalCalls->getValue();
                if(latencyInMillis >= latencyMinToReportToInfluxDb) {
                        MLOG(1) <<"module latency in millis:"
                                << nameOfModule
                                << " : "<<latencyInMillis;
                        entityToModuleStateStats->
                        addStateModuleForEntityWithValue (nameOfModule+"Latency",
                                                          latencyInMillis,
                                                          "BidderLatencyModule",
                                                          EntityToModuleStateStats::all);
                }

        }
}

BidderLatencyRecorder::~BidderLatencyRecorder() {
        isAllowedToRun->setValue(false); //set the flag to false to make the thread stop
        while(!threadIsFinished->getValue()) {
                LOG(ERROR) << "waiting for the BidderLatencyRecorder thread to stop";
                gicapods::Util::sleepViaBoost(_L_, 1);
        }
}

#ifndef BlockedIabCategoryFilter_H
#define BlockedIabCategoryFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include "TgMarker.h"
#include "ObjectVectorHolder.h"
#include "CacheUpdateWatcher.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class BlockedIabCategoryFilter;

class BlockedIabCategoryFilter : public TgMarker, public CacheUpdateWatcher {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

BlockedIabCategoryFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~BlockedIabCategoryFilter();



void cacheWasUpdatedEvent();

std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

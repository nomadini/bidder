/*
 * GeoLocationFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef GeoLocation_H_
#define GeoLocation_H_


#include "Object.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
#include "GeoLocation.h"
class OpportunityContext;
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "TgMarker.h"
class GeoLocationCacheService;
class TargetGroupGeoLocationCacheService;

class GeoLocationFilter : public TgMarker, public Object {

private:
TargetGroupGeoLocationCacheService* targetGroupGeoLocationCacheService;
GeoLocationCacheService* geoLocationCacheService;
public:

GeoLocationFilter(TargetGroupGeoLocationCacheService* targetGroupGeoLocationCacheService,
                  GeoLocationCacheService* geoLocationCacheService,
                  std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

bool geoLocationMatch(std::shared_ptr<OpportunityContext> context, std::shared_ptr<GeoLocation> geoLocation);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > filterTargetGroups(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

virtual ~GeoLocationFilter();
};



#endif /* GeoLocation_H_ */

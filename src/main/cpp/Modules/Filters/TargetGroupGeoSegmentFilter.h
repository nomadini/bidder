/*
 * TargetGroupGeoSegmentFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPGEOSEGMENTFILTER_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPGEOSEGMENTFILTER_H_



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CreativeTypeDefs.h"
class Creative;
#include "Object.h"
#include "CollectionUtil.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "GeoSegment.h"
#include "GeoSegmentList.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class TargetGroupGeoSegmentCacheService;

class TargetGroupGeoSegmentFilter : public BidderModule, public Object {

private:

public:

TargetGroupGeoSegmentCacheService* targetGroupGeoSegmentCacheService;

TargetGroupGeoSegmentFilter(
        TargetGroupGeoSegmentCacheService* targetGroupGeoSegmentCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual std::string getName();

bool processGeoSegmentValidationForTg(std::shared_ptr<TargetGroup> tg,
                                      std::vector<std::shared_ptr<GeoSegmentList> >& geoSegmentListForTg,
                                      std::shared_ptr<OpportunityContext> context);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
virtual ~TargetGroupGeoSegmentFilter();
};



#endif /* GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_MODULES_TARGETGROUPGEOSEGMENTFILTER_H_ */

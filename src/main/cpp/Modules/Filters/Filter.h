// //
// // Created by Mahmoud Taabodi on 3/6/16.
// //
//
// #ifndef Filter_H
// #define Filter_H
//
//
// #include "Status.h" //this is needed in every module
// class OpportunityContext;
// #include <memory>
// #include <string>
// class EntityToModuleStateStats;
// #include "AtomicLong.h"
// #include <unordered_map>
// #include "TgMarker.h"
// #include "GUtil.h"//this has a lot of logging and Common stuff in it
// #include "ConcurrentHashMap.h"
// #include "Object.h"
// class TargetGroup;
// class Filter;
//
//
// class Filter : public TgMarker, public Object {
//
// private:
//
// public:
//
// Filter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
//
// virtual void beforeFiltering(std::shared_ptr<OpportunityContext> context);
//
// virtual void afterFiltering(std::shared_ptr<OpportunityContext> context);
//
// virtual std::string getName()=0;
//
// virtual ~Filter();
// };
//
// #endif //BIDDER_FILTER_H

/*
 * DayPartFilter.h
 *
 *  Created on: Sep 6, 2015
 *      Author: mtaabodi
 */

#ifndef DayPart_H_
#define DayPart_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CollectionUtil.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "Object.h"
#include "CacheUpdateWatcher.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "ConcurrentHashMap.h"
#include "BidderModule.h"
class TargetGroupDayPartCacheService;
class EntityToModuleStateStats;

class DayPartFilter : public CacheUpdateWatcher, public BidderModule, public Object {

private:

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

static int getUserTimeZoneDifferenceWithUTC(std::shared_ptr<OpportunityContext> context);

TargetGroupDayPartCacheService* targetGroupDayPartCacheService;

DayPartFilter(TargetGroupDayPartCacheService* targetGroupDayPartCacheService,
														EntityToModuleStateStats* entityToModuleStateStats,
														std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);


std::string getName();

void cacheWasUpdatedEvent();

virtual void process(std::shared_ptr<OpportunityContext> context);
void filterOutTgBasedOnDayparting(
								std::shared_ptr<TargetGroup> tg,
								int userActiveInUTC,
								std::shared_ptr<OpportunityContext> context);

virtual ~DayPartFilter();


bool filterTargetGroup(std::shared_ptr<TargetGroup> tg,std::shared_ptr<OpportunityContext> context);
};



#endif /* DayPart_H_ */

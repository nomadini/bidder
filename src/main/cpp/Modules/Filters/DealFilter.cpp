#include "DealFilter.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CreativeSizeFilter.h"
#include <boost/foreach.hpp>

DealFilter::DealFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker(filterNameToFailureCounts), Object(__FILE__) {
}

DealFilter::~DealFilter() {

}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > DealFilter::filterTargetGroups(
        std::shared_ptr<OpportunityContext> context) {
        markFilteredTgs(context);
}

bool DealFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;

        return filterTg;
}
std::string DealFilter::getName() {
        return "DealFilter";
}

#ifndef OldRealTimeInfoFilter_H
#define OldRealTimeInfoFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "Object.h"
class EntityDeliveryInfoCacheService;

class OldRealTimeInfoFilter;



class OldRealTimeInfoFilter : public TgMarker, public Object {
private:
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
public:
OldRealTimeInfoFilter(EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
                      std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~OldRealTimeInfoFilter();

std::string getName();

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > filterTargetGroups(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

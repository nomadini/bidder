#ifndef TargetGroupBudgetFilterChainModule_H
#define TargetGroupBudgetFilterChainModule_H

#include "TargetGroupTypeDefs.h"
class TargetGroup;
class TargetGroupCacheService;
#include "CreativeTypeDefs.h"
class Creative;
class OpportunityContext;
class EntityToModuleStateStats;
#include "BidderModule.h"
#include <memory>
#include <string>
#include "BidderModule.h"
#include "EntityProviderService.h"
#include "TgMarker.h"
#include "Object.h"

class CampaignCacheService;
class TargetGroupCacheService;
class CreativeCacheService;
class MySqlBWListService;
class TargetGroupBWListCacheService;
class TargetGroupCreativeCacheService;
class TargetGroupGeoSegmentCacheService;
class TargetGroupDayPartCacheService;
class TargetGroupGeoLocationCacheService;
class TgFilterMeasuresService;
class EntityDeliveryInfoCacheService;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class BidderLatencyRecorder;

/*
   this class combines all the filters that are related to budget in a filterChain
 */
class TargetGroupBudgetFilterChainModule : public BidderModule, public Object {

private:
public:

TargetGroupCacheService* targetGroupCacheService;
EntityToModuleStateStats* entityToModuleStateStats;
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;

TgFilterMeasuresService* tgFilterMeasuresService;
BidderLatencyRecorder* bidderLatencyRecorder;

TargetGroupBudgetFilterChainModule(EntityToModuleStateStats* entityToModuleStateStats,
                                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
void setUpFilters();
std::vector<std::shared_ptr<TgMarker> > orderedListOfFiltersToRun;

std::string originalName;

void addFilterToMap(const std::string& filterName, std::shared_ptr<TgMarker> filter);

std::shared_ptr<TgMarker> getFilter(const std::string& filterName);

void initFilter(std::shared_ptr<TgMarker> filter);

public:

virtual void process(std::shared_ptr<OpportunityContext> context);

virtual std::string getName();

virtual ~TargetGroupBudgetFilterChainModule();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> > > getNameToFilterMap();
};



#endif

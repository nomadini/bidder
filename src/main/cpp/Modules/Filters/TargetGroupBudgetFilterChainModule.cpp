
#include "Status.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"
#include "Creative.h"

#include "CollectionUtil.h"
#include "GeoLocation.h"
#include "GeoSegment.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "BidderModule.h"
#include "TargetGroupFilterChainModule.h"
#include "BidderApplicationContext.h"
#include "TargetGroupBudgetFilterChainModule.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"

#include "DayPartFilter.h"
#include "CreativeSizeFilter.h"

#include "MatchingIabCategoryFilter.h"
#include "DomainBlackListedFilter.h"
#include "DomainWhiteListedFilter.h"
#include "ActiveFilterModule.h"
#include "BlockedAdvertiserDomainFilter.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BlockedIabCategoryFilter.h"
#include "AdPositionFilter.h"
#include "PacingBasedBugdetLimitEnforcerFilter.h"
#include "PacingBasedImpressionLimitEnforcerFilter.h"
#include "CreativeBannerApiFilter.h"
#include "OldRealTimeInfoFilter.h"
#include "TgMarker.h"
#include "CappedBiddingPerHourFilter.h"
#include <boost/foreach.hpp>
#include "CappedBiddingForTargetGroupPerDayFilter.h"

TargetGroupBudgetFilterChainModule::TargetGroupBudgetFilterChainModule
        (EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        originalName  = "TgBudgetFilterChain";


}
void TargetGroupBudgetFilterChainModule::setUpFilters() {
        LOG(INFO) << "initializing the filter map";
        initFilter (std::make_shared<OldRealTimeInfoFilter> (entityDeliveryInfoCacheService, filterNameToFailureCounts));

        auto cappedBiddingPerHourFilter = std::make_shared<CappedBiddingPerHourFilter> (filterNameToFailureCounts);
        cappedBiddingPerHourFilter->tgFilterMeasuresService = tgFilterMeasuresService;
        initFilter (cappedBiddingPerHourFilter);

        auto cappedBiddingForTargetGroupPerDayFilter = std::make_shared<CappedBiddingForTargetGroupPerDayFilter>(filterNameToFailureCounts);
        cappedBiddingForTargetGroupPerDayFilter->tgFilterMeasuresService = tgFilterMeasuresService;
        initFilter (cappedBiddingForTargetGroupPerDayFilter);

        auto pacingBasedImpressionLimitEnforcerFilter = std::make_shared<PacingBasedImpressionLimitEnforcerFilter>(filterNameToFailureCounts);
        pacingBasedImpressionLimitEnforcerFilter->tgFilterMeasuresService = tgFilterMeasuresService;
        initFilter (pacingBasedImpressionLimitEnforcerFilter);

        auto pacingBasedBugdetLimitEnforcerFilter = std::make_shared<PacingBasedBugdetLimitEnforcerFilter>(filterNameToFailureCounts);
        pacingBasedBugdetLimitEnforcerFilter->tgFilterMeasuresService = tgFilterMeasuresService;
        initFilter (pacingBasedBugdetLimitEnforcerFilter);
}

std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> > > TargetGroupBudgetFilterChainModule::getNameToFilterMap() {
        static std::shared_ptr<tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> > > map =
                std::make_shared<tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> > > ();
        return map;
}


void TargetGroupBudgetFilterChainModule::initFilter(std::shared_ptr<TgMarker> filter) {
        filter->entityToModuleStateStats = this->entityToModuleStateStats;
        // assertAndThrow (filter->entityToModuleStateStats != NULL);
        // addFilterToMap (filter->getName () , filter);
        //
        // getFilter(filter->getName ()); //validate filter was added;
        orderedListOfFiltersToRun.push_back (filter);
}

void TargetGroupBudgetFilterChainModule::addFilterToMap(
        const std::string &filtername,
        std::shared_ptr<TgMarker> filter) {
        tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> >::accessor accessor;
        getNameToFilterMap ()->insert (accessor, filtername);
        accessor->second = filter;
}


std::shared_ptr<TgMarker> TargetGroupBudgetFilterChainModule::getFilter(const std::string &filterName) {
        tbb::concurrent_hash_map<std::string, std::shared_ptr<TgMarker> >::accessor a;
        bool found = getNameToFilterMap ()->find (a, filterName);
        if (!found) {
                throwEx ("filter was not found in map : " + filterName);
        }
        return a->second;
}

void TargetGroupBudgetFilterChainModule::process(std::shared_ptr<OpportunityContext> context) {
        entityToModuleStateStats->addStateModuleForEntity (
                "sizeOfTargetGroup_StartOfFilterChain"
                + StringUtil::toStr(context->getSizeOfUnmarkedTargetGroups()),
                originalName,
                "ALL");



        //make all these filters use tgs in the context. you don't need to pass any tgs around
        for(std::shared_ptr<TgMarker> filter :  orderedListOfFiltersToRun) {

                //I am adding try catch only around filterTargetGroups method invocation since I don't want
                //any exception thrown in that function , winds up the stack and I dont get the correct lastFilterName
                try {

                        auto beforeTgSize = context->getSizeOfUnmarkedTargetGroups();
                        if (beforeTgSize > 0) {
                                TargetGroupFilterChainModule::runFilter(filter.get(),
                                                                        context,
                                                                        entityToModuleStateStats,
                                                                        bidderLatencyRecorder);

                                auto afterTgSize = context->getSizeOfUnmarkedTargetGroups();
                                int filterPercentage = 0;
                                if (beforeTgSize != 0) {
                                        filterPercentage  = (beforeTgSize - afterTgSize) * 100 / beforeTgSize;
                                }
                                entityToModuleStateStats->addStateModuleForEntityWithValue((filter)->getName(),
                                                                                           filterPercentage,
                                                                                           originalName+"-PercentOfFilteringTg",
                                                                                           "ALL");
                                if (afterTgSize == 0) {
                                        entityToModuleStateStats->addStateModuleForEntity(
                                                (filter)->getName(),
                                                "TargetGroupBudgetFilterChainModule-LastChainFilter",
                                                "ALL"
                                                );
                                }
                                LOG_EVERY_N(INFO, 10000)<< "percenteOfFilteringTg for filter "<< filter->getName()<< " is "<< filterPercentage;
                        }
                } catch (std::exception const &e) {
                        entityToModuleStateStats->addStateModuleForEntity ("Exception : " + StringUtil::toStr(e.what()),
                                                                           originalName,
                                                                           "ALL");
                } catch (...) {
                        entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                           originalName,
                                                                           "ALL");
                }




        }

        //http://69.200.247.201:9980/pages/filtermap
        //printFilterNameToTargetGroupCounterMap();
        //this line should be commented out later, this line is for disabling the target group filter!


}

std::string TargetGroupBudgetFilterChainModule::getName() {

        return originalName;
}
bool TargetGroupBudgetFilterChainModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        throwEx("should not be callsed");
        return filterTg;
}

TargetGroupBudgetFilterChainModule::~TargetGroupBudgetFilterChainModule() {

}

/**************************************/
/**************************************/
/************ helper methods***********/
/**************************************/
/**************************************/


//<bean id="evalTargetFilterChain" class="org.apache.commons.chain.impl.ChainBase">
//	<constructor-arg type="java.util.Collection">
//		<!-- order by filter % - heavy-duty filtering first -->
//		<list>
//			<ref bean="filterBySpendSegmentTargetCommand" />
//			<!-- geo filters a lot -->
//			<ref bean="filterBySpendGeoTargetCommand" />
//			<ref bean="filterByCreativeGeoTargetCommand" />
//
//			<!-- pacing filters need to run after spend-segment & geo (as per DS) -->
//			<ref bean="filterByCampaignPacingTargetCommand" />
//			<ref bean="filterBySpendPacingTargetCommand" />
//			<ref bean="filterByCreativePacingTargetCommand" />
//
//			<ref bean="filterByATExclusionCommand" />
//			<ref bean="filterByHostIdsTargetCommand" />
//            <ref bean="filterByAppIdsTargetCommand" />
//			<ref bean="filterByInventoryMarketerTargetCommand" />
//			<ref bean="filterBySpendDeviceClassTargetCommand" />
//			<ref bean="filterBySpendEnvTypeTargetCommand" />
//
//			<ref bean="filterByBmsExtCommand" />
//			<ref bean="filterByBmsIntCommand" />
//
//			<ref bean="filterByCreativeVideoDataCommand" />
//			<ref bean="filterBySpendSectionTargetCommand" />
//			<ref bean="filterSpendByRemoteAgencyCommand"/>
//			<ref bean="filterBySpendInventoryTargetCommand" />
//			<ref bean="filterBySpendDayPartCommand" />
//			<ref bean="filterByDelayFreqRecencyTargetsCommand" />
//			<ref bean="filterByIndustryCategoriesCommand" />
//			<ref bean="filterByIabCategoriesCommand" />
//			<ref bean="filterByTopLevelDomainCommand" />
//			<ref bean="filterByTechSpecTargetCommand" />
//			<ref bean="filterBySpendAdPositionTargetCommand" />
//			<ref bean="filterBySpendPublisherCategoryTargetCommand" />
//			<ref bean="filterByCreativeRemoteStatusCommand" />
//			<ref bean="filterByCreativeVendorCommand" />
//			<ref bean="filterByCreativeAdAttributeCommand" />
//			<ref bean="filterInventoryByEnvTypeCommand" />

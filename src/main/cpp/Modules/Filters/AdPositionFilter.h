#ifndef AdPositionFilter_H
#define AdPositionFilter_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "TgMarker.h"
#include "CacheUpdateWatcher.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
class AdPositionFilter;

class AdPositionFilter : public TgMarker, public CacheUpdateWatcher {

public:
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, ObjectVectorHolder<TargetGroup> > > attributesToGoodTgs;

AdPositionFilter(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~AdPositionFilter();
void cacheWasUpdatedEvent();

std::string getName();



bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

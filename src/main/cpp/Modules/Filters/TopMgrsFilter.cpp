#include "Status.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "TopMgrsFilter.h"
#include "TargetGroupMgrsSegmentCacheService.h"
#include <boost/foreach.hpp>

TopMgrsFilter::TopMgrsFilter(
        TargetGroupMgrsSegmentCacheService* targetGroupMgrsSegmentCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker(filterNameToFailureCounts),  Object(__FILE__) {
        this->targetGroupMgrsSegmentCacheService =  targetGroupMgrsSegmentCacheService;
}

std::string TopMgrsFilter::getName() {
        return "TopMgrsFilter";
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > TopMgrsFilter::filterTargetGroups(std::shared_ptr<OpportunityContext> context) {

        markFilteredTgs(context);

}

bool TopMgrsFilter::topMgrsSegmentMatch(std::shared_ptr<OpportunityContext> context,
                                        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> > topMgrsSegment) {
        //we don't compare city for topMgrsSegment Filter
        NULL_CHECK(topMgrsSegment);
        auto mgrs100mKey = StringUtil::toLowerCase(context->mgrs100m);

        if (topMgrsSegment->getOptional(mgrs100mKey) != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity("MGRS_MATCH_FOUND",
                                                                  getName(),
                                                                  "ALL");
                return true;
        }

        return false;
}

bool TopMgrsFilter::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        bool filterTg = true;
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TopMgrsSegment> >
        topMgrsSegmentMap = targetGroupMgrsSegmentCacheService->
                            getAllTargetGroupTopMgrsSegmentsMap()->
                            getOptional(tg->getId());
        if (!tg->typesOfTargeting->exists(TargetGroup::TARGETTING_TYPE_TOP_MGRS_TARGETING)) {
                //this tg is not supposed to be strictly targeted for recency...
                //so we pass it
                filterTg = false;
                entityToModuleStateStats->addStateModuleForEntity("PASSING_TG_NOT_STRICT_WITH_MGRS_TARGETING",
                                                                  "TopMgrsFilter",
                                                                  "ALL",
                                                                  tg->idAsString);

                return filterTg;
        }

        if (topMgrsSegmentMap != nullptr) {
                bool failed = true;
                if (topMgrsSegmentMatch(context, topMgrsSegmentMap)) {
                        MLOG(3)<<"found matching mgrs100 : "<< context->mgrs100m << " for tg "<< tg->id;
                        context->addTargetingTypeForTg(tg->getId(), TargetGroup::TARGETTING_TYPE_TOP_MGRS_TARGETING);
                        entityToModuleStateStats->addStateModuleForEntity("PASSED_FOR_MATCHING_TopMgrsSegment",
                                                                          getName(),
                                                                          StringUtil::toStr("tg") +
                                                                          StringUtil::toStr(tg->getId()));
                        filterTg = false;
                        failed = false;

                } else {

                        entityToModuleStateStats->addStateModuleForEntity("FAILED_FOR_UN_MATCHING_TopMgrsSegment",
                                                                          getName(),
                                                                          StringUtil::toStr("tg") +
                                                                          StringUtil::toStr(tg->getId()));
                }
        } else {
                //we still fail here, because tg is set to be targeted for mgrs targeting
                //but there is no mgrs assigned to it
        }

        return filterTg;
}

TopMgrsFilter::~TopMgrsFilter() {

}

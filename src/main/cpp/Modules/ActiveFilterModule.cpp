#include "ActiveFilterModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"

#include "GUtil.h"
#include "OpportunityContext.h"
#include "Campaign.h"
#include "CacheService.h"
#include "Advertiser.h"
#include "Client.h"

#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"
ActiveFilterModule::ActiveFilterModule(EntityToModuleStateStats* entityToModuleStateStats,
                                       std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

ActiveFilterModule::~ActiveFilterModule() {

}

std::string ActiveFilterModule::getName() {
        return "ActiveFilterModule";
}

void ActiveFilterModule::process(std::shared_ptr<OpportunityContext> context) {
        entityToModuleStateStats->addStateModuleForEntity ("sizeOfTargetGroup"  + StringUtil::toStr(context->getSizeOfUnmarkedTargetGroups()),
                                                           "ActiveFilterModule",
                                                           "ALL");

        markFilteredTgs(context);
}

bool ActiveFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        NULL_CHECK(tg);
        bool filterTg = true;
        if (isTargetGroupStatusActive(tg) &&
            isCampignStatusActive(tg) &&
            isAdvertiserStatusActive(tg) &&
            isClientStatusActive(tg)) {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED",
                                                                   "ActiveFilterModule",
                                                                   StringUtil::toStr ("tg") +
                                                                   tg->idAsString);

                filterTg = false;
        }

        return filterTg;
}

bool ActiveFilterModule::isStatusActive(std::string statusOfEntity,
                                        int entityId,
                                        std::string entityType) {
        bool result = false;
        if (StringUtil::equalsIgnoreCase (statusOfEntity,"active")) {
                result = true;
        } else {

                if (!StringUtil::equalsIgnoreCase (statusOfEntity, "inactive")) {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_WRONG_STATUS",
                                                                           "ActiveFilterModule",
                                                                           entityType +
                                                                           StringUtil::toStr (entityId),
                                                                           EntityToModuleStateStats::exception);
                        LOG (ERROR) << "unknown status for this "<< entityType << " with id " <<
                                entityId << "status : " << statusOfEntity;
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("FAILED_FOR_INACTIVE",
                                                                           "ActiveFilterModule",
                                                                           entityType +
                                                                           StringUtil::toStr (entityId));
                }
        }

        return result;
}

bool ActiveFilterModule::isTargetGroupStatusActive(std::shared_ptr<TargetGroup> tg) {
        return isStatusActive(tg->getStatus(), tg->getId(), "tg");
}

bool ActiveFilterModule::isCampignStatusActive(std::shared_ptr<TargetGroup> tg) {
        auto cmp = this->campaignCacheService->findByEntityId(tg->getCampaignId());
        return isStatusActive(cmp->getStatus(), cmp->getId(), "cmp") &&
               isCurrent(cmp->startDate, cmp->endDate);

}
bool ActiveFilterModule::isAdvertiserStatusActive(std::shared_ptr<TargetGroup> tg) {
        auto cmp = this->campaignCacheService->findByEntityId(tg->getCampaignId());
        auto adv = this->advertiserCacheService->findByEntityId(cmp->getAdvertiserId());
        return isStatusActive(adv->getStatus(), adv->getId(), "adv");
}
bool ActiveFilterModule::isClientStatusActive(std::shared_ptr<TargetGroup> tg) {
        auto cmp = this->campaignCacheService->findByEntityId(tg->getCampaignId());
        auto adv = this->advertiserCacheService->findByEntityId(cmp->getAdvertiserId());
        auto cln = this->clientCacheService->findByEntityId(adv->clientId);
        return isStatusActive(cln->status, cln->id, "cln");
}

bool ActiveFilterModule::isCurrent(
        Poco::DateTime startDate,
        Poco::DateTime endDate) {
        Poco::DateTime now;
        return (now >= startDate && now <= endDate);
}

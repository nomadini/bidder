#ifndef BidderResponseModule_H
#define BidderResponseModule_H


#include "Status.h"
#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "AeroCacheService.h"
#include "Object.h"
class EventLog;
class LastTimeSeenSource;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class TgFilterMeasuresService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
/**
 * this module does the necessary operations that we need to do after we make a bid
 */
class BidderResponseModule : public BidderModule, public Object {

private:

public:
LastTimeSeenSource* lastTimeSeenSource;
AeroCacheService<EventLog>* realTimeEventLogCacheService;
int acceptableIntervalToBidOnAUserInSeconds;
TgFilterMeasuresService* tgFilterMeasuresService;
TgBiddingPerformanceMetricInBiddingPeriodService* tgBiddingPerformanceMetricInBiddingPeriodService;
//this will be reset every hour
std::shared_ptr<gicapods::AtomicLong> numberOfBidsForPixelMatchingPurposePerHour;

BidderResponseModule(LastTimeSeenSource* lastTimeSeenSource,
                     int acceptableIntervalToBidOnAUserInSeconds,
                     EntityToModuleStateStats* entityToModuleStateStats,
                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void verifyEventWasWrittenForTesting(std::shared_ptr<OpportunityContext> context);
virtual ~BidderResponseModule();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};



#endif

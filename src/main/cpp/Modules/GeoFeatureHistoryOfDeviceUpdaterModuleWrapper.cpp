#include "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "OpportunityContext.h"


GeoFeatureHistoryOfDeviceUpdaterModuleWrapper::GeoFeatureHistoryOfDeviceUpdaterModuleWrapper(
        std::string featureType,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->featureType = featureType;
}

std::string GeoFeatureHistoryOfDeviceUpdaterModuleWrapper::getName() {
        return "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper";
}

void GeoFeatureHistoryOfDeviceUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device == nullptr || context->mgrs100m.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                                                                   "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                                   "ALL");

                throwEx("unknown mgrs100m or device");
        }

        deviceGeoFeatureUpdaterModule->process(featureType, context->mgrs100m, context->device);
        entityToModuleStateStats->addStateModuleForEntity ("ADDING_DEVICE_FEATURE_HISTORY",
                                                           "GeoFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                           "ALL");

}

GeoFeatureHistoryOfDeviceUpdaterModuleWrapper::~GeoFeatureHistoryOfDeviceUpdaterModuleWrapper() {

}
bool GeoFeatureHistoryOfDeviceUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

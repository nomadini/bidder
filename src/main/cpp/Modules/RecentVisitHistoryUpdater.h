#ifndef RecentVisitHistoryUpdater_H
#define RecentVisitHistoryUpdater_H


#include "BidderModule.h"
#include "EntityProviderService.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include <unordered_map>
#include "Object.h"
class RecentVisitHistoryScoreCard;
class AerospikeDriverInterface;
class Device;
class TargetGroupCacheService;
class RecencyModel;
class TargetGroupRecencyModelMapCacheService;
class RecencyModel;
class TgModelEligibilityRecord;
class TargetGroup;
class RecentVisitHistoryScoreReader;
class RecentVisitHistoryUpdater : public BidderModule, public Object {

public:
static std::string RECENT_VISIT_HISTORY_BIN_NAME;
static std::string RECENT_VISIT_HISTORY_SCORE_BIN_NAME;
static std::string RECENT_VISIT_HISTORY_NAMESPACE;
static std::string RECENT_VISIT_HISTORY_SET;
AerospikeDriverInterface* aeroSpikeDriver;
TargetGroupCacheService* targetGroupCacheService;

RecentVisitHistoryUpdater(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

std::string writeAdHistory(
        std::string featureType,
        std::string featureValue,
        std::shared_ptr<Device> device);

virtual ~RecentVisitHistoryUpdater();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};


#endif

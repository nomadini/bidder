#ifndef TargetGroupSelectorModule_H
#define TargetGroupSelectorModule_H

#include "BidderModule.h"
#include "Object.h"
class OpportunityContext;
#include "Status.h"
#include <memory>
#include <string>
class CampaignCacheService;
#include "Advertiser.h"
#include "EntityProviderService.h"
#include "CacheService.h"
#include "CreativeSizeFilter.h"
class TargetGroupCacheService;
class CreativeCacheService;

class TargetGroupTargetingTypeValidatorModule;
class TargetGroupSelectorModule : public BidderModule, public Object {

private:

public:

CampaignCacheService* campaignCacheService;
CreativeSizeFilter* creativeSizeFilter;
CacheService<Advertiser>* advertiserCacheService;
CreativeCacheService* creativeCacheService;
TargetGroupCacheService* targetGroupCacheService;
TargetGroupTargetingTypeValidatorModule* targetGroupTargetingTypeValidatorModule;

TargetGroupSelectorModule(EntityToModuleStateStats* entityToModuleStateStats,
                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

void selectTheOptimalTargetGroup(std::shared_ptr<OpportunityContext> context,
                                 std::vector<std::shared_ptr<TargetGroup> > rawOrdered);
bool checkNextTargetGroupIfCurrentSelectionHasNoCreative(std::shared_ptr<TargetGroup> tgCandidate);
void setRequiredContextVariablesBasedOnChosenTg(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<TargetGroup>& tgCandidate);

void selectTheBestCreative(std::shared_ptr<OpportunityContext> context);

bool creativeIsValidInTermsOfSSL(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<Creative> crt);

virtual void process(std::shared_ptr<OpportunityContext> context);


std::string getName();

virtual ~TargetGroupSelectorModule();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};



#endif

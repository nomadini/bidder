/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupWithoutSegmentFilterModule_H_
#define TargetGroupWithoutSegmentFilterModule_H_
#include "Object.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "ConcurrentHashSet.h"
class TargetGroupSegmentCacheService;
// namespace gicapods { template <typename K> class ConcurrentHashSet; }

class TargetGroupWithoutSegmentFilterModule : public BidderModule, public Object {

private:

public:

std::unique_ptr<gicapods::ConcurrentHashSet<int> > targetGroupsWithoutSegments;

virtual std::string getName();
TargetGroupSegmentCacheService* targetGroupSegmentCacheService;

TargetGroupWithoutSegmentFilterModule(
        TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif /* TargetGroupWithoutSegmentFilterModule_H_ */

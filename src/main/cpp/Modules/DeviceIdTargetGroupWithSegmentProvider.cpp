#include "DeviceIdTargetGroupWithSegmentProvider.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"

#include "GUtil.h"
#include <boost/foreach.hpp>
#include "DeviceSegmentHistoryCassandraService.h"
#include "TargetGroupSegmentCacheService.h"
#include "SegmentCacheService.h"
#include "ConverterUtil.h"
#include "DeviceSegmentPair.h"
#include "EntityToModuleStateStats.h"
#include "DeviceSegmentHistory.h"
#include "DeviceIdSegmentModuleHelper.h"
#include "ObjectVectorHolder.h"
#include "ConcurrentHashMap.h"

DeviceIdTargetGroupWithSegmentProvider::DeviceIdTargetGroupWithSegmentProvider(
        TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
        SegmentCacheService* segmentCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        EntityToModuleStateStats* entityToModuleStateStats,
        DeviceIdSegmentModuleHelper* deviceIdSegmentModuleHelper,
        RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory,
        long ignoreSegmentsOlderThanXMilliSeconds) : Object(__FILE__) {
        this->targetGroupSegmentCacheService = targetGroupSegmentCacheService;
        this->segmentCacheService = segmentCacheService;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->deviceIdSegmentModuleHelper = deviceIdSegmentModuleHelper;
        this->realTimeEntityCacheServiceDeviceSegmentHistory = realTimeEntityCacheServiceDeviceSegmentHistory;
        this->ignoreSegmentsOlderThanXMilliSeconds = ignoreSegmentsOlderThanXMilliSeconds;
}

DeviceIdTargetGroupWithSegmentProvider::~DeviceIdTargetGroupWithSegmentProvider() {

}

std::vector<std::shared_ptr<TargetGroup> > DeviceIdTargetGroupWithSegmentProvider::findAllTargetGroupIdsWithUserSegments(
        std::vector<std::string> uniqueNameOfSegmentForDevice) {

        std::vector<std::shared_ptr<TargetGroup> > allTgsWithCorrectSegmentsForDevice;

        for(std::string uniqueNameOfSegment :  uniqueNameOfSegmentForDevice) {

                auto segmentPairPtr = segmentCacheService->allNameToSegmentsMap->find (uniqueNameOfSegment);
                if (segmentPairPtr != segmentCacheService->allNameToSegmentsMap->end()) {
                        int segmentMysqlId = segmentPairPtr->second->id;
                        MLOG(10)<< "segmentMysqlId : "<< segmentMysqlId;

                        entityToModuleStateStats->addStateModuleForEntity ("DEVICE_SEGMENT_WAS_FOUND_IN_DB",
                                                                           getName (),
                                                                           uniqueNameOfSegment);

                        auto targetGroupsHavingSegments = fetchTargetGroupsHavingSegment(
                                uniqueNameOfSegment,
                                segmentMysqlId);
                        allTgsWithCorrectSegmentsForDevice =
                                CollectionUtil::combineTwoLists (
                                        allTgsWithCorrectSegmentsForDevice, targetGroupsHavingSegments);
                } else {
                        entityToModuleStateStats->addStateModuleForEntity ("NO_SEGMENT_FOUND_FOR_ID",
                                                                           getName (),
                                                                           uniqueNameOfSegment,
                                                                           EntityToModuleStateStats::exception);
                        LOG_EVERY_N(ERROR, 1000) << "there is no segment for id : " << uniqueNameOfSegment << " in the map";
                }
        }
        return allTgsWithCorrectSegmentsForDevice;
}


std::vector<std::shared_ptr<TargetGroup> >
DeviceIdTargetGroupWithSegmentProvider::fetchTargetGroupsHavingSegment(std::string uniqueNameOfSegment, int segmentMysqlId) {
        std::vector<std::shared_ptr<TargetGroup> > empty;

        std::shared_ptr<ObjectVectorHolder<TargetGroup> > targetGroupsHavingSegments =
                targetGroupSegmentCacheService->getAllSegmentTargetGroupListMap ()
                ->getOptional (segmentMysqlId);
        if (targetGroupsHavingSegments != nullptr) {
                deviceIdSegmentModuleHelper->recordTgHavingSpecificSegments(uniqueNameOfSegment,
                                                                            *targetGroupsHavingSegments->values);
                return *targetGroupsHavingSegments->values;
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("NO_TG_FOR_SEGMENT_ID_FOUND",
                                                                   getName (),
                                                                   uniqueNameOfSegment);
                MLOG(10) << "there is no target group list for segment id " <<
                        segmentMysqlId <<
                        " in the map";
        }
        return empty;
}

tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> >
DeviceIdTargetGroupWithSegmentProvider::readSegmentsAndFetchAllTgsAttached(std::shared_ptr<OpportunityContext> context) {
        std::vector<std::shared_ptr<TargetGroup> > allTgsWithCorrectSegmentsForDevice;
        auto deviceSegmentHistory = std::make_shared<DeviceSegmentHistory>(context->device);

        std::shared_ptr<DeviceSegmentHistory> deviceSegments =
                realTimeEntityCacheServiceDeviceSegmentHistory->readDataOptional(deviceSegmentHistory);

        if (deviceSegments != nullptr) {
                entityToModuleStateStats->addStateModuleForEntity ("DEVICE_SEGMENT_FOUND_FOR_DEVICE",
                                                                   getName (),
                                                                   "ALL");
                std::vector<std::string> uniqueNameOfSegmentForDevice;

                for (auto deviceSegment : *deviceSegments->deviceSegmentAssociations) {
                        context->allSegmentIdsFoundList.push_back(deviceSegment->segment->id);
                        if (DateTimeUtil::getNowInMilliSecond() -
                            deviceSegment->segment->dateCreatedInMillis >= ignoreSegmentsOlderThanXMilliSeconds) {
                                //ignoring this segment
                        } else {
                                uniqueNameOfSegmentForDevice.push_back(deviceSegment->segment->getUniqueName());
                                context->chosenSegmentIdsList.push_back(deviceSegment->segment->id);
                        }

                }


                deviceIdSegmentModuleHelper->recordDeviceHavingSegments(context, uniqueNameOfSegmentForDevice);
                allTgsWithCorrectSegmentsForDevice  =
                        findAllTargetGroupIdsWithUserSegments (
                                uniqueNameOfSegmentForDevice);
        } else {
                entityToModuleStateStats->addStateModuleForEntity ("NO_DEVICE_SEGMENT_FOUND_FOR_DEVICE",
                                                                   getName (),
                                                                   "ALL");
        }

        tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> > allTgsWithCorrectSegmentsForDeviceAsMap =
                CollectionUtil::convertListToConcurrentMap<std::shared_ptr<TargetGroup> >
                        (allTgsWithCorrectSegmentsForDevice);

        return allTgsWithCorrectSegmentsForDeviceAsMap;
}

std::string DeviceIdTargetGroupWithSegmentProvider::getName() {
        return "DeviceIdTargetGroupWithSegmentProvider";
}

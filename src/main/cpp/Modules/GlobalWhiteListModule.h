#ifndef GlobalWhiteListModule_H
#define GlobalWhiteListModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "BidderModule.h"
#include "Object.h"
#include "MySqlBWListService.h"
class GlobalWhiteListedCacheService;

class GlobalWhiteListModule;


/*
   globalWhiteListedCacheService has a list of domains that we are interested on bidding
   this module makes sure that we bid only on those modules
 */
class GlobalWhiteListModule : public BidderModule, public Object {

public:
GlobalWhiteListedCacheService* globalWhiteListedCacheService;

GlobalWhiteListModule(GlobalWhiteListedCacheService* globalWhiteListedCacheService,
                      EntityToModuleStateStats* entityToModuleStateStats,
                      std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~GlobalWhiteListModule();

void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

std::string getName();
};

#endif

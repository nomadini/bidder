#ifndef DeviceIdSegmentModuleHelper_H
#define DeviceIdSegmentModuleHelper_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "BidderModule.h"
class TargetGroupSegmentCacheService;
class TargetGroupCacheService;
class SegmentCacheService;
#include <tbb/concurrent_hash_map.h>
#include "EntityProviderService.h"
#include "RealTimeEntityCacheService.h"
#include "Object.h"
class TargetGroupWithoutSegmentFilterModule;
class DeviceIdSegmentModuleHelper;
class DeviceSegmentHistory;
class EntityToModuleStateStats;


class DeviceIdSegmentModuleHelper : public Object {

public:
TargetGroupSegmentCacheService* targetGroupSegmentCacheService;
TargetGroupCacheService* targetGroupCacheService;
TargetGroupWithoutSegmentFilterModule* targetGroupWithoutSegmentFilterModule;
EntityToModuleStateStats* entityToModuleStateStats;
//this service uses both aerospike and cassandra to read the segemnts as fast as it can
// it uses the aerospike as the caching layer
RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory;
SegmentCacheService* segmentCacheService;

DeviceIdSegmentModuleHelper(TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
                            SegmentCacheService* segmentCacheService,
                            std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                            EntityToModuleStateStats* entityToModuleStateStats);
virtual ~DeviceIdSegmentModuleHelper();

void recordTgNotHavingSegments(std::shared_ptr<TargetGroup>& tg);
void recordTgHavingSpecificSegments(
        std::string& uniqueNameOfSegment,
        std::vector<std::shared_ptr<TargetGroup> >& targetGroupsHavingSegments);
void recordDeviceHavingSegments(
        std::shared_ptr<OpportunityContext> context,
        std::vector<std::string>& uniqueNameOfSegmentForDevice);
};

#endif

#ifndef GeoFeatureHistoryOfDeviceUpdaterModuleWrapper_H
#define GeoFeatureHistoryOfDeviceUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include "DeviceHistoryUpdaterModule.h"
#include "Object.h"
class GeoFeatureHistoryOfDeviceUpdaterModuleWrapper : public BidderModule, public Object {

private:
std::string featureType;
public:
DeviceHistoryUpdaterModule* deviceGeoFeatureUpdaterModule;
GeoFeatureHistoryOfDeviceUpdaterModuleWrapper(
        std::string featureType,
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~GeoFeatureHistoryOfDeviceUpdaterModuleWrapper();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

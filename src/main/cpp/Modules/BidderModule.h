#ifndef BidderModule_H
#define BidderModule_H


#include "Status.h" //this is needed in every module
class OpportunityContext;
#include <memory>
#include <string>
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
#include "TgMarker.h"
#include "ConcurrentHashMap.h"
#include "Object.h"
class BidderModule;


class BidderModule : public TgMarker, public Object {

private:

public:

BidderModule(EntityToModuleStateStats* entityToModuleStateStats,
													std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void beforeProcessing(std::shared_ptr<OpportunityContext> context);

virtual void process(std::shared_ptr<OpportunityContext> context)=0;

virtual void afterProcessing(std::shared_ptr<OpportunityContext> context);

virtual ~BidderModule();
};

#endif

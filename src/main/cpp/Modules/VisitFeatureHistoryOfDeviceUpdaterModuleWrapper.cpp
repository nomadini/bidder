#include "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "GUtil.h"
#include "OpportunityContext.h"
#include "DeviceFeatureHistoryCassandraService.h"
#include "DeviceFeatureHistory.h"
#include "Feature.h"

VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::VisitFeatureHistoryOfDeviceUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

std::string VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::getName() {
        return "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper";
}

void VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device == nullptr || context->siteDomain.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                                                                   "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                                   "ALL");

                throwEx("unknown siteDomain or device");
        }

        visitFeatureDeviceHistoryUpdaterModule->process(Feature::generalTopLevelDomain,
                                                        context->siteDomain,
                                                        context->device);

        //we save device to iabcat here which is used by clustering app
        for (auto cat : context->siteCategory) {
                visitFeatureDeviceHistoryUpdaterModule->process(Feature::IAB_CAT,
                                                                cat,
                                                                context->device);
        }
        entityToModuleStateStats->addStateModuleForEntity ("ADDING_DEVICE_FEATURE_HISTORY",
                                                           "VisitFeatureHistoryOfDeviceUpdaterModuleWrapper",
                                                           "ALL");

}

VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::~VisitFeatureHistoryOfDeviceUpdaterModuleWrapper() {

}
bool VisitFeatureHistoryOfDeviceUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

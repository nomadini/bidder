#ifndef EventLogCreatorModule_h
#define EventLogCreatorModule_h


#include "Status.h"
#include <memory>
#include <string>
class OpportunityContext;
class EventLog;
#include "BidderModule.h"
class EventLogCassandraService;
#include <tbb/concurrent_queue.h>
#include <Poco/ThreadPool.h>
#include "Object.h"

class EventLogCreatorModule : public BidderModule, public Object {

public:
std::string appHostname;
std::string appVersion;
EventLogCreatorModule(
								EntityToModuleStateStats* entityToModuleStateStats,
								std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
								std::string appHostname,
								std::string appVersion);

virtual std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

std::shared_ptr<EventLog> createEventLogFrom(std::shared_ptr<OpportunityContext> context);


bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};


#endif

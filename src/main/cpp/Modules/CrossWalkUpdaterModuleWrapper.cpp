#include "CrossWalkUpdaterModuleWrapper.h"
#include "DateTimeUtil.h"
#include "CrossWalkUpdaterModule.h"
#include "OpportunityContext.h"
CrossWalkUpdaterModuleWrapper::CrossWalkUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts
        ) : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

}

std::string CrossWalkUpdaterModuleWrapper::getName() {
        return "CrossWalkUpdaterModuleWrapper";
}


void CrossWalkUpdaterModuleWrapper::process(std::shared_ptr<OpportunityContext> context) {

        if (context->device == nullptr || context->deviceIp.empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("SKIPPING_BECAUSEOF_UNKNOWN_USER",
                                                                   "CrossWalkUpdaterModuleWrapper",
                                                                   "ALL");

                throwEx("unknown deviceIp or device");
        }
        crossWalkUpdaterModule->recordIpToDeviceAssociations(
                context->deviceIp,
                context->device);

}

bool CrossWalkUpdaterModuleWrapper::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

CrossWalkUpdaterModuleWrapper::~CrossWalkUpdaterModuleWrapper() {

}

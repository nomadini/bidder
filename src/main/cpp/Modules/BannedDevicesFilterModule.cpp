#include "BannedDevicesFilterModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"

#include "GUtil.h"
#include "OpportunityContext.h"
#include "Campaign.h"
#include "Advertiser.h"
#include "Client.h"
#include "Device.h"

BannedDevicesFilterModule::BannedDevicesFilterModule(EntityToModuleStateStats* entityToModuleStateStats,
                                                     std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

BannedDevicesFilterModule::~BannedDevicesFilterModule() {

}

std::string BannedDevicesFilterModule::getName() {
        return "BannedDevicesFilterModule";
}

void BannedDevicesFilterModule::process(std::shared_ptr<OpportunityContext> context) {

        context->status->value = CONTINUE_PROCESSING;
        if (bannedDevices->exists(context->device->getDeviceId())) {
                entityToModuleStateStats->addStateModuleForEntity ("bannedDevicesSeen",
                                                                   "BannedDevicesFilterModule",
                                                                   "ALL");

                context->status->value = STOP_PROCESSING;
        }


        if (bannedIps->exists(context->deviceIp)) {
                entityToModuleStateStats->addStateModuleForEntity ("bannedIpsSeen",
                                                                   "BannedDevicesFilterModule",
                                                                   "ALL");

                context->status->value = STOP_PROCESSING;
        }


        if (bannedMgrs10ms->exists(context->mgrs10m)) {
                entityToModuleStateStats->addStateModuleForEntity ("bannedMgrs10msSeen",
                                                                   "BannedDevicesFilterModule",
                                                                   "ALL");

                context->status->value = STOP_PROCESSING;
        }


}
bool BannedDevicesFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

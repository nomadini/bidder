#ifndef GicapodsIdToExchangeIdMapModule_H
#define GicapodsIdToExchangeIdMapModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "AtomicLong.h"
#include "BidderModule.h"
#include "Object.h"
class GicapodsIdToExchangeIdsMapCassandraService;

class GicapodsIdToExchangeIdMapModule;

/*
   if the request is from a desktop requst && gicapodsId is not present
   this module will query the gicapodsIdToExchangeIdsMap in cassandra with the exchange id to get the nomadiniDeviceId.
   at the end of this module, if nomadiniDeviceId is still empty, we nobid on this request

   if request is mobile,
   this module sets the idfa if not present we set sha1 as the nomadiniDeviceId

 */
class GicapodsIdToExchangeIdMapModule : public BidderModule, public Object {

public:

GicapodsIdToExchangeIdsMapCassandraService* gicapodsIdToExchangeIdsMapCassandraService;
std::shared_ptr<gicapods::AtomicLong> numberOfBidsForPixelMatchingPurposePerHourLimit;
std::shared_ptr<gicapods::AtomicLong> numberOfBidsForPixelMatchingPurposePerHour;
GicapodsIdToExchangeIdMapModule(EntityToModuleStateStats* entityToModuleStateStats,
                                std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~GicapodsIdToExchangeIdMapModule();

bool doWeNeedToBidForPixelMatchingOpportunity(std::shared_ptr<OpportunityContext> context);

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

#ifndef RecentVisitHistoryScoreReader_H
#define RecentVisitHistoryScoreReader_H


#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include <unordered_map>
#include "Object.h"
class RecentVisitHistoryScoreCard;
class AerospikeDriverInterface;
class Device;
class TargetGroupCacheService;
class RecencyModel;
class TargetGroupRecencyModelMapCacheService;
class RecencyModel;
class TgModelEligibilityRecord;
class TargetGroup;
class DeviceRecentVisitHistory;

class RecentVisitHistoryScoreReader : public Object {
public:
RecentVisitHistoryScoreReader();
virtual ~RecentVisitHistoryScoreReader();
AerospikeDriverInterface* aeroSpikeDriver;
TargetGroupRecencyModelMapCacheService* targetGroupRecencyModelMapCacheService;

std::shared_ptr<RecentVisitHistoryScoreCard> readTheScoreCardForDevice(std::shared_ptr<Device> device);

std::vector<std::shared_ptr<RecencyModel> > getAllRecentModelsOfTg(int targetGroupId);

std::shared_ptr<DeviceRecentVisitHistory> readRecentVisitHistory(std::shared_ptr<Device> device);
};

#endif

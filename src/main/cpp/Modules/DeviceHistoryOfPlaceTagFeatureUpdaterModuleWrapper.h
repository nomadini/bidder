#ifndef DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper_H
#define DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
class Feature;
class FeatureDeviceHistory;
#include <memory>
#include <string>
#include "FeatureDeviceHistoryUpdaterModule.h"
#include "Object.h"
class DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper : public BidderModule, public Object {

private:

public:
std::string featureCategory;
FeatureDeviceHistoryUpdaterModule* featureDeviceHistoryUpdaterModule;

DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory);

std::string getName();

std::string toString();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);


};

#endif

#include "LastXSecondSeenIpVisitorModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "LastTimeIpSeenSource.h"
#include "Histogram.h"
LastXSecondSeenIpVisitorModule::LastXSecondSeenIpVisitorModule(LastTimeIpSeenSource* lastTimeIpSeenSource,
                                                               EntityToModuleStateStats* entityToModuleStateStats,
                                                               std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeIpSeenSource = lastTimeIpSeenSource;
        this->histoOfIpCounts = std::make_shared<Histogram>(0, 1000, 50);
}

LastXSecondSeenIpVisitorModule::~LastXSecondSeenIpVisitorModule() {

}

std::string LastXSecondSeenIpVisitorModule::getName() {
        return "LastXSecondSeenIpVisitorModule";
}

void LastXSecondSeenIpVisitorModule::process(std::shared_ptr<OpportunityContext> context) {

        //we check if we have seen this ip in the last 30 seconds or not
        bool seen = lastTimeIpSeenSource->hasSeenInLastXSeconds(context->deviceIp);
        if (seen) {
                /**
                 * we have seen this user before
                 */
                MLOG(3)<<"context->deviceIp "<< context->deviceIp <<  " was seen in last n seconds.aborting";
                context->status->value = STOP_PROCESSING;
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_BECAUSE_SAW_LATELY",
                                                                   "LastXSecondSeenIpVisitorModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);


        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_BECAUSE_DIDNT_SEE_LATELY",
                                                                   "LastXSecondSeenIpVisitorModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);
                context->status->value = CONTINUE_PROCESSING;

        }

        //we mark this ip as seen for 30 seconds in cache....
        lastTimeIpSeenSource->markAsSeenInLastXSeconds(context->deviceIp, 30);
}

bool LastXSecondSeenIpVisitorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

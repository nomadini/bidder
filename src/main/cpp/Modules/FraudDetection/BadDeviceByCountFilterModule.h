#ifndef BadDeviceByCountFilterModule_H
#define BadDeviceByCountFilterModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "BidderModule.h"
#include "Object.h"
class OpportunityContext;
class Histogram;
class LastTimeSeenSource;

class BadDeviceByCountFilterModule : public BidderModule, public Object {

public:
LastTimeSeenSource* lastTimeSeenSource;
std::shared_ptr<Histogram> histoOfDeviceCounts;

BadDeviceByCountFilterModule(LastTimeSeenSource* lastTimeSeenSource,
                             EntityToModuleStateStats* entityToModuleStateStats,
                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                             std::shared_ptr<Histogram> histoOfDeviceCounts);

virtual ~BadDeviceByCountFilterModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void processBadDeviceByCountFilter(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

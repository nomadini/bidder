#ifndef LastXSecondSeenVisitorModule_H
#define LastXSecondSeenVisitorModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "BidderModule.h"
#include "Object.h"
class OpportunityContext;
class Histogram;
class LastTimeSeenSource;

class LastXSecondSeenVisitorModule;


class LastXSecondSeenVisitorModule : public BidderModule, public Object {

public:
LastTimeSeenSource* lastTimeSeenSource;
std::shared_ptr<Histogram> histoOfIpCounts;
LastXSecondSeenVisitorModule(LastTimeSeenSource* lastTimeSeenSource,
                             EntityToModuleStateStats* entityToModuleStateStats,
                             std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~LastXSecondSeenVisitorModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void processDeviceCountFilter(std::shared_ptr<OpportunityContext> context);
void processDeviceLastSeenFilter(std::shared_ptr<OpportunityContext> context);
void recordDeviceInBadDeviceIds(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

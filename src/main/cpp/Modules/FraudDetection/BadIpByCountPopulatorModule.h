#ifndef BadIpByCountPopulatorModule_H
#define BadIpByCountPopulatorModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "Object.h"
#include "BidderModule.h"
class OpportunityContext;
class Histogram;
class LastTimeIpSeenSource;

class BadIpByCountPopulatorModule : public BidderModule, public Object {

public:
LastTimeIpSeenSource* lastTimeIpSeenSource;
std::shared_ptr<Histogram> histoOfIpCounts;

BadIpByCountPopulatorModule(LastTimeIpSeenSource* lastTimeIpSeenSource,
                            EntityToModuleStateStats* entityToModuleStateStats,
                            std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                            std::shared_ptr<Histogram> histoOfIpCounts);

virtual ~BadIpByCountPopulatorModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void populateBadIpByCountCache(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

#ifndef LastXSecondSeenIpVisitorModule_H
#define LastXSecondSeenIpVisitorModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "BidderModule.h"
#include "Object.h"
class OpportunityContext;
class Histogram;
class LastTimeIpSeenSource;

class LastXSecondSeenIpVisitorModule : public BidderModule, public Object {

public:
LastTimeIpSeenSource* lastTimeIpSeenSource;
std::shared_ptr<Histogram> histoOfIpCounts;

LastXSecondSeenIpVisitorModule(LastTimeIpSeenSource* lastTimeIpSeenSource,
                               EntityToModuleStateStats* entityToModuleStateStats,
                               std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~LastXSecondSeenIpVisitorModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

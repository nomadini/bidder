#include "LastXSecondSeenVisitorModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
#include "Device.h"
#include "Histogram.h"
#include "LastTimeSeenSource.h"

LastXSecondSeenVisitorModule::LastXSecondSeenVisitorModule(LastTimeSeenSource* lastTimeSeenSource,
                                                           EntityToModuleStateStats* entityToModuleStateStats,
                                                           std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeSeenSource = lastTimeSeenSource;
        this->histoOfIpCounts = std::make_shared<Histogram>(0, 1000, 50);
}

LastXSecondSeenVisitorModule::~LastXSecondSeenVisitorModule() {

}

std::string LastXSecondSeenVisitorModule::getName() {
        return "LastXSecondSeenVisitorModule";
}

void LastXSecondSeenVisitorModule::process(std::shared_ptr<OpportunityContext> context) {

        processDeviceCountFilter(context);

        processDeviceLastSeenFilter(context);

}

void LastXSecondSeenVisitorModule::processDeviceLastSeenFilter(std::shared_ptr<OpportunityContext> context) {
        if (context->isAsyncPiplineExecuting) {
                //this logic is only allowed to run in sync pipeline
                return;
        }
        //we mark user as seen, if we have bid on it ever...we don't want to bid on a user too often
        bool seen = lastTimeSeenSource->hasSeenInLastXSeconds(context->device);
        if (seen) {
                /**
                 * we have seen this user before
                 */
                context->status->value = STOP_PROCESSING;
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_BECAUSE_SAW_LATELY",
                                                                   "LastXSecondSeenVisitorModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_BECAUSE_DIDNT_SEE_LATELY",
                                                                   "LastXSecondSeenVisitorModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);
                context->status->value = CONTINUE_PROCESSING;

        }
}

void LastXSecondSeenVisitorModule::recordDeviceInBadDeviceIds(std::shared_ptr<OpportunityContext> context) {

}

void LastXSecondSeenVisitorModule::processDeviceCountFilter(std::shared_ptr<OpportunityContext> context) {
        if (!context->isAsyncPiplineExecuting) {
                //this logic is only allowed to run in async pipeline
                return;
        }
        long timesSeen = lastTimeSeenSource->countAndGetTimesSeen(
                context->device,
                DateTimeUtil::daysInSeconds);
        LOG_EVERY_N(ERROR, 1000)<<google::COUNTER<<"nth : device id seen times : " <<timesSeen;

        //TODO : block the highest offenders
        histoOfIpCounts->record(timesSeen);
        if (histoOfIpCounts->totalCount->getValue() >= 10000) {
                auto isSuspiciousDevice = histoOfIpCounts->isAmongTopXPercentOfDataWithHighestValues(timesSeen, 20);
                if (isSuspiciousDevice) {
                        recordDeviceInBadDeviceIds(context);
                }
        }

        if (gicapods::Util::allowedToCall(_FL_, 10000)) {
                histoOfIpCounts->printBinPercentages();
        }
}
bool LastXSecondSeenVisitorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

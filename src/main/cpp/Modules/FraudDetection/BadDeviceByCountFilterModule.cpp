#include "BadDeviceByCountFilterModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
#include "Device.h"

#include "LastTimeSeenSource.h"
#include "Histogram.h"
BadDeviceByCountFilterModule::BadDeviceByCountFilterModule(LastTimeSeenSource* lastTimeSeenSource,
                                                           EntityToModuleStateStats* entityToModuleStateStats,
                                                           std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                                           std::shared_ptr<Histogram> histoOfDeviceCounts)
        : BidderModule(entityToModuleStateStats,
                       filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeSeenSource = lastTimeSeenSource;
        this->histoOfDeviceCounts = histoOfDeviceCounts;
}

BadDeviceByCountFilterModule::~BadDeviceByCountFilterModule() {

}

std::string BadDeviceByCountFilterModule::getName() {
        return "BadDeviceByCountFilterModule";
}


void BadDeviceByCountFilterModule::process(std::shared_ptr<OpportunityContext> context) {

        processBadDeviceByCountFilter(context);

}

void BadDeviceByCountFilterModule::processBadDeviceByCountFilter(std::shared_ptr<OpportunityContext> context) {
        bool isBadDeviceByCount =
                lastTimeSeenSource->isBadDeviceByCount(context->device);
        if (isBadDeviceByCount) {
                /**
                 * we have seen this user before
                 */
                context->status->value = STOP_PROCESSING;
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_BECAUSE_BAD_DEVICE_BY_COUNT",
                                                                   "BadDeviceByCountFilterModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);


        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_BECAUSE_NOT_BAD_DEVICE_BY_COUNT",
                                                                   "BadDeviceByCountFilterModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);
                context->status->value = CONTINUE_PROCESSING;

        }

}


bool BadDeviceByCountFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

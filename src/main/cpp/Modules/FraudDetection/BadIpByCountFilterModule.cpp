#include "BadIpByCountFilterModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "LastTimeIpSeenSource.h"
#include "Histogram.h"
BadIpByCountFilterModule::BadIpByCountFilterModule(LastTimeIpSeenSource* lastTimeIpSeenSource,
                                                   EntityToModuleStateStats* entityToModuleStateStats,
                                                   std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                                   std::shared_ptr<Histogram> histoOfIpCounts)
        : BidderModule(entityToModuleStateStats,
                       filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeIpSeenSource = lastTimeIpSeenSource;
        this->histoOfIpCounts = histoOfIpCounts;
}




BadIpByCountFilterModule::~BadIpByCountFilterModule() {

}

std::string BadIpByCountFilterModule::getName() {
        return "BadIpByCountFilterModule";
}


void BadIpByCountFilterModule::process(std::shared_ptr<OpportunityContext> context) {

        processBadIpByCountFilter(context);

}

void BadIpByCountFilterModule::processBadIpByCountFilter(std::shared_ptr<OpportunityContext> context) {
        bool isBadDeviceByCount = lastTimeIpSeenSource->isBadIpByCount(context->deviceIp);
        if (isBadDeviceByCount) {
                /**
                 * we have seen this user before
                 */
                context->status->value = STOP_PROCESSING;
                entityToModuleStateStats->addStateModuleForEntity ("FAILED_BECAUSE_BAD_DEVICE_BY_IP",
                                                                   "BadIpByCountFilterModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);


        } else {
                entityToModuleStateStats->addStateModuleForEntity ("PASSED_BECAUSE_NOT_BAD_DEVICE_BY_IP",
                                                                   "BadIpByCountFilterModule",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);
                context->status->value = CONTINUE_PROCESSING;

        }

}

bool BadIpByCountFilterModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

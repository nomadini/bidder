#include "BadDeviceByCountPopulatorModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"
#include "Device.h"
#include "LastTimeSeenSource.h"
#include "Histogram.h"
BadDeviceByCountPopulatorModule::BadDeviceByCountPopulatorModule(LastTimeSeenSource* lastTimeSeenSource,
                                                                 EntityToModuleStateStats* entityToModuleStateStats,
                                                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                                                 std::shared_ptr<Histogram> histoOfDeviceCounts,
                                                                 long maxAcceptableTimeSeenInLastTenMinute)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeSeenSource = lastTimeSeenSource;
        this->histoOfDeviceCounts = histoOfDeviceCounts;
        this->maxAcceptableTimeSeenInLastTenMinute = maxAcceptableTimeSeenInLastTenMinute;
}

BadDeviceByCountPopulatorModule::~BadDeviceByCountPopulatorModule() {

}

std::string BadDeviceByCountPopulatorModule::getName() {
        return "BadDeviceByCountPopulatorModule";
}


void BadDeviceByCountPopulatorModule::process(std::shared_ptr<OpportunityContext> context) {
        /*
           we populate the cache in async pipeline
         */
        populateBadDeviceByCountCache(context);


}

void BadDeviceByCountPopulatorModule::populateBadDeviceByCountCache(std::shared_ptr<OpportunityContext> context) {
        if (!context->isAsyncPiplineExecuting) {
                //this logic is only allowed to run in async pipeline
                return;
        }

        //we check to see if we have seen the device in the past 10 minutes
        // and its among 20 percent of devices and timesSeen is more than 30
        long timesSeen = lastTimeSeenSource->countAndGetTimesSeen(
                context->device,
                DateTimeUtil::minutesInSeconds * 10);

        // //TODO : remove this...
        // //fixing the messed up bad devices for now
        // lastTimeSeenSource->markAsGoodDeviceByCount(
        //         context->device,
        //         2 * DateTimeUtil::daysInSeconds);


        //TODO : block the highest offenders
        histoOfDeviceCounts->record(timesSeen);
        if (gicapods::Util::allowedToCall(_FL_, 10000)) {
                histoOfDeviceCounts->printBinPercentages();
        }

        if (histoOfDeviceCounts->isAmongTopXPercentOfDataWithHighestValues(timesSeen, 10)
            && timesSeen > maxAcceptableTimeSeenInLastTenMinute) {
                //the top x percent of devices will go to BadDeviceCache in AeroSpike, we maintain
                //each record for two days in this cache
                //
                LOG_EVERY_N(ERROR, 10) << google::COUNTER<<"nth : timeSeen marked as Bad is "<<timesSeen;
                entityToModuleStateStats->addStateModuleForEntity(
                        "MARKING_DEVICE_AS_BAD_BY_COUNT",
                        "BadDeviceByCountPopulatorModule",
                        "ALL",
                        "IMPORTANT"
                        );
                lastTimeSeenSource->markAsBadDeviceByCount(
                        context->device,
                        2 * DateTimeUtil::daysInSeconds);
        }

}

bool BadDeviceByCountPopulatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

#ifndef BadDeviceByCountPopulatorModule_H
#define BadDeviceByCountPopulatorModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "BidderModule.h"
#include "Object.h"
class OpportunityContext;
class Histogram;
class LastTimeSeenSource;

class BadDeviceByCountPopulatorModule : public BidderModule, public Object {

public:
LastTimeSeenSource* lastTimeSeenSource;
std::shared_ptr<Histogram> histoOfDeviceCounts;
long maxAcceptableTimeSeenInLastTenMinute;

BadDeviceByCountPopulatorModule(LastTimeSeenSource* lastTimeSeenSource,
                                EntityToModuleStateStats* entityToModuleStateStats,
                                std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                std::shared_ptr<Histogram> histoOfDeviceCounts,
                                long maxAcceptableTimeSeenInLastTenMinute);

virtual ~BadDeviceByCountPopulatorModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
void populateBadDeviceByCountCache(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

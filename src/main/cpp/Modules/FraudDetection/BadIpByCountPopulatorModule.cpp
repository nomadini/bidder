#include "BadIpByCountPopulatorModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "OpportunityContext.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "DateTimeUtil.h"

#include "LastTimeIpSeenSource.h"
#include "Histogram.h"
BadIpByCountPopulatorModule::BadIpByCountPopulatorModule(LastTimeIpSeenSource* lastTimeIpSeenSource,
                                                         EntityToModuleStateStats* entityToModuleStateStats,
                                                         std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
                                                         std::shared_ptr<Histogram> histoOfIpCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeIpSeenSource = lastTimeIpSeenSource;
        this->histoOfIpCounts = histoOfIpCounts;
}

BadIpByCountPopulatorModule::~BadIpByCountPopulatorModule() {

}

std::string BadIpByCountPopulatorModule::getName() {
        return "BadIpByCountPopulatorModule";
}


void BadIpByCountPopulatorModule::process(std::shared_ptr<OpportunityContext> context) {
        /*
           we populate the cache in async pipeline
         */
        populateBadIpByCountCache(context);


}

void BadIpByCountPopulatorModule::populateBadIpByCountCache(std::shared_ptr<OpportunityContext> context) {
        if (!context->isAsyncPiplineExecuting) {
                //this logic is only allowed to run in async pipeline
                return;
        }

        long timesSeen = lastTimeIpSeenSource->countAndGetTimesSeen(
                context->deviceIp,
                DateTimeUtil::daysInSeconds);
        //TODO : block the highest offenders

        histoOfIpCounts->record(timesSeen);
        if (gicapods::Util::allowedToCall(_FL_, 10000)) {
                histoOfIpCounts->printBinPercentages();
        }

        if (histoOfIpCounts->isAmongTopXPercentOfDataWithHighestValues(timesSeen, 20) && timesSeen > 100) {
                //the top x percent of devices will go to BadDeviceCache in AeroSpike, we maintain
                //each record for two days in this cache
                //
                entityToModuleStateStats->
                addStateModuleForEntity("MARK_AS_BAD_IP",
                                        "BadIpByCountPopulatorModule",
                                        "ALL",
                                        "IMPORTANT");
                lastTimeIpSeenSource->markAsBadIpByCount(
                        context->deviceIp,
                        2 * DateTimeUtil::daysInSeconds);
        }

}
bool BadIpByCountPopulatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

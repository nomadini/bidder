#ifndef ModuleContainer_H
#define ModuleContainer_H

#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "BidderModule.h"
#include "Object.h"



class BidderLatencyRecorder;
class IpInfoFetcherModule;
class MatchingIabCategoryFilter;
class KafkaProxyPopulator;
class TargetingTypeLessTargetGroupFilter;
class TargetGroupFrequencyCapModule;
class FeatureRecorderPipeline;
class ThrottlerModule;
class BidRequestHandlerPipelineProcessor;
class RecentVisitHistoryScorer;
class RecentVisitHistoryScoreReader;
class TargetGroupTargetingTypeValidatorModule;
class RecentVisitHistoryScoreReader;
class MGRSBuilderModule;
class BidderMainPipelineProcessor;
class TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule;
class RecentVisitHistoryUpdater;
class RecencyVisitScoreFilter;

class AdHistoryCassandraService;
#include "TargetGroupFilterStatistic.h"
class EntityToModuleStateStats;
class TgBiddingPerformanceMetricInBiddingPeriodService;
class TgBiddingPerformanceMetricInBiddingPeriod;

class EventLogCreatorModule;
class FeatureToFeatureHistoryUpdaterModuleWrapper;
class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper;
class BannedDevicesFilterModule;
class PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper;
class GeoFeatureHistoryOfDeviceUpdaterModuleWrapper;
class DeviceGeoFeatureUpdaterModule;
class OpportunityContextBuilder;
class DeviceHistoryOfVisitFeatureUpdaterModuleWrapper;
class DeviceHistoryOfGeoFeatureUpdaterModuleWrapper;
class DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper;

class TargetGroupSelectorModule;
class GlobalWhiteListModule;
class AsyncPipelineFeederModule;
class ProcessorInvokerModule;
class CreativeContentTypeFilter;
class DeviceIdSegmentModule;
class TargetGroupGeoSegmentFilter;
class CrossWalkUpdaterModuleWrapper;
class TargetGroupBudgetFilterChainModule;
class OriginalBidderCallerModule;
class BidPriceCalculatorModule;
class BidderResponseModule;
class LastXSecondSeenVisitorModule;
class LastXSecondSeenIpVisitorModule;
class CassandraDriverInterface;
class HttpUtilService;

class BeanFactory;
class ServiceFactory;

class TargetGroupFilterChainModule;
class TargetGroupSelectorModule;
class EventLogCreatorModule;
class GlobalWhiteListModule;
class GicapodsIdToExchangeIdMapModule;
class CrossWalkUpdaterModuleWrapper;
class DeviceIdSegmentModule;
class VisitFeatureHistoryOfDeviceUpdaterModuleWrapper;
class FeatureDeviceHistoryUpdaterModule;
class BidPriceCalculatorModule;
class TargetGroupPerMinuteBidCapFilter;
class EnqueueForScoringModule;
class AtomicLong;
class TgFilterMeasuresService;
class TgBiddingPerformanceFilterMeasures;
class ActiveFilterModule;

class GeoFeatureFilter;
class DeviceIdService;
class DeviceHistoryUpdaterModule;
class ContextPopulatorModule;
class KafkaProducer;
class LastTimeSeenSource;
class CreativeSizeFilter;
class LastTimeIpSeenSource;
class DeviceSegmentHistory;
class EventLog;
class AdHistory;
class DataReloadPipeline;
class FeatureDeviceHistory;
class TargetGroupWithoutSegmentFilterModule;
class DeviceFeatureHistory;
class BidEventRecorderModule;
class DeviceGeoFeatureHistory;

class Feature;
class BiddingMode;
class BadIpByCountFilterModule;
class BadDeviceByCountFilterModule;
class BadIpByCountPopulatorModule;
class BadDeviceByCountPopulatorModule;

class AdServerStatusChecker;
class DeviceHistoryOfGeoFeatureUpdaterModuleWrapper;
class BidProbabilityEnforcerModule;
class NoBidModeEnforcerModule;
class BidderApplicationContext;
class BidModeControllerService;
class OpportunityContextBuilderOpenRtb2_3;
class BidResponseCreatorModule;
class FeatureToFeatureHistoryUpdaterModule;
class RecentVisitHistoryUpdater;

namespace OpenRtb2_3_0 {
class OpenRtb2_3_0BidResponseBuilder;
};

#include <unordered_set>
#include <tbb/concurrent_hash_map.h>
#include <tbb/concurrent_queue.h>
#include "AtomicDouble.h"
#include "AtomicBoolean.h"

#include "Place.h"
#include "AeroCacheService.h"
#include "ConfigService.h"


class DomainBlackListedFilter;
class BlockedAdvertiserDomainFilter;
class DomainWhiteListedFilter;
class CreativeBannerApiFilter;
class AdPositionFilter;
class BidderWiretapModule;
class CreativeSizeFilter;
class BlockedBannerAdTypeFilter;
class BlockedIabCategoryFilter;
class BlockedCreativeAttributeFilter;
class DeviceTypeFilter;
class OsTypeFilter;
class TopMgrsFilter;
class GeoLocationFilter;
class DayPartFilter;

namespace gicapods {
template <typename K, typename V>
class ConcurrentHashMap;
};

#include "FeatureDeviceHistoryUpdaterModule.h"

template <typename T>
class RealTimeEntityCacheService;
class TgBiddingPerformanceFilterMeasures;
class TgMarker;


class ModuleContainer {

public:
unsigned short bidderPort;

std::shared_ptr<MatchingIabCategoryFilter> matchingIabCategoryFilter;
std::shared_ptr<DomainBlackListedFilter> domainBlackListedFilter;
std::shared_ptr<DomainWhiteListedFilter> domainWhiteListedFilter;
std::shared_ptr<CreativeBannerApiFilter> creativeBannerApiFilter;
std::shared_ptr<AdPositionFilter> adPositionFilter;
std::shared_ptr<CreativeSizeFilter> creativeSizeFilter;
std::shared_ptr<BlockedAdvertiserDomainFilter> blockedAdvertiserDomainFilter;
std::shared_ptr<BlockedIabCategoryFilter> blockedIabCategoryFilter;
std::shared_ptr<BlockedBannerAdTypeFilter> blockedBannerAdTypeFilter;
std::shared_ptr<BlockedCreativeAttributeFilter> blockedCreativeAttributeFilter;
std::shared_ptr<CreativeContentTypeFilter> creativeContentTypeFilter;
std::shared_ptr<DeviceTypeFilter> deviceTypeFilter;
std::shared_ptr<OsTypeFilter> osTypeFilter;
std::shared_ptr<GeoLocationFilter> geoLocationFilter;
std::shared_ptr<TopMgrsFilter> topMgrsFilter;
std::shared_ptr<DayPartFilter> dayPartFilter;
std::shared_ptr<std::unordered_map<std::string, TgMarker*> > allMarkersMap;
std::unique_ptr<IpInfoFetcherModule> ipInfoFetcherModule;
std::unique_ptr<BidderWiretapModule> bidderWiretapModule;


std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingEvents;
std::shared_ptr<OpportunityContextBuilderOpenRtb2_3> opportunityContextBuilderOpenRtb2_3;

std::shared_ptr<tbb::concurrent_hash_map<int, std::shared_ptr<TgBiddingPerformanceFilterMeasures> > > targetGroupIdToFilterMeasuresMap;
std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriodService> tgBiddingPerformanceMetricInBiddingPeriodService;
std::shared_ptr<gicapods::ConcurrentHashMap<int, TgBiddingPerformanceMetricInBiddingPeriod> > tgBiddingPerformanceMap;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetGroupsEligibleForBidding;
std::shared_ptr<std::unordered_set<std::string> > targetGroupToGeoCollectionKeys;
std::unique_ptr<TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule> targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule;

std::shared_ptr<gicapods::AtomicLong> totalNumberOfRequestsProcessedInFilterCountPeriod;
std::shared_ptr<OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder> openRtb2_3_0BidResponseBuilder;
std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfRequestsProcessingNow;
std::shared_ptr<gicapods::AtomicLong> bidPercentage;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingMessagesInKafka;
std::shared_ptr<FeatureToFeatureHistoryUpdaterModule> domainFeatureToFeatureHistoryUpdaterModule;

BeanFactory* beanFactory;
ServiceFactory* serviceFactory;
std::shared_ptr<TgFilterMeasuresService> tgFilterMeasuresService;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, FeatureDeviceHistory> > mapOfInterestingFeatures;
std::shared_ptr<gicapods::AtomicBoolean> isNoBidModeIsTurnedOn;

std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isMetricPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isDeliveryInfoSnapshotPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isReloadProcessHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isFetchingDeliveryInfoFromPacerHealthy;

std::shared_ptr<gicapods::AtomicLong> numberOfBidsForPixelMatchingPurposePerHour;
std::shared_ptr<gicapods::AtomicLong> sizeOfKafkaScoringQueue;

std::unique_ptr<TargetGroupTargetingTypeValidatorModule> targetGroupTargetingTypeValidatorModule;
std::unique_ptr<BidModeControllerService> bidModeControllerService;
std::unique_ptr<BidResponseCreatorModule> bidResponseCreatorModule;
std::unique_ptr<NoBidModeEnforcerModule> noBidModeEnforcerModule;
std::unique_ptr<BidProbabilityEnforcerModule> bidProbabilityEnforcerModule;
std::unique_ptr<MGRSBuilderModule> mgrsBuilderModule;
std::unique_ptr<FeatureRecorderPipeline> featureRecorderPipeline;
std::unique_ptr<BidderMainPipelineProcessor> bidderMainPipelineProcessor;
std::unique_ptr<TargetGroupGeoSegmentFilter> targetGroupGeoSegmentFilter;
std::unique_ptr<TargetGroupWithoutSegmentFilterModule> targetGroupWithoutSegmentFilterModule;
std::unique_ptr<ActiveFilterModule> activeFilterModule;
std::unique_ptr<DataReloadPipeline> dataReloadPipeline;
std::unique_ptr<TargetGroupBudgetFilterChainModule> targetGroupBudgetFilterChainModule;
std::unique_ptr<BidPriceCalculatorModule> bidPriceCalculatorModule;
std::unique_ptr<BidderResponseModule> bidderResponseModule;

std::unique_ptr<LastXSecondSeenVisitorModule> lastXSecondSeenVisitorModule;
std::unique_ptr<LastXSecondSeenIpVisitorModule> lastXSecondSeenIpVisitorModule;
std::unique_ptr<BadIpByCountFilterModule> badIpByCountFilterModule;
std::unique_ptr<BadDeviceByCountFilterModule> badDeviceByCountFilterModule;
std::unique_ptr<AdServerStatusChecker> adServerStatusChecker;
std::unique_ptr<BadIpByCountPopulatorModule> badIpByCountPopulatorModule;
std::unique_ptr<BadDeviceByCountPopulatorModule> badDeviceByCountPopulatorModule;
std::unique_ptr<LastTimeIpSeenSource> lastTimeIpSeenSource;
std::unique_ptr<GlobalWhiteListModule> globalWhiteListModule;
std::unique_ptr<AsyncPipelineFeederModule> asyncPipelineFeederModule;

std::unique_ptr<DeviceHistoryOfVisitFeatureUpdaterModuleWrapper> deviceHistoryOfVisitFeatureUpdaterModuleWrapper;
std::unique_ptr<DeviceHistoryOfGeoFeatureUpdaterModuleWrapper> deviceHistoryOfGeoFeatureUpdaterModuleWrapper;
std::unique_ptr<DeviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper> deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper;
std::unique_ptr<VisitFeatureHistoryOfDeviceUpdaterModuleWrapper> visitFeatureHistoryOfDeviceUpdaterModuleWrapper;
std::unique_ptr<GeoFeatureHistoryOfDeviceUpdaterModuleWrapper> geoFeatureHistoryOfDeviceUpdaterModuleWrapper;
std::unique_ptr<PlaceTagFeatureHistoryOfDeviceUpdaterModuleWrapper> placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper;
std::unique_ptr<FeatureToFeatureHistoryUpdaterModuleWrapper> domainFeatureToFeatureHistoryUpdaterModuleWrapper;

std::unique_ptr<BannedDevicesFilterModule> bannedDevicesFilterModule;
std::unique_ptr<RecentVisitHistoryUpdater> recentVisitHistoryUpdater;
std::unique_ptr<RecentVisitHistoryScorer> recentVisitHistoryScorer;
std::unique_ptr<RecentVisitHistoryScoreReader> recentVisitHistoryScoreReader;



std::unique_ptr<GicapodsIdToExchangeIdMapModule> gicapodsIdToExchangeIdMapModule;
std::unique_ptr<ProcessorInvokerModule> processorInvokerModule;
std::unique_ptr<DeviceIdSegmentModule> deviceIdSegmentModule;
std::unique_ptr<TargetGroupFrequencyCapModule> targetGroupFrequencyCapModule;
std::unique_ptr<ThrottlerModule> throttlerModule;
std::unique_ptr<BidRequestHandlerPipelineProcessor> bidRequestHandlerPipelineProcessor;

std::unique_ptr<TargetGroupFilterChainModule> targetGroupFilterChainModule;
std::unique_ptr<TargetGroupSelectorModule> targetGroupSelectorModule;
std::unique_ptr<EventLogCreatorModule> eventLogCreatorModule;
std::unique_ptr<BidEventRecorderModule> bidEventRecorderModule;
std::unique_ptr<CrossWalkUpdaterModuleWrapper> crossWalkUpdaterModuleWrapper;
std::unique_ptr<TargetGroupPerMinuteBidCapFilter> targetGroupPerMinuteBidCapFilter;
std::unique_ptr<TargetingTypeLessTargetGroupFilter> targetingTypeLessTargetGroupFilter;
std::unique_ptr<EnqueueForScoringModule> enqueueForScoringModule;

std::shared_ptr<GeoFeatureFilter> geoFeatureFilter;
std::unique_ptr<DeviceHistoryUpdaterModule> deviceFeatureHistoryUpdaterModule;
std::unique_ptr<DeviceHistoryUpdaterModule> deviceGeoFeatureUpdaterModule;
std::unique_ptr<DeviceHistoryUpdaterModule> devicePlaceTagFeatureUpdaterModule;
std::unique_ptr<FeatureDeviceHistoryUpdaterModule> featureDeviceHistoryUpdaterModule;
std::unique_ptr<FeatureDeviceHistoryUpdaterModule> placeTagFeatureDeviceHistoryUpdaterModule;
std::unique_ptr<FeatureDeviceHistoryUpdaterModule> geoFeatureDeviceHistoryUpdaterModule;

std::unique_ptr<ContextPopulatorModule> contextPopulatorModule;

std::unique_ptr<KafkaProducer> kafkaProducer;

std::unique_ptr<OpportunityContextBuilder> opportunityContextBuilder;
std::unique_ptr<LastTimeSeenSource> directLastTimeSeenSource;
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<OpportunityContext> > > queueOfContexts;
std::unique_ptr<RecencyVisitScoreFilter> recencyVisitScoreFilter;
std::unique_ptr<KafkaProxyPopulator> kafkaProxyPopulator;
std::unique_ptr<BidderLatencyRecorder> bidderLatencyRecorder;

AeroCacheService<EventLog>* realTimeEventLogCacheService;

bool enbaleSendingScoringMessageToKafka;
ModuleContainer();

void addFilters();
void initializeModules();
void createVisitFeatureDeviceHistoryUpdaterModule();
void createGeoFeatureDeviceHistoryUpdaterModule();
void createPlaceTagFeatureDeviceHistoryUpdaterModule();
void createDomainFeatureToFeatureHistoryUpdaterModule();
virtual ~ModuleContainer();

};

#endif

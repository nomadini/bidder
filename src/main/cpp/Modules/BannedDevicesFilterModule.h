#ifndef BannedDevicesFilterModule_H
#define BannedDevicesFilterModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class Client;
class TargetGroupCacheService;
class CampaignCacheService;

#include "Object.h"
#include "ConcurrentHashSet.h"
template <class T>
class CacheService;

#include "BidderModule.h"

class BannedDevicesFilterModule;


class BannedDevicesFilterModule : public BidderModule, public Object {

public:
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedDevices;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedIps;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedMgrs10ms;
BannedDevicesFilterModule(EntityToModuleStateStats* entityToModuleStateStats,
                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);
virtual ~BannedDevicesFilterModule();

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

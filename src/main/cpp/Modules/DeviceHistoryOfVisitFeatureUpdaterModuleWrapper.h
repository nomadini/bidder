#ifndef DeviceHistoryOfVisitFeatureUpdaterModuleWrapper_H
#define DeviceHistoryOfVisitFeatureUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
class FeatureDeviceHistory;
class Feature;
class FeatureDeviceHistoryUpdaterModule;
#include <memory>
#include <string>
#include "Object.h"
class DeviceHistoryOfVisitFeatureUpdaterModuleWrapper : public BidderModule, public Object {

private:

public:
std::string featureCategory;

FeatureDeviceHistoryUpdaterModule* featureDeviceHistoryUpdaterModule;

DeviceHistoryOfVisitFeatureUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        std::string featureCategory);

std::string getName();

std::string toString();

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual ~DeviceHistoryOfVisitFeatureUpdaterModuleWrapper();
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};



#endif

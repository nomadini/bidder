#include "GicapodsIdToExchangeIdMapModule.h"
#include "TargetGroup.h"
#include "CollectionUtil.h"
#include "EntityToModuleStateStats.h"
#include "JsonUtil.h"
#include "TargetGroupCacheService.h"
#include "GUtil.h"
#include "Device.h"
#include "OpportunityContext.h"
#include "GicapodsIdToExchangeIdsMapCassandraService.h"
GicapodsIdToExchangeIdMapModule::GicapodsIdToExchangeIdMapModule(EntityToModuleStateStats* entityToModuleStateStats,
                                                                 std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts) :
        BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
}

GicapodsIdToExchangeIdMapModule::~GicapodsIdToExchangeIdMapModule() {

}

std::string GicapodsIdToExchangeIdMapModule::getName() {
        return "GicapodsIdToExchangeIdMapModule";
}

void GicapodsIdToExchangeIdMapModule::process(std::shared_ptr<OpportunityContext> context) {
        if (context->device != nullptr) {
                //we already have our own userId from sent to us from the exchange
                return;
        }


        if (StringUtil::equalsIgnoreCase(context->environmentType, "DESKTOP") == true) {
                //user is unknown to us, we query our db to see if we already have a mapping for this user
                //this means that exchange has not sent us our mapped id, but we can find our id
                //in db based on previous mapping... this case shouldn't happen alot
                auto gicapodsIdToExchangeIdsMap =
                        gicapodsIdToExchangeIdsMapCassandraService->
                        readGicapodsIdMappedToExchangeId(
                                context->exchangeName,
                                context->exchangeId);
                if (!gicapodsIdToExchangeIdsMap->nomadiniDeviceId.empty()) {
                        context->device = std::make_shared<Device> (gicapodsIdToExchangeIdsMap->nomadiniDeviceId, "DESKTOP");
                        entityToModuleStateStats->addStateModuleForEntity("MAPPED_INITIAL_UNKNOW_USER_WITH_SUCCESS",
                                                                          "GicapodsIdToExchangeIdMapModule",
                                                                          EntityToModuleStateStats::all,
                                                                          EntityToModuleStateStats::important);
                } else {
                        entityToModuleStateStats->addStateModuleForEntity("UNMAPPED_USER", "GicapodsIdToExchangeIdMapModule", "ALL");
                        if (doWeNeedToBidForPixelMatchingOpportunity(context) == true ) {
                                //TODO : add a limit per hour for this
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "BIDDING_TO_TRY_MATCHING_PIXEL",
                                        "GicapodsIdToExchangeIdMapModule",
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::important);
                                //we set this to true to ignore the DeviceSegments filter for this user
                                context->biddingOnlyForMatchingPixel = true;

                        } else {
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "IGNORE_UNKNOWN_DESKTOP_USER",
                                        "GicapodsIdToExchangeIdMapModule",
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::important);
                                context->status->value = STOP_PROCESSING;
                        }

                }

        } else {
                entityToModuleStateStats->addStateModuleForEntity("NO_DESKTOP_ENV", "GicapodsIdToExchangeIdMapModule", "ALL");
        }


}


bool GicapodsIdToExchangeIdMapModule::doWeNeedToBidForPixelMatchingOpportunity(std::shared_ptr<OpportunityContext> context) {
        if (numberOfBidsForPixelMatchingPurposePerHour->getValue() >=
            numberOfBidsForPixelMatchingPurposePerHourLimit->getValue()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "BIDDING_FOR_MATCHING_PIXEL_LIMIT_REACHED",
                        "GicapodsIdToExchangeIdMapModule",
                        "ALL");

                return false;
        }
        return true;
}
bool GicapodsIdToExchangeIdMapModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

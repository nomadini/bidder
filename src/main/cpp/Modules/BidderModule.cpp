#include "BidderModule.h"

#include "EntityToModuleStateStats.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "Poco/DateTimeParser.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/LocalDateTime.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "OpportunityContext.h"
BidderModule::BidderModule(EntityToModuleStateStats* entityToModuleStateStats,
                           std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : TgMarker (filterNameToFailureCounts), Object(__FILE__) {

        assertAndThrow(entityToModuleStateStats != NULL);
        this->entityToModuleStateStats = entityToModuleStateStats;
}

BidderModule::~BidderModule() {
}

void BidderModule::beforeProcessing(std::shared_ptr<OpportunityContext> context) {

}

void BidderModule::afterProcessing(std::shared_ptr<OpportunityContext> context) {

}

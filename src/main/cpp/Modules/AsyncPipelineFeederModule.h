#ifndef AsyncPipelineFeederModule_H
#define AsyncPipelineFeederModule_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "BidderModule.h"
#include <tbb/concurrent_queue.h>

class AsyncPipelineFeederModule;

#include "Object.h"
class AsyncPipelineFeederModule : public BidderModule, public Object {

public:

AsyncPipelineFeederModule(EntityToModuleStateStats* entityToModuleStateStats,
                          std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual ~AsyncPipelineFeederModule();

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<OpportunityContext> > > queueOfContexts;

std::string getName();

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

#ifndef CrossWalkUpdaterModuleWrapper_H
#define CrossWalkUpdaterModuleWrapper_H


#include "BidderModule.h"
class OpportunityContext;
#include <memory>
#include <string>
class OpportunityContext;
#include "CrossWalkUpdaterModule.h"
#include "Object.h"
class CrossWalkUpdaterModuleWrapper : public BidderModule, public Object {

private:

public:
CrossWalkUpdaterModule* crossWalkUpdaterModule;
CrossWalkUpdaterModuleWrapper(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::string getName();

std::string toString();

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
virtual ~CrossWalkUpdaterModuleWrapper();

};



#endif

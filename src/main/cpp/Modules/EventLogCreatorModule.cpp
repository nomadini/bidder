
#include "EventLogCreatorModule.h"
#include "CollectionUtil.h"
#include "DateTimeUtil.h"
#include <tbb/concurrent_queue.h>
#include <thread>
#include "Encoder.h"
#include "OpportunityContext.h"
#include "EventLog.h"
#include "Device.h"
#include "CollectionUtil.h"
#include "TargetGroup.h"
#include "JsonArrayUtil.h"
EventLogCreatorModule::EventLogCreatorModule(
								EntityToModuleStateStats* entityToModuleStateStats,
								std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
								std::string appHostname,
								std::string appVersion)
								: BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
								this->appHostname = appHostname;
								this->appVersion = appVersion;
}

std::string EventLogCreatorModule::getName() {
								return "EventLogCreatorModule";
}

void EventLogCreatorModule::process(std::shared_ptr<OpportunityContext> context) {
								//this module runs after we have selected a bid

								assertAndThrow(StringUtil::equalsIgnoreCase(context->bidDecisionResult, EventLog::EVENT_TYPE_BID));
								context->eventLog = createEventLogFrom(context);
								LOG_EVERY_N(INFO, 1000) << google::COUNTER<< "th sample eventLog : " << context->eventLog->toJson();

								context->status->value = CONTINUE_PROCESSING;

}

std::shared_ptr<EventLog> EventLogCreatorModule::createEventLogFrom(
								std::shared_ptr<OpportunityContext> context) {



								auto eventLog = std::make_shared<EventLog>(
																context->getTransactionId(),
																context->bidDecisionResult,
																JsonArrayUtil::convertSetToJson(
																								*context->getTargetingTypesForTg(context->chosenTargetGroupId))
																);
								eventLog->eventTime = StringUtil::toStr(DateTimeUtil::getNowInMicroSecond());
								eventLog->exchangeName = context->exchangeName;
								eventLog->clientId = context->clientId;
								eventLog->targetGroupId = context->getChosenTargetGroupId();
								eventLog->creativeId = context->getChosenCreativeId();
								eventLog->campaignId = context->getCampaignId();
								eventLog->advertiserId = context->getAdvertiserId();
								eventLog->publisherId = context->getPublisherId();

								eventLog->geoSegmentListId = context->geoSegmentListId;//this is optional ,defaults to 0
								eventLog->winBidPrice = context->getWinBidPrice();
								eventLog->bidPrice= context->getBidPrice();
								eventLog->platformCost = context->getPlatformCost();

								eventLog->userTimeZone = context->getUserTimeZone();
								eventLog->userTimeZonDifferenceWithUTC = context->getUserTimeZonDifferenceWithUTC();
								eventLog->deviceUserAgent = context->getDeviceUserAgent();
								eventLog->deviceIp = context->getDeviceIp();
								eventLog->device = context->device;
								eventLog->deviceLat = context->deviceLat;
								eventLog->deviceLon = context->deviceLon;
								eventLog->deviceCountry = context->getDeviceCountry();
								eventLog->deviceState = context->getDeviceState();
								eventLog->deviceCity = context->getDeviceCity();
								eventLog->deviceZipcode = context->getDeviceZipcode();
								eventLog->deviceIpAddress = context->deviceIp;

								eventLog->winBidPrice = 0;
								eventLog->siteCategory = JsonArrayUtil::convertListToJson(context->getSiteCategory());
								eventLog->siteDomain = context->getSiteDomain();
								eventLog->sitePage = context->getSitePage();
								eventLog->sitePublisherDomain = context->getSitePublisherDomain();
								eventLog->sitePublisherName = context->getSitePublisherName();
								eventLog->adSize = context->getAdSize();
								eventLog->lastModuleRunInPipeline = context->getLastModuleRunInPipeline();
								eventLog->biddingOnlyForMatchingPixel = context->biddingOnlyForMatchingPixel;
								eventLog->exchangeName = context->exchangeName;
								eventLog->impressionType = context->impressionType;

								eventLog->mgrs1km  = context->mgrs1km;
								eventLog->mgrs100m  = context->mgrs100m;
								eventLog->mgrs10m  = context->mgrs10m;

								eventLog->appHostname = appHostname;
								eventLog->appVersion = appVersion;
								eventLog->bidderCallbackServletUrl = context->bidderCallbackServletUrl;
								eventLog->chosenSegmentIdsList = context->chosenSegmentIdsList;
								eventLog->allSegmentIdsFoundList = context->allSegmentIdsFoundList;
								if (context->strictEventLogChecking) {
																//sometimes we want to record no_bid events in cassandra,
																//in BidEventRecorderModule so we set this boolean flag as false
																//and we don't check these
																assertAndThrow(!eventLog->eventTime.empty());
																assertAndThrow(!eventLog->eventType.empty());
																assertAndThrow(!eventLog->eventId.empty());
																assertAndThrow(eventLog->targetGroupId > 0);
																assertAndThrow(eventLog->creativeId > 0);
																assertAndThrow(eventLog->campaignId > 0);
																assertAndThrow(eventLog->advertiserId > 0);
																assertAndThrow(eventLog->clientId > 0);

																assertAndThrow(eventLog->winBidPrice == 0);
																assertAndThrow(eventLog->bidPrice > 0);
																// assertAndThrow(!eventLog->userTimeZone.empty());
																assertAndThrow(!eventLog->deviceUserAgent.empty());
																assertAndThrow(!eventLog->deviceIp.empty());
																assertAndThrow(eventLog->deviceLat != 0);
																assertAndThrow(eventLog->deviceLon != 0);
																assertAndThrow(!eventLog->deviceCountry.empty());
																assertAndThrow(!eventLog->deviceCity.empty());
																assertAndThrow(!eventLog->deviceState.empty());
																assertAndThrow(!eventLog->deviceZipcode.empty());
																assertAndThrow(!eventLog->deviceIpAddress.empty());
																assertAndThrow(!eventLog->siteCategory.empty());
																assertAndThrow(!eventLog->siteDomain.empty());
																assertAndThrow(!eventLog->sitePage.empty());
																assertAndThrow(!eventLog->sitePublisherDomain.empty());
																assertAndThrow(!eventLog->sitePublisherName.empty());
																assertAndThrow(!eventLog->adSize.empty());

																assertAndThrow(!eventLog->lastModuleRunInPipeline.empty());
																assertAndThrow(!eventLog->exchangeName.empty());
																assertAndThrow(!eventLog->impressionType.empty());

																assertAndThrow(!eventLog->mgrs1km.empty());
																assertAndThrow(!eventLog->mgrs100m.empty());
																assertAndThrow(!eventLog->mgrs10m.empty());

																assertAndThrow(!eventLog->appHostname.empty());
																assertAndThrow(!eventLog->bidderCallbackServletUrl.empty());
																assertAndThrow(!eventLog->appVersion.empty());
								}

								return eventLog;

}
bool EventLogCreatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
								throwEx("should not be used");
}

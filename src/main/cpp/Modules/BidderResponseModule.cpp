#include "Status.h"
#include "BidderModule.h"
#include "BidderResponseModule.h"
#include "JsonUtil.h"
#include "OpportunityContext.h"
#include "AeroCacheService.h"
#include "EventLog.h"
#include "LastTimeSeenSource.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"

BidderResponseModule::BidderResponseModule(LastTimeSeenSource* lastTimeSeenSource,
                                           int acceptableIntervalToBidOnAUserInSeconds,
                                           EntityToModuleStateStats* entityToModuleStateStats,
                                           std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        this->lastTimeSeenSource = lastTimeSeenSource;
        this->acceptableIntervalToBidOnAUserInSeconds = acceptableIntervalToBidOnAUserInSeconds;

}


std::string BidderResponseModule::getName() {
        return "BidderResponseModule";
}

void BidderResponseModule::process(std::shared_ptr<OpportunityContext> context) {

        //we increment the number of bids we did for this target group
        assertAndThrow (context->chosenTargetGroupId > 0);
        assertAndThrow (context->chosenCreativeId > 0);
        assertAndThrow (context->getChosenTargetGroup() != NULL);
        int chosenTargetGroupId = context->chosenTargetGroupId;
        assertAndThrow(chosenTargetGroupId != 0);
        tgFilterMeasuresService->updateBidMetrics(chosenTargetGroupId, context->bidPrice);
        tgBiddingPerformanceMetricInBiddingPeriodService->updateBidMetrics(chosenTargetGroupId, context->bidPrice);
        if (context->biddingOnlyForMatchingPixel == true) {
                //we are only bidding on this user to match our pixels with exchangeName
                entityToModuleStateStats->addStateModuleForEntity("BIDDING_FOR_MATCHING_PIXEL",
                                                                  "BidderResponseModule",
                                                                  "ALL");
                numberOfBidsForPixelMatchingPurposePerHour->increment();
                //we set the exchangeId as nomadiniDeviceId to store it in aerospike in order
                //not to bid too much for pixel matching on this user
                auto device = Device::buildAStandardDeviceIdBasedOn(context->exchangeUserId, context->deviceType);
                //we want to make sure the device is already unknown to us at this point
                assertAndThrow(context->device == nullptr);
                context->device = device;
        }

        lastTimeSeenSource->markAsSeenInLastXSeconds(context->device,
                                                     acceptableIntervalToBidOnAUserInSeconds);

        realTimeEventLogCacheService->putDataInCache(context->eventLog);

        verifyEventWasWrittenForTesting(context);
}

void BidderResponseModule::verifyEventWasWrittenForTesting(std::shared_ptr<OpportunityContext> context) {
        /*
           //this is just for testing if we have written stuff in cache or not
                auto dataInCache = realTimeEventLogCacheService->readDataOptional(context->eventLog);
                //TODO remove this after testing
                if (dataInCache == nullptr) {
                        throwEx("unable to read bid event data from cache");
                } else {
                        LOG_EVERY_N(ERROR, 1) << "read event data from cache : "<< dataInCache->toJson();
                }
         */
}

BidderResponseModule::~BidderResponseModule() {

}
bool BidderResponseModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

#ifndef KafkaProxyPopulator_h
#define KafkaProxyPopulator_h


#include "Status.h"

#include "KafkaProducer.h"
#include "Sampler.h"
#include "TransAppMessage.h"
namespace gicapods { class ConfigService; }
#include <memory>
#include <string>
class OpportunityContext;
#include "BidderModule.h"
#include "Poco/NotificationQueue.h"
#include "AtomicLong.h"
#include "BidderApplicationContext.h"
#include <tbb/concurrent_queue.h>

class PocoSession;
class KafkaProxyPopulator;

#include "Poco/Net/HTTPClientSession.h"
#include "BiddingMonitor.h"
#include "LoadPercentageBasedSampler.h"
#include "Object.h"

class KafkaProxyPopulator : public BiddingMonitor, public Object {
public:
std::string kafkaProxyServiceHostUrl;

std::shared_ptr<PocoSession> session;

KafkaProxyPopulator(EntityToModuleStateStats* entityToModuleStateStats,
                    std::string kafkaProxyServiceHostUrl,
                    gicapods::ConfigService* configService,
                    int failureThresholdPercentageArg);

void sendMessageToKafkaOverHttp(std::string msgToKafka);

virtual ~KafkaProxyPopulator();
};

#endif

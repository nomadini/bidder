#ifndef DeviceIdTargetGroupWithSegmentProvider_H
#define DeviceIdTargetGroupWithSegmentProvider_H



#include <memory>
#include <string>
#include <vector>
#include <set>
#include "TargetGroupTypeDefs.h"
class TargetGroup;
class OpportunityContext;
#include "BidderModule.h"
class TargetGroupSegmentCacheService;
class SegmentCacheService;
#include <tbb/concurrent_hash_map.h>
#include "RealTimeEntityCacheService.h"
#include "Object.h"
class EntityToModuleStateStats;
class DeviceIdSegmentModuleHelper;
class TargetGroupWithoutSegmentFilterModule;
class DeviceIdTargetGroupWithSegmentProvider;
class DeviceSegmentHistory;


class DeviceIdTargetGroupWithSegmentProvider : public Object {

public:
TargetGroupSegmentCacheService* targetGroupSegmentCacheService;
long ignoreSegmentsOlderThanXMilliSeconds;
TargetGroupWithoutSegmentFilterModule* targetGroupWithoutSegmentFilterModule;
//this service uses both aerospike and cassandra to read the segemnts as fast as it can
// it uses the aerospike as the caching layer
RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory;
SegmentCacheService* segmentCacheService;
DeviceIdSegmentModuleHelper* deviceIdSegmentModuleHelper;
EntityToModuleStateStats* entityToModuleStateStats;
DeviceIdTargetGroupWithSegmentProvider(
        TargetGroupSegmentCacheService* targetGroupSegmentCacheService,
        SegmentCacheService* segmentCacheService,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts,
        EntityToModuleStateStats* entityToModuleStateStats,
        DeviceIdSegmentModuleHelper* deviceIdSegmentModuleHelper,
        RealTimeEntityCacheService<DeviceSegmentHistory>* realTimeEntityCacheServiceDeviceSegmentHistory,
        long ignoreSegmentsOlderThanXMilliSeconds);

std::vector<std::shared_ptr<TargetGroup> > findAllTargetGroupIdsWithUserSegments(std::vector<std::string> segmentIds);

std::vector<std::shared_ptr<TargetGroup> > fetchTargetGroupsHavingSegment(std::string uniqueNameOfSegment, int segmentMysqlId);
tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> >  readSegmentsAndFetchAllTgsAttached(std::shared_ptr<OpportunityContext> context);

virtual ~DeviceIdTargetGroupWithSegmentProvider();

std::string getName();
};

#endif

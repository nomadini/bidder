/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef TargetGroupWithoutCreativeFilterModule_H_
#define TargetGroupWithoutCreativeFilterModule_H_
#include "Object.h"
#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
class TargetGroupCreativeCacheService;

class TargetGroupWithoutCreativeFilterModule : public BidderModule, public Object {

private:

public:


TargetGroupCreativeCacheService* targetGroupCreativeCacheService;

TargetGroupWithoutCreativeFilterModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
virtual std::string getName();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};




#endif /* TargetGroupWithoutCreativeFilterModule_H_ */

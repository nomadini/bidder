#ifndef AdServerStatusChecker_H
#define AdServerStatusChecker_H


#include "Status.h"
class EntityToModuleStateStats;
#include "Entity.h"
#include <memory>
#include <string>
#include "AtomicBoolean.h"
#include "BidderApplicationContext.h"
#include "Object.h"
#include "BiddingMode.h"
/**
 * this class is now responsible for making sure adserves are functioning
 * in order to bid
 *
 */
class AdServerStatusChecker : public Object {

private:

public:
std::shared_ptr<gicapods::AtomicBoolean> areAdserversHealthy;
std::shared_ptr<gicapods::AtomicBoolean> adServerStatusCheckerIsDisabled;

std::string allAdServerHostUrls;
EntityToModuleStateStats* entityToModuleStateStats;

AdServerStatusChecker(std::string allAdServerHostUrls);


void checkStatusOfOneAdServer(std::string adServerUrl,std::vector<std::string>& bustedAdServers);

void checkAdServerStatus();

void checkStatus();

virtual std::string getName();

};



#endif

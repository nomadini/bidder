#include "Status.h"
#include "TargetGroup.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "HttpUtil.h"
#include "EntityRealTimeDeliveryInfo.h"
#include "AppStatusRequest.h"
#include "Entity.h"
#include "AdServerStatusChecker.h"
#include "JsonUtil.h"
#include "ConfigService.h"
#include "TargetGroupCacheService.h"
#include "BidderApplicationContext.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "LogLevelManager.h"

AdServerStatusChecker::AdServerStatusChecker(
								std::string allAdServerHostUrls) :  Object(__FILE__) {
								this->allAdServerHostUrls = allAdServerHostUrls;
								areAdserversHealthy  = std::make_shared<gicapods::AtomicBoolean> ();
								adServerStatusCheckerIsDisabled  = std::make_shared<gicapods::AtomicBoolean> ();
}

void AdServerStatusChecker::checkAdServerStatus() {
								try {
																if (adServerStatusCheckerIsDisabled->getValue()) {
																								//if true... in case of debugging with valgrind
																								//we always return without checking ;
																								areAdserversHealthy->setValue(true);
																								return;
																}

																checkStatus ();
								} catch (...) {
																gicapods::Util::showStackTrace();
																areAdserversHealthy->setValue(false);
																entityToModuleStateStats->addStateModuleForEntity("EXCEPTION : NO_BID_BECAUSE_THREAD_FAILED", "AdServerStatusChecker", "ALL");

								}

}

void AdServerStatusChecker::checkStatus() {

								std::shared_ptr<AppStatusRequest> appStatusRequest  = std::make_shared<AppStatusRequest> ();

								std::string allAdServerUrlsProperty = allAdServerHostUrls;
								MLOG(2) << " allAdServerHostUrls : "<< allAdServerHostUrls;
								std::vector<std::string> allAdServerUrls = StringUtil::split (allAdServerUrlsProperty, ",");
								std::vector<std::string> bustedAdServers;
								for(std::string adServUrl :  allAdServerUrls) {
																std::string adServerUrl = adServUrl + StringUtil::toStr ("/AdServer/status");

																for (int numberOfAttempts = 0; numberOfAttempts < 3; numberOfAttempts++) {
																								try {
																																checkStatusOfOneAdServer(adServerUrl, bustedAdServers);
																																//if we suceeded in getting the adserver status we break
																																//out of the loop
																																break;
																								} catch(...) {
																																// in case of any http excetion we try for at most 3 times
																																if(numberOfAttempts >= 2) {
																																								LOG (ERROR) << "cannot connect to adserver : " << adServerUrl;
																																								bustedAdServers.push_back (adServerUrl);
																																								entityToModuleStateStats->addStateModuleForEntity
																																																("EXCEPTION : NO_BID_BECAUSE_THIS_ADSERVER_IS_BUSTED:" + adServerUrl, "AdServerStatusChecker", "ALL");

																																}
																																//sleep 2 seconds before trying again
																																gicapods::Util::sleepViaBoost(_L_, 2);
																								}
																}

								}

								if (bustedAdServers.empty ()) {

																areAdserversHealthy->setValue(true);
																entityToModuleStateStats->addStateModuleForEntity("DO_BID_BECAUSE_ALL_ADSERVERES_ARE_GOOD", "AdServerStatusChecker", "ALL");
								} else {
																//this else statement is very important to make sure if a adserver goes down, after
																//it has been up, we go to no bid mode
																areAdserversHealthy->setValue(false);
								}
}

void AdServerStatusChecker::checkStatusOfOneAdServer(std::string adServerUrl, std::vector<std::string>& bustedAdServers) {
								MLOG(3)<< "adServerUrl is "<< adServerUrl;
								//TODO : fix this
								std::string adServerResponsePayload = HttpUtil::sendGetRequest (
																adServerUrl,
																1000,
																2); //we try 2 times for 3 times each, then we go RED
								std::shared_ptr<AppStatusRequest> payload = AppStatusRequest::fromJson (adServerResponsePayload);
								MLOG(3)<<"adserver response " << payload->toJson();
								if(!StringUtil::equalsIgnoreCase(payload->status, "GREEN")) {
																//adserver is busted , we should go to no bid mode
																areAdserversHealthy->setValue(false);
																bustedAdServers.push_back (adServerUrl);
																entityToModuleStateStats->addStateModuleForEntity("ADSERVER_IS_RED", "AdServerStatusChecker", "ALL");
																LOG(ERROR)<< "ADSERVER IS RED..going to no bid mode";
								}
}

std::string AdServerStatusChecker::getName() {
								return "AdServerStatusChecker";
}

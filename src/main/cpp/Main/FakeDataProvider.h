#ifndef FakeDataProvider_H
#define FakeDataProvider_H

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
class BidderModule;
class DateTimeService;
class ModuleContainer;
#include "Object.h"
class BeanFactory;

class FakeDataProvider;

class FakeDataProvider : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
BeanFactory* beanFactory;
FakeDataProvider(
        EntityToModuleStateStats* entityToModuleStateStats,
        BeanFactory* beanFactory);

virtual ~FakeDataProvider();

void addFakeData();

};

#endif

#ifndef bidderAsyncJobsService_H_
#define bidderAsyncJobsService_H_

class GUtil;

#include "Poco/Net/HTTPServer.h"
#include <memory>
#include <string>
#include <boost/thread.hpp>
#include <thread>
#include "DateTimeMacro.h"
#include "TgMarker.h"
#include "AtomicDouble.h"
#include "EntityProviderService.h"
#include "Object.h"
class EntityDeliveryInfoFetcher;
class AdServerStatusChecker;
class EntityToModuleStateStats;
class BidderApplicationContext;
class MySqlTargetGroupBWListMapService;
class EntityToModuleStateStatsPersistenceService;
class BidModeControllerService;
class DataReloadService;

class DataReloadPipeline;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class BeanFactory;

class BidderMainPipelineProcessor;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class MySqlTgBiddingPerformanceMetricDtoService;
class MySqlTargetGroupFilterCountDbRecordService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
#include "TargetGroupFilterStatistic.h"

class FeatureDeviceHistory;
class BidderAsyncJobsService;
class EnqueueForScoringModule;
class RealTimeFeatureRegistryCacheUpdaterService;
class FeatureGuardService;
class BidderAsyncPipelineProcessor;
class CacheUpdateWatcher;
class FakeDataProvider;
class BidderLatencyRecorder;


namespace gicapods {
class AtomicLong;
class ConfigService;
template <typename K, typename V>
class ConcurrentHashMap;

template <typename K>
class ConcurrentHashSet;
}




class BidderAsyncJobsService : public std::enable_shared_from_this<BidderAsyncJobsService>, public Object {

public:

TimeType lastTimeDataWasReloadedInSeconds;
TimeType lastTimeAdServerStatusChecked;
TimeType lastTimeEveryMinuteThreadRan;
TimeType lastTimeEvery10SecondsThreadRan;
TimeType lastTimeEveryHourThreadRan;
std::shared_ptr<Poco::Net::HTTPServer> bidderServer;
int reloadingCacheInterval;
int maxAgeOfAcceptableDataInSeconds;
int adservStatusCheckIntervalInSecond;
std::string appName;

std::string propertyFileName;
std::shared_ptr<gicapods::AtomicBoolean> bidderIsStopped;
std::shared_ptr<gicapods::AtomicBoolean> asyncThreadIsStopped;
std::vector<CacheUpdateWatcher*> cacheUpdateWatcherModules;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
std::shared_ptr<std::unordered_map<std::string, TgMarker*> > allMarkersMap;
std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;
std::shared_ptr<FakeDataProvider> fakeDataProvider;
TgFilterMeasuresService* tgFilterMeasuresService;
BidModeControllerService* bidModeControllerService;
BidderLatencyRecorder* bidderLatencyRecorder;
FeatureGuardService* featureGuardService;
RealTimeFeatureRegistryCacheUpdaterService* realTimeFeatureRegistryCacheUpdaterService;
std::shared_ptr<TgBiddingPerformanceMetricInBiddingPeriodService> tgBiddingPerformanceMetricInBiddingPeriodService;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, FeatureDeviceHistory> > mapOfInterestingFeatures;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isMetricPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isDeliveryInfoSnapshotPersistenceInProgress;
std::shared_ptr<gicapods::AtomicLong> totalNumberOfRequestsProcessedInFilterCountPeriod;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingEvents;
std::shared_ptr<gicapods::AtomicLong> numberOfBidsForPixelMatchingPurposePerHour;

gicapods::ConfigService* configService;
EntityDeliveryInfoFetcher* entityDeliveryInfoFetcher;
BidderMainPipelineProcessor* bidderMainPipelineProcessor;
AdServerStatusChecker* adServerStatusChecker;
EntityToModuleStateStats* entityToModuleStateStats;
DataReloadService* dataReloadService;
EnqueueForScoringModule* enqueueForScoringModule;
BidderAsyncPipelineProcessor* bidderAsyncPipelineProcessor;
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetGroupsEligibleForBidding;

EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;
BeanFactory* beanFactory;

DataReloadPipeline* dataReloadPipeline;

MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService;
MySqlTargetGroupFilterCountDbRecordService* mySqlTargetGroupFilterCountDbRecordService;

std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedDevices;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedIps;
std::shared_ptr<gicapods::ConcurrentHashSet<std::string> > bannedMgrs10ms;

BidderAsyncJobsService();
void persistFilterCounts();

void runAllThreads();
/**
   runs all the modules that control the different types of cap, and different types of pacing
   every n minutes, right after reloading the entities
 */
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > runDataReloadPipeline(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > candidateTargetGroups);

virtual ~BidderAsyncJobsService();
void refillTargetGroupsEligibileForBidding(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetgroupsAvailableAfterPipeline);
void populateMetricsForMemory();
void startRealTimeManager();

void startAdServerStatusChecker();

void runEveryMinute();

void refreshFiles(std::string fileName);

void runEveryTenMinutes();

void runEvery10Seconds();
void updateProcessAliveFile();

void init();

void runEveryHour();

void reloadCaches();

void reloadBannedDevices(std::string fileName);
void reloadBannedIps(std::string fileName);
void reloadBannedMgrs10ms(std::string fileName);

std::string getName();

void startAsyncThreads();

private:
bool _helpRequested;
};

#endif

#ifndef BidModeControllerService_H
#define BidModeControllerService_H

#include "BiddingMode.h"
class EntityToModuleStateStats;
class AdServerStatusChecker;
class OpportunityContext;
#include "AtomicBoolean.h"
#include "AtomicLong.h"
#include "AtomicDouble.h"
#include <tbb/concurrent_queue.h>
#include "Object.h"
#include "ConfigService.h"
#include "BiddingMonitor.h"
class BidModeControllerService : public Object {


public:
// this service updates this field, which is read by NoBidModeEnforcerModule to decided whether to bid or not
std::shared_ptr<BiddingMode> globalBidMode;

EntityToModuleStateStats* entityToModuleStateStats;
AdServerStatusChecker* adServerStatusChecker;
gicapods::ConfigService* configService;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingEvents;
std::shared_ptr<gicapods::AtomicBoolean> isNoBidModeIsTurnedOn;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isReloadProcessHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isMetricPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isDeliveryInfoSnapshotPersistenceInProgress;
std::shared_ptr<gicapods::AtomicBoolean> isFetchingDeliveryInfoFromPacerHealthy;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInWritingMessagesInKafka;
std::shared_ptr<gicapods::AtomicLong> sizeOfKafkaScoringQueue;
std::shared_ptr<gicapods::AtomicLong> queueSizeWaitingForAck;
std::shared_ptr<gicapods::AtomicLong> numberOfCuncurrentReuqestsNow;
std::shared_ptr<gicapods::AtomicLong> numberOfContinuousCallInNoBidMode;

std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<OpportunityContext> > > queueOfContexts;

std::shared_ptr<std::vector<BiddingMonitor* > > allBiddingMonitors;
BidModeControllerService();

void init();
// determines the current bid mode and updates the globalBidMode
void determineBidMode();
void updateGlobalBidMode(std::shared_ptr<BiddingMode> currentBidMode);


void resetAllMonitors();
virtual ~BidModeControllerService();
void recordNoBidModeReasons(std::vector<std::string> reasons);
std::shared_ptr<BiddingMode> goToNoBidMode(std::vector<std::string> reasons);
};



#endif

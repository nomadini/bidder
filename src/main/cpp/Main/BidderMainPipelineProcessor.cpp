
#include "Status.h"
#include "RandomSampler.h"
#include "AtomicLong.h"
#include "StringUtil.h"
#include "TargetGroupGeoSegmentFilter.h"
#include "BidderMainPipelineProcessor.h"
#include "BidderModule.h"
#include "EntityToModuleStateStats.h"
#include "OpportunityContext.h"
#include "EventLog.h"
#include "TargetGroupFilterChainModule.h"
#include <memory>
#include <string>
#include "BidderModule.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"
#include "ModuleContainer.h"
#include "EntityToModuleStateStats.h"
#include "TgFilterMeasuresService.h"
#include "TgMarker.h"
#include "TargetGroup.h"
#include "BidderLatencyRecorder.h"

BidderMainPipelineProcessor::BidderMainPipelineProcessor()  : Object(__FILE__) {
        numberProcessorCalls = std::make_shared<gicapods::AtomicLong>();
        acceptableModuleLatencyInMillis = 2;
}

//this needs to be in its own pipeline in order to make RON targeting easier
void BidderMainPipelineProcessor::addTargetingModules() {

        //some of the preBidModules here belong to bidValidation pipeline

        addToPreBidModuleList ((BidderModule*)moduleContainer->gicapodsIdToExchangeIdMapModule.get());


        // makes sure we only bid on certain websites
        addToPreBidModuleList ((BidderModule*)moduleContainer->globalWhiteListModule.get());

        addToPreBidModuleList ((BidderModule*)moduleContainer->deviceIdSegmentModule.get());

        addToPreBidModuleList ((BidderModule*)moduleContainer->targetGroupFilterChainModule.get());

        addToPreBidModuleList ((BidderModule*)moduleContainer->targetGroupFrequencyCapModule.get());
        addToPreBidModuleList ((BidderModule*)moduleContainer->targetGroupGeoSegmentFilter.get());
        addToPreBidModuleList ((BidderModule*)moduleContainer->recencyVisitScoreFilter.get());

}

void BidderMainPipelineProcessor::setTheModuleList() {

        addToPreBidModuleList((BidderModule*) moduleContainer->mgrsBuilderModule.get());

        // this checks if user has been seen in last n seconds, we don't bid on it
        addToPreBidModuleList ((BidderModule*) moduleContainer->lastXSecondSeenVisitorModule.get());
        addToPreBidModuleList ((BidderModule*) moduleContainer->lastXSecondSeenIpVisitorModule.get());
        addToPreBidModuleList ((BidderModule*)moduleContainer->badIpByCountFilterModule.get());
        addToPreBidModuleList ((BidderModule*)moduleContainer->badDeviceByCountFilterModule.get());


        addTargetingModules();

        addToPreBidModuleList ((BidderModule*) moduleContainer->targetGroupBudgetFilterChainModule.get());

        addToPreBidModuleList ((BidderModule*) moduleContainer->bidPriceCalculatorModule.get());


        addToPreBidModuleList ((BidderModule*) moduleContainer->targetGroupPerMinuteBidCapFilter.get());

        //this has to run before targetGroupSelectorModule always
        addToPreBidModuleList ((BidderModule*) moduleContainer->targetingTypeLessTargetGroupFilter.get());
        addToPreBidModuleList ((BidderModule*) moduleContainer->targetGroupSelectorModule.get());

        /*
           Post Bid Modules, these modules run only if we are bidding
         */
        addToPostBidModuleList((BidderModule*) moduleContainer->eventLogCreatorModule.get());
        addToPostBidModuleList((BidderModule*) moduleContainer->bidderResponseModule.get());

        //async pipeline mighe need some data like places that we might get in the GeoFeatureFilter
        //so we run it as a finalStageModule, or some modules like BidEventRecorderModule that
        //need to record a 'bid' event sometimes
        addToFinalStageModuleList((BidderModule*) moduleContainer->asyncPipelineFeederModule.get());
}

void BidderMainPipelineProcessor::addToPreBidModuleList(BidderModule* module) {
        NULL_CHECK(module);
        std::string moduleName = module->getName();
        std::string propertyValue = configService->get(moduleName + "IsEnabled");
        if (StringUtil::equalsIgnoreCase(propertyValue, "true")) {
                this->preBidModules.push_back (module);
        } else if (StringUtil::equalsIgnoreCase(propertyValue, "false")) {
                logExclusion(moduleName, entityToModuleStateStats);
        } else {
                LOG(ERROR) << "property is moduleName : "
                           <<moduleName<<", wrong : "<< propertyValue;
                EXIT("");
        }
}

void BidderMainPipelineProcessor::logExclusion(std::string moduleName,
                                               EntityToModuleStateStats* entityToModuleStateStats) {
        LOG(ERROR) << "excluding the "<< moduleName << " module";
        //this is a very important change, we need to add it to the exceptions
        //so we know whats functionality is being excluded
        entityToModuleStateStats->addStateModuleForEntity(
                "excluding_" + moduleName,
                "BidderMainPipelineProcessor",
                EntityToModuleStateStats::all,
                EntityToModuleStateStats::exception
                );
}

void BidderMainPipelineProcessor::addToPostBidModuleList(BidderModule* module) {
        NULL_CHECK(module);
        std::string moduleName = module->getName();
        std::string propertyValue = configService->get(moduleName + "IsEnabled");
        if (StringUtil::equalsIgnoreCase(propertyValue, "true")) {
                this->postBidModules.push_back (module);
        } else {
                logExclusion(moduleName, entityToModuleStateStats);
        }
}


void BidderMainPipelineProcessor::addToFinalStageModuleList(BidderModule* module) {
        NULL_CHECK(module);
        std::string moduleName = module->getName();
        std::string propertyValue = configService->get(moduleName + "IsEnabled");
        if (StringUtil::equalsIgnoreCase(propertyValue, "true")) {
                this->finalStageModules.push_back (module);
        } else {
                logExclusion(moduleName, entityToModuleStateStats);
        }
}


void BidderMainPipelineProcessor::process(std::shared_ptr<OpportunityContext> context) {
        numberProcessorCalls->increment();
        context->acceptableModuleLatencyInMillis = acceptableModuleLatencyInMillis;
        entityToModuleStateStats->addStateModuleForEntity("#OfProcessingBidRequests",
                                                          "BidderMainPipelineProcessor",
                                                          "ALL");

        if (context->getAllAvailableTgs()->empty()) {
                throw std::logic_error("no targetgroup available to process");
        }


        //this is very important to know which targetgroups are being processed in the
        //main pipeline
        for (auto tg : *context->getAllAvailableTgs()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        tg->idAsString,
                        "BidderMainPipelineProcessor-tg-for-considerations",
                        "ALL");

        }


        for (auto module : preBidModules) {
                try {

                        if (context->status->value == STOP_PROCESSING) {
                                //sometimes when building context...
                                //we dont get ip info or we dont get user timeZoneValue
                                // we dont want to process anything
                                //so we set status value STOP_PROCESSING in the beginning
                                break;
                        }
                        auto beforeTgSize = context->getSizeOfUnmarkedTargetGroups();
                        if (beforeTgSize > 0) {
                                runModule(module, context, bidderLatencyRecorder);
                                recordFilteringPercentage(context, module, beforeTgSize);
                        }

                        if (context->status->value == STOP_PROCESSING ||
                            context->getSizeOfUnmarkedTargetGroups() == 0) {
                                context->lastModuleRunInPipeline = module->getName ();
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "LastModule-" + module->getName (), "BidderMainPipelineProcessor", "ALL");
                                break;
                        }

                } catch(std::exception& e) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_WHILE_RUNNING_MODULE:" + _toStr(e.what()),
                                "BidderMainPipelineProcessor",
                                "ALL",
                                EntityToModuleStateStats::exception);
                        //we stop running other modules in case of an exception
                        EXIT("EXCEPTION_WHILE_RUNNING_MODULE:" + _toStr(e.what()));
                }
        }


        entityToModuleStateStats->addStateModuleForEntity("processResult:'" + context->bidDecisionResult +"'",
                                                          "BidderMainPipelineProcessor",
                                                          "ALL");

        if (StringUtil::equalsIgnoreCase(EventLog::EVENT_TYPE_BID, context->bidDecisionResult)) {
                entityToModuleStateStats->addStateModuleForEntity("runPostBidModules",
                                                                  "BidderMainPipelineProcessor",
                                                                  "ALL");
                runPostBidModules(context);
        }

        runFinalStageModules(context);
}

void BidderMainPipelineProcessor::recordFilteringPercentage(
        std::shared_ptr<OpportunityContext> context,
        BidderModule* module,
        int beforeTgSize) {
        assertAndThrow(beforeTgSize > 0);
        auto afterTgSize = context->getSizeOfUnmarkedTargetGroups();

        int filterPercentage = 0;
        if (beforeTgSize != 0) {
                filterPercentage  = (beforeTgSize - afterTgSize) * 100 / beforeTgSize;
        }
        entityToModuleStateStats->addStateModuleForEntityWithValue(module->getName(),
                                                                   filterPercentage,
                                                                   "BidderMainPipelineProcessor-PercentageOfFilterCalculator",
                                                                   "ALL");

        LOG_EVERY_N(INFO, 10000)<< "percenteOfFilteringTg for module "<< module->getName()<< " is "<< filterPercentage;

}

/**
   we only run the modules in postBidModules when we are bidding on a request
 */
void BidderMainPipelineProcessor::runPostBidModules(std::shared_ptr<OpportunityContext> context) {
        runGenericModules(postBidModules, context);
}

void BidderMainPipelineProcessor::runFinalStageModules(std::shared_ptr<OpportunityContext> context) {
        runGenericModules(finalStageModules, context);
}

void BidderMainPipelineProcessor::runModule(
        BidderModule* module, std::shared_ptr<OpportunityContext> context,
        BidderLatencyRecorder* bidderLatencyRecorder) {
        Poco::Timestamp moduleStartTime;
        module->beforeProcessing(context);
        module->process (context);
        module->afterProcessing(context);

        BidderLatencyRecorder::logTimeTaken(
                module->getName(), moduleStartTime, 10000, 4);

        bidderLatencyRecorder->recordLatency(moduleStartTime, 100, module->getName());
}

void BidderMainPipelineProcessor::runGenericModules(
        std::vector<BidderModule*>& modules,
        std::shared_ptr<OpportunityContext> context) {

        for (std::vector<BidderModule*>::iterator it = modules.begin ();
             it != modules.end (); ++it) {
                runModule(*it, context, bidderLatencyRecorder);
        }
}

std::string BidderMainPipelineProcessor::getName() {
        return "BidderMainPipelineProcessor";
}

BidderMainPipelineProcessor::~BidderMainPipelineProcessor() {

}

#include "Status.h"
#include "BidderAsyncPipelineProcessor.h"
#include "BidderModule.h"
#include "ModuleContainer.h"
#include "AtomicLong.h"
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
#include "BidderModule.h"
#include "OpportunityContext.h"
#include "LoadPercentageBasedSampler.h"
#include "ExceptionUtil.h"
#include "BidderMainPipelineProcessor.h"
#include "FeatureRecorderPipeline.h"
#include "BidderLatencyRecorder.h"
#include <thread>
BidderAsyncPipelineProcessor::BidderAsyncPipelineProcessor() : Object(__FILE__) {
        numberProcessorCalls = std::make_shared<gicapods::AtomicLong>();

}

void BidderAsyncPipelineProcessor::setTheModuleList() {
        //modulesList will run for every request
        NULL_CHECK(moduleContainer->enqueueForScoringModule.get());

        this->modulesList.push_back ((BidderModule*)moduleContainer->enqueueForScoringModule.get());
        NULL_CHECK(moduleContainer->badIpByCountPopulatorModule.get());

        this->modulesList.push_back ((BidderModule*)moduleContainer->bannedDevicesFilterModule.get());
        NULL_CHECK(moduleContainer->bannedDevicesFilterModule.get());

        this->modulesList.push_back ((BidderModule*)moduleContainer->badIpByCountPopulatorModule.get());
        NULL_CHECK(moduleContainer->badDeviceByCountPopulatorModule.get());
        this->modulesList.push_back ((BidderModule*)moduleContainer->badDeviceByCountPopulatorModule.get());

        // we don't to save too much data , because of that, we run
        // these modules based on a sampler
        NULL_CHECK(moduleContainer->bidEventRecorderModule.get());
        this->sampledModulesList.push_back((BidderModule*)moduleContainer->bidEventRecorderModule.get());

        NULL_CHECK(moduleContainer->crossWalkUpdaterModuleWrapper.get());
        this->sampledModulesList.push_back((BidderModule*)moduleContainer->crossWalkUpdaterModuleWrapper.get());

        //we update the recenct visit data in the sampledModulesList
        NULL_CHECK(moduleContainer->recentVisitHistoryUpdater.get());
        this->sampledModulesList.push_back((BidderModule*)moduleContainer->recentVisitHistoryUpdater.get());
        //we score devices based on sampled list too
        NULL_CHECK(moduleContainer->recentVisitHistoryScorer.get());
        this->sampledModulesList.push_back((BidderModule*)moduleContainer->recentVisitHistoryScorer.get());

        //this pipeline is responsible for adding data to our systems
        featureRecorderPipeline = moduleContainer->featureRecorderPipeline.get();

        NULL_CHECK(featureRecorderPipeline);
}


void BidderAsyncPipelineProcessor::processManyMessages() {
        int numerOfReadsInOneGo = 50000;
        for (int i=0; i < numerOfReadsInOneGo; i++) {
                readOneContextOffQueue();
        }

}
void BidderAsyncPipelineProcessor::readOneContextOffQueue() {

        try {
                std::shared_ptr<OpportunityContext> context;
                if(queueOfContexts->try_pop(context)) {
                        //we popped something off the queue
                        entityToModuleStateStats->addStateModuleForEntity("process_msg",
                                                                          "BidderAsyncPipelineProcessor",
                                                                          "ALL");

                        //we set this to true, to run async specific logics in some of the modules
                        context->isAsyncPiplineExecuting = true;
                        this->process(context);
                }



        } catch (std::exception const &e) {
                ExceptionUtil::logException(e, entityToModuleStateStats);
        } catch(...) {
                LOG_EVERY_N(ERROR, 100)<< google::COUNTER<<"th error in BidderAsyncPipelineProcessor";
                entityToModuleStateStats->addStateModuleForEntity("ERROR_IN_PIPELINE",
                                                                  "BidderAsyncPipelineProcessor",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);

                gicapods::Util::showStackTrace();
        }
}

void BidderAsyncPipelineProcessor::process(std::shared_ptr<OpportunityContext> opt) {
        NULL_CHECK(opt);
        numberProcessorCalls->increment();
        entityToModuleStateStats->addStateModuleForEntity
                ("#OfProcessingBidRequests",
                "BidderAsyncPipelineProcessor",
                "ALL");

        runModules(opt, modulesList);
        //feature modules have a sampler in them...so we don't run
        //the feature recorder Pipeline within the sampler
        featureRecorderPipeline->process(opt);

        //we run the modules that are sampled
        if (sampledModuleSampler->isPartOfSample()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "part_of_sample",
                        "BidderAsyncPipelineProcessor",
                        "ALL");
                runModules(opt, sampledModulesList);
        } else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "not_part_of_sample",
                        "BidderAsyncPipelineProcessor",
                        "ALL");
        }


}

void BidderAsyncPipelineProcessor::runModules(std::shared_ptr<OpportunityContext> opt,
                                              std::vector<BidderModule*>& modules) {
        for (std::vector<BidderModule*>::iterator it = modules.begin ();
             it != modules.end (); ++it) {
                NULL_CHECK(*it);
                BidderMainPipelineProcessor::runModule(*it, opt, bidderLatencyRecorder);
        }
}

std::string BidderAsyncPipelineProcessor::getName() {
        return "BidderAsyncPipelineProcessor";
}

BidderAsyncPipelineProcessor::~BidderAsyncPipelineProcessor() {

}

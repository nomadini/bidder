

#include "BidModeControllerService.h"
#include "StringUtil.h"
#include "GUtil.h"
#include "EntityToModuleStateStats.h"
#include "ImportantStats.h"
#include "CollectionUtil.h"
#include "EventLog.h"
#include "JsonArrayUtil.h"

#include "AdServerStatusChecker.h"
BidModeControllerService::BidModeControllerService() : Object(__FILE__) {

}

void BidModeControllerService::init() {
        numberOfContinuousCallInNoBidMode = std::make_shared<gicapods::AtomicLong>();
        allBiddingMonitors = std::make_shared<std::vector<BiddingMonitor* > > ();
        globalBidMode = std::make_shared<BiddingMode> (entityToModuleStateStats);
}

void BidModeControllerService::determineBidMode() {

        auto bidMode =  std::make_shared<BiddingMode>(entityToModuleStateStats);
        std::vector<std::string> reasons;
        if(!adServerStatusChecker->areAdserversHealthy->getValue())
        {
                std::string reason = "AdServerStatusCheckerIsBusted";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService", reason);
        }

        for (auto monitor : *allBiddingMonitors) {
                if (monitor->isFailing()) {
                        std::string reason = monitor->getName() + "IsBusted";
                        reasons.push_back(reason);
                        bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService", reason);
                }
        }

        if(!isReloadProcessHealthy->getValue()) {
                std::string reason ="dataReloadServiceIsBusted";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",  reason);
        }

        if (!isFetchingDeliveryInfoFromPacerHealthy->getValue()) {
                std::string reason ="isFetchingDeliveryInfoFromPacerHealthyIsFalse";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason);
        }

        if (isMetricPersistenceInProgress->getValue()) {
                std::string reason =  "bidderIsPersistingMetrics";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason );
        }

        if (isDeliveryInfoSnapshotPersistenceInProgress->getValue()) {
                std::string reason = "bidderIsPersistingDeliveryInfoSnapshots";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService", reason);
        }

        if (isNoBidModeIsTurnedOn->getValue()) {
                std::string reason = "bidderFactoryIsInNoBidMode";
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason);
        }
        if (numberOfExceptionsInWritingEvents->getValue() > 10 ) {
                std::string reason = "eventLogHavingProblem:" + StringUtil::toStr(numberOfExceptionsInWritingEvents->getValue());
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason);

        }

        if (sizeOfKafkaScoringQueue->getValue() > configService->getAsInt("limitOfScoringQueueInBidder")) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "sizeOfKafkaScoringQueue_too_large",
                        "BidModeControllerService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);

                std::string reason = "EnqueueForScoringModule_sizeOfKafkaScoringQueue:" + StringUtil::toStr(sizeOfKafkaScoringQueue->getValue());
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason);
        }

        if (queueOfContexts->unsafe_size() > 10000 ) {
                LOG_EVERY_N(ERROR, 100)<< google::COUNTER<< "th warning : size_async_job_queue_is_too_large : "<< queueOfContexts->unsafe_size();
        }
        if (queueOfContexts->unsafe_size() > configService->getAsInt("limitOfAsyncPipelineQueueInBidder") ) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "size_async_job_queue_is_too_large",
                        "BidModeControllerService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                // queueOfContexts->unsafe_size()
                std::string reason = "AsyncPipelineIsSlow queueSize :" + StringUtil::toStr(queueOfContexts->unsafe_size());
                reasons.push_back(reason);
                bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService", reason);
        }


        if (numberOfCuncurrentReuqestsNow->getValue() > 10) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "tooMuchLoadOnBidder",
                        "BidModeControllerService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                LOG_EVERY_N(ERROR, 1000) <<" numberOfCuncurrentReuqestsNow : "
                                         << numberOfCuncurrentReuqestsNow->getValue() << std::endl;
        }

        if (queueSizeWaitingForAck->getValue() > 1000) {
                //         std::string reason = "EnqueueForScoringModule_queueSizeWaitingForAck:" + StringUtil::toStr(queueSizeWaitingForAck->getValue());
                //         reasons.push_back(reason);
                //         bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService",reason);
                LOG_EVERY_N(ERROR, 1000) << "kafka queueSizeWaitingForAck : " << queueSizeWaitingForAck->getValue();
        }

        //set to DO_BID, if none of NO_BID conditions are true
        if (!StringUtil::equalsIgnoreCase(bidMode->getStatus(), EventLog::EVENT_TYPE_NO_BID)) {
                bidMode->setStatus("DO_BID",
                                   "BidModeControllerService",
                                   "ALL_IN_GOOD_CONDITIONS_BEFORE_EVALUATION");
                entityToModuleStateStats->addStateModuleForEntity(
                        "DO_BID",
                        "BidModeControllerService",
                        "ALL"
                        );
                numberOfContinuousCallInNoBidMode->setValue(0);
        } else {
                bidMode = goToNoBidMode(reasons);
        }


        updateGlobalBidMode(bidMode);

}

void BidModeControllerService::updateGlobalBidMode(std::shared_ptr<BiddingMode> currentBidMode) {
        if (StringUtil::equalsIgnoreCase(currentBidMode->getStatus(), EventLog::EVENT_TYPE_NO_BID)) {
                globalBidMode->bidMode->setValue(false);
        } else {
                globalBidMode->bidMode->setValue(true);
        }

        globalBidMode->bidModeReasons = currentBidMode->bidModeReasons;
}

std::shared_ptr<BiddingMode> BidModeControllerService::goToNoBidMode(
        std::vector<std::string> reasons) {
        auto bidMode =  std::make_shared<BiddingMode>(entityToModuleStateStats);

        std::string allReasons = JsonArrayUtil::convertListToJson(reasons);
        //remove the illegal characters for saving metric
        allReasons = StringUtil::replaceString(allReasons, "\"", "_");
        allReasons = StringUtil::replaceString(allReasons, ",", "_");
        numberOfContinuousCallInNoBidMode->increment();
        if (numberOfContinuousCallInNoBidMode->getValue() > 10000) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "ManyContinuousCallsInNoBidMode",
                        "BidModeControllerService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
        }

        //record no bid mode reasons
        recordNoBidModeReasons(reasons);

        bidMode->setStatus(EventLog::EVENT_TYPE_NO_BID, "BidModeControllerService", allReasons);



        LOG_EVERY_N(ERROR, 100) << "bidder is NO_BID_MODE, reasons : " << allReasons;

        return bidMode;
}

void BidModeControllerService::recordNoBidModeReasons(std::vector<std::string> reasons) {
        for (auto reason : reasons) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "NO_BID_FOR_" + reason,
                        "BidModeControllerService",
                        "ALL"
                        );
        }
}

void BidModeControllerService::resetAllMonitors() {
        MLOG(1)<<"resetting all monitors";
        for (auto monitor : *allBiddingMonitors) {
                monitor->resetMeasures();
        }

}

BidModeControllerService::~BidModeControllerService() {

}

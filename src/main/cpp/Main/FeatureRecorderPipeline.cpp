
#include "FeatureRecorderPipeline.h"
#include "BeanFactory.h"
#include "TargetGroupFilterStatistic.h"
#include "OpportunityContext.h"
#include "BidderModule.h"
#include "ModuleContainer.h"
#include "DateTimeService.h"

FeatureRecorderPipeline::FeatureRecorderPipeline(
        EntityToModuleStateStats* entityToModuleStateStats,
        ModuleContainer* moduleContainer) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->moduleContainer = moduleContainer;

}

FeatureRecorderPipeline::~FeatureRecorderPipeline() {
}

void FeatureRecorderPipeline::setTheModuleList() {

        //saves domains' device history
        NULL_CHECK((BidderModule*) moduleContainer->deviceHistoryOfVisitFeatureUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->deviceHistoryOfVisitFeatureUpdaterModuleWrapper.get());

        //saves geoFeatures' device history
        NULL_CHECK((BidderModule*) moduleContainer->deviceHistoryOfGeoFeatureUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->deviceHistoryOfGeoFeatureUpdaterModuleWrapper.get());

        //saves the device's domain history
        NULL_CHECK((BidderModule*) moduleContainer->visitFeatureHistoryOfDeviceUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->visitFeatureHistoryOfDeviceUpdaterModuleWrapper.get());

        //saves the device's geo history
        NULL_CHECK((BidderModule*) moduleContainer->geoFeatureHistoryOfDeviceUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->geoFeatureHistoryOfDeviceUpdaterModuleWrapper.get());

        //saves the device's placeTag history...we only save Tag not place
        NULL_CHECK((BidderModule*) moduleContainer->placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->placeTagFeatureHistoryOfDeviceUpdaterModuleWrapper.get());

        //saves placeTags' device history
        NULL_CHECK((BidderModule*) moduleContainer->deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->deviceHistoryOfPlaceTagFeatureUpdaterModuleWrapper.get());

        //saves domain features' feature history
        NULL_CHECK((BidderModule*) moduleContainer->domainFeatureToFeatureHistoryUpdaterModuleWrapper.get());
        this->modules.push_back((BidderModule*) moduleContainer->domainFeatureToFeatureHistoryUpdaterModuleWrapper.get());

        //saves the device's domain history only last 30 minutes in aerospike
        NULL_CHECK((BidderModule*) moduleContainer->recentVisitHistoryUpdater.get());
        this->modules.push_back((BidderModule*) moduleContainer->recentVisitHistoryUpdater.get());

}

void FeatureRecorderPipeline::process(std::shared_ptr<OpportunityContext> context) {
        NULL_CHECK(context);
        for (auto module : modules) {
                NULL_CHECK(module);
                module->process (context);
        }
}

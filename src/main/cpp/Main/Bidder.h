#ifndef GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_H_
#define GICAPODS_GICAPODSSERVER_SRC_BIDDER_BIDDER_H_


#include "BiddingMode.h"
#include <memory>
#include <string>
#include "BidderRequestHandlerFactory.h"
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include "Object.h"
#include "AtomicBoolean.h"
class BidderApplicationContext;
class EntityToModuleStateStatsPersistenceService;
class DataReloadService;
namespace gicapods { class ConfigService; }

#include "Poco/Net/HTTPServer.h"
#include <boost/thread.hpp>
#include <thread>
class BidderRequestHandlerFactory;
class BidderAsyncJobsService;
class BidConfirmedWinsContainerPerTargetGroupAndBidder;
class ComparatorService;
class TgFilterMeasuresService;
class BeanFactory;
class ModuleContainer;
class Bidder;



class Bidder : public std::enable_shared_from_this<Bidder>
        , public Poco::Util::ServerApplication
{

public:

gicapods::ConfigService* configService;
TgFilterMeasuresService* tgFilterMeasuresService;

std::shared_ptr<Poco::Net::HTTPServer> bidderServer;
std::shared_ptr<Poco::Net::HTTPServer> metaServer;
std::shared_ptr<gicapods::AtomicBoolean> bidderIsStopped;

std::shared_ptr<gicapods::AtomicBoolean> asyncThreadIsStopped;
std::shared_ptr<gicapods::AtomicBoolean> stopConsumingThread;
std::shared_ptr<gicapods::AtomicBoolean> dequeueThreadIsRunning;

BidderRequestHandlerFactory* bidderRequestHandlerFactory;

EntityToModuleStateStats* entityToModuleStateStats;

DataReloadService* dataReloadService;

std::string propertyFileName;

BidderAsyncJobsService* bidderAsyncJobsService;
Bidder();
virtual ~Bidder();
void initFactory();
std::string getName();

int main(const std::vector<std::string> &args);

private:
bool _helpRequested;
};

#endif

#ifndef FeatureRecorderPipeline_H
#define FeatureRecorderPipeline_H

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
class BidderModule;
class DateTimeService;
class ModuleContainer;
#include "Object.h"
class BeanFactory;

class FeatureRecorderPipeline;


/*
   This pipeline is a series of modules that record the feature data in mysql
   and cassandra for modeling

   This pipeline is run as part of async pipeline sampled list
 */
class FeatureRecorderPipeline : public Object {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
ModuleContainer* moduleContainer;
FeatureRecorderPipeline(
        EntityToModuleStateStats* entityToModuleStateStats,
        ModuleContainer* moduleContainer);

virtual ~FeatureRecorderPipeline();

std::vector<BidderModule*> modules;

void setTheModuleList();

void addToModuleList(BidderModule* module);

void process(std::shared_ptr<OpportunityContext> context);

};

#endif


#include "FakeDataProvider.h"
#include "BeanFactory.h"
#include "TargetGroupFilterStatistic.h"
#include "OpportunityContext.h"
#include "BidderModule.h"
#include "ModuleContainer.h"
#include "DateTimeService.h"

FakeDataProvider::FakeDataProvider(
        EntityToModuleStateStats* entityToModuleStateStats,
        BeanFactory* beanFactory) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->beanFactory = beanFactory;

}

FakeDataProvider::~FakeDataProvider() {
}

void FakeDataProvider::addFakeData() {

}

#include "GUtil.h"
#include "BeanFactory.h"
#include "BidderApplicationContext.h"

#include "EntityDeliveryInfoFetcher.h"
#include "BiddingMode.h"

#include "SignalHandler.h"
#include "Poco/Util/HelpFormatter.h"
#include "Bidder.h"
#include "Poco/Net/ServerSocket.h"
#include "ConverterUtil.h"
#include <boost/exception/all.hpp>
#include "EntityToModuleStateStats.h"
#include "Poco/ThreadPool.h"
#include "DateTimeUtil.h"
#include "FileUtil.h"
#include "ConfigService.h"
#include "BidderRequestHandlerFactory.h"
#include "BidderAsyncJobsService.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "BidderAsyncJobsService.h"
#include "BidderApplicationContext.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "DataReloadService.h"
#include "ModuleContainer.h"
#include "ThrottlerModule.h"
#include "PocoHttpServer.h"


#include <new>
#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>
#include <sstream>


Bidder::Bidder() {

}

Bidder::~Bidder() {
}

std::string Bidder::getName() {
        return "Bidder";
}

int Bidder::main(const std::vector<std::string> &args) {

        // start the HTTPServer
        bidderServer->start ();
        metaServer->start ();
        // wait for CTRL-C or kill

        waitForTerminationRequest ();
        // Stop the HTTPServer
        bidderServer->stop ();
        bidderServer->stop ();
        bidderIsStopped->setValue(true);
        stopConsumingThread->setValue(true);
        LOG(INFO)<<"server is shutting down......";
        //sleeping for 3 seconds for threads to stop
        gicapods::Util::sleepViaBoost(_L_, 2);
        while(asyncThreadIsStopped->getValue() == false) {
                LOG_EVERY_N(INFO, 1)<<google::COUNTER<<"th waiting for asyncThread to stop.";
                gicapods::Util::sleepViaBoost(_L_, 1);
        }
        while(dequeueThreadIsRunning->getValue() == true) {
                LOG_EVERY_N(INFO, 1)<<google::COUNTER<<"th waiting for dequeueThreadIsRunning to stop.";
                gicapods::Util::sleepViaBoost(_L_, 1);
        }

        LOG(INFO)<<"exiting program.";
        return Application::EXIT_OK;
}

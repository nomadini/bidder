#ifndef BidderMainPipelineProcessor_H
#define BidderMainPipelineProcessor_H

class OpportunityContext;
class TargetGroupFilterChainModule;
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "Object.h"
#include "BidderModule.h"

class EntityToModuleStateStats;

class ModuleContainer;
class EntityToModuleStateStats;
class DateTimeService;
class GlobalWhiteListedCacheService;
class BidderLatencyRecorder;
class TgFilterMeasuresService;

namespace gicapods { class ConfigService; }

class BidderMainPipelineProcessor;



class BidderMainPipelineProcessor : public Object {
public:
int acceptableModuleLatencyInMillis;
TargetGroupFilterChainModule* targetGroupFilterChainModule;
DateTimeService* dateTimeService;
BidderLatencyRecorder* bidderLatencyRecorder;
gicapods::ConfigService* configService;
GlobalWhiteListedCacheService* globalWhiteListedCacheService;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
TgFilterMeasuresService* tgFilterMeasuresService;

std::vector<BidderModule*> preBidModules;

//the modules that will run only after we make a bid
std::vector<BidderModule*> postBidModules;

//the modules that will run at the final stage
std::vector<BidderModule*> finalStageModules;
std::shared_ptr<gicapods::AtomicLong> numberProcessorCalls;
ModuleContainer* moduleContainer;
EntityToModuleStateStats* entityToModuleStateStats;
BidderMainPipelineProcessor();

void addTargetingModules();
void setTheModuleList();

void addToPreBidModuleList(BidderModule* module);
void addToPostBidModuleList(BidderModule* module);
void addToFinalStageModuleList(BidderModule* module);
void recordFilteringPercentage(std::shared_ptr<OpportunityContext> context,
                               BidderModule* module,
                               int beforeTgSize);

static void logExclusion(std::string moduleName,
                         EntityToModuleStateStats* entityToModuleStateStats);
void runPostBidModules(std::shared_ptr<OpportunityContext> context);
void runFinalStageModules(std::shared_ptr<OpportunityContext> context);
void runGenericModules(
        std::vector<BidderModule*>& modules,
        std::shared_ptr<OpportunityContext> context);

static void runModule(BidderModule* module,
                      std::shared_ptr<OpportunityContext> context,
                      BidderLatencyRecorder* bidderLatencyRecorder);

virtual void process(std::shared_ptr<OpportunityContext> context);

std::string getName();

virtual ~BidderMainPipelineProcessor();
};

#endif

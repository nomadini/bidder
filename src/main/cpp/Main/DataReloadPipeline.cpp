
#include "DataReloadPipeline.h"
#include "CampaignBudgetCappingFilterModule.h"
#include "CampaignImpressionCappingFilterModule.h"
#include "TargetGroupPacingByBudgetModule.h"
#include "TargetGroupPacingByImpressionModule.h"
#include "TargetGroupImpressionCappingFilterModule.h"
#include "TargetGroupBudgetCappingFilterModule.h"
#include "ActiveFilterModule.h"
#include "TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule.h"
#include "TargetGroupWithoutSegmentFilterModule.h"
#include "BeanFactory.h"
#include "CollectionUtil.h"
#include "TargetGroupFilterStatistic.h"
#include "OpportunityContext.h"
#include "BidderModule.h"
#include "TargetGroup.h"
#include "DateTimeService.h"
#include "TargetGroupPerMinuteBidCapFilter.h"
#include "TargetGroupWithoutCreativeFilterModule.h"
#include "CollectionUtil.h"
DataReloadPipeline::DataReloadPipeline(EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;

}

DataReloadPipeline::~DataReloadPipeline() {

}

void DataReloadPipeline::setTheModuleList() {

        NULL_CHECK(filterNameToFailureCounts);

        targetGroupPacingByBudgetModule = std::make_unique<TargetGroupPacingByBudgetModule>
                                                  (beanFactory->entityDeliveryInfoCacheService.get(),
                                                  dateTimeService,
                                                  entityToModuleStateStats,
                                                  filterNameToFailureCounts);
        targetGroupPacingByBudgetModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupPacingByBudgetModule->getName(), (TgMarker*) targetGroupPacingByBudgetModule.get()));

        targetGroupPacingByImpressionModule =
                std::make_unique<TargetGroupPacingByImpressionModule> (
                        beanFactory->entityDeliveryInfoCacheService.get(),
                        dateTimeService,
                        entityToModuleStateStats,
                        filterNameToFailureCounts);
        targetGroupPacingByImpressionModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupPacingByImpressionModule->getName(),
                                             (TgMarker*) targetGroupPacingByImpressionModule.get()));

        targetGroupImpressionCappingFilterModule = std::make_unique<TargetGroupImpressionCappingFilterModule>
                                                           (beanFactory->entityDeliveryInfoCacheService.get(),
                                                           beanFactory->entityToModuleStateStats.get(),
                                                           filterNameToFailureCounts);
        targetGroupImpressionCappingFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupImpressionCappingFilterModule->getName(),
                                             (TgMarker*) targetGroupImpressionCappingFilterModule.get()));

        targetGroupBudgetCappingFilterModule =
                std::make_unique<TargetGroupBudgetCappingFilterModule>(
                        beanFactory->entityDeliveryInfoCacheService.get(),
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);
        targetGroupBudgetCappingFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupBudgetCappingFilterModule->getName(),
                                             (TgMarker*) targetGroupBudgetCappingFilterModule.get()));

        campaignImpressionCappingFilterModule =
                std::make_unique<CampaignImpressionCappingFilterModule>(
                        beanFactory->campaignCacheService.get(),
                        entityToModuleStateStats,
                        beanFactory->entityDeliveryInfoCacheService.get(),
                        filterNameToFailureCounts);
        campaignImpressionCappingFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(campaignImpressionCappingFilterModule->getName(),
                                             (TgMarker*) campaignImpressionCappingFilterModule.get()));

        campaignBudgetCappingFilterModule =
                std::make_unique<CampaignBudgetCappingFilterModule> (
                        beanFactory->campaignCacheService.get(),
                        entityToModuleStateStats,
                        beanFactory->entityDeliveryInfoCacheService.get(),
                        filterNameToFailureCounts);
        campaignBudgetCappingFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(campaignBudgetCappingFilterModule->getName(),
                                             (TgMarker*) campaignBudgetCappingFilterModule.get()));

        targetGroupWithoutCreativeFilterModule =
                std::make_unique<TargetGroupWithoutCreativeFilterModule> (
                        beanFactory->entityToModuleStateStats.get(),
                        filterNameToFailureCounts);
        targetGroupWithoutCreativeFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupWithoutCreativeFilterModule->getName(),
                                             (TgMarker*) targetGroupWithoutCreativeFilterModule.get()));


        targetGroupWithoutCreativeFilterModule->targetGroupCreativeCacheService =
                beanFactory->targetGroupCreativeCacheService.get();
        targetGroupWithoutCreativeFilterModule->markerType = TgMarker::NON_FREQUENT_FILTER;
        allMarkersMap->insert(std::make_pair(targetGroupWithoutCreativeFilterModule->getName(),
                                             (TgMarker*) targetGroupWithoutCreativeFilterModule.get()));


        addToModuleList (this->activeFilterModule);
        addToModuleList (this->targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule);
        addToModuleList (targetGroupPacingByBudgetModule.get());
        addToModuleList (targetGroupPacingByImpressionModule.get());
        addToModuleList (dayPartFilter);
        addToModuleList (campaignImpressionCappingFilterModule.get());
        addToModuleList (campaignBudgetCappingFilterModule.get());
        addToModuleList (targetGroupImpressionCappingFilterModule.get());
        addToModuleList (targetGroupBudgetCappingFilterModule.get());

        //this will filter out all target groups that don't have any assigned creatives
        addToModuleList (targetGroupWithoutCreativeFilterModule.get());
        addToModuleList (targetGroupWithoutSegmentFilterModule);
}

void DataReloadPipeline::addToModuleList(BidderModule* module) {
        this->modules.push_back (module);
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > >
DataReloadPipeline::process(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > tgCandidates) {
        if (tgCandidates == nullptr) {
                LOG(ERROR) << "tgCandidates are null";
        }
        auto context = std::make_shared<OpportunityContext>();
        for (auto tg : *tgCandidates) {
                context->getAllAvailableTgs()->push_back(tg);
        }

        entityToModuleStateStats->addStateModuleForEntity("BEGINNING_OF_PIPELINE:#OfAvailableTargetGroupsAtStart = "
                                                          + StringUtil::toStr(context->getSizeOfUnmarkedTargetGroups()),
                                                          "DataReloadPipeline",
                                                          "ALL");

        for (auto module : modules) {
                int beforeTgSize = context->getSizeOfUnmarkedTargetGroups();
                std::vector<std::shared_ptr<TargetGroup> > beforeFilteringTgs;
                for (auto tg : *tgCandidates) {
                        beforeFilteringTgs.push_back(tg);
                }

                module->process (context);

                auto afterFilteringTgs = TgMarker::getPassingTargetGroups(context);


                std::string allFilteredTgsSoFar;
                auto map = context->targetGroupIdsToRemove->getCopyOfMap();
                typename tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> >::iterator iter;
                for (auto iter = map->begin ();
                     iter != map->end ();
                     iter++) {
                        allFilteredTgsSoFar += _toStr(iter->first) + ",";
                }

                LOG(ERROR) << "DataReloadPipeline ran "
                           << module->getName()
                           << " left  "
                           << context->getSizeOfUnmarkedTargetGroups() << " available targetgroups"
                           <<" filtered tgs are : "<<allFilteredTgsSoFar;

                entityToModuleStateStats->addStateModuleForEntityWithValue(module->getName() + ":AfterTgSize",
                                                                           context->getSizeOfUnmarkedTargetGroups(),
                                                                           "DataReloadPipeline",
                                                                           "ALL");


        }
        auto tgsLeftForBidding = TgMarker::getPassingTargetGroups(context);
        auto allTargetGroups = std::make_shared<std::vector<std::shared_ptr<TargetGroup> > >(
                tgsLeftForBidding->begin(),  tgsLeftForBidding->end()
                );

        entityToModuleStateStats->addStateModuleForEntity("END_OF_PIPELINE:#OfAvailableTargetGroupsAtEnd = "
                                                          + StringUtil::toStr(allTargetGroups->size()),
                                                          "DataReloadPipeline",
                                                          "ALL");
        return allTargetGroups;
}

#ifndef DataReloadPipeline_H
#define DataReloadPipeline_H


class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <unordered_map>
#include <string>
#include <tbb/concurrent_hash_map.h>
class BidderModule;
class TgMarker;
class DateTimeService;
class TargetGroupPerMinuteBidCapFilter;
class DateTimeService;
class TargetGroupWithoutCreativeFilterModule;
class TargetGroupFilterStatistic;
class TargetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule;
class TargetGroup;
#include "Object.h"
namespace gicapods {
template <typename K, typename V>
class ConcurrentHashMap;
}
class BeanFactory;

class DataReloadPipeline;


/*
   Thsi pipeline will be called after we reload data
 */
class DataReloadPipeline : public Object {

private:

public:
std::shared_ptr<std::unordered_map<std::string, TgMarker*> > allMarkersMap;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
EntityToModuleStateStats* entityToModuleStateStats;
DateTimeService* dateTimeService;
BeanFactory* beanFactory;

std::unique_ptr<TargetGroupWithoutCreativeFilterModule> targetGroupWithoutCreativeFilterModule;
BidderModule* activeFilterModule;
BidderModule* targetGroupHourlyDailyBudgetAndImpSoftLimitCalculatorModule;
std::unique_ptr<BidderModule> targetGroupPacingByBudgetModule;
std::unique_ptr<BidderModule> targetGroupPacingByImpressionModule;
std::unique_ptr<BidderModule> targetGroupImpressionCappingFilterModule;
std::unique_ptr<BidderModule> targetGroupBudgetCappingFilterModule;
std::unique_ptr<BidderModule> campaignImpressionCappingFilterModule;
std::unique_ptr<BidderModule> campaignBudgetCappingFilterModule;
BidderModule* dayPartFilter;
BidderModule* targetGroupWithoutSegmentFilterModule;

DataReloadPipeline(EntityToModuleStateStats* entityToModuleStateStats);

virtual ~DataReloadPipeline();

std::vector<BidderModule*> modules;

void setTheModuleList();

void addToModuleList(BidderModule* module);


std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > process(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > tgCandidates);

};

#endif

#include "GUtil.h"
#include "BidderApplicationContext.h"
#include "EntityDeliveryInfoFetcher.h"
#include "BiddingMode.h"

#include "ImportantStats.h"
#include "JsonArrayUtil.h"
#include "SignalHandler.h"
#include "BidderLatencyRecorder.h"
#include "Poco/Util/HelpFormatter.h"
#include "BidderAsyncJobsService.h"
#include "PocoHttpServer.h"
#include "EnqueueForScoringModule.h"
#include "TgFilterMeasuresService.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "ConfigService.h"
#include "FeatureGuardService.h"
#include "BidModeControllerService.h"
#include "RealTimeFeatureRegistryCacheUpdaterService.h"
#include "ConverterUtil.h"
#include "StringUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "EntityToModuleStateStats.h"
#include "DateTimeUtil.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "ConcurrentHashSet.h"
#include "TargetGroupFilterCountDbRecord.h"
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include "EventLog.h"
#include "FileUtil.h"
#include "HttpUtil.h"
#include "GlobalWhiteListedModelCacheService.h"
#include "TargetGroupCacheService.h"
#include "EntityDeliveryInfoFetcher.h"
#include "AdServerStatusChecker.h"
#include "EntityToModuleStateStats.h"
#include "BidderApplicationContext.h"
#include "FakeDataProvider.h"
#include "MySqlTargetGroupBWListMapService.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "DataReloadService.h"
#include "ConfigService.h"
#include "DataReloadPipeline.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include <boost/thread.hpp>
#include <thread>
#include "BeanFactory.h"
#include "AtomicLong.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BidderMainPipelineProcessor.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "MySqlTgBiddingPerformanceMetricDtoService.h"
#include "MySqlTargetGroupFilterCountDbRecordService.h"
#include "DataClient.h"
#include "ConcurrentHashMap.h"
#include "FeatureDeviceHistory.h"
#include "DateTimeUtil.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "BidderAsyncPipelineProcessor.h"
#include "CacheUpdateWatcher.h"
#include "TargetGroup.h"
#include "Campaign.h"

BidderAsyncJobsService::BidderAsyncJobsService() : Object(__FILE__) {
        lastTimeDataWasReloadedInSeconds = 0;
        lastTimeAdServerStatusChecked  = 0;
        lastTimeEveryMinuteThreadRan  = 0;
        lastTimeEvery10SecondsThreadRan  = 0;
        lastTimeEveryHourThreadRan  = 0;

        maxAgeOfAcceptableDataInSeconds  = 61;
        reloadingCacheInterval  = 60;

}

void BidderAsyncJobsService::refreshFiles(std::string fileName) {
        // std::string fileName = "/tmp/top_common_devices";
        std::string fileFullName = "/tmp/" + fileName + ".txt";
        if (!FileUtil::checkIfFileExists(fileFullName)
            || DateTimeUtil::getNowInSecond() - FileUtil::getLastModifiedInSeconds(fileFullName) > 600) {
                LOG(INFO) << "downling file from server : "<< fileFullName;
                std::string url = "scp://67.205.170.169/tmp/" + fileName + "/part-00000";
                if (FileUtil::checkIfFileExists(fileFullName)) {
                        FileUtil::deleteFile(fileFullName);
                }
                HttpUtil::downloadFileUsingScpWithCurl(url, fileFullName);
        }
}

void BidderAsyncJobsService::init() {
        reloadingCacheInterval = ConverterUtil::convertTo<int> (
                configService->get ("reloadCachesIntervalInSecond"));
        maxAgeOfAcceptableDataInSeconds =
                ConverterUtil::convertTo<int> (
                        configService->get ("maxAgeOfAcceptableDataInSeconds"));
        adservStatusCheckIntervalInSecond = 0;
        beanFactory->configService->get("adservStatusCheckIntervalInSecond", adservStatusCheckIntervalInSecond);

        assertAndThrow(adservStatusCheckIntervalInSecond >= 1);
        assertAndThrow(reloadingCacheInterval >= 1);
}

BidderAsyncJobsService::~BidderAsyncJobsService() {

}

void BidderAsyncJobsService::persistFilterCounts() {
        std::vector<std::shared_ptr<TargetGroupFilterCountDbRecord> > allFilterCounts;

        int totalCount = totalNumberOfRequestsProcessedInFilterCountPeriod->getValue();
        if (totalCount < 100) {
                LOG(ERROR)<<" low number of recuest processed : "<< totalCount<<", is BidderForwarder On?";
                entityToModuleStateStats->addStateModuleForEntity("LOW_NUMBER_OF_REQUEST_PROCESSED",
                                                                  "BidderAsyncJobsService",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);


        }
        if (totalCount == 0) {
                totalCount = 1;
        }
        MLOG(10) << "totalCount : "<< totalCount;
        auto map = filterNameToFailureCounts->moveMap();
        for(auto const& entry :  *map) {
                auto targetGroupFilterStatistic = entry.second;
                auto tgAndNamePair = entry.first;
                std::string str = targetGroupFilterStatistic->toJson();
                MLOG(10)<< "tgAndNamePair : " << tgAndNamePair <<
                        "targetGroupFilterStatistic : "<< str;
                auto tgAndNamePairVector = StringUtil::tokenizeString(tgAndNamePair, "___");

                assertAndThrow(tgAndNamePairVector.size() == 3);
                auto dbRecord = std::make_shared<TargetGroupFilterCountDbRecord>();
                dbRecord->entityId = tgAndNamePairVector.at(1) + tgAndNamePairVector.at(0); //we want to make this searchable in datatables
                dbRecord->entityType = tgAndNamePairVector.at(1);
                dbRecord->filterName = tgAndNamePairVector.at(2);
                int numberOfFailures = targetGroupFilterStatistic->numberOfFailures->getValue();
                int numberOfPasses = targetGroupFilterStatistic->numberOfPasses->getValue();
                dbRecord->numberOfFailures->setValue(numberOfFailures);
                dbRecord->numberOfPasses->setValue(numberOfPasses);
                dbRecord->totalNumberOfRequest->setValue(totalCount);
                auto pair = allMarkersMap->find(dbRecord->filterName);
                if (pair == allMarkersMap->end()) {
                        MLOG(2)<<"no marker found for filterName : " << dbRecord->filterName;
                        dbRecord->filterType = TgMarker::FREQUNT_MARKER;
                } else {
                        dbRecord->filterType = pair->second->markerType;
                }

                int total = numberOfFailures +  numberOfPasses;
                double failurePercentage = 0.0;
                if (total > 0) {
                        failurePercentage = numberOfFailures * 100 / (total);
                }
                dbRecord->percentageOfFailures->setValue(failurePercentage);

                NULL_CHECK(dbRecord->totalNumberOfCalls);
                int totalCalls = numberOfFailures +  numberOfPasses;
                dbRecord->totalNumberOfCalls->setValue(totalCalls);

                Poco::DateTime now;
                dbRecord->timeReported = now;
                allFilterCounts.push_back(dbRecord);
        }
        //
        mySqlTargetGroupFilterCountDbRecordService->insert(allFilterCounts);
        totalNumberOfRequestsProcessedInFilterCountPeriod->setValue(0);
        mySqlTargetGroupFilterCountDbRecordService->deleteDataOlderThanNDays(10);
        mySqlTgBiddingPerformanceMetricDtoService->deleteDataOlderThanNDays(10);
}

void BidderAsyncJobsService::runEveryHour() {
        try {

                entityToModuleStateStats->addStateModuleForEntity ("runEveryHour",
                                                                   "BidderAsyncJobsService",
                                                                   "ALL");

                tgFilterMeasuresService->resetAllTargetGroupHourlyPerformanceMetrics();
                numberOfBidsForPixelMatchingPurposePerHour->setValue(0);

        } catch (std::exception const &e) {
                gicapods::Util::showStackTrace();
                //TODO : go red and show in dashboard that this is red
                LOG(ERROR) << "error happening when runEveryHour  " << boost::diagnostic_information (e);
        }

        realTimeFeatureRegistryCacheUpdaterService->updateFeaturesUnderReview();
        realTimeFeatureRegistryCacheUpdaterService->populateAerospikeFeatureCache();

        lastTimeEveryHourThreadRan = DateTimeUtil::getNowInSecond();

}

void BidderAsyncJobsService::populateMetricsForMemory() {
        double virtualMemory = 0;
        double residentMemory = 0;

        gicapods::Util::getMemoryUsage(virtualMemory, residentMemory);
        virtualMemory = virtualMemory / (1024 * 1024);
        residentMemory = residentMemory / (1024 * 1024);

        bidderLatencyRecorder->addStateModuleForEntityWithValuePerMinute(
                ImportantStats::getName("virtualMemoryUsedInGB"),
                virtualMemory,
                ImportantStats::getName("OverallBidderHealth"),
                EntityToModuleStateStats::all
                );

        bidderLatencyRecorder->addStateModuleForEntityWithValuePerMinute(
                ImportantStats::getName("residentMemoryUsedInGB"),
                virtualMemory,
                ImportantStats::getName("OverallBidderHealth"),
                EntityToModuleStateStats::all
                );
}

void BidderAsyncJobsService::runEveryTenMinutes() {
        PocoHttpServer::printServerInfo(bidderServer);
        refreshFiles("top_common_devices_last7days");
        reloadBannedDevices("top_common_devices_last7days");

        refreshFiles("top_common_ips_last7days");
        reloadBannedIps("top_common_ips_last7days");

        refreshFiles("top_common_mgrs10m_last7days");
        reloadBannedMgrs10ms("top_common_mgrs10m_last7days");
}

void BidderAsyncJobsService::reloadBannedDevices(std::string fileName) {
        std::string fileFullName = "/tmp/" + fileName + ".txt";
        std::vector<std::string> allLines = FileUtil::readFileLineByLine(fileFullName);
        for(auto line : allLines) {
                auto contents = StringUtil::tokenizeString(line, ",");
                assertAndThrow(contents.size() == 2);
                bannedDevices->put(contents.at(0));
        }
}

void BidderAsyncJobsService::reloadBannedIps(std::string fileName) {
        std::string fileFullName = "/tmp/" + fileName + ".txt";
        std::vector<std::string> allLines = FileUtil::readFileLineByLine(fileFullName);
        for(auto line : allLines) {
                auto contents = StringUtil::tokenizeString(line, ",");
                assertAndThrow(contents.size() == 2);
                bannedIps->put(contents.at(0));
        }
}

void BidderAsyncJobsService::updateProcessAliveFile() {
        FileUtil::writeStringToFile("/tmp/lastTimeBidderRan.txt", _toStr(DateTimeUtil::getNowInSecond()));
}

void BidderAsyncJobsService::reloadBannedMgrs10ms(std::string fileName) {
        std::string fileFullName = "/tmp/" + fileName + ".txt";
        std::vector<std::string> allLines = FileUtil::readFileLineByLine(fileFullName);
        for(auto line : allLines) {
                auto contents = StringUtil::tokenizeString(line, ",");
                assertAndThrow(contents.size() == 2);
                //a bannedLatLon has a form like this "lat_lon" as string like "72.23_54.34"
                bannedMgrs10ms->put(contents.at(0));
        }
}

void BidderAsyncJobsService::runEveryMinute() {
        try {

                populateMetricsForMemory();
                updateProcessAliveFile();
                bidModeControllerService->resetAllMonitors();

                entityToModuleStateStats->addStateModuleForEntity ("runEveryMinute",
                                                                   "BidderAsyncJobsService",
                                                                   "ALL");

                numberOfBidRequestRecievedPerMinute->setValue(0);

                // //we are going to modify TgBiddingPerformanceMetric map, so
                // //all the operations on that map should be done in the same thread
                tgFilterMeasuresService->resetAllTargetGroupMinutelyPerformanceMetrics();

                persistFilterCounts();

                if (numberOfExceptionsInWritingEvents->getValue() < 2 &&
                    numberOfExceptionsInWritingEvents->getValue() >0 ) {
                        //in case of two exceptions per minute , we force the event log writer to become healthy again.
                        numberOfExceptionsInWritingEvents->setValue(0);
                        LOG(WARNING) << "forcing eventlog writer to become healthy again";
                }


                int oldnessOfReloadedData = DateTimeUtil::getNowInSecond() - lastTimeDataWasReloadedInSeconds;
                if (oldnessOfReloadedData > maxAgeOfAcceptableDataInSeconds) {
                        entityToModuleStateStats->addStateModuleForEntity(
                                "DATA_IS_TOO_OLD",
                                "BidderAsyncJobsService",
                                EntityToModuleStateStats::all,
                                EntityToModuleStateStats::exception
                                );
                }



        } catch (std::exception &e) {
                gicapods::Util::showStackTrace(&e);
                entityToModuleStateStats->addStateModuleForEntity(
                        "EXCEPTION_IN_RUN_EVERY_MINUTE",
                        "BidderAsyncJobsService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception
                        );
        }  catch (...) {
                gicapods::Util::showStackTrace();
                entityToModuleStateStats->addStateModuleForEntity(
                        "EXCEPTION_IN_RUN_EVERY_MINUTE",
                        "BidderAsyncJobsService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception
                        );

        }

        lastTimeEveryMinuteThreadRan = DateTimeUtil::getNowInSecond();
}

void BidderAsyncJobsService::runEvery10Seconds() {
        try {


                //now we persist whatever in map we have, this persist function
                // will add the values of minutely to all the counters in db
                MLOG(10) << "persistNumberOfBidsMadeByBidderForAllTargetGroups";
                tgBiddingPerformanceMetricInBiddingPeriodService->persistNumberOfBidsMadeByBidderForAllTargetGroups();

                //we write a lot of messages to kafka here
                enqueueForScoringModule->processManyMessages();


                LOG_EVERY_N(INFO, 1) << "numberOfBidRequestRecievedPerMinute : "
                                     << numberOfBidRequestRecievedPerMinute->getValue();
                LOG_EVERY_N(INFO, 1) << "numberOfBidRequestRecievedAvgPerSecond : "
                                     << numberOfBidRequestRecievedPerMinute->getValue() / 60;

                //we process the async bidder pipeline here
                bidderAsyncPipelineProcessor->processManyMessages();


        } catch (std::exception const &e) {
                gicapods::Util::showStackTrace();
                //TODO : go red and show in dashboard that this is red
                LOG(ERROR) << "error happening when runEvery10Seconds  " << boost::diagnostic_information (e);
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_runEvery10Seconds",
                        "BidderAsyncJobsService",
                        "ALL",
                        EntityToModuleStateStats::exception);
        }  catch (...) {
                gicapods::Util::showStackTrace();
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_IN_runEvery10Seconds",
                        "BidderAsyncJobsService",
                        "ALL",
                        EntityToModuleStateStats::exception);
        }

        lastTimeEvery10SecondsThreadRan = DateTimeUtil::getNowInSecond();
}


void BidderAsyncJobsService::startAdServerStatusChecker() {
        adServerStatusChecker->checkAdServerStatus ();
        lastTimeAdServerStatusChecked = DateTimeUtil::getNowInSecond();
}

std::string BidderAsyncJobsService::getName() {
        return "BidderAsyncJobsService";
}

void BidderAsyncJobsService::reloadCaches() {
        try {

                isReloadingDataInProgress->setValue(true);

                //sleep 4 seconds for all bid requests to be processed
                gicapods::Util::sleepViaBoost (_L_, 1);

                NULL_CHECK(entityToModuleStateStats);
                entityToModuleStateStats->addStateModuleForEntity ("reloadCaches",
                                                                   "BidderAsyncJobsService",
                                                                   "ALL");


                // now we reload caches and targetgroups
                dataReloadService->reloadDataViaHttp();
                //right after reloading fresh data. we add some fake data
                //to validate our logic on
                fakeDataProvider->addFakeData();


                auto currentTgs = beanFactory->targetGroupCacheService->getAllCurrentTargetGroups();
                if (currentTgs->empty()) {
                        LOG(ERROR) << "no current targetgroups exists";
                }
                auto allTgIds = beanFactory->targetGroupCacheService->getAllEntityIds();
                if (allTgIds.empty()) {
                        LOG(ERROR) << "no targetgroups ids loaded from data master";
                }

                std::vector<int> allCampaignIds;
                for(auto tg : *currentTgs) {
                        allCampaignIds.push_back(tg->getCampaignId());
                }


                //TODO add client delivey cap too
                // std::vector<int> allClientIds;
                // auto beanFactory->clientCacheService->getAllCurrentTargetGroups();
                // entityDeliveryInfoFetcher->getLatestRealTimeInfo (Client::getEntityName(), allCampaignIds);

                entityDeliveryInfoFetcher->getLatestRealTimeInfo (TargetGroup::getEntityName(), allTgIds);
                entityDeliveryInfoFetcher->getLatestRealTimeInfo (Campaign::getEntityName(), allCampaignIds);


                auto size = StringUtil::toStr(currentTgs->size());
                entityToModuleStateStats->addStateModuleForEntity ("#CurrentTargetGroups:" + size,
                                                                   "BidderAsyncJobsService",
                                                                   "ALL");

                // here we run the targetgroups data filters
                auto targetgroupsAvailableAfterPipeline = runDataReloadPipeline(currentTgs);

                size = StringUtil::toStr(targetgroupsAvailableAfterPipeline->size());
                entityToModuleStateStats->addStateModuleForEntity ("#ClearedTargetGroups:" + size,
                                                                   "BidderAsyncJobsService",
                                                                   "ALL");
                tgFilterMeasuresService->
                initFilterMeasuresAndPerformanceMapsForCurrentTgs(targetgroupsAvailableAfterPipeline);

                //restore the persisted numberOfBidsMadeToday for each targetgroups loaded
                tgFilterMeasuresService->readPerformanceMetricFromDb();

                //here we refill the targetgroups that are eligible for bidding
                refillTargetGroupsEligibileForBidding(targetgroupsAvailableAfterPipeline);



                mapOfInterestingFeatures->clear();
                auto map = beanFactory->globalWhiteListedModelCacheService->allWhiteListedDomains->getCopyOfMap();
                for (auto&& entry : *map) {
                        mapOfInterestingFeatures->put(entry.first, nullptr);
                }

                //very important to clear these caches
                for (auto module : cacheUpdateWatcherModules) {
                        // we clear all the filtered calculated caches.
                        //very important!!
                        module->cacheWasUpdatedEvent();
                }
                if (dataReloadService->isReloadProcessHealthy->getValue() == true) {
                        //we only record this time when we have successfully reloaded data
                        //if dataReloadService is unhealthy, we should retry again
                        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();
                }
        } catch (std::exception const &e) {
                LOG(ERROR) << "error happening when reloading caches  " << boost::diagnostic_information (e);
                entityToModuleStateStats->addStateModuleForEntity(
                        "ERROR_WHILE_RELOADING_CACHES",
                        "BidderAsyncJobsService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                gicapods::Util::showStackTrace();
        }

        //sleep 4 seconds for all AdServers and Pacers to reload their caches as well
        //so they have updated data
        gicapods::Util::sleepViaBoost (_L_, 4);


        isReloadingDataInProgress->setValue(false);
        LOG(INFO) << "done with cache reload";

}


void BidderAsyncJobsService::refillTargetGroupsEligibileForBidding(
        std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetgroupsAvailableAfterPipeline) {
        targetGroupsEligibleForBidding->clear();
        std::copy(std::begin(*targetgroupsAvailableAfterPipeline),
                  std::end(*targetgroupsAvailableAfterPipeline),
                  std::back_inserter(*targetGroupsEligibleForBidding));

        if(targetGroupsEligibleForBidding->empty()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "NO_TARGET_GROUP_ELIGIBLE_FOR_BIDDING",
                        "BidderAsyncJobsService",
                        EntityToModuleStateStats::all,
                        EntityToModuleStateStats::exception);
                LOG(ERROR) << "no targetgroups selected for pipeline, "
                           <<  "initial loaded current tgs : "
                           << beanFactory->targetGroupCacheService->getAllCurrentTargetGroups()->size()
                           <<  "\ninitial loaded tgs : "
                           << beanFactory->targetGroupCacheService->getAllEntities()->size();

        }
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > >
BidderAsyncJobsService::runDataReloadPipeline(
        std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > candidateTargetGroups) {
        NULL_CHECK(candidateTargetGroups);
        return dataReloadPipeline->process(candidateTargetGroups);
}

void BidderAsyncJobsService::runAllThreads() {
        while(bidderIsStopped->getValue() == false) {
                //every second we run this async thread...that is responsible for running other async tasks
                gicapods::Util::sleepViaBoost (_L_, 1);
                bidModeControllerService->determineBidMode();


                auto now = DateTimeUtil::getNowInSecond();

                // assertAndThrow(now - lastTimeAdServerStatusChecked < adservStatusCheckIntervalInSecond + 1);
                if ((now - lastTimeAdServerStatusChecked) >= adservStatusCheckIntervalInSecond ) {
                        LOG_EVERY_N(INFO, 100)<<google::COUNTER<<"th startAdServerStatusChecker";
                        try { startAdServerStatusChecker(); } catch(...) {}
                }

                // assertAndThrow(now - lastTimeDataWasReloadedInSeconds < reloadingCacheInterval + 1);
                if ((now - lastTimeDataWasReloadedInSeconds) >= reloadingCacheInterval ) {
                        LOG(INFO)<<"reloadCaches";
                        try {
                                // HeapProfilerStart("BidderAsyncJobsService_reloadCaches");
                                reloadCaches();
                                // HeapProfilerDump("reloadCachesProfile");
                                // HeapProfilerStop();
                        } catch(...) {}
                }


                // assertAndThrow(now - lastTimeEveryMinuteThreadRan < 100);
                if ((now - lastTimeEveryMinuteThreadRan) >= 600 ) {
                        LOG_EVERY_N(INFO, 100)<<google::COUNTER<<"th runEveryTenMinutes";
                        try { runEveryTenMinutes(); } catch(...) {}
                }
                if ((now - lastTimeEveryMinuteThreadRan) >= 60 ) {
                        LOG_EVERY_N(INFO, 100)<<google::COUNTER<<"th runEveryMinute";
                        try { runEveryMinute(); } catch(...) {}
                }

                // assertAndThrow(now - lastTimeEvery10SecondsThreadRan < 20);
                if ((now - lastTimeEvery10SecondsThreadRan) >= 10 ) {
                        LOG_EVERY_N(INFO, 100)<<"runEvery10Seconds";
                        try { runEvery10Seconds(); } catch(...) {}
                }

                // assertAndThrow(now - lastTimeEveryHourThreadRan < 5000);
                if ((now - lastTimeEveryHourThreadRan) >= 3600 ) {
                        LOG(INFO)<<"runEveryHour";
                        try { runEveryHour(); } catch(...) {}
                }

        }


        asyncThreadIsStopped->setValue(true);
}

void BidderAsyncJobsService::startAsyncThreads() {
        std::thread runAllThreads (&BidderAsyncJobsService::runAllThreads, this);
        runAllThreads.detach ();

        this->entityToModuleStateStatsPersistenceService->startThread();

}

#ifndef BidderAsyncPipelineProcessor_H
#define BidderAsyncPipelineProcessor_H


#include <tbb/concurrent_queue.h>
#include <memory>
class ModuleContainer;
class AtomicLong;
class EntityToModuleStateStats;

class AtomicLong;
#include "BidderModule.h"
class HttpUtilService;
class FeatureRecorderPipeline;
class OpportunityContext;
class LoadPercentageBasedSampler;
class BidderLatencyRecorder;
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupFilterStatistic.h"
#include "Object.h"
class BidderAsyncPipelineProcessor : public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;
std::shared_ptr<gicapods::AtomicLong> numberProcessorCalls;
std::vector<BidderModule*> modulesList;
std::vector<BidderModule*> sampledModulesList;

BidderLatencyRecorder* bidderLatencyRecorder;
LoadPercentageBasedSampler* sampledModuleSampler;
FeatureRecorderPipeline* featureRecorderPipeline;
HttpUtilService* httpUtilService;
ModuleContainer* moduleContainer;
std::shared_ptr<tbb::concurrent_queue<std::shared_ptr<OpportunityContext> > > queueOfContexts;

void readOneContextOffQueue();

BidderAsyncPipelineProcessor();

void setTheModuleList();

void processManyMessages();

virtual void process(std::shared_ptr<OpportunityContext> opt);

void runModules(
        std::shared_ptr<OpportunityContext> opt,
        std::vector<BidderModule*>& modulesList);

std::string getName();

virtual ~BidderAsyncPipelineProcessor();
};



#endif

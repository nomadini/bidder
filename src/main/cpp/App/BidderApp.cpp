
#include "BidderAppStarter.h"
#include "GUtil.h"
#include <memory>
#include <string>

#include <stdio.h>
#include <curl/curl.h>

int main(int argc, char* argv[]) {

        auto bidderAppStarter = std::make_shared<BidderAppStarter>();

        bidderAppStarter->startTheApp(argc, argv);
}

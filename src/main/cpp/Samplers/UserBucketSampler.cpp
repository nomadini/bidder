
#include "AdEntry.h"
#include "Device.h"
#include "GUtil.h"
#include "OpportunityContext.h"
#include "Sampler.h"
#include "UserBucketSampler.h"
#include <boost/functional/hash.hpp>
#include "RandomUtil.h"

UserBucketSampler::UserBucketSampler(int range)  : Object(__FILE__) {
        this->range = range;
        numberOfTotalCalls = std::make_shared<gicapods::AtomicLong>();
        numberOfFoundInSample = std::make_shared<gicapods::AtomicLong>();
        percentageOfSampleRate = std::make_shared<gicapods::AtomicDouble>();

        userBucketRandomizer = std::make_shared<RandomUtil> (range);
}

bool UserBucketSampler::isPartOfSample(std::shared_ptr<OpportunityContext> context) {
        boost::hash<std::string> string_hash;
        std::size_t hasheValue = string_hash(context->device->getDeviceId());
        numberOfTotalCalls->increment();
        long randomNumber = userBucketRandomizer->randomNumberV2();
        if (hasheValue % randomNumber == 0) {
                numberOfFoundInSample->increment();
                percentageOfSampleRate->setValue(
                        (numberOfFoundInSample->getValue() /
                         numberOfTotalCalls->getValue() ) * 100);
                return true;
        }  else {
                return false;
        }

        LOG_EVERY_N(ERROR, 10000) << "percentageOfSampleRate : " << percentageOfSampleRate->getValue()
                                  << " for range : " << range;

}
UserBucketSampler::~UserBucketSampler() {

}

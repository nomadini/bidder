//
// Created by Mahmoud Taabodi on 11/24/15.
//

#ifndef GICAPODS_RANDOMRandomSampler_H
#define GICAPODS_RANDOMRandomSampler_H

#include "AdEntry.h"

class OpportunityContext;
#include "Sampler.h"
#include <memory>
#include <string>
#include "Object.h"
class RandomSampler;




class RandomSampler : public Sampler, public Object {

private:

public:

RandomSampler();

bool isPartOfSample(std::shared_ptr<OpportunityContext> context);

virtual ~RandomSampler();

};

#endif //GICAPODS_RANDOMRandomSampler_H

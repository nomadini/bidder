#ifndef Sampler_H
#define Sampler_H


#include "AdEntry.h"

class OpportunityContext;
#include <memory>
#include <string>
class Sampler;



#include "Object.h"
class Sampler : public Object {

private:

public:

Sampler();

virtual bool isPartOfSample(std::shared_ptr<OpportunityContext> context);

virtual ~Sampler();

};
/*

   SamplerPtrList SamplerList;
   for (SamplerPtrList::iterator it = SamplerList.begin();it != SamplerPtrList.end(); ++it)
   {

   }
 */

#endif

//
// Created by Mahmoud Taabodi on 11/24/16.
//

#ifndef UserBucketSampler_H
#define UserBucketSampler_H

#include "AdEntry.h"

class OpportunityContext;
#include "Sampler.h"
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "AtomicDouble.h"
class RandomUtil;
class UserBucketSampler;
#include "Object.h"


//checks if a user nomadiniDeviceId is part of sample rate given
class UserBucketSampler : public Sampler, public Object {

private:

int range;
std::shared_ptr<gicapods::AtomicLong> numberOfTotalCalls;
std::shared_ptr<gicapods::AtomicLong> numberOfFoundInSample;
std::shared_ptr<gicapods::AtomicDouble> percentageOfSampleRate;
public:
std::shared_ptr<RandomUtil> userBucketRandomizer;
UserBucketSampler(int range);

bool isPartOfSample(std::shared_ptr<OpportunityContext> context);

virtual ~UserBucketSampler();

};

#endif //GICAPODS_RANDOMUserBucketSampler_H

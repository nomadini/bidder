#include "SignalHandler.h"


#include "GUtil.h"
#include "FileUtil.h"
#include "HttpUtil.h"
#include "EntityToModuleStateStats.h"
#include "BidderStaticPageHandler.h"
#include "JsonUtil.h"
#include <boost/algorithm/string.hpp>
#include "StringUtil.h"

BidderStaticPageHandler::BidderStaticPageHandler(EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
}

void BidderStaticPageHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                            Poco::Net::HTTPServerResponse &response) {

        LOG(ERROR)<< "request.getURI  : "<<request.getURI();
        std::string requestBody = HttpUtil::getRequestBody(request);

        std::string currentDirectory = FileUtil::getCurrentDirectory ();
        currentDirectory.append (request.getURI ());
        std::string filename (currentDirectory);

        std::string fileContent = FileUtil::readFileInString (filename);

        HttpUtil::printRequestProperties (request);
        HttpUtil::printQueryParamsOfRequest (request);

        response.setChunkedTransferEncoding (true);
        response.setContentType ("text/html; charset=ISO-8859-1 ");

        if (StringUtil::equalsIgnoreCase (filename,"/staticpages/1-pixel-offer01.png")) {

        }
        //http://69.200.247.201:9980/staticpages/filtermap
        if (boost::algorithm::contains (request.getURI (), "/filtermap")) {
//        std::string htmlContent =
//                this->entityToModuleStateStats->printFilterNameToTargetGroupCounterMapAsHtml ();
//        std::ostream &ostr = response.send ();
//        ostr << htmlContent;

        } else {
                std::ostream &ostr = response.send ();
                ostr << fileContent;
        }

//		Poco::Net::HTTPCookie cookie("sglist", "A:B:C");
//		cookie.setPath("/");
//		cookie.setMaxAge(3600*24*365*100); // 100 YEARS
//		response.addCookie(cookie);
//

}

BidderStaticPageHandler::~BidderStaticPageHandler() {

}

std::string BidderStaticPageHandler::getFileNameFromRequest(
        Poco::Net::HTTPServerRequest &request) { //fix this logic later

        std::string line (request.getURI ());
        std::vector<std::string> strs;
        boost::split (strs, line, boost::is_any_of ("/staticpages/"),
                      boost::token_compress_on);

        MLOG(10) << "strs.size() : " << strs.size ();
        if (!strs.empty ())
                return "default.html";

        return "goForNow";
}

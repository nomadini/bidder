#ifndef StaticPageHandler_H
#define StaticPageHandler_H


#include "SignalHandler.h"



#include "FileUtil.h"
#include "HttpUtil.h"
class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Object.h"
class EntityToModuleStateStats;
class BidderStaticPageHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:
EntityToModuleStateStats* entityToModuleStateStats;
BidderStaticPageHandler(EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse & response);

virtual ~BidderStaticPageHandler();

std::string getFileNameFromRequest(Poco::Net::HTTPServerRequest& request);

};



#endif

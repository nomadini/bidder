//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef BiddingMonitor_H
#define BiddingMonitor_H

#include "OpenRtbBidRequest.h"
class OpportunityContext;
#include <memory>
#include <string>
class EntityToModuleStateStats;
#include "MaxMindService.h"
#include <tbb/concurrent_hash_map.h>
#include "CreativeAttribute.h"
#include "OpenRtbBidRequestParser.h"
#include "AtomicLong.h"
#include "Object.h"

#include "Poco/Net/HTTPClientSession.h"
class HttpUtilService;
class OpportunityContext;
class RandomUtil;
/*
   a BiddingMonitor is used by BidModeControllerService to determineBidMode
 */
class BiddingMonitor : public Object {

private:
std::shared_ptr<gicapods::AtomicLong> failureThresholdPercentage;
std::string nameOfMonitor;
public:

std::shared_ptr<gicapods::AtomicLong> numberOfFailures;
std::shared_ptr<gicapods::AtomicLong> numberOfTotal;

void init();
EntityToModuleStateStats* entityToModuleStateStats;


BiddingMonitor(EntityToModuleStateStats* entityToModuleStateStats,
               int failureThresholdPercentageArg,
               std::string nameOfMonitor);
virtual ~BiddingMonitor();

void resetMeasures();

std::string getName();

bool isFailing();

};

#endif //BIDDER_OPPORTUNITYCONTEXTBUILDER_H

#ifndef OpportunityContext_H
#define OpportunityContext_H



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CreativeTypeDefs.h"
class Creative;

#include "CampaignTypeDefs.h"
class StringUtil;
#include <memory>
#include <string>
#include "Poco/Net/NameValueCollection.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPResponse.h"
#include "tbb/concurrent_hash_map.h"
#include "OpenRtbVersion.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "AtomicLong.h"
#include "DateTimeMacro.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "ObjectVectorHolder.h"
#include "IntWrapper.h"
#include <tbb/concurrent_unordered_set.h>
#include <unordered_set>

class OpportunityContext;
class EventLog;
class AdHistory;
class BidResponseBuilder;
class RecentVisitHistoryScoreCard;
class Device;
class Place;

class OpportunityContext : public Object {

private:

public:

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > allAvailableTgs;

Poco::Net::HTTPServerRequest* request;

std::vector<std::shared_ptr<Place> > allPlaces;
std::vector<int> tagIds;
std::vector<int> chosenSegmentIdsList;
std::vector<int> allSegmentIdsFoundList;
std::shared_ptr<std::unordered_map<int, std::shared_ptr<TargetGroup> > > mapOfDomainBlockingTgs;
bool strictEventLogChecking;
bool isAsyncPiplineExecuting;
bool requestIsParsedOnlyForForwarding;
bool fetchingPlacesFromRemoteExecuted;
int acceptableModuleLatencyInMillis;

//we need this for -PASSING_VALUE states that we record for every entity
std::string allBlockedBannerTypesAsStr;
std::string bidderCallbackServletUrl;


std::string processingBidderUrl;
std::string servletPathToForwardTo;
std::string requestBody;
tbb::concurrent_hash_map<int, std::shared_ptr<TargetGroup> > allTgsWithCorrectSegmentsForDeviceAsMap;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedBannerTypesMap;
std::shared_ptr<RecentVisitHistoryScoreCard> recentVisitHistoryScoreCardOfDevice;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedAdvertiserMap;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedIabCategoriesMap;
std::shared_ptr<std::unordered_map<int, std::shared_ptr<std::unordered_set<std::string> > > > tgIdToTypesOfTargetingChosen;


//this is incremented for every context // its good for sampling purposes
std::shared_ptr<gicapods::AtomicLong> uniqueNumberOfContext;
std::shared_ptr<AdHistory> adHistory;
std::shared_ptr<BidResponseBuilder> bidResponseBuilder;
std::shared_ptr<EventLog> eventLog;
std::shared_ptr<Device> device;
std::string exchangeUserId;
std::string impressionType;
std::string bidRequestId;
std::string impressionIdInBidRequest;
OpenRtbVersion openRtbVersion;
std::string exchangeId;
std::string exchangeName;
std::string winNotificationUrl;
std::string environmentType;
std::string transactionId;
std::string creativeContentType; //like DISPLAY OR VIDEO
std::string finalBidResponseContent;
int chosenTargetGroupId;
int chosenCreativeId;
std::string protocolVersion;     //something like OpenRtb2.3 or Casale2.1 that tells us what protocol this bid request is formed based on
std::string eventType;
int campaignId;
int advertiserId;
int clientId;
int geoSegmentListId;
int publisherId;
bool biddingOnlyForMatchingPixel;
int chosenSegmentId;//this field is deprecated
Poco::Net::HTTPResponse::HTTPStatus bidResponseStatusCode;
std::string adPosition;

double bidPrice;     //this is the price that we bid with
double bidFloor;
double winBidPrice;
double platformCost;

std::string userTimeZone;
int userTimeZonDifferenceWithUTC;

/**
 *
 * these are the variables that are common , and will
 * be mapped to by bid request coming from any exchange
 *
 */


std::string deviceUserAgent;
std::string deviceOsFamily;
std::string deviceIp;
std::string deviceType;

double deviceLat;
double deviceLon;
std::string deviceCountry;
std::string deviceCity;
std::string deviceState;
std::string deviceZipcode;
std::vector<std::string> siteCategory;
std::string siteDomain;
std::string sitePage;
std::string sitePublisherDomain;
std::string sitePublisherName;
std::string adSize;
std::vector<std::string> creativeAPI;
bool secureCreativeRequired;

std::vector<std::string> blockedAttributes;
std::string inventory;

std::string mgrs1km;
std::string mgrs100m;
std::string mgrs10m;

std::string rawBidRequest;     //this is just for debugging purposes

std::string bidDecisionResult;
std::string lastModuleRunInPipeline;
std::string finalCreativeContent;
Poco::Net::NameValueCollection cookieMapIncoming;     // this is what will be written in response. it includes all the incoming cookies
Poco::Net::NameValueCollection cookieMapOutgoing;



std::shared_ptr<gicapods::ConcurrentHashMap<int, TargetGroup> > targetGroupIdsToRemove;

std::shared_ptr<TargetGroup> chosenTargetGroup;
std::shared_ptr<Creative> chosenCreative;

std::shared_ptr<gicapods::Status> status;

void addTargetingTypeForTg(int tgId, std::string typesOfTargeting);
std::shared_ptr<std::unordered_set<std::string> > getTargetingTypesForTg(int tgId);


OpportunityContext();

std::string toString();

std::string toJson();

virtual ~OpportunityContext();

static std::string createRandomTransactionId();

int getSizeOfUnmarkedTargetGroups();
void addBlockedBannerType(std::string bannerType);

void addBlockedAdvertiser(std::string adv);

void addBlockedIabCategory(std::string cat);

std::string getId();

void setId(std::string id);

std::string getTransactionId();

void setTransactionId(std::string transactionId);

int getChosenTargetGroupId();

void setChosenTargetGroupId(int chosenTargetGroupId);

int getChosenCreativeId();

void setChosenCreativeId(int chosenCreativeId);

std::string getProtocolVersion();

void setProtocolVersion(std::string protocolVersion);

std::string getEventType();

void setEventType(std::string eventType);

int getCampaignId();

void setCampaignId(int campaignId);

int getAdvertiserId();

void setAdvertiserId(int advertiserId);

int getPublisherId();

void setPublisherId(int publisherId);

int getChosenSegmentId();

void setChosenSegmentId(int chosenSegmentId);

std::string getAdPosition();

void setAdPosition(std::string adPosition);

double getBidPrice();

void setBidPrice(double bidPrice);

double getWinBidPrice();

void setWinBidPrice(double winBidPrice);

double getPlatformCost();

void setPlatformCost(double platformCost);

std::string getUserTimeZone();

void setUserTimeZone(std::string userTimeZone);

int getUserTimeZonDifferenceWithUTC();

void setUserTimeZonDifferenceWithUTC(int userTimeZonDifferenceWithUTC);

std::string getDeviceUserAgent();

void setDeviceUserAgent(std::string deviceUserAgent);

std::string getDeviceIp();

void setDeviceIp(std::string deviceIp);

double getDeviceLat();

void setDeviceLat(double deviceLat);

double getDeviceLon();

void setDeviceLon(double deviceLon);

std::string getDeviceCountry();

void setDeviceCountry(std::string deviceCountry);

std::string getDeviceCity();

void setDeviceCity(std::string deviceCity);

std::string getDeviceState();

void setDeviceState(std::string deviceState);

std::string getDeviceZipcode();

void setDeviceZipcode(std::string deviceZipcode);

std::vector<std::string> getSiteCategory();

void setSiteCategory(std::string siteCategory);

std::string getSiteDomain();

void setSiteDomain(std::string siteDomain);

std::string getSitePage();

void setSitePage(std::string sitePage);

std::string getSitePublisherDomain();

void setSitePublisherDomain(std::string sitePublisherDomain);

std::string getSitePublisherName();

void setSitePublisherName(std::string sitePublisherName);

std::string getAdSize();

void setAdSize(std::string adSize);

std::vector<std::string> getCreativeAPI();

void setCreativeAPI(std::vector<std::string> creativeAPI);

bool IsSecureCreativeRequired();

void setSecureCreativeRequired(bool secureCreativeRequired);

std::vector<std::string> getBlockedAttributes();

void setBlockedAttributes(std::vector<std::string> blockedAttributes);

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > getBlockedBannerTypesMap();

void setBlockedBannerTypesMap(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedBannerTypesMap);

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > getBlockedAdvertiserMap();

void setBlockedAdvertiserMap(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedAdvertiserMap);

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > getBlockedIabCategoriesMap();

void setBlockedIabCategoriesMap(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedIabCategoriesMap);

std::string getInventory();

void setInventory(std::string inventory);

std::string getRawBidRequest();

void setRawBidRequest(std::string rawBidRequest);

std::vector<std::shared_ptr<TargetGroup> > getTargetGroups();

void setTargetGroups(std::vector<std::shared_ptr<TargetGroup> > targetGroups);

std::string getLastModuleRunInPipeline();

void setLastModuleRunInPipeline(std::string lastModuleRunInPipeline);

std::string getFinalCreativeContent();

void setFinalCreativeContent(std::string finalCreativeContent);

Poco::Net::NameValueCollection getCookieMapIncoming();

void setCookieMapIncoming(Poco::Net::NameValueCollection cookieMapIncoming);

Poco::Net::NameValueCollection getCookieMapOutgoing();

void setCookieMapOutgoing(Poco::Net::NameValueCollection cookieMapOutgoing);

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > getAllAvailableTgs();

void setAllAvailableTgs(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > allAvailableTgs);

std::shared_ptr<TargetGroup> getChosenTargetGroup();

void setChosenTargetGroup(std::shared_ptr<TargetGroup> chosenTargetGroup);

std::shared_ptr<Creative> getChosenCreative();

void setChosenCreative(std::shared_ptr<Creative> chosenCreative);

std::shared_ptr<gicapods::Status> getStatus();

void setStatus(std::shared_ptr<gicapods::Status> status);

};

#endif

//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef BIDDER_OPPORTUNITYCONTEXTBUILDER_H
#define BIDDER_OPPORTUNITYCONTEXTBUILDER_H

class OpportunityContext;
#include <memory>
#include <string>
class EntityToModuleStateStats;
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class OpportunityContextBuilderOpenRtb2_3;
#include "AtomicLong.h"
#include "OpenRtbVersion.h"
#include "Object.h"
class OpportunityContextBuilder;


class OpportunityContextBuilder : public Object {

private:

public:
std::shared_ptr<gicapods::AtomicLong> uniqueNumberOfContext;
EntityToModuleStateStats* entityToModuleStateStats;
OpportunityContextBuilderOpenRtb2_3* opportunityContextBuilderOpenRtb2_3;
OpportunityContextBuilder();
void configureContextFromBidRequest(
        std::shared_ptr<OpportunityContext> context,
        Poco::Net::HTTPServerRequest &request,
        OpenRtbVersion openRtbVersion,
        std::string exchangeName);

};


#endif //BIDDER_OPPORTUNITYCONTEXTBUILDER_H

//
// Created by Mahmoud Taabodi on 5/7/16.
//

#include "IpInfoFetcherModule.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidRequestParser.h"

#include "JsonArrayUtil.h"
#include "BidderMainPipelineProcessor.h"
#include "SignalHandler.h"
#include "Poco/Exception.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "Device.h"
#include "BiddingMode.h"
#include "IpInfoFetcherModule.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "EventLog.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "AdPositionConverter.h"
#include "CreativeAPIConverter.h"
#include "BannerAdTypeConverter.h"
#include "DeviceTypeConverter.h"
#include "CollectionUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "OpenRtbVersion.h"
#include "OpportunityContext.h"
#include "AtomicLong.h"
#include "BiddingMonitor.h"
#include "HttpUtilService.h"
#include "Poco/URI.h"
#include "PocoSession.h"

IpInfoFetcherModule::IpInfoFetcherModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService,
        int failureThresholdPercentageArg) :
        BiddingMonitor(entityToModuleStateStats,
                       failureThresholdPercentageArg,
                       "IpInfoFetcherModuleMonitor"),
        Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;

        this->ipInfoFetcherAppHostUrl =   configService->get("ipInfoFetcherAppHostUrl");

        session = std::make_shared<PocoSession>("IpInfoFetcherModule",
                                                configService->getAsBooleanFromString("ipInfoFetcherCallKeepAliveMode"),
                                                configService->getAsInt("ipInfoFetcherKeepAliveTimeoutInMillis"),
                                                configService->getAsInt("ipInfoFetcherCallTimeoutInMillis"),
                                                configService->get("ipInfoFetcherAppHostUrl"),
                                                entityToModuleStateStats);

}

IpInfoFetcherModule::~IpInfoFetcherModule() {

}


std::string IpInfoFetcherModule::getIpInfoOverHttp(std::shared_ptr<OpportunityContext> opt) {

        auto docResponse = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(docResponse.get(), "deviceIp", opt->deviceIp, value);
        std::string requestBody = JsonUtil::valueToString (value);
        numberOfTotal->increment();
        try {
                auto response = session->sendRequest(
                        *PocoHttpRequest::createSimplePostRequest(ipInfoFetcherAppHostUrl + "/maxmind-data-fetch/",
                                                                  requestBody));

                return response->body;
        } catch(...) {
                numberOfFailures->increment();
        }

        throwEx("error in talking to maxMindService");
}

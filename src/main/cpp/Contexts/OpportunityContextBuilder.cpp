//
// Created by Mahmoud Taabodi on 5/7/16.
//

#include "BidderMainPipelineProcessor.h"
#include "SignalHandler.h"
#include "OpportunityContextBuilder.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "StringUtil.h"
#include "OpportunityContextBuilderOpenRtb2_3.h"
#include "HttpUtil.h"
#include "OpportunityContext.h"
OpportunityContextBuilder::OpportunityContextBuilder() : Object(__FILE__) {

}


void OpportunityContextBuilder::configureContextFromBidRequest(
        std::shared_ptr<OpportunityContext> opt,
        Poco::Net::HTTPServerRequest &request,
        OpenRtbVersion openRtbVersion,
        std::string exchangeName) {
        opt->exchangeName = exchangeName;
        opt->openRtbVersion = openRtbVersion;
        bool parsed = false;
        if (openRtbVersion == OpenRtbVersion::V2_3) {
                opportunityContextBuilderOpenRtb2_3->configureContextFromBidRequest(
                        opt,
                        opt->requestBody);
                parsed = true;

        } else if (openRtbVersion == OpenRtbVersion::V2_2) {
                //configure the other version

        } else {
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNKNOWN_OPEN_RTB_SEEN",
                        "OpportunityContextBuilder",
                        "ALL");
        }
        assertAndThrow(parsed);
        opt->uniqueNumberOfContext = uniqueNumberOfContext;
}

#include "GUtil.h"
#include "Status.h"

#include "TargetGroup.h"
#include "Creative.h"

#include "AdHistory.h"
#include "EventLog.h"
#include "Place.h"

#include "Campaign.h"
#include "StringUtil.h"
#include "OpportunityContext.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"

#include "Device.h"
OpportunityContext::OpportunityContext() : Object(__FILE__) {
        //this field allow us to know if context is being called in asyncPipeline or not
        //depending on that, we decided to execute some logic or not
        isAsyncPiplineExecuting = false;
        bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_OK;
        requestIsParsedOnlyForForwarding = false;
        strictEventLogChecking = true;
        allAvailableTgs = std::make_shared<std::vector<std::shared_ptr<TargetGroup> > >();
        mapOfDomainBlockingTgs = nullptr;
        targetGroupIdsToRemove = std::make_shared< gicapods::ConcurrentHashMap<int, TargetGroup> > ();
        //the primitives has to be initialized to 0
        //because if not, they will have some random number from stack memory values
        chosenTargetGroupId = 0;
        chosenCreativeId = 0;
        campaignId = 0;
        advertiserId = 0;
        winBidPrice = 0;
        bidPrice = 0;
        clientId = 0;
        geoSegmentListId = 0;
        publisherId = 0;
        biddingOnlyForMatchingPixel = false;
        chosenSegmentId = 0;//this field is deprecated
        userTimeZonDifferenceWithUTC = 0;
        platformCost = 0;
        geoSegmentListId = 0;
        clientId = 0;
        deviceLat = 0;
        deviceLon = 0;
        bidFloor = 0;
        bidDecisionResult = EventLog::EVENT_TYPE_NO_BID;
        acceptableModuleLatencyInMillis = 2;
        blockedBannerTypesMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > ();
        blockedAdvertiserMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > ();
        blockedIabCategoriesMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > ();
        status = std::make_shared<gicapods::Status> ();
        biddingOnlyForMatchingPixel = false;
        fetchingPlacesFromRemoteExecuted = false;

        this->tgIdToTypesOfTargetingChosen =
                std::make_shared<std::unordered_map<int, std::shared_ptr<std::unordered_set<std::string> > > > ();

}

OpportunityContext::~OpportunityContext() {
}



std::string OpportunityContext::toString() {
        return this->toJson ();
}


void OpportunityContext::addTargetingTypeForTg(int tgId, std::string typesOfTargeting) {
        auto pairPtr = tgIdToTypesOfTargetingChosen->find(tgId);
        if (pairPtr == tgIdToTypesOfTargetingChosen->end()) {
                auto value = std::make_shared<std::unordered_set<std::string> > ();
                value->insert(typesOfTargeting);
                tgIdToTypesOfTargetingChosen->insert(std::make_pair(tgId, value));

        } else {
                pairPtr->second->insert(typesOfTargeting);
        }
}

std::shared_ptr<std::unordered_set<std::string> > OpportunityContext::getTargetingTypesForTg(int tgId) {
        auto valuePair = tgIdToTypesOfTargetingChosen->find (tgId);
        if (valuePair != tgIdToTypesOfTargetingChosen->end()) {
                return valuePair->second;
        }
        auto empty = std::make_shared<std::unordered_set<std::string> >();
        return empty;
}

std::string OpportunityContext::toJson() {

        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "transactionId", transactionId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "finalBidResponseContent", finalBidResponseContent, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "chosenCreativeId", chosenCreativeId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "chosenTargetGroupId", chosenTargetGroupId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "eventType", eventType, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "campaignId", campaignId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "advertiserId", advertiserId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "publisherId", publisherId, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "nomadiniDeviceId", device->getDeviceId(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceType", device->getDeviceType(), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "deviceIp", StringUtil::toStr (deviceIp), value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "chosenSegmentId", chosenSegmentId, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "bidPrice", bidPrice, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(),  "winBidPrice", winBidPrice, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "platformCost", platformCost, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "userTimeZone", StringUtil::toStr (userTimeZone), value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "userTimeZonDifferenceWithUTC",
                                            StringUtil::toStr (userTimeZonDifferenceWithUTC), value);



        JsonUtil::addMemberToValue_FromPair(doc.get(),"secureCreativeRequired", secureCreativeRequired, value);

        JsonUtil::addMemberToValue_FromPair(doc.get(), "inventory", StringUtil::toStr (inventory), value);

        return JsonUtil::valueToString (value);
}

int OpportunityContext::getSizeOfUnmarkedTargetGroups() {

        return allAvailableTgs->size() - targetGroupIdsToRemove->size();
}

void OpportunityContext::addBlockedBannerType(std::string bannerType) {
        blockedBannerTypesMap->put (StringUtil::toLowerCase (bannerType), 0);
}

void OpportunityContext::addBlockedAdvertiser(std::string adv) {
        blockedAdvertiserMap->put (StringUtil::toLowerCase (adv), 0);

}

void OpportunityContext::addBlockedIabCategory(std::string cat) {
        blockedIabCategoriesMap->put (StringUtil::toLowerCase (cat), 0);

}

std::string OpportunityContext::createRandomTransactionId() {
        return EventLog::createRandomTransactionId();
}

std::string OpportunityContext::getTransactionId() {
        return transactionId;
}

void OpportunityContext::setTransactionId(std::string transactionId) {
        this->transactionId = transactionId;
}

int OpportunityContext::getChosenTargetGroupId() {
        return chosenTargetGroupId;
}

void OpportunityContext::setChosenTargetGroupId(int chosenTargetGroupId) {
        this->chosenTargetGroupId = chosenTargetGroupId;
}

int OpportunityContext::getChosenCreativeId() {
        return chosenCreativeId;
}

void OpportunityContext::setChosenCreativeId(int chosenCreativeId) {
        this->chosenCreativeId = chosenCreativeId;
}

std::string OpportunityContext::getProtocolVersion() {
        return protocolVersion;
}

void OpportunityContext::setProtocolVersion(std::string protocolVersion) {
        this->protocolVersion = protocolVersion;
}

std::string OpportunityContext::getEventType() {
        return eventType;
}

void OpportunityContext::setEventType(std::string eventType) {
        this->eventType = eventType;
}

int OpportunityContext::getCampaignId() {
        return campaignId;
}

void OpportunityContext::setCampaignId(int campaignId) {
        this->campaignId = campaignId;
}

int OpportunityContext::getAdvertiserId() {
        return advertiserId;
}

void OpportunityContext::setAdvertiserId(int advertiserId) {
        this->advertiserId = advertiserId;
}

int OpportunityContext::getPublisherId() {
        return publisherId;
}

void OpportunityContext::setPublisherId(int publisherId) {
        this->publisherId = publisherId;
}

int OpportunityContext::getChosenSegmentId() {
        return chosenSegmentId;
}

void OpportunityContext::setChosenSegmentId(int chosenSegmentId) {
        this->chosenSegmentId = chosenSegmentId;
}

std::string OpportunityContext::getAdPosition() {
        return adPosition;
}

void OpportunityContext::setAdPosition(std::string adPosition) {
        this->adPosition = adPosition;
}

double OpportunityContext::getBidPrice() {
        return bidPrice;
}

void OpportunityContext::setBidPrice(double bidPrice) {
        this->bidPrice = bidPrice;
}

double OpportunityContext::getWinBidPrice() {
        return winBidPrice;
}

void OpportunityContext::setWinBidPrice(double winBidPrice) {
        this->winBidPrice = winBidPrice;
}

double OpportunityContext::getPlatformCost() {
        return platformCost;
}

void OpportunityContext::setPlatformCost(double platformCost) {
        this->platformCost = platformCost;
}

std::string OpportunityContext::getUserTimeZone() {
        return userTimeZone;
}

void OpportunityContext::setUserTimeZone(std::string userTimeZone) {
        this->userTimeZone = userTimeZone;
}

int OpportunityContext::getUserTimeZonDifferenceWithUTC() {
        return userTimeZonDifferenceWithUTC;
}

void OpportunityContext::setUserTimeZonDifferenceWithUTC(int userTimeZonDifferenceWithUTC) {
        this->userTimeZonDifferenceWithUTC = userTimeZonDifferenceWithUTC;
}

std::string OpportunityContext::getDeviceUserAgent() {
        return deviceUserAgent;
}

void OpportunityContext::setDeviceUserAgent(std::string deviceUserAgent) {
        this->deviceUserAgent = deviceUserAgent;
}

std::string OpportunityContext::getDeviceIp() {
        return deviceIp;
}

void OpportunityContext::setDeviceIp(std::string deviceIp) {
        this->deviceIp = deviceIp;
}

double OpportunityContext::getDeviceLat() {
        return deviceLat;
}

void OpportunityContext::setDeviceLat(double deviceLat) {
        this->deviceLat = deviceLat;
}

double OpportunityContext::getDeviceLon() {
        return deviceLon;
}

void OpportunityContext::setDeviceLon(double deviceLon) {
        this->deviceLon = deviceLon;
}

std::string OpportunityContext::getDeviceCountry() {
        return deviceCountry;
}

void OpportunityContext::setDeviceCountry(std::string deviceCountry) {
        this->deviceCountry = deviceCountry;
}

std::string OpportunityContext::getDeviceCity() {
        return deviceCity;
}

void OpportunityContext::setDeviceCity(std::string deviceCity) {
        this->deviceCity = deviceCity;
}

std::string OpportunityContext::getDeviceState() {
        return deviceState;
}

void OpportunityContext::setDeviceState(std::string deviceState) {
        this->deviceState = deviceState;
}

std::string OpportunityContext::getDeviceZipcode() {
        return deviceZipcode;
}

void OpportunityContext::setDeviceZipcode(std::string deviceZipcode) {
        this->deviceZipcode = deviceZipcode;
}

std::vector<std::string> OpportunityContext::getSiteCategory() {
        return siteCategory;
}

void OpportunityContext::setSiteCategory(std::string siteCategory) {
        this->siteCategory.push_back(siteCategory);
}

std::string OpportunityContext::getSiteDomain() {
        return siteDomain;
}

void OpportunityContext::setSiteDomain(std::string siteDomain) {
        this->siteDomain = siteDomain;
}

std::string OpportunityContext::getSitePage() {
        return sitePage;
}

void OpportunityContext::setSitePage(std::string sitePage) {
        this->sitePage = sitePage;
}

std::string OpportunityContext::getSitePublisherDomain() {
        return sitePublisherDomain;
}

void OpportunityContext::setSitePublisherDomain(std::string sitePublisherDomain) {
        this->sitePublisherDomain = sitePublisherDomain;
}

std::string OpportunityContext::getSitePublisherName() {
        return sitePublisherName;
}

void OpportunityContext::setSitePublisherName(std::string sitePublisherName) {
        this->sitePublisherName = sitePublisherName;
}

std::string OpportunityContext::getAdSize() {
        return adSize;
}

void OpportunityContext::setAdSize(std::string adSize) {
        this->adSize = adSize;
}

std::vector<std::string> OpportunityContext::getCreativeAPI() {
        return creativeAPI;
}

void OpportunityContext::setCreativeAPI(std::vector<std::string> creativeAPI) {
        this->creativeAPI = creativeAPI;
}

bool OpportunityContext::IsSecureCreativeRequired() {
        return secureCreativeRequired;
}

void OpportunityContext::setSecureCreativeRequired(bool secureCreativeRequired) {
        this->secureCreativeRequired = secureCreativeRequired;
}

std::vector<std::string> OpportunityContext::getBlockedAttributes() {
        return blockedAttributes;
}

void OpportunityContext::setBlockedAttributes(std::vector<std::string> blockedAttributes) {
        this->blockedAttributes = blockedAttributes;
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > OpportunityContext::getBlockedBannerTypesMap() {
        return blockedBannerTypesMap;
}

void OpportunityContext::setBlockedBannerTypesMap(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedBannerTypesMap) {
        this->blockedBannerTypesMap = blockedBannerTypesMap;
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > OpportunityContext::getBlockedAdvertiserMap() {
        return blockedAdvertiserMap;
}

void OpportunityContext::setBlockedAdvertiserMap(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedAdvertiserMap) {
        this->blockedAdvertiserMap = blockedAdvertiserMap;
}

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > OpportunityContext::getBlockedIabCategoriesMap() {
        return blockedIabCategoriesMap;
}

void OpportunityContext::setBlockedIabCategoriesMap(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, gicapods::AtomicLong> > blockedIabCategoriesMap) {
        this->blockedIabCategoriesMap = blockedIabCategoriesMap;
}

std::string OpportunityContext::getInventory() {
        return inventory;
}

void OpportunityContext::setInventory(std::string inventory) {
        this->inventory = inventory;
}

std::string OpportunityContext::getRawBidRequest() {
        return rawBidRequest;
}

void OpportunityContext::setRawBidRequest(std::string rawBidRequest) {
        this->rawBidRequest = rawBidRequest;
}

std::vector<std::shared_ptr<TargetGroup> > OpportunityContext::getTargetGroups() {
        throwEx("delete this function later");
}

void OpportunityContext::setTargetGroups(std::vector<std::shared_ptr<TargetGroup> > targetGroups) {
        throwEx("delete this func later");
}

std::string OpportunityContext::getLastModuleRunInPipeline() {
        return lastModuleRunInPipeline;
}

void OpportunityContext::setLastModuleRunInPipeline(std::string lastModuleRunInPipeline) {
        this->lastModuleRunInPipeline = lastModuleRunInPipeline;
}

std::string OpportunityContext::getFinalCreativeContent() {
        return finalCreativeContent;
}

void OpportunityContext::setFinalCreativeContent(std::string finalCreativeContent) {
        this->finalCreativeContent = finalCreativeContent;
}

Poco::Net::NameValueCollection OpportunityContext::getCookieMapIncoming() {
        return cookieMapIncoming;
}

void OpportunityContext::setCookieMapIncoming(Poco::Net::NameValueCollection cookieMapIncoming) {
        this->cookieMapIncoming = cookieMapIncoming;
}

Poco::Net::NameValueCollection OpportunityContext::getCookieMapOutgoing() {
        return cookieMapOutgoing;
}

void OpportunityContext::setCookieMapOutgoing(Poco::Net::NameValueCollection cookieMapOutgoing) {
        this->cookieMapOutgoing = cookieMapOutgoing;
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > OpportunityContext::getAllAvailableTgs() {
        return allAvailableTgs;
}

void OpportunityContext::setAllAvailableTgs(std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > allAvailableTgs) {
        this->allAvailableTgs = allAvailableTgs;
}

std::shared_ptr<TargetGroup> OpportunityContext::getChosenTargetGroup() {
        return chosenTargetGroup;
}

void OpportunityContext::setChosenTargetGroup(std::shared_ptr<TargetGroup> chosenTargetGroup) {
        this->chosenTargetGroup = chosenTargetGroup;
}

std::shared_ptr<Creative> OpportunityContext::getChosenCreative() {
        return chosenCreative;
}

void OpportunityContext::setChosenCreative(std::shared_ptr<Creative> chosenCreative) {
        this->chosenCreative = chosenCreative;
}

std::shared_ptr<gicapods::Status> OpportunityContext::getStatus() {
        return status;
}

void OpportunityContext::setStatus(std::shared_ptr<gicapods::Status> status) {
        this->status = status;
}

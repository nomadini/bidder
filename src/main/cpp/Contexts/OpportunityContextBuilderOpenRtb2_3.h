//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef OpportunityContextBuilderOpenRtb2_3_H
#define OpportunityContextBuilderOpenRtb2_3_H

#include "OpenRtbBidRequest.h"
class OpportunityContext;
#include <memory>
#include <string>
class EntityToModuleStateStats;
#include "MaxMindService.h"
#include <tbb/concurrent_hash_map.h>
#include "CreativeAttribute.h"
#include "OpenRtbBidRequestParser.h"
#include "AtomicLong.h"
#include "Object.h"
#include "ConcurrentHashMap.h"
#include "IntWrapper.h"
#include "spdlog/spdlog.h"
#include "Poco/Net/HTTPClientSession.h"
class IpInfoFetcherModule;
class HttpUtilService;
class OpportunityContext;
class RandomUtil;
class DateTimeService;

#include "UaParser.h"

class OpportunityContextBuilderOpenRtb2_3 : public Object {

private:
std::shared_ptr<RandomUtil> transactionIdRandomizer;
std::shared_ptr<uacpp::UaParser> uaParser;
std::shared_ptr<gicapods::ConcurrentHashMap<std::string, IntWrapper> > timeZoneToOffsetWithUtcMap;
void setGeoLocationInfoToContext(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest,
                                 std::shared_ptr<OpportunityContext> opt);

std::shared_ptr<OpenRtb2_3_0::CreativeAttributeClass> creativeAttributeClass;

public:

IpInfoFetcherModule* ipInfoFetcherModule;
DateTimeService* dateTimeService;
std::shared_ptr<gicapods::AtomicLong> uniqueNumberOfContext;
EntityToModuleStateStats* entityToModuleStateStats;
OpenRtb2_3_0::OpenRtbBidRequestParser* openRtbBidRequestParser;
std::shared_ptr<spdlog::logger> async_us_ip_file_logger;
void parseUserAgent(std::shared_ptr<OpportunityContext> opt);

OpportunityContextBuilderOpenRtb2_3(EntityToModuleStateStats* entityToModuleStateStats);
virtual ~OpportunityContextBuilderOpenRtb2_3();


void configureContextFromBidRequest(
        std::shared_ptr<OpportunityContext> context,
        std::string requestBody
        );

void getIpInfoOverHttp(std::shared_ptr<OpportunityContext> opt);
bool isMobileRequest(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest);
void fetchUserTimeZone(std::shared_ptr<OpportunityContext> opt);
void recordStatesForDebugging(std::shared_ptr<OpportunityContext> opt);
void logTheUSipForBookKeeping(std::shared_ptr<OpportunityContext> opt);

std::string convertMapToJsonArray (gicapods::ConcurrentHashMap<std::string, IntWrapper>& map);

void setDeviceForContext(
        std::shared_ptr<OpportunityContext> opt,
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest);
};


#endif //BIDDER_OPPORTUNITYCONTEXTBUILDER_H

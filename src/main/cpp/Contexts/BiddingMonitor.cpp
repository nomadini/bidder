//
// Created by Mahmoud Taabodi on 5/7/16.
//

#include "BiddingMonitor.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidRequestParser.h"

#include "JsonArrayUtil.h"
#include "BidderMainPipelineProcessor.h"
#include "SignalHandler.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "Device.h"
#include "BiddingMode.h"
#include "BiddingMonitor.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "EventLog.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "StringUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "AdPositionConverter.h"
#include "CreativeAPIConverter.h"
#include "BannerAdTypeConverter.h"
#include "DeviceTypeConverter.h"
#include "CollectionUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "OpenRtbVersion.h"
#include "OpportunityContext.h"
#include "AtomicLong.h"
#include "HttpUtilService.h"
#include "Poco/URI.h"

BiddingMonitor::BiddingMonitor(
        EntityToModuleStateStats* entityToModuleStateStats,
        int failureThresholdPercentageArg,
        std::string nameOfMonitor) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        numberOfFailures = std::make_shared<gicapods::AtomicLong>();
        numberOfTotal = std::make_shared<gicapods::AtomicLong>();
        this->nameOfMonitor = nameOfMonitor;

        assertAndThrow(failureThresholdPercentageArg >= 0);
        assertAndThrow(failureThresholdPercentageArg <= 100);
        failureThresholdPercentage = std::make_shared<gicapods::AtomicLong>(failureThresholdPercentageArg);

}


std::string BiddingMonitor::getName() {
        return nameOfMonitor;
}

void BiddingMonitor::resetMeasures() {
        numberOfFailures->setValue(0);
        numberOfTotal->setValue(0);
}

bool BiddingMonitor::isFailing() {

        if (numberOfTotal->getValue() == 0 || numberOfTotal->getValue() < 100) {
                //we return false for the first 100 requests
                return false;
        }
        double actualPercentageFailure = (numberOfFailures->getValue() * 100 ) / numberOfTotal->getValue();
        if (actualPercentageFailure >= failureThresholdPercentage->getValue()) {
                LOG_EVERY_N(ERROR, 1) <<
                        "nameOfMonitor : "<< nameOfMonitor<<
                        "actualPercentageFailure: " << actualPercentageFailure <<
                        "numberOfFailures: " << numberOfFailures->getValue() <<
                        "numberOfTotal: " << numberOfTotal->getValue() <<
                        "failureThresholdPercentage: " << failureThresholdPercentage->getValue();
                return true;
        }

        return false;
}

BiddingMonitor::~BiddingMonitor() {

}

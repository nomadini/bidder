//
// Created by Mahmoud Taabodi on 5/7/16.
//

#include "OpportunityContextBuilderOpenRtb2_3.h"
#include "OpenRtbBidRequest.h"
#include "OpenRtbBidRequestParser.h"

#include "JsonArrayUtil.h"
#include "Creative.h"
#include "BidderMainPipelineProcessor.h"
#include "JsonMapUtil.h"
#include "SignalHandler.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "Device.h"
#include "BiddingMode.h"
#include "OpportunityContextBuilderOpenRtb2_3.h"
#include "JsonUtil.h"
#include "DateTimeUtil.h"
#include "DateTimeService.h"
#include "EventLog.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "IpInfoFetcherModule.h"
#include "StringUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "HttpUtil.h"
#include "RandomUtil.h"
#include "AdPositionConverter.h"
#include "CreativeAPIConverter.h"
#include "BannerAdTypeConverter.h"
#include "DeviceTypeConverter.h"
#include "CollectionUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "OpenRtbVersion.h"
#include "OpportunityContext.h"
#include "AtomicLong.h"
#include "HttpUtilService.h"
#include "EventLog.h"
#include "UaParser.h"
#include "DomainUtil.h"
#include "Poco/URI.h"
#include "spdlog/spdlog.h"

OpportunityContextBuilderOpenRtb2_3::OpportunityContextBuilderOpenRtb2_3(
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        transactionIdRandomizer = std::make_shared<RandomUtil> (100000000);
        this->entityToModuleStateStats = entityToModuleStateStats;
        //TODO : configDriven
        uaParser = uacpp::UaParser::instance();


        timeZoneToOffsetWithUtcMap = std::make_shared<gicapods::ConcurrentHashMap<std::string, IntWrapper> >();
        this->creativeAttributeClass = std::make_shared<OpenRtb2_3_0::CreativeAttributeClass>();
        this->creativeAttributeClass->initEnumMaps();

        // async_us_ip_file_logger = spdlog::daily_logger_st("async_us_ip_file_logger", "/tmp/logs/us_ip_log.txt");
        // size_t q_size = 4096;//queue size must be power of 2
        // // spdlog::set_pattern("*** [%H:%M:%S %z] [thread %t] %v ***");
        // spdlog::set_pattern("%v");
        // spdlog::set_async_mode(q_size);
}

OpportunityContextBuilderOpenRtb2_3::~OpportunityContextBuilderOpenRtb2_3() {

}

void OpportunityContextBuilderOpenRtb2_3::configureContextFromBidRequest(
        std::shared_ptr<OpportunityContext> context,
        std::string requestBody) {

        Poco::Timestamp parseBidRequestTime;
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest =
                openRtbBidRequestParser->parseBidRequest (requestBody);
        MLOG(5) << "parseBidRequest took " << parseBidRequestTime.elapsed () / 1000 <<" milis ";



        // MLOG(4) << "bid request body " << openRtbBidRequest->toJson ();

        entityToModuleStateStats->addStateModuleForEntity(
                "CONTEXT_BUILDING_ATTEMPTS",
                "OpportunityContextBuilderOpenRtb2_3",
                "ALL");

        uniqueNumberOfContext->increment();
        context->uniqueNumberOfContext = uniqueNumberOfContext;

        setDeviceForContext(context, openRtbBidRequest);
        if (context->requestIsParsedOnlyForForwarding) {
                //we only parse the process the device part...for forwarding purposes
                return;
        }


        context->exchangeUserId = openRtbBidRequest->user->id;

        if (context->exchangeUserId.empty() && context->device == nullptr) {
                entityToModuleStateStats->addStateModuleForEntity("NO_KNOWN_USER_NO_EXCHANGE_ID", "OpportunityContextBuilderOpenRtb2_3", "ALL");
                context->status->value = STOP_PROCESSING;
        }

        context->creativeAPI = OpenRtb2_3_0::CreativeAPIConverter::convertApiToName (openRtbBidRequest->imp->banner->api, "2.3");
        context->impressionType = "Banner"; //video, app or banner

        context->adPosition = OpenRtb2_3_0::AdPositionConverter::convert (
                openRtbBidRequest->imp->banner->pos,
                OpenRtbVersion::V2_3);

        context->deviceUserAgent = openRtbBidRequest->deviceOpenRtb->ua;
        parseUserAgent(context);
        context->deviceIp = openRtbBidRequest->deviceOpenRtb->ip;
        context->deviceLat = openRtbBidRequest->deviceOpenRtb->geo->lat;
        context->deviceLon = openRtbBidRequest->deviceOpenRtb->geo->lon;

        context->creativeContentType = Creative::CONTENT_TYPE_DISPLAY;
        context->environmentType = "DESKTOP"; //versus MOBILE  for mobile bid requests
        context->exchangeId = "exchangeA"; // TODO : separate bid parsing logics for different exchanges later and different enviornments
        setGeoLocationInfoToContext(openRtbBidRequest, context);


        context->deviceZipcode = openRtbBidRequest->deviceOpenRtb->geo->zip;
        assertAndThrow(!context->deviceZipcode.empty());
        context->protocolVersion = "OpenRtb2.3";

        for(std::string adv : openRtbBidRequest->badv) {
                auto badv = DomainUtil::getDomainFromUrlString(adv);
                context->addBlockedAdvertiser (badv);
        }

        MLOG(2) <<"context->getBlockedAdvertiserMap() size "<<context->getBlockedAdvertiserMap()->size();

        for(std::string cat : openRtbBidRequest->bcat) {
                context->addBlockedIabCategory (cat);
        }

        MLOG(2) <<"context->getBlockedIabCategoriesMap() size "<<context->getBlockedIabCategoriesMap()->size();

        for(int attr : openRtbBidRequest->imp->banner->battr) {
                auto pairPtr = creativeAttributeClass->intToNameCreativeAttributesMap.find (attr);
                if (pairPtr != creativeAttributeClass->intToNameCreativeAttributesMap.end ()) {
                        context->blockedAttributes.push_back (pairPtr->second);
                } else {
                        throwEx( "creative attribute was not found in map :  " + attr);
                }

        }
        MLOG(2) <<"context->getBlockedBannerTypesMap() size "<<context->getBlockedBannerTypesMap()->size();

        std::string allBlockedBannerTypesAsStr;
        for(int type :  openRtbBidRequest->imp->banner->btype) {
                std::string bannerAdType = OpenRtb2_3_0::BannerAdTypeConverter::convert (type, "2.3");
                allBlockedBannerTypesAsStr.append(bannerAdType + "_");
                context->addBlockedBannerType (bannerAdType);
        }

        context->allBlockedBannerTypesAsStr = allBlockedBannerTypesAsStr;

        context->siteCategory.push_back(openRtbBidRequest->site->cat.at (0));
        context->siteDomain = openRtbBidRequest->site->domain;
        assertAndThrow(!context->siteDomain.empty());
        context->sitePage = openRtbBidRequest->site->page;
        context->sitePublisherDomain = openRtbBidRequest->site->publisher->domain;
        context->sitePublisherName = openRtbBidRequest->site->publisher->name;

        context->setAdSize(StringUtil::toStr(openRtbBidRequest->imp->banner->w)
                           + "x" +
                           StringUtil::toStr(openRtbBidRequest->imp->banner->h));

        context->setLastModuleRunInPipeline("NONE_YET");

        context->bidFloor = openRtbBidRequest->imp->bidFloor;

        if (openRtbBidRequest->imp->secure == 1) {
                context->secureCreativeRequired = true;
        } else {
                context->secureCreativeRequired = false;
        }

        context->rawBidRequest = requestBody;

        recordStatesForDebugging(context);
        entityToModuleStateStats->addStateModuleForEntity("CONTEXT_BUILDING_SUCCESS", "OpportunityContextBuilderOpenRtb2_3", "ALL");
}

bool OpportunityContextBuilderOpenRtb2_3::isMobileRequest(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest) {
        return false; //TODO : fix this
}

void OpportunityContextBuilderOpenRtb2_3::parseUserAgent(std::shared_ptr<OpportunityContext> context) {

        auto uaParsed = uaParser->get(context->deviceUserAgent);
        MLOG(3) << "uaParsed : "<< uaParsed->ua.toFullString()
                << "os family : "<< uaParsed->ua.os.family
                << "Browser : "<< uaParsed->ua.browser.toString();;

        if (
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "iOs") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Mac OS")
                ) {
                context->deviceOsFamily = Device::GENERAL_BIDDING_DEVICE_OS_TYPE_MAC;
        } else if (StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "windows")) {
                context->deviceOsFamily = Device::GENERAL_BIDDING_DEVICE_OS_TYPE_WINDOWS;
        } else if (
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "linux") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "solaris") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "FreeBSD") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "SUSE") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Gentoo") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Slackware") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "NetBSD") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "OpenBSD") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Mandriva") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "CentOS") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Chrome OS") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "red hat") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Android") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Debian") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Fedora") ||
                StringUtil::containsCaseInSensitive(uaParsed->ua.os.family, "Ubuntu")) {
                context->deviceOsFamily = Device::GENERAL_BIDDING_DEVICE_OS_TYPE_LINUX;
        } else {

                if (!uaParsed->ua.os.family.empty() &&
                    !StringUtil::equalsIgnoreCase(uaParsed->ua.os.family, "Other")) {
                        LOG_EVERY_N(ERROR, 10)<< "setting os family as unknown for os family"<< uaParsed->ua.os.family;
                }
                context->deviceOsFamily = "UNKNOWN";
        }
// ASSERT_EQ("Mobile Safari", uagent.browser.family);

// ASSERT_EQ("5", uagent.browser.major);
// ASSERT_EQ("1", uagent.browser.minor);
// ASSERT_EQ("", uagent.browser.patch);
// ASSERT_EQ("Mobile Safari 5.1.0", uagent.browser.toString());
// ASSERT_EQ("5.1.0", uagent.browser.toVersionString());
//
// ASSERT_EQ("iOS", uagent.os.family);
// ASSERT_EQ("5", uagent.os.major);
// ASSERT_EQ("1", uagent.os.minor);
// ASSERT_EQ("1", uagent.os.patch);
// ASSERT_EQ("iOS 5.1.1", uagent.os.toString());
// ASSERT_EQ("5.1.1", uagent.os.toVersionString());
//
// ASSERT_EQ("Mobile Safari 5.1.0/iOS 5.1.1", uagent.toFullString());
//
// ASSERT_EQ("iPhone", uagent.device.family);
//
// ASSERT_FALSE(uagent.isSpider());

}

void OpportunityContextBuilderOpenRtb2_3::getIpInfoOverHttp(std::shared_ptr<OpportunityContext> context) {

        std::string responseFromMaxmindService = ipInfoFetcherModule->getIpInfoOverHttp(context);
        if (responseFromMaxmindService.empty()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "NO_RESPONSE_FROM_IP_FETCHER",
                        "OpportunityContextBuilderOpenRtb2_3",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );
                context->status->value = STOP_PROCESSING;
                return;

        }
        auto doc = parseJsonSafely(responseFromMaxmindService);
        context->deviceCity= JsonUtil::GetStringSafely(*doc,"deviceCity");
        context->deviceCountry = JsonUtil::GetStringSafely(*doc, "deviceCountry");
        context->deviceState = JsonUtil::GetStringSafely(*doc,"deviceState");
        context->userTimeZone = JsonUtil::GetStringSafely(*doc,"userTimeZone");
        LOG_EVERY_N(INFO, 100000)<< google::COUNTER<<"th responseFromMaxmindService : "
                                 << responseFromMaxmindService;
}

void OpportunityContextBuilderOpenRtb2_3::setGeoLocationInfoToContext(std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest,
                                                                      std::shared_ptr<OpportunityContext> context) {
        //soosan : todo .. compare the bid request geo info with max mind and fill the lacking info with max mind data or
        //discard the bid request info if needed //TODO : make this its own class later


        if (isMobileRequest(openRtbBidRequest)) {
                context->deviceCountry =
                        StringUtil::replaceStringCaseInsensitive(openRtbBidRequest->deviceOpenRtb->geo->country, "'", "");
                context->deviceCity =
                        StringUtil::replaceStringCaseInsensitive(openRtbBidRequest->deviceOpenRtb->geo->city, "'", "");
                //TODO : get this from a hash map in memory that comes from DB
                //getTimeZone based on city
                context->deviceState = "unknown";
        } else {
                Poco::Timestamp time;
                getIpInfoOverHttp(context);
                LOG_EVERY_N(INFO, 1000000) << google::COUNTER <<"th getIpInfoOverHttp took " << time.elapsed () / 1000 <<" milis ";
        }

        context->bidRequestId = openRtbBidRequest->id;
        context->impressionIdInBidRequest = openRtbBidRequest->imp->id;
        // context->setPublisherId(openRtbBidRequest->site->publisher->id);

        Poco::Timestamp time;
        int randomNumber = transactionIdRandomizer->randomNumberV2();
        LOG_EVERY_N(INFO, 1000000) << google::COUNTER<<"th randomNumberV2 took " << time.elapsed () / 1000 <<" milis ";

        // this will be the eventLog in Json Format
        context->setTransactionId(openRtbBidRequest->id + StringUtil::toStr(randomNumber));

        context->setDeviceUserAgent(openRtbBidRequest->deviceOpenRtb->ua);

        if (!StringUtil::equalsIgnoreCase(context->deviceCountry, "US")
            && !StringUtil::equalsIgnoreCase(context->deviceCountry, "USA")
            ) {
                MLOG(3)<<"bid request coming from outside US "<< context->deviceCountry;
                //we don't bid on requests coming from other than US
                entityToModuleStateStats->addStateModuleForEntity("NON_US_REQUEST",
                                                                  "OpportunityContextBuilderOpenRtb2_3",
                                                                  "ALL",
                                                                  EntityToModuleStateStats::exception);
                context->status->value = STOP_PROCESSING;
                return;
        } else {

                //logTheUSipForBookKeeping(context);
                //this is the standard USA name
                context->deviceCountry = "USA";
        }

        fetchUserTimeZone(context);
        if (context->deviceCountry.empty() ) {
                context->deviceCountry = "unknown";
        }
        if (context->deviceState.empty() ) {
                context->deviceState = "unknown";
        }
        if (context->deviceCity.empty() ) {
                context->deviceCity = "unknown";
        }


}


void OpportunityContextBuilderOpenRtb2_3::logTheUSipForBookKeeping(
        std::shared_ptr<OpportunityContext> context) {
        //we logged a bunch of us ips for exchange simulator
        // async_us_ip_file_logger->info(context->deviceIp);

}

void OpportunityContextBuilderOpenRtb2_3::fetchUserTimeZone(
        std::shared_ptr<OpportunityContext> context)
{
        Poco::Timestamp timeZoneLookUpTime;
        if (context->userTimeZone.empty()) {
                entityToModuleStateStats->addStateModuleForEntity(
                        "USER_TIME_ZONE_NOT_DEFINED",
                        "OpportunityContextBuilderOpenRtb2_3",
                        "ALL",
                        EntityToModuleStateStats::exception
                        );
                context->status->value = STOP_PROCESSING;
                return;

        }
        auto timeZoneValue = timeZoneToOffsetWithUtcMap->getOptional(context->userTimeZone);
        if (timeZoneValue != nullptr) {
                context->userTimeZonDifferenceWithUTC = timeZoneValue->getValue();
        } else {
                context->userTimeZonDifferenceWithUTC = dateTimeService->getUtcOffsetBasedOnTimeZone(context->userTimeZone);
                auto value = std::make_shared<IntWrapper>();
                value->setValue(context->userTimeZonDifferenceWithUTC);
                timeZoneToOffsetWithUtcMap->put(context->userTimeZone, value);

        }

        auto jsonArray = convertMapToJsonArray(*timeZoneToOffsetWithUtcMap);

        MLOG(4) << google::COUNTER
                << "th time : timeZoneLookUpTime took : "
                << (timeZoneLookUpTime.elapsed () / 1000) << "  milis "
                << "jsonArray : "<< jsonArray;


}

std::string OpportunityContextBuilderOpenRtb2_3::convertMapToJsonArray (
        gicapods::ConcurrentHashMap<std::string, IntWrapper>& map) {
        auto doc = JsonArrayUtil::createDocumentAsArray ();

        //we create a vector from map and sort it...to send a sorted json back every time
        // typename std::vector<std::pair<K, V> > sorted_elements(map.begin(), map.end());
        // std::sort(sorted_elements.begin(), sorted_elements.end());
        auto newMap = map.getCopyOfMap();
        for(auto elemnt : *newMap) {

                RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
                RapidJsonValueTypeNoRef keyJsonValue(rapidjson::kObjectType);

                keyJsonValue.SetString (elemnt.first.c_str (), elemnt.first.size (), doc->GetAllocator ());

                RapidJsonValueTypeNoRef valueJsonValue(rapidjson::kObjectType);
                JsonUtil::populateValue(valueJsonValue, elemnt.second, doc.get());


                JsonUtil::addMemberToValue_FromPair(
                        doc.get(),
                        "key", keyJsonValue, value);

                JsonUtil::addMemberToValue_FromPair(
                        doc.get(),
                        "value", valueJsonValue, value);

                doc->PushBack (value, doc->GetAllocator ());
        }

        return JsonUtil::docToString (doc.get());
}


void OpportunityContextBuilderOpenRtb2_3::setDeviceForContext(
        std::shared_ptr<OpportunityContext> context,
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidRequest> openRtbBidRequest) {
        assertAndThrow(!context->exchangeName.empty());
        context->deviceType = DeviceTypeConverter::convert
                                      (openRtbBidRequest->deviceOpenRtb->deviceType, OpenRtbVersion::V2_3);
        if (openRtbBidRequest->user->buyeruid.empty()) {
                //exchange has not sent us our id or the user is unknown to us
                entityToModuleStateStats->addStateModuleForEntity(
                        "UNMAPPED_USER_FROM-exchange" + context->exchangeName,
                        "OpportunityContextBuilderOpenRtb2_3",
                        "ALL");
        } else {
                //we rebuild the device to make sure id sent to us is in correct format
                context->device = std::make_shared<Device> (openRtbBidRequest->user->buyeruid, context->deviceType);

                entityToModuleStateStats->addStateModuleForEntity("MAPPED_USER", "OpportunityContextBuilderOpenRtb2_3", "ALL");
        }
}

void OpportunityContextBuilderOpenRtb2_3::recordStatesForDebugging(std::shared_ptr<OpportunityContext> context) {

        return;//don't do anything for now
        entityToModuleStateStats->addStateModuleForEntity("CREATIVE_SECURE_REQUIRED:" +StringUtil::toStr(context->secureCreativeRequired),
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");

        entityToModuleStateStats->addStateModuleForEntity("SITE_NAME : " + context->siteDomain,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");


        entityToModuleStateStats->addStateModuleForEntity("DEVICE_TYPE : " + context->deviceType,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");


        entityToModuleStateStats->addStateModuleForEntity("AD_SIZE : " + context->adSize,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");


        entityToModuleStateStats->addStateModuleForEntity("DEVICE_COUNTRY : " + context->deviceCountry,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");
        entityToModuleStateStats->addStateModuleForEntity("DEVICE_CITY : " + context->deviceCity,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");

        entityToModuleStateStats->addStateModuleForEntity("DEVICE_STATE : " + context->deviceState,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");
        entityToModuleStateStats->addStateModuleForEntity("DEVICE_TIME_ZONE : " + context->userTimeZone,
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");

        entityToModuleStateStats->addStateModuleForEntity("CREATIVE_API:" + JsonArrayUtil::convertListToJson(context->creativeAPI),
                                                          "OpportunityContextBuilderOpenRtb2_3",
                                                          "ALL");

}

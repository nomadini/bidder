//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef IpInfoFetcherModule_H
#define IpInfoFetcherModule_H

#include "OpenRtbBidRequest.h"
class OpportunityContext;
#include <memory>
#include <string>
class EntityToModuleStateStats;
#include "MaxMindService.h"
#include <tbb/concurrent_hash_map.h>
#include "CreativeAttribute.h"
#include "OpenRtbBidRequestParser.h"
#include "AtomicLong.h"
#include "Object.h"
#include "BiddingMonitor.h"
#include "ConfigService.h"

#include "Poco/Net/HTTPClientSession.h"
class HttpUtilService;
class PocoSession;
class OpportunityContext;
class RandomUtil;

class IpInfoFetcherModule : public BiddingMonitor, public Object {

private:

std::shared_ptr<PocoSession> session;

std::string ipInfoFetcherAppHostUrl;

public:
EntityToModuleStateStats* entityToModuleStateStats;

IpInfoFetcherModule(EntityToModuleStateStats* entityToModuleStateStats,
                    gicapods::ConfigService* configService,
                    int failureThresholdPercentageArg);
virtual ~IpInfoFetcherModule();


std::string getIpInfoOverHttp(std::shared_ptr<OpportunityContext> opt);
};

#endif //BIDDER_OPPORTUNITYCONTEXTBUILDER_H

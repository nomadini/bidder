#ifndef BidRequestHandler_H
#define BidRequestHandler_H

#include "SignalHandler.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class OpportunityContext;
class TargetGroup;
class BidderMainPipelineProcessor;
class BidRequestHandlerPipelineProcessor;
class OpportunityContextBuilder;
class EntityToModuleStateStats;
class BidResponseBuilder;
class TargetGroupCacheService;
class BidModeControllerService;
class BidRequestHandlerPipelineProcessor;
class BidderLatencyRecorder;
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "Object.h"
class BidRequestHandler;


class BidRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:

BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor;
BidderLatencyRecorder* bidderLatencyRecorder;
std::shared_ptr<OpportunityContext> context;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicLong> totalNumberOfRequestsProcessedInFilterCountPeriod;

std::shared_ptr<gicapods::AtomicLong> numberOfRequestsProcessingNow;
BidRequestHandler(
        std::shared_ptr<OpportunityContext> opportunityContext,
        BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor,
        EntityToModuleStateStats* entityToModuleStateStats);

void handleRequest(Poco::Net::HTTPServerRequest& request,
                   Poco::Net::HTTPServerResponse& response);

void handleRequestLevel1(Poco::Net::HTTPServerRequest &request,
                         Poco::Net::HTTPServerResponse &response);
bool throttleBidRequests();

void logTimeAndCounts(Poco::Timestamp &timeToProcessRequest);

void handleException(std::exception const &e);

virtual ~BidRequestHandler();
};

#endif

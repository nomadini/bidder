#ifndef BidderCommonRequestHandlerFactory_H
#define BidderCommonRequestHandlerFactory_H

#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"


#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
class EntityToModuleStateStats;
class BidderCommonRequestHandlerFactory;
class DataMasterRequestHandlerFactory;
class EntityDeliveryInfoCacheService;
class MySqlMetricService;

namespace gicapods {
class ConfigService;
class BootableConfigService;
}



class BidderCommonRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {



public:
EntityToModuleStateStats* entityToModuleStateStats;
gicapods::ConfigService* configService;

MySqlMetricService* mySqlMetricService;
EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService;
MySqlTargetGroupFilterCountDbRecordService* mySqlTargetGroupFilterCountDbRecordService;
MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService;
BidderCommonRequestHandlerFactory();

static std::shared_ptr<BidderCommonRequestHandlerFactory> shared_instance(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService,
        MySqlMetricService* mySqlMetricService,
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        MySqlTargetGroupFilterCountDbRecordService* mySqlTargetGroupFilterCountDbRecordService,
        MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService
        );

Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);

Poco::Net::HTTPRequestHandler* evaluetePersistentMetricsRequestHanlder(const Poco::Net::HTTPServerRequest &request);
Poco::Net::HTTPRequestHandler* evalueteEntityDeliveryInfoCacheRequestHandler(const Poco::Net::HTTPServerRequest &request);
};

#endif

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef ThrottlerModule_H_
#define ThrottlerModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "ConcurrentHashSet.h"

class ThrottlerModule : public BidderModule, public Object {

private:

public:
std::shared_ptr<gicapods::AtomicLong> numberOfBidRequestRecievedPerMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfbidsRequestAllowedToProcessPerSecond;
virtual std::string getName();

ThrottlerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool throttleBidRequests();

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef ProcessorInvokerModule_H_
#define ProcessorInvokerModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
class OpportunityContextBuilder;
class BidderMainPipelineProcessor;
class TargetGroup;
class TargetGroupCacheService;
class BidderLatencyRecorder;
#include <memory>
#include <string>
#include "EntityProviderService.h"
#include "BidderModule.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "ConcurrentHashSet.h"

class ProcessorInvokerModule : public BidderModule, public Object {

private:

public:
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetGroupsEligibleForBidding;
BidderMainPipelineProcessor* bidderMainPipelineProcessor;
TargetGroupCacheService* targetGroupCacheService;
BidderLatencyRecorder* bidderLatencyRecorder;

virtual std::string getName();
ProcessorInvokerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "ThrottlerModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "HttpUtil.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "BidRequestHandler.h"

std::string ThrottlerModule::getName() {
        return "ThrottlerModule";
}

ThrottlerModule::ThrottlerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        numberOfbidsRequestAllowedToProcessPerSecond = std::make_shared<gicapods::AtomicLong>();
}

void ThrottlerModule::process(std::shared_ptr<OpportunityContext> context) {
        if(throttleBidRequests() == true) {
                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsThrottled",
                                                                   "ThrottlerModule",
                                                                   "ALL");
                LOG_EVERY_N(ERROR, 1000)<<google::COUNTER<<"th throttling bid requests";

                context->finalBidResponseContent = "";
                context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_NO_CONTENT;
                context->status->value = STOP_PROCESSING;
                return;
        }

        //this should be right after throttle function,
        //this will help us count the number of requests recieved and compare against the number
        //we are allowed to process
        numberOfBidRequestRecievedPerMinute->increment();
}

bool ThrottlerModule::throttleBidRequests() {
        return numberOfBidRequestRecievedPerMinute->getValue() >= numberOfbidsRequestAllowedToProcessPerSecond->getValue();
}
bool ThrottlerModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

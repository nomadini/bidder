
#include "Status.h"
#include "RandomSampler.h"
#include "AtomicLong.h"
#include "StringUtil.h"
#include "TargetGroupGeoSegmentFilter.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "BidderModule.h"
#include "EntityToModuleStateStats.h"
#include "BidderMainPipelineProcessor.h"
#include "OpportunityContext.h"
#include "TargetGroupFilterChainModule.h"
#include <memory>
#include <string>
#include "BidderModule.h"
#include "EntityToModuleStateStats.h"
#include "ConfigService.h"
#include "EntityToModuleStateStats.h"
#include "TgFilterMeasuresService.h"
#include "BidderLatencyRecorder.h"
#include "TgMarker.h"

BidRequestHandlerPipelineProcessor::BidRequestHandlerPipelineProcessor() : Object(__FILE__) {
        maxAllowedTimeToProcessARequestInMillis = std::make_shared<gicapods::AtomicLong> ();
}

void BidRequestHandlerPipelineProcessor::setTheModuleList()  {
        preBidModules.push_back(throttlerModule);
        preBidModules.push_back(noBidModeEnforcerModule);
        preBidModules.push_back(bidProbabilityEnforcerModule);
        preBidModules.push_back(contextPopulatorModule);
        preBidModules.push_back(processorInvokerModule);
        preBidModules.push_back(bidResponseCreatorModule);
        preBidModules.push_back(bidderWiretapModule);
}

void BidRequestHandlerPipelineProcessor::process(std::shared_ptr<OpportunityContext> opt) {

        entityToModuleStateStats->addStateModuleForEntity("#OfProcessingBidRequests",
                                                          "BidRequestHandlerPipelineProcessor",
                                                          "ALL",
                                                          EntityToModuleStateStats::important);

        Poco::Timestamp timeToProcessRequest;

        for (std::vector<BidderModule*>::iterator it = preBidModules.begin ();
             it != preBidModules.end (); ++it) {
                BidderMainPipelineProcessor::runModule(*it,
                                                       opt,
                                                       bidderLatencyRecorder);

                if (opt->status->value == STOP_PROCESSING) {
                        opt->lastModuleRunInPipeline = (*it)->getName ();
                        entityToModuleStateStats->addStateModuleForEntity("LastModule-" + (*it)->getName (), "BidRequestHandlerPipelineProcessor", "ALL");
                        break;
                }

                long pipelineTookInMillis = timeToProcessRequest.elapsed () / 1000;
                if (pipelineTookInMillis > maxAllowedTimeToProcessARequestInMillis->getValue()) {
                        opt->lastModuleRunInPipeline = (*it)->getName ();
                        entityToModuleStateStats->addStateModuleForEntity(
                                "Yanking_Request",
                                "BidRequestHandlerPipelineProcessor",
                                "ALL",
                                EntityToModuleStateStats::important);
                        LOG_EVERY_N(ERROR, 1000)<<google::COUNTER<<"th yanking in pipeline, pipelineTookInMillis : "<< pipelineTookInMillis;
                        break;
                }
        }



}

std::string BidRequestHandlerPipelineProcessor::getName() {
        return "BidRequestHandlerPipelineProcessor";
}

BidRequestHandlerPipelineProcessor::~BidRequestHandlerPipelineProcessor() {

}

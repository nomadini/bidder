/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BidderWiretapModule_H_
#define BidderWiretapModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "Object.h"
#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
class TargetGroup;
class TargetGroupCacheService;
class WiretapCassandraService;

#include <memory>
#include <string>
#include "BidderModule.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "LoadPercentageBasedSampler.h"
#include "ConcurrentHashSet.h"

class BidderWiretapModule : public BidderModule, public Object {

private:

public:
WiretapCassandraService* wiretapCassandraService;
std::unique_ptr<LoadPercentageBasedSampler> wiretapBidRequestResponseSampler;

virtual std::string getName();
BidderWiretapModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

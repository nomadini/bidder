/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BidProbabilityEnforcerModule_H_
#define BidProbabilityEnforcerModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;

class StatisticsUtil;
class EntityToModuleStateStats;
class BidModeControllerService;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "AtomicLong.h"

class BidProbabilityEnforcerModule : public BidderModule, public Object {

private:

public:
std::shared_ptr<gicapods::AtomicLong> bidPercentage;
std::shared_ptr<StatisticsUtil> statisticsUtil;

virtual std::string getName();

BidProbabilityEnforcerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

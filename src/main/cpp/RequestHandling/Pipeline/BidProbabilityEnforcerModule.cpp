#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "BidProbabilityEnforcerModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "HttpUtil.h"
#include "ConverterUtil.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "BidRequestHandler.h"
#include "StatisticsUtil.h"
#include "StringUtil.h"
#include "BidModeControllerService.h"


std::string BidProbabilityEnforcerModule::getName() {
        return "BidProbabilityEnforcerModule";
}

BidProbabilityEnforcerModule::BidProbabilityEnforcerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {
        statisticsUtil = std::make_shared<StatisticsUtil>();
}

void BidProbabilityEnforcerModule::process(std::shared_ptr<OpportunityContext> context) {
        double probabilityToBid = ConverterUtil::TO <long, double>(this->bidPercentage->getValue()) / 100;
        MLOG(10)<<"Processing probabilityToBid : "<<probabilityToBid;
        if (!statisticsUtil->happensWithProb(probabilityToBid)) {

                entityToModuleStateStats->addStateModuleForEntity ("NO_BID_BECAUSE_IN_SLOW_BIDE_MODE",
                                                                   "BidRequestHandler",
                                                                   "ALL");
                context->finalBidResponseContent =  "";
                context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_NO_CONTENT;
                context->status->value = STOP_PROCESSING;
        }

}

bool BidProbabilityEnforcerModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

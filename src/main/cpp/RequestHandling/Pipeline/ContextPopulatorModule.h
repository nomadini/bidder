/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef ContextPopulatorModule_H_
#define ContextPopulatorModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;
#include "Object.h"
class EntityToModuleStateStats;
class OpportunityContextBuilder;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "ConcurrentHashSet.h"

class ContextPopulatorModule : public BidderModule, public Object {

private:

public:
virtual std::string getName();
OpportunityContextBuilder* opportunityContextBuilder;
ContextPopulatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);


};

#endif

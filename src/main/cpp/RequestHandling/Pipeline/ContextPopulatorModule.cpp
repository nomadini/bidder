#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "ContextPopulatorModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "OpportunityContextBuilder.h"

std::string ContextPopulatorModule::getName() {
        return "ContextPopulatorModule";
}

ContextPopulatorModule::ContextPopulatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

}

void ContextPopulatorModule::process(std::shared_ptr<OpportunityContext> context) {
        opportunityContextBuilder->configureContextFromBidRequest
                (context,
                *context->request,
                context->openRtbVersion,
                context->exchangeName);

}

bool ContextPopulatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

#ifndef BidRequestHandlerPipelineProcessor_H
#define BidRequestHandlerPipelineProcessor_H

class OpportunityContext;
#include <memory>
#include <string>
#include "AtomicLong.h"
#include "BidderModule.h"
#include "Object.h"
#include "Poco/Timestamp.h"
class EntityToModuleStateStats;
class BidderLatencyRecorder;
namespace gicapods {class ConfigService; }

class ModuleContainer;
class EntityToModuleStateStats;

class BidRequestHandlerPipelineProcessor : public Object {
public:
std::shared_ptr<gicapods::AtomicLong> maxAllowedTimeToProcessARequestInMillis;
BidderModule* throttlerModule;
BidderModule* noBidModeEnforcerModule;
BidderModule* bidProbabilityEnforcerModule;
BidderModule* contextPopulatorModule;
BidderModule* processorInvokerModule;
BidderModule* bidResponseCreatorModule;
BidderModule* bidderWiretapModule;
BidderLatencyRecorder* bidderLatencyRecorder;

std::vector<BidderModule*> preBidModules;
EntityToModuleStateStats* entityToModuleStateStats;
BidRequestHandlerPipelineProcessor();

void setTheModuleList();
virtual void process(std::shared_ptr<OpportunityContext> opt);

std::string getName();

virtual ~BidRequestHandlerPipelineProcessor();
};

#endif

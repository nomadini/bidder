/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef NoBidModeEnforcerModule_H_
#define NoBidModeEnforcerModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;

#include "CollectionUtil.h"
class OpportunityContext;
#include "Object.h"
class EntityToModuleStateStats;
class BidModeControllerService;
#include <memory>
#include <string>
#include "BidderModule.h"
#include "AtomicBoolean.h"

class NoBidModeEnforcerModule : public BidderModule, public Object {

private:

public:

BidModeControllerService* bidModeControllerService;
virtual std::string getName();

NoBidModeEnforcerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);

};

#endif

#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "NoBidModeEnforcerModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "HttpUtil.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "BidRequestHandler.h"
#include "StringUtil.h"
#include "BidModeControllerService.h"


std::string NoBidModeEnforcerModule::getName() {
        return "NoBidModeEnforcerModule";
}

NoBidModeEnforcerModule::NoBidModeEnforcerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

}

void NoBidModeEnforcerModule::process(std::shared_ptr<OpportunityContext> context) {
        if (bidModeControllerService->globalBidMode->bidMode->getValue() == false) {
                LOG_EVERY_N(ERROR, 1000) << google::COUNTER <<"th msg : sending no bid as we are in NoBid mode : "<<
                        JsonArrayUtil::convertListToJson(bidModeControllerService->globalBidMode->bidModeReasons);
                entityToModuleStateStats->addStateModuleForEntity ("NO_BID_BECAUSE_IN_NO_BIDE_MODE",
                                                                   "BidRequestHandler",
                                                                   "ALL");

                //TODO : change the line below to pass "NO_BID_MODE" only in case of test
                context->finalBidResponseContent= "";
                context->bidResponseStatusCode= Poco::Net::HTTPResponse::HTTP_NO_CONTENT;

                // we send this text
                //so that we don't count these in the loadtester as a good valid bid
                context->status->value = STOP_PROCESSING;
        }
}

bool NoBidModeEnforcerModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

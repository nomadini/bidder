#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "BidResponseCreatorModule.h"
#include "JsonUtil.h"

#include "EventLog.h"
#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "OpportunityContextBuilder.h"
#include "BidderMainPipelineProcessor.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"
#include "BidResponseBuilder.h"
#include "OpenRtb2_3_0BidResponseBuilder.h"

std::string BidResponseCreatorModule::getName() {
        return "BidResponseCreatorModule";
}

BidResponseCreatorModule::BidResponseCreatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {


}

void BidResponseCreatorModule::selectTheRightVersionOfResponseBuilder(std::shared_ptr<OpportunityContext> context) {
        if (context->openRtbVersion == OpenRtbVersion::V2_3) {
                context->bidResponseBuilder = openRtb2_3_0BidResponseBuilder;

        } else if (context->openRtbVersion == OpenRtbVersion::V2_2) {
                //configure the other version
        }
        NULL_CHECK(context->bidResponseBuilder);
}

void BidResponseCreatorModule::process(std::shared_ptr<OpportunityContext> context) {

        selectTheRightVersionOfResponseBuilder(context);

        assertAndThrow(!context->bidDecisionResult.empty());
        if (StringUtil::equalsIgnoreCase (context->bidDecisionResult, EventLog::EVENT_TYPE_NO_BID) ||
            context->status->value == STOP_PROCESSING) {
                context->finalBidResponseContent = "";
                context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_NO_CONTENT;

                entityToModuleStateStats->addStateModuleForEntity ("numberOfNoBidsResponses",
                                                                   "BidRequestHandler",
                                                                   "ALL");
        } else if (StringUtil::equalsIgnoreCase (context->bidDecisionResult, EventLog::EVENT_TYPE_BID)) {

                context->finalBidResponseContent = context->bidResponseBuilder->createTheResponse (context);
                assertAndThrow(!context->finalBidResponseContent.empty());
                context->bidResponseStatusCode = Poco::Net::HTTPResponse::HTTP_OK;

                LOG_EVERY_N(INFO, 100)<<google::COUNTER
                                      << "th : sample bid response : " <<
                        context->finalBidResponseContent;
                entityToModuleStateStats->addStateModuleForEntity ("#GoodBids",
                                                                   "BidRequestHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);


                //we show these in dashboards... very important metric
                entityToModuleStateStats->addStateModuleForEntity ("#Bids_with_TG:" + StringUtil::toStr(context->chosenTargetGroupId)
                                                                   + "_CRT:"+StringUtil::toStr(context->chosenCreativeId),
                                                                   "BidStats",
                                                                   EntityToModuleStateStats::all);

                //we show these in dashboards... very important metric
                //we use this to track bids and wins,
                //there is metric in adserver that is called #TG_XX_Wins, that is related to this
                entityToModuleStateStats->addStateModuleForEntity ("#TG_"+StringUtil::toStr(context->chosenTargetGroupId)+"_Bids",
                                                                   "BidWinTracker",
                                                                   EntityToModuleStateStats::all);

        } else {
                EXIT("process result is not correct : "+ context->bidDecisionResult);
        }
}

bool BidResponseCreatorModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

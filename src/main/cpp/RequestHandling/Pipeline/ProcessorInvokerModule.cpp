#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "ProcessorInvokerModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "BidderLatencyRecorder.h"
#include "ModulesToStatePercentages.h"
#include "EventLog.h"
#include "EntityToModuleStateStats.h"
#include "OpportunityContextBuilder.h"
#include "BidderMainPipelineProcessor.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"

std::string ProcessorInvokerModule::getName() {
        return "ProcessorInvokerModule";
}

ProcessorInvokerModule::ProcessorInvokerModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {

}

void ProcessorInvokerModule::process(std::shared_ptr<OpportunityContext> context) {
        assertAndThrow(targetGroupsEligibleForBidding != nullptr);

        std::copy(std::begin(*targetGroupsEligibleForBidding), std::end(*targetGroupsEligibleForBidding),
                  std::back_inserter(*context->getAllAvailableTgs()));


        if (context->getAllAvailableTgs()->empty()) {
                entityToModuleStateStats->addStateModuleForEntity ("NO_TG_TO_PROCESS",
                                                                   "BidRequestHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                context->bidDecisionResult =  EventLog::EVENT_TYPE_NO_BID;
                LOG_EVERY_N(ERROR, 1000) << google::COUNTER
                                         << "thTime : no targetgroups selected for pipeline, "
                                         <<  "initial loaded current tgs : "
                                         << targetGroupCacheService->getAllCurrentTargetGroups()->size()
                                         <<  "\ninitial loaded tgs : "
                                         << targetGroupCacheService->getAllEntities()->size();

        } else {
                entityToModuleStateStats->addStateModuleForEntity ("INVOKING_MAIN_PROCESSOR",
                                                                   "BidRequestHandler",
                                                                   "ALL");

                Poco::Timestamp bidderMainPipelineProcessorTime;
                bidderMainPipelineProcessor->process (context);

                bidderLatencyRecorder->recordLatency(bidderMainPipelineProcessorTime, 100, "BidderMainPipelineProcessor");
        }
}
bool ProcessorInvokerModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        throwEx("should not be used");
}

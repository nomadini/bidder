/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef BidResponseCreatorModule_H_
#define BidResponseCreatorModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "Object.h"
#include "CollectionUtil.h"
class OpportunityContext;

class EntityToModuleStateStats;
class OpportunityContextBuilder;
class BidderMainPipelineProcessor;
class TargetGroup;
class TargetGroupCacheService;
class BidResponseBuilder;
namespace OpenRtb2_3_0 {
class OpenRtb2_3_0BidResponseBuilder;
};
#include <memory>
#include <string>
#include "BidderModule.h"
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "ConcurrentHashSet.h"

class BidResponseCreatorModule : public BidderModule, public Object {

private:

public:
std::shared_ptr<OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder> openRtb2_3_0BidResponseBuilder;

virtual std::string getName();
BidResponseCreatorModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

virtual void process(std::shared_ptr<OpportunityContext> context);
void selectTheRightVersionOfResponseBuilder(std::shared_ptr<OpportunityContext> context);

bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context);
};

#endif

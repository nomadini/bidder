#include "Status.h"

#include "CollectionUtil.h"
#include "OpportunityContext.h"

#include "EntityToModuleStateStats.h"
#include "BidderWiretapModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "OpportunityContextBuilder.h"
#include "BidderMainPipelineProcessor.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"
#include "BidResponseBuilder.h"
#include "WiretapCassandraService.h"

std::string BidderWiretapModule::getName() {
        return "BidderWiretapModule";
}

BidderWiretapModule::BidderWiretapModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : BidderModule(entityToModuleStateStats, filterNameToFailureCounts), Object(__FILE__) {


}

void BidderWiretapModule::process(std::shared_ptr<OpportunityContext> context) {

        if(!wiretapBidRequestResponseSampler->isPartOfSample()) {
                return;
        }

        assertAndThrow(!context->bidDecisionResult.empty());
        auto wiretap = std::make_shared<Wiretap>();
        wiretap->eventId = context->transactionId;
        wiretap->appName = "Bidder";
        // std::string appVersion;
        // std::string appHost;
        wiretap->moduleName = "BidRequestHandler";
        wiretap->request = context->requestBody;
        wiretap->response = context->finalBidResponseContent;
        wiretapCassandraService->record(wiretap);

}

bool BidderWiretapModule::filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) {
        return false;
}

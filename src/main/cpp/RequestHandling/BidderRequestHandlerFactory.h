#ifndef BidderRequestHandlerFactory_H
#define BidderRequestHandlerFactory_H

#include "BiddingMode.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include <unordered_map>
class OpportunityContext;
class BidRequestHandlerPipelineProcessor;
class EntityToModuleStateStats;
class TargetGroup;
class BidRequestHandler;
class CommonRequestHandlerFactory;
namespace gicapods { class ConfigService; }
class BidModeControllerService;
class TgBiddingPerformanceMetricInBiddingPeriodService;
#include "AtomicLong.h"
#include "BidderCommonRequestHandlerFactory.h"
#include "AtomicBoolean.h"
#include "AtomicDouble.h"
#include "BidderLatencyRecorder.h"
#include "Poco/Logger.h"
#include "Object.h"
class BidderRequestHandlerFactory;



class BidderRequestHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory, public Object {

public:
std::string bidderPort;
std::string GOOGLE_OPEN_RTB_2_3_SERVLET_PATH;
std::string CONFIRMED_WINS_SERVLET_PATH;

std::shared_ptr<gicapods::AtomicBoolean> redirectToBidderIsEnabled;


std::shared_ptr<gicapods::AtomicLong> numberOfRequestsProcessingNow;

std::shared_ptr<gicapods::AtomicLong> uniqueNumberOfContext;
std::vector<std::string> nomadiniAdServerDomains;
std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > targetGroupsEligibleForBidding;
std::shared_ptr<BiddingMode> bidMode;
CommonRequestHandlerFactory* commonRequestHandlerFactory;
BidderCommonRequestHandlerFactory* bidderCommonRequestHandlerFactory;
TgBiddingPerformanceMetricInBiddingPeriodService* tgBiddingPerformanceMetricInBiddingPeriodService;
BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor;
BidderLatencyRecorder* bidderLatencyRecorder;

EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicLong> bidPercentage;

std::shared_ptr<gicapods::AtomicBoolean> isNoBidModeIsTurnedOn;
gicapods::ConfigService* configService;

std::shared_ptr<gicapods::AtomicLong> totalNumberOfRequestsProcessedInFilterCountPeriod;

BidderRequestHandlerFactory(BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor,
                            EntityToModuleStateStats* entityToModuleStateStats,
                            std::string bidderPort,
                            gicapods::ConfigService* configService);

Poco::Net::HTTPRequestHandler *createRequestHandler(const Poco::Net::HTTPServerRequest &request);

void dontAcceptMoreThanXNumberOfBidRequestsPerSecond();
BidRequestHandler* createDirectBidRequestHandler(std::string servletPath);
};

#endif

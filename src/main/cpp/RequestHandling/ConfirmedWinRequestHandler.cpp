
#include "SignalHandler.h"
#include "BiddingMode.h"
#include "ConfirmedWinRequestHandler.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "ConfirmedWinRequest.h"


#include <boost/exception/all.hpp>
#include "HttpUtil.h"
#include "IntWrapper.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "HashMap.h"

ConfirmedWinRequestHandler::ConfirmedWinRequestHandler() : Object(__FILE__) {

}

void ConfirmedWinRequestHandler::handleRequest(
								Poco::Net::HTTPServerRequest& request,
								Poco::Net::HTTPServerResponse& response) {

								std::string requestBody = HttpUtil::getRequestBody(request);
								auto confirmedWinRequest = ConfirmedWinRequest::fromJson(requestBody);
								MLOG(3)<<"recieved confirmedWinRequest : " << confirmedWinRequest->toJson();
								for (auto& pair : *confirmedWinRequest->targetGroupIdWins->map) {
																tgBiddingPerformanceMetricInBiddingPeriodService->
																updateConfirmedWins(pair.first, (*pair.second).getValue());
								}

								std::ostream& ostr = response.send();
								ostr << "success";
}

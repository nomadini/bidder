
#include "SignalHandler.h"
#include "BiddingMode.h"
#include "BidderStatusRequestHandler.h"
#include "JsonUtil.h"
#include "GUtil.h"


#include <boost/exception/all.hpp>
#include "HttpUtil.h"

BidderStatusRequestHandler::BidderStatusRequestHandler() : Object(__FILE__) {

}

void BidderStatusRequestHandler::handleRequest(
								Poco::Net::HTTPServerRequest& request,
								Poco::Net::HTTPServerResponse& response) {
								std::string jsonResponse;
								try {

																jsonResponse =  bidMode->getStatus();

																std::string requestBody = HttpUtil::getRequestBody(request);

																MLOG(10)<<"bidder status request body "<<requestBody;

																MLOG(10)<<"response "<<jsonResponse;

								} catch (std::exception const&  e) {
																LOG(ERROR)<<"error happening when handling request  "<<boost::diagnostic_information(e);
																LOG(WARNING)<<"sending no bid as a result of exception";
								}

								catch (std::exception const&  e) {
																LOG(ERROR)<<"unknown error happening when handling request";
																LOG(WARNING)<<"sending no bid as a result of exception";
								}
								std::ostream& ostr = response.send();
								ostr << jsonResponse;
}

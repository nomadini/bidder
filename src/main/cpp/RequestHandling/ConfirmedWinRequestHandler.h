#ifndef ConfirmedWinRequestHandler_H
#define ConfirmedWinRequestHandler_H


#include "SignalHandler.h"
#include "BiddingMode.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Object.h"
class TgBiddingPerformanceMetricInBiddingPeriodService;
/**
   it will increment the confirmed wins for a set of target groups
   that will end up in mysql...
 */

class ConfirmedWinRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:
TgBiddingPerformanceMetricInBiddingPeriodService* tgBiddingPerformanceMetricInBiddingPeriodService;
ConfirmedWinRequestHandler();

void handleRequest(Poco::Net::HTTPServerRequest& request,
																			Poco::Net::HTTPServerResponse& response);
};

#endif

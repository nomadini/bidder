#ifndef BidderStatusRequestHandler_H
#define BidderStatusRequestHandler_H


#include "SignalHandler.h"
#include "BiddingMode.h"
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Object.h"
/**
   tasks of this class :
   1. provides an end point for other apps to get the status of bidder

 */
class BidderStatusRequestHandler : public Poco::Net::HTTPRequestHandler, public Object {

public:

std::shared_ptr<BiddingMode> bidMode;

BidderStatusRequestHandler();

void handleRequest(Poco::Net::HTTPServerRequest& request,
																			Poco::Net::HTTPServerResponse& response);
};

#endif

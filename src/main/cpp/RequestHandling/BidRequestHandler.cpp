
#include "BidRequestHandler.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>

#include "BidResponseBuilder.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "StringUtil.h"
#include "ConverterUtil.h"
#include "BidderRequestHandlerFactory.h"
#include "HttpUtil.h"
#include "StatisticsUtil.h"
#include "BidderRequestHandlerFactory.h"
#include <signal.h>//for extreme debugging , when you want to raise a signal
#include "OpportunityContext.h"
#include "ImportantStats.h"
#include "BidderLatencyRecorder.h"
#include "ExceptionUtil.h"
#include "BidderLatencyRecorder.h"
BidRequestHandler::BidRequestHandler(
        std::shared_ptr<OpportunityContext> context,
        BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor,
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->bidRequestHandlerPipelineProcessor = bidRequestHandlerPipelineProcessor;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->context = context;

}

void BidRequestHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                      Poco::Net::HTTPServerResponse &response) {

        Poco::Timestamp timeToProcessRequest;
        numberOfRequestsProcessingNow->increment();


        handleRequestLevel1(request, response);


        logTimeAndCounts(timeToProcessRequest);
        numberOfRequestsProcessingNow->decrement();
}

void BidRequestHandler::handleRequestLevel1(Poco::Net::HTTPServerRequest &request,
                                            Poco::Net::HTTPServerResponse &response) {

        try {
                //this is an important metric, don't remove
                entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsRecieved",
                                                                   "BidRequestHandler",
                                                                   "ALL");

                context->request = &request;
                context->requestBody = HttpUtil::getRequestBody (request);
                // LOG(ERROR)<< "context->requestBody : "<<  context->requestBody;


                bidRequestHandlerPipelineProcessor->process(context);

                assertAndThrow(context->bidResponseStatusCode ==  Poco::Net::HTTPResponse::HTTP_OK
                               || context->bidResponseStatusCode ==  Poco::Net::HTTPResponse::HTTP_NO_CONTENT);
                response.setStatus (context->bidResponseStatusCode);
                std::ostream &ostr = response.send ();
                ostr << context->finalBidResponseContent;
                return;
        } catch (std::exception const &e) {

                try {
                        handleException(e);
                } catch (...) {
                        //sometimes timeout exception happen here, if time out in bidder is set to low micro seconds
                }
        }

        catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("Unknown Exception ",
                                                                   "BidRequestHandler",
                                                                   "ALL");
        }

        HttpUtil::setEmptyResponse (response);
}

void BidRequestHandler::logTimeAndCounts(Poco::Timestamp &timeToProcessRequest) {

        BidderLatencyRecorder::logTimeTaken(
                "BidRequestHandler", timeToProcessRequest, 10000, 10);

        entityToModuleStateStats->addStateModuleForEntity ("numberOfBidsProcessed",
                                                           "BidRequestHandler",
                                                           "ALL");
        totalNumberOfRequestsProcessedInFilterCountPeriod->increment();

        bidderLatencyRecorder->recordLatency(timeToProcessRequest, 100,
                                             "BidRequestHandler");

}

void BidRequestHandler::handleException(std::exception const &e) {
        ExceptionUtil::logException(e, entityToModuleStateStats, 1000);
        // raise(6); //SIGABRT this is dangerous, use it just for debuggin

        auto diagnosticInformation = boost::diagnostic_information (e);

        //ignore these expcetions and add them as warning
        if (StringUtil::containsCaseInSensitive(diagnosticInformation, "failed to find timeZone for") ||
            StringUtil::containsCaseInSensitive(diagnosticInformation, "NON-US-REQUEST")

            ) {

                entityToModuleStateStats->addStateModuleForEntity ("Warning : " + StringUtil::toStr(e.what()),
                                                                   "BidRequestHandler",
                                                                   "ALL");
                return;
        }

        entityToModuleStateStats->addStateModuleForEntity ("Exception : " + StringUtil::toStr(e.what()),
                                                           "BidRequestHandler",
                                                           "ALL");
}

BidRequestHandler::~BidRequestHandler() {
}

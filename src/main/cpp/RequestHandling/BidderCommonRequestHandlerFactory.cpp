



#include "StringUtil.h"
#include "GUtil.h"
#include "ConverterUtil.h"
#include "UnknownRequestHandler.h"
#include "BidderCommonRequestHandlerFactory.h"
#include "EntityToModuleStateStatsHandler.h"
#include "EntityToModuleStateStats.h"
#include "LogLevelManagerRequestHandler.h"
#include "ForceReloadCachesHandler.h"
#include "DashboardRequestServiceHandler.h"
#include "ConfigRequestServiceHandler.h"
#include "DataClient.h"
#include "ConfigService.h"
#include "BootableConfigService.h"
#include "DoNothingRequestHandler.h"
#include "CassandraDataRequestHandler.h"
#include "DataMasterRequestHandlerFactory.h"
#include "DataMasterRequestHandlerFactory.h"
#include "PersistentMetricsRequestServiceHandler.h"
#include "LogLevelManagerRequestHandler.h"
#include "EntityDeliveryInfoCacheServiceRequestHandler.h"
#include "EntityDeliveryInfoCacheService.h"


BidderCommonRequestHandlerFactory::BidderCommonRequestHandlerFactory() {
}

Poco::Net::HTTPRequestHandler* BidderCommonRequestHandlerFactory::createRequestHandler(
        const Poco::Net::HTTPServerRequest &request) {
        try {

                MLOG(3) << "request.getURI() : " << request.getURI ();
                Poco::Net::HTTPRequestHandler* handler;

                handler = evaluetePersistentMetricsRequestHanlder(request);
                if(handler != nullptr) { return handler; }
                handler = evalueteEntityDeliveryInfoCacheRequestHandler(request);
                if(handler != nullptr) { return handler; }


                if (StringUtil::containsCaseInSensitive (request.getURI (), "/favicon.ico")) {
                        return new DoNothingRequestHandler(entityToModuleStateStats);
                }

        } catch (std::exception const &e) {
                LOG(ERROR) << "unknown error happening when handling request";
        }
        MLOG(3) << "found no handler for request with uri : " << request.getURI ();
        return new UnknownRequestHandler(entityToModuleStateStats);
}


Poco::Net::HTTPRequestHandler*
BidderCommonRequestHandlerFactory::evaluetePersistentMetricsRequestHanlder(const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "/persistentmetrics") ||
            StringUtil::endsWith(request.getURI (), "/persistentmetrics")) {
                auto handler = new PersistentMetricsRequestServiceHandler(
                        mySqlMetricService,
                        mySqlTargetGroupFilterCountDbRecordService,
                        mySqlTgBiddingPerformanceMetricDtoService
                        );
                return handler;
        }

        return nullptr;

}

Poco::Net::HTTPRequestHandler*
BidderCommonRequestHandlerFactory::evalueteEntityDeliveryInfoCacheRequestHandler(
        const Poco::Net::HTTPServerRequest &request) {
        if (StringUtil::containsCaseInSensitive (request.getURI (), "EntityDeliveryInfo") ||
            StringUtil::endsWith(request.getURI (), "EntityDeliveryInfo")) {
                auto handler =  new EntityDeliveryInfoCacheServiceRequestHandler();
                handler->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
                return handler;
        }

        return nullptr;
}

std::shared_ptr<BidderCommonRequestHandlerFactory>
BidderCommonRequestHandlerFactory::shared_instance(
        EntityToModuleStateStats* entityToModuleStateStats,
        gicapods::ConfigService* configService,
        MySqlMetricService* mySqlMetricService,
        EntityDeliveryInfoCacheService* entityDeliveryInfoCacheService,
        MySqlTargetGroupFilterCountDbRecordService* mySqlTargetGroupFilterCountDbRecordService,
        MySqlTgBiddingPerformanceMetricDtoService* mySqlTgBiddingPerformanceMetricDtoService) {

        static auto instance = std::make_shared<BidderCommonRequestHandlerFactory>();

        instance->entityToModuleStateStats = entityToModuleStateStats;
        instance->configService = configService;
        instance->mySqlMetricService = mySqlMetricService;
        instance->entityDeliveryInfoCacheService = entityDeliveryInfoCacheService;
        instance->mySqlTargetGroupFilterCountDbRecordService = mySqlTargetGroupFilterCountDbRecordService;
        instance->mySqlTgBiddingPerformanceMetricDtoService = mySqlTgBiddingPerformanceMetricDtoService;

        return instance;
}

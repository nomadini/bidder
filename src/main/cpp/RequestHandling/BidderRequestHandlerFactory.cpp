#include "BidderRequestHandlerFactory.h"
#include "StringUtil.h"
#include "BidderStatusRequestHandler.h"
#include "BidRequestHandler.h"
#include "TempUtil.h"

#include "BidderApplicationContext.h"

#include "CommonRequestHandlerFactory.h"
#include "GUtil.h"
#include "UnknownRequestHandler.h"
#include "ConverterUtil.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "OpportunityContext.h"
#include <boost/foreach.hpp>
#include "ConfigService.h"
#include "TargetGroup.h"
#include "NetworkUtil.h"

#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "HttpUtil.h"
#include "ConfirmedWinRequestHandler.h"
#include <gperftools/heap-profiler.h>
BidderRequestHandlerFactory::BidderRequestHandlerFactory(BidRequestHandlerPipelineProcessor* bidRequestHandlerPipelineProcessor,
                                                         EntityToModuleStateStats* entityToModuleStateStats,
                                                         std::string bidderPort,
                                                         gicapods::ConfigService* configService) : Object(__FILE__) {


        this->bidRequestHandlerPipelineProcessor = bidRequestHandlerPipelineProcessor;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->configService = configService;
        redirectToBidderIsEnabled = std::make_shared<gicapods::AtomicBoolean> ();
        this->bidderPort = bidderPort;
        GOOGLE_OPEN_RTB_2_3_SERVLET_PATH = "/bid/google/openrtb2.3";
        CONFIRMED_WINS_SERVLET_PATH = "/confirmedwins";
        // HeapProfilerStart("BidderRequestHandler");



}

void BidderRequestHandlerFactory::dontAcceptMoreThanXNumberOfBidRequestsPerSecond() {
        // HeapProfilerStop();
}

Poco::Net::HTTPRequestHandler
*BidderRequestHandlerFactory::createRequestHandler(const Poco::Net::HTTPServerRequest &request) {

        try {


                MLOG(10) << "BidderRequestHandlerFactory : request uri to process : " << request.getURI ();

                if (StringUtil::containsCaseInSensitive (request.getURI (), "/slowBidMode/")) {
                        MLOG(2) <<"checking slow BidMode";
                        // regex to capture the digit after the /slowBidMode/ ==>  \/slowBidMode\/.\d*
                        auto matchedSubstrings = StringUtil::searchWithRegex(request.getURI (), "\\/slowBidMode\\/.\\d*");
                        if (matchedSubstrings.size() != 1) {
                                throwEx("wrong way to call service : " + request.getURI () );
                        }

                        std::string slowBidPercStr = StringUtil::replaceString (matchedSubstrings.at(0), "/slowBidMode/", "");

                        bidPercentage->setValue(ConverterUtil::convertTo <int>(slowBidPercStr));
                        if (bidPercentage->getValue() < 0 || bidPercentage->getValue() > 100) {
                                throwEx ("wrong bidPercentage " + slowBidPercStr);
                        }
                        MLOG (1)<<"bidPercentage changed to : "<<bidPercentage->getValue();
                } else if (StringUtil::containsCaseInSensitive (request.getURI (), "/no_bid_mode_turn_on")) {
                        isNoBidModeIsTurnedOn->setValue(true);
                        entityToModuleStateStats->addStateModuleForEntity ("NO_BID_MODE_VIA_HTTP",
                                                                           "BidderRequestHandlerFactory",
                                                                           "ALL");
                } else if (StringUtil::containsCaseInSensitive (request.getURI (), "/no_bid_mode_turn_off")) {
                        isNoBidModeIsTurnedOn->setValue(false);
                        entityToModuleStateStats->addStateModuleForEntity ("BID_MODE_VIA_HTTP",
                                                                           "BidderRequestHandlerFactory",
                                                                           "ALL");

                } else if (StringUtil::containsCaseInSensitive (request.getURI (), GOOGLE_OPEN_RTB_2_3_SERVLET_PATH)) {

                        auto bidRequestHandler = createDirectBidRequestHandler (CONFIRMED_WINS_SERVLET_PATH);
                        return bidRequestHandler;

                }  else if (StringUtil::containsCaseInSensitive (request.getURI (), "/status")) {
                        return new BidderStatusRequestHandler ();
                } else if (StringUtil::containsCaseInSensitive (request.getURI (), CONFIRMED_WINS_SERVLET_PATH)) {
                        auto confirmedWinRequestHandler =  new ConfirmedWinRequestHandler ();
                        confirmedWinRequestHandler->tgBiddingPerformanceMetricInBiddingPeriodService =
                                tgBiddingPerformanceMetricInBiddingPeriodService;
                        return confirmedWinRequestHandler;
                }

                auto commonRequestHandler = commonRequestHandlerFactory->createRequestHandler(request);
                if (commonRequestHandler != nullptr) {
                        return commonRequestHandler;
                }

                auto bidderCommonRequestHandler = bidderCommonRequestHandlerFactory->createRequestHandler(request);
                if (bidderCommonRequestHandler != nullptr) {
                        return bidderCommonRequestHandler;
                }

        } catch (...) {
                LOG(ERROR) <<"unknow exception happened";
        }
        return new UnknownRequestHandler (entityToModuleStateStats);
}

BidRequestHandler* BidderRequestHandlerFactory::createDirectBidRequestHandler(std::string servletPath) {
        auto context  = std::make_shared<OpportunityContext>();
        context->openRtbVersion = OpenRtbVersion::V2_3;;
        context->exchangeName = "google";
        context->bidderCallbackServletUrl = "http://" + NetworkUtil::getHostName() + ":" + bidderPort + servletPath;
        MLOG(10)<< " bidderCallbackServletUrl : "<< context->bidderCallbackServletUrl;
        auto bidRequestHandler = new BidRequestHandler (
                context,
                bidRequestHandlerPipelineProcessor,
                entityToModuleStateStats);

        bidRequestHandler->bidderLatencyRecorder = bidderLatencyRecorder;
        bidRequestHandler->totalNumberOfRequestsProcessedInFilterCountPeriod = totalNumberOfRequestsProcessedInFilterCountPeriod;

        assertAndThrow(targetGroupsEligibleForBidding != nullptr);

        bidRequestHandler->numberOfRequestsProcessingNow = numberOfRequestsProcessingNow;
        return bidRequestHandler;
}

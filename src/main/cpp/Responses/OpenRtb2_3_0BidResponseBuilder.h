//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef OpenRtb2_3_0BidResponseBuilder_H
#define OpenRtb2_3_0BidResponseBuilder_H

class OpportunityContext;
#include <memory>
#include <string>
#include <vector>
#include "BidResponseBuilder.h"
#include "CacheService.h"
#include "EntityProviderService.h"
#include "Advertiser.h"
class EntityToModuleStateStats;

class CreativeCacheService;
class CampaignCacheService;
class RandomUtil;
namespace OpenRtb2_3_0 {
class OpenRtbBidResponseJsonConverter;
class OpenRtbBidResponse;


class OpenRtb2_3_0BidResponseBuilder : public BidResponseBuilder, public Object {

private:


public:
EntityToModuleStateStats* entityToModuleStateStats;
CacheService<Advertiser>* advertiserCacheService;
CreativeCacheService* creativeCacheService;
CampaignCacheService* campaignCacheService;
std::vector<std::string> nomadiniAdServerDomains;
std::shared_ptr<RandomUtil> adservHostRandomizer;
OpenRtb2_3_0::OpenRtbBidResponseJsonConverter* openRtbBidResponseJsonConverter;
OpenRtb2_3_0BidResponseBuilder(std::vector<std::string> nomadiniAdServerDomains,
                               EntityToModuleStateStats* entityToModuleStateStats,
                               CacheService<Advertiser>* advertiserCacheService,
                               CreativeCacheService* creativeCacheService,
                               CampaignCacheService* campaignCacheService);

std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> buildBidResponseFromContext(std::shared_ptr<OpportunityContext> context);
int getTheRightReasonForNotBidding(std::shared_ptr<OpportunityContext> context);
std::string createWinNotificationUrl(std::shared_ptr<OpportunityContext> context);
std::string createTheResponse(std::shared_ptr<OpportunityContext> context);
std::string convertEventToParam(std::shared_ptr<OpportunityContext> context);

};

}

#endif

//
// Created by Mahmoud Taabodi on 5/7/16.
//

#ifndef BidResponseBuilder_H
#define BidResponseBuilder_H

#include <memory>
#include <string>
#include <vector>
#include "Object.h"
class OpportunityContext;

class BidResponseBuilder : public Object {

private:


public:

BidResponseBuilder();

virtual int getTheRightReasonForNotBidding(std::shared_ptr<OpportunityContext> context)= 0;
virtual std::string createWinNotificationUrl(std::shared_ptr<OpportunityContext> context) = 0;
virtual std::string createTheResponse(std::shared_ptr<OpportunityContext> context)= 0;
virtual std::string convertEventToParam(std::shared_ptr<OpportunityContext> context)= 0;

virtual ~BidResponseBuilder();
};

#endif

//
// Created by Mahmoud Taabodi on 5/7/16.
//

#include "OpenRtb2_3_0BidResponseBuilder.h"
#include "EntityToModuleStateStats.h"

#include "CreativeCacheService.h"
#include "CollectionUtil.h"
#include "Device.h"
#include "EventLog.h"
#include "CampaignCacheService.h"
#include "SignalHandler.h"
#include "OpenRtb2_3_0BidResponseBuilder.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include <boost/exception/all.hpp>
#include <boost/foreach.hpp>
#include "RandomUtil.h"
#include "StringUtil.h"
#include "HttpUtil.h"
#include "AdPositionConverter.h"
#include "CreativeAPIConverter.h"
#include "BannerAdTypeConverter.h"
#include "DeviceTypeConverter.h"
#include "Encoder.h"
#include "Creative.h"
#include "Poco/URI.h"
#include "OpportunityContext.h"
#include "TargetGroup.h"
#include "Campaign.h"
#include "Advertiser.h"
#include "Creative.h"
#include "JsonArrayUtil.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "OpenRtbBidResponse.h"
#include "EntityProviderService.h"
#include "ConverterUtil.h"

OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::OpenRtb2_3_0BidResponseBuilder(
        std::vector<std::string> nomadiniAdServerDomains,
        EntityToModuleStateStats* entityToModuleStateStats,
        CacheService<Advertiser>* advertiserCacheService,
        CreativeCacheService* creativeCacheService,
        CampaignCacheService* campaignCacheService) : Object(__FILE__) {
        this->nomadiniAdServerDomains = nomadiniAdServerDomains;
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->advertiserCacheService = advertiserCacheService;
        this->creativeCacheService = creativeCacheService;
        this->campaignCacheService = campaignCacheService;
        assertAndThrow(!nomadiniAdServerDomains.empty());
        this->adservHostRandomizer =
                std::make_shared<RandomUtil> (
                        nomadiniAdServerDomains.size());
}

std::string OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::createTheResponse(std::shared_ptr<OpportunityContext> context) {
        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> bidResponse = buildBidResponseFromContext(context);

        auto doc = JsonUtil::createADcoument (rapidjson::kObjectType);
        RapidJsonValueTypeNoRef value(rapidjson::kObjectType);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "id", bidResponse->id, value);
        JsonUtil::addMemberToValue_FromPair(doc.get(), "cur", bidResponse->cur, value);

        RapidJsonValueTypeNoRef bidArrayValue (rapidjson::kArrayType);
        openRtbBidResponseJsonConverter->writeBidObject (bidResponse, doc, bidArrayValue);
        RapidJsonValueTypeNoRef seatBidChildObject (rapidjson::kObjectType);

        JsonArrayUtil::addArrayValueAsMemberToValue (doc.get(), "bid", bidArrayValue,
                                                     seatBidChildObject);

        RapidJsonValueTypeNoRef seatBidArrayValue (rapidjson::kArrayType);

        JsonArrayUtil::addMemberToArray (doc.get(), seatBidArrayValue,
                                         seatBidChildObject);

        JsonUtil::addMemberToValue_FromPair (doc.get(), "seatbid", seatBidArrayValue, value);
        std::string responseStr =  JsonUtil::valueToString (value);
        LOG_EVERY_N(INFO, 100) << google::COUNTER<<"th : bid response : "<< responseStr;
        MLOG(3) <<" bid response : "<< responseStr;
        return responseStr;
}

std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse>
OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::buildBidResponseFromContext(std::shared_ptr<OpportunityContext> context) {

        entityToModuleStateStats->addStateModuleForEntity("BID_RESPONSE_BUILDING_ATTEMPTS", "OpenRtb2_3_0BidResponseBuilder", "ALL");

        std::shared_ptr<OpenRtb2_3_0::OpenRtbBidResponse> bidResponse = std::make_shared<OpenRtb2_3_0::OpenRtbBidResponse> ();

        bidResponse->id = context->bidRequestId;
        bidResponse->cur = "USD";
        bidResponse->customdata = "";
        // bidResponse->nbr = getTheRightReasonForNotBidding(context);
        std::shared_ptr<OpenRtb2_3_0::SeatBid> seatBid = std::make_shared<OpenRtb2_3_0::SeatBid> ();
        bidResponse->seatbid = seatBid;

        seatBid->seat = StringUtil::random_string (4);

        std::shared_ptr<OpenRtb2_3_0::Bid> bidPtr = std::make_shared<OpenRtb2_3_0::Bid> ();

        bidPtr->impid = context->impressionIdInBidRequest;

        std::shared_ptr<Creative> chosenCreative = creativeCacheService->findByEntityId(context->chosenCreativeId);
        auto advertiser = advertiserCacheService->findByEntityId(chosenCreative->getAdvertiserId());
        std::vector<std::string> allChosenDomains = CollectionUtil::convertKeysOfConcurrentMapToList <std::string, int>(*advertiser->domainNames);
        assertAndThrow(!allChosenDomains.empty());

        bidPtr->adomain = allChosenDomains.at(0);

        bidPtr->crid =  StringUtil::toStr(chosenCreative->getId());//this is probably remote id for the exchange


        std::shared_ptr<Campaign> chosenCampaign = campaignCacheService->findByEntityId(context->campaignId);

        bidPtr->cid = StringUtil::toStr(chosenCampaign->getId());//this is probably the remote id for the exchange
        bidPtr->id = context->transactionId;

        bidPtr->price = context->bidPrice;

        bidPtr->nurl = createWinNotificationUrl(context); //"http://adserver.com/winnotice?impid=102",

        seatBid->bid = bidPtr;

        bidResponse->seatbid = seatBid;

        entityToModuleStateStats->addStateModuleForEntity("BID_RESPONSE_BUILDING_SUCCESS", "OpenRtb2_3_0BidResponseBuilder", "ALL");

        return bidResponse;
}

std::string OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::convertEventToParam(std::shared_ptr<OpportunityContext> context) {
        /*
           don't delete this method, you might need to switch back to this way.
           it took a while to figure this out
         */
        convertEventToParam(context);
        Poco::URI transactionEncoded("abc");
        //TODO : use crypto++ to crypto this
        transactionEncoded.addQueryParameter("event", context->eventLog->toJson());// we do this , because we need to encode the
        return transactionEncoded.getRawQuery();
}

std::string OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::createWinNotificationUrl(std::shared_ptr<OpportunityContext> context) {

        std::string winNotificationUrl;
        winNotificationUrl.append(
                nomadiniAdServerDomains.at(
                        adservHostRandomizer->randomNumberV2()
                        )
                );
        winNotificationUrl.append("/win?won=${AUCTION_PRICE}&");
        winNotificationUrl.append("transactionId=");
        //make sure this transactionId is valid characters
        winNotificationUrl.append(context->eventLog->eventId);

        // std::string eventParam = convertEventToParam(context);
        // winNotificationUrl.append(eventParam);



        MLOG(3)<<"winNotificationUrl : "<<winNotificationUrl;
        context->winNotificationUrl = winNotificationUrl;
        return context->winNotificationUrl;
}

int OpenRtb2_3_0::OpenRtb2_3_0BidResponseBuilder::getTheRightReasonForNotBidding(std::shared_ptr<OpportunityContext> context) {
        return 0;
//   0
// Unknown Error
// 1
// Technical Error
// 2
// Invalid Request
// OpenRTB API Specification Version 2.3 RTB Project
// Page 47
// 3
// Known Web Spider
// 4
// Suspected Non-Human Traffic
// 5
// Cloud, Data center, or Proxy IP
// 6
// Unsupported Device
// 7
// Blocked Publisher or Site
// 8
// Unmatched User
}

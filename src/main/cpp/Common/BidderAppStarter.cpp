#include "GUtil.h"
#include "StringUtil.h"
#include "BidderAppStarter.h"
#include "ConfigService.h"
#include "Bidder.h"
#include "TempUtil.h"
#include "ConverterUtil.h"
#include <thread>
#include "ThrottlerModule.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "EntityDeliveryInfoCacheService.h"
#include "MySqlPlaceService.h"
#include "BidderAsyncPipelineProcessor.h"
#include "OpenRtbBidResponseJsonConverter.h"
#include "ConcurrentHashMap.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "FeatureDeviceHistory.h"
#include "BeanFactory.h"
#include "BidderMainPipelineProcessor.h"
#include "ModuleContainer.h"
#include "LoadPercentageBasedSampler.h"
#include "EntityDeliveryInfoFetcher.h"
#include "EntityDeliveryInfoFetcher.h"
#include "MySqlTargetGroupFilterCountDbRecordService.h"
#include "OpportunityContextBuilderOpenRtb2_3.h"
#include "OpenRtbBidRequestParser.h"
#include "BidderAsyncJobsService.h"
#include "CassandraDriverInterface.h"
#include "ServiceFactory.h"
#include "BlockedCreativeAttributeFilter.h"
#include "DataReloadService.h"
#include "ProgramOptionParser.h"
#include "TgFilterMeasuresService.h"
#include "BidRequestHandlerPipelineProcessor.h"
#include "TgBiddingPerformanceFilterMeasures.h"
#include "BidderApplicationContext.h"
#include "TgBiddingPerformanceMetricInBiddingPeriodService.h"
#include "CommonRequestHandlerFactory.h"
#include "IntWrapper.h"
#include "HashMap.h"
#include "CacheUpdateWatcher.h"

#include "DomainBlackListedFilter.h"
#include "BlockedAdvertiserDomainFilter.h"
#include "DomainWhiteListedFilter.h"
#include "CreativeBannerApiFilter.h"
#include "AdPositionFilter.h"
#include "CreativeSizeFilter.h"
#include "BlockedBannerAdTypeFilter.h"
#include "BlockedIabCategoryFilter.h"
#include "BlockedCreativeAttributeFilter.h"
#include "DeviceTypeFilter.h"
#include "OsTypeFilter.h"
#include "DayPartFilter.h"
#include "Place.h"
#include <fstream>
#include <string>
#include "BidderRequestHandlerFactory.h"
#include "BidderAsyncJobsService.h"
#include "BidConfirmedWinsContainerPerTargetGroupAndBidder.h"
#include "TgFilterMeasuresService.h"
#include "BidderAsyncJobsService.h"
#include "BidderApplicationContext.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "DataReloadService.h"
#include "ModuleContainer.h"
#include "ThrottlerModule.h"
#include "PocoHttpServer.h"

void BidderAppStarter::processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion) {


        //The default_value will be used when the option is not specified at all. The implicit_value will be used when the option is specific without a value. If a value is specified, it will override the default and implicit.
        namespace po = boost::program_options;
        boost::program_options::options_description desc("Allowed options");
        desc.add_options()
        // First parameter describes option name/short name
        // The second is parameter to option
        // The third is description
                ("help,h", "print usage message")
                ("appname,a",
                po::value(&appName)->default_value("Bidder"), "app name")
                ("property,p", boost::program_options::value(&propertyFileName)->default_value("bidder.properties"), "bidder property file name")
                ("port,t", boost::program_options::value(&port)->default_value(1), "bidder port")
                ("version,v", boost::program_options::value(&appVersion)->default_value("SNAPSHOT"), "version")
                ("log_dir,l", boost::program_options::value(&logDirectory)->default_value("/var/log/"), "bidder log directory location");

        boost::program_options::variables_map vm;
        boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

        if (vm.count("help")) {
                std::cout << desc << "\n";
                return;
        }


        std::cout << "property = " << vm["property"].as<std::string>() << "\n";
        std::cout << "port = " << vm["port"].as<unsigned short>() << "\n";
        std::cout << "appname = " << vm["appname"].as<std::string>() << "\n";

        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        //THE REAL ASSIGNMENT HAPPENS HERE !!
        appName = vm["appname"].as<std::string>();
        appVersion = vm["version"].as<std::string>();
        propertyFileName = vm["property"].as<std::string>();
        port = vm["port"].as<unsigned short>();
}

void BidderAppStarter::startTheApp(int argc, char* argv[]) {


        std::string appName = "";
        std::string appVersion = "";
        std::string logDirectory = "";
        std::string propertyFileName = "bidder.properties";
        unsigned short bidderPort = 1;

        processArguments(argc,
                         argv,
                         propertyFileName,
                         bidderPort,
                         appName,
                         logDirectory,
                         appVersion);


        TempUtil::configureLogging (appName, argv);
        LOG(ERROR) << appName <<" starting with properties "<< propertyFileName <<
                std::endl<<" original arguments : argc "<< argc << " argv : " <<  *argv;


        auto beanFactory = std::make_unique<BeanFactory>(appVersion);
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;

        beanFactory->initializeModules();
        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();


        // if(beanFactory->configService->getAsBooleanFromString("setValgrindProperties")) {
        //         for(int i = 0; i < 20; i++) {
        //                 LOG(ERROR)<< " SETTING VALGRIND PROPERTIES FOR DEBUGGING MEMORY ISSUES";
        //         }
        //         //we sleep here to bring dev to attention
        //         gicapods::Util::sleepViaBoost(_L_, 3);
        //         beanFactory->configService->readProperties("bidder-valgrind.properties", true);
        //
        // }

        auto moduleContainer = std::make_unique<ModuleContainer>();
        moduleContainer->beanFactory = beanFactory.get();
        moduleContainer->serviceFactory = serviceFactory.get();
        moduleContainer->bidderPort =  beanFactory->configService->getAsInt("httpPort");
        moduleContainer->initializeModules();

        std::string pollingRealtimeTargetGroupIntervalInSec = beanFactory->configService->get("pollingRealtimeTargetGroupIntervalInSec");

        auto entityDeliveryInfoFetcher = std::make_shared<EntityDeliveryInfoFetcher> (
                beanFactory->entityDeliveryInfoCacheService.get(),
                beanFactory->entityToModuleStateStats.get());
        entityDeliveryInfoFetcher->updateRealTimeInfoInterval = ConverterUtil::convertTo<int> (pollingRealtimeTargetGroupIntervalInSec);
        entityDeliveryInfoFetcher->isFetchingDeliveryInfoFromPacerHealthy = moduleContainer->isFetchingDeliveryInfoFromPacerHealthy;
        entityDeliveryInfoFetcher->mySqlEntityRealTimeDeliveryInfoService = beanFactory->mySqlEntityRealTimeDeliveryInfoService.get();


        auto bidderAsyncPipelineProcessor = std::make_shared<BidderAsyncPipelineProcessor> ();
        bidderAsyncPipelineProcessor->queueOfContexts = moduleContainer->queueOfContexts;
        bidderAsyncPipelineProcessor->bidderLatencyRecorder =  moduleContainer->bidderLatencyRecorder.get();
        bidderAsyncPipelineProcessor->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidderAsyncPipelineProcessor->moduleContainer = moduleContainer.get();
        auto loadPercentageBasedSampler = std::make_shared<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentageOfBidRequestsToRunInAsyncPipelineProcessor"),
                "percentageOfBidRequestsToRunInAsyncPipelineProcessor"
                );
        bidderAsyncPipelineProcessor->sampledModuleSampler = loadPercentageBasedSampler.get();
        bidderAsyncPipelineProcessor->setTheModuleList();



        auto bidderAsyncJobsService = std::make_shared<BidderAsyncJobsService>();
        bidderAsyncJobsService->dataReloadService = serviceFactory->dataReloadService.get();
        bidderAsyncJobsService->bidderLatencyRecorder = moduleContainer->bidderLatencyRecorder.get();
        bidderAsyncJobsService->allMarkersMap = moduleContainer->allMarkersMap;
        bidderAsyncJobsService->bannedDevices = beanFactory->bannedDevices;
        bidderAsyncJobsService->bannedIps = beanFactory->bannedIps;
        bidderAsyncJobsService->bannedMgrs10ms = beanFactory->bannedMgrs10ms;
        bidderAsyncJobsService->tgBiddingPerformanceMetricInBiddingPeriodService = moduleContainer->tgBiddingPerformanceMetricInBiddingPeriodService;

        bidderAsyncJobsService->mapOfInterestingFeatures = moduleContainer->mapOfInterestingFeatures;
        bidderAsyncJobsService->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();
        bidderAsyncJobsService->dataReloadPipeline = moduleContainer->dataReloadPipeline.get();
        bidderAsyncJobsService->entityDeliveryInfoFetcher = entityDeliveryInfoFetcher.get();
        bidderAsyncJobsService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidderAsyncJobsService->adServerStatusChecker = moduleContainer->adServerStatusChecker.get();
        bidderAsyncJobsService->mySqlTgBiddingPerformanceMetricDtoService = beanFactory->mySqlTgBiddingPerformanceMetricDtoService.get();
        bidderAsyncJobsService->beanFactory = beanFactory.get();
        bidderAsyncJobsService->numberOfBidsForPixelMatchingPurposePerHour = moduleContainer->numberOfBidsForPixelMatchingPurposePerHour;
        bidderAsyncJobsService->appName = appName;
        bidderAsyncJobsService->numberOfBidRequestRecievedPerMinute = moduleContainer->numberOfBidRequestRecievedPerMinute;
        bidderAsyncJobsService->tgFilterMeasuresService = moduleContainer->tgFilterMeasuresService.get();
        bidderAsyncJobsService->configService = beanFactory->configService.get();
        bidderAsyncJobsService->propertyFileName = propertyFileName;
        bidderAsyncJobsService->bidderMainPipelineProcessor = moduleContainer->bidderMainPipelineProcessor.get();
        bidderAsyncJobsService->isReloadingDataInProgress = moduleContainer->isReloadingDataInProgress;
        bidderAsyncJobsService->isMetricPersistenceInProgress = moduleContainer->isMetricPersistenceInProgress;
        bidderAsyncJobsService->isDeliveryInfoSnapshotPersistenceInProgress = moduleContainer->isDeliveryInfoSnapshotPersistenceInProgress;
        bidderAsyncJobsService->mySqlTargetGroupFilterCountDbRecordService = beanFactory->mySqlTargetGroupFilterCountDbRecordService.get();
        bidderAsyncJobsService->totalNumberOfRequestsProcessedInFilterCountPeriod = moduleContainer->totalNumberOfRequestsProcessedInFilterCountPeriod;
        bidderAsyncJobsService->filterNameToFailureCounts = moduleContainer->filterNameToFailureCounts;
        bidderAsyncJobsService->numberOfExceptionsInWritingEvents = moduleContainer->numberOfExceptionsInWritingEvents;
        bidderAsyncJobsService->realTimeFeatureRegistryCacheUpdaterService = beanFactory->realTimeFeatureRegistryCacheUpdaterService.get();
        bidderAsyncJobsService->enqueueForScoringModule = moduleContainer->enqueueForScoringModule.get();
        bidderAsyncJobsService->enqueueForScoringModule = moduleContainer->enqueueForScoringModule.get();
        bidderAsyncJobsService->bidderAsyncPipelineProcessor = bidderAsyncPipelineProcessor.get();
        bidderAsyncJobsService->targetGroupsEligibleForBidding = moduleContainer->targetGroupsEligibleForBidding;

        bidderAsyncJobsService->bidderIsStopped = beanFactory->bidderIsStopped;
        bidderAsyncJobsService->asyncThreadIsStopped = beanFactory->asyncThreadIsStopped;
        bidderAsyncJobsService->bidModeControllerService = moduleContainer->bidModeControllerService.get();

        //we keep this cacheUpdateWatcherModules feature for now..someday might be helpful
        bidderAsyncJobsService->cacheUpdateWatcherModules.push_back(moduleContainer->domainBlackListedFilter.get());

        //we need to mark every marker as cache marker so we call the right methods
        for (auto cacheMarker : bidderAsyncJobsService->cacheUpdateWatcherModules) {
                auto tgMarker = (TgMarker*) cacheMarker;
                NULL_CHECK(tgMarker);
        }

        bidderAsyncJobsService->init();

        std::shared_ptr<Bidder> bidder = std::make_shared<Bidder>();

        bidder->configService = beanFactory->configService.get();
        bidder->propertyFileName = propertyFileName;
        bidder->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();
        bidder->tgFilterMeasuresService = moduleContainer->tgFilterMeasuresService.get();
        bidder->bidderAsyncJobsService = bidderAsyncJobsService.get();

        bidder->dataReloadService = serviceFactory->dataReloadService.get();

        bidder->dequeueThreadIsRunning = beanFactory->dequeueThreadIsRunning;
        bidder->bidderIsStopped = beanFactory->bidderIsStopped;
        bidder->asyncThreadIsStopped = beanFactory->asyncThreadIsStopped;
        bidder->stopConsumingThread = beanFactory->stopConsumingThread;


        bidder->bidderRequestHandlerFactory = new BidderRequestHandlerFactory(
                moduleContainer->bidRequestHandlerPipelineProcessor.get(),
                beanFactory->entityToModuleStateStats.get(),
                _toStr(moduleContainer->bidderPort),
                beanFactory->configService.get()
                );
        bidder->bidderRequestHandlerFactory->tgBiddingPerformanceMetricInBiddingPeriodService =
                moduleContainer->tgBiddingPerformanceMetricInBiddingPeriodService.get();
        bidder->bidderRequestHandlerFactory->bidderLatencyRecorder =
                moduleContainer->bidderLatencyRecorder.get();
        bool redirectToBidderIsEnabled;
        beanFactory->configService->get("bidForwardingToAnotherBidderIsEnbaled", redirectToBidderIsEnabled);
        bidder->bidderRequestHandlerFactory->redirectToBidderIsEnabled->setValue(redirectToBidderIsEnabled);

        bidder->bidderRequestHandlerFactory->targetGroupsEligibleForBidding =  moduleContainer->targetGroupsEligibleForBidding;
        bidder->bidderRequestHandlerFactory->totalNumberOfRequestsProcessedInFilterCountPeriod = moduleContainer->totalNumberOfRequestsProcessedInFilterCountPeriod;
        bidder->bidderRequestHandlerFactory->commonRequestHandlerFactory = serviceFactory->commonRequestHandlerFactory.get();


        auto bidderCommonRequestHandlerFactory  = std::make_shared<BidderCommonRequestHandlerFactory>();
        bidder->bidderRequestHandlerFactory->bidderCommonRequestHandlerFactory = bidderCommonRequestHandlerFactory.get();

        bidder->bidderRequestHandlerFactory->bidPercentage =  moduleContainer->bidPercentage;
        bidder->bidderRequestHandlerFactory->numberOfRequestsProcessingNow = moduleContainer->numberOfRequestsProcessingNow;

        bidder->bidderRequestHandlerFactory->isNoBidModeIsTurnedOn = moduleContainer->isNoBidModeIsTurnedOn;

        try {
                MLOG(10) << "going to run daemon threads for bidder" << _L_;
                DateTimeUtil::initTimeZoneObject();         //we need this for getting the timeoffset of
        } catch (...) {
                LOG(ERROR) << "error booting up....";
        }

        unsigned short port = (unsigned short) beanFactory->configService->getAsInt("bidderMeteServerPort");
        LOG(ERROR) << "meta server binding to port " << port;
        bidder->bidderServer = PocoHttpServer::createHttpServer(beanFactory->configService.get(),
                                                                bidder->bidderRequestHandlerFactory);
        bidder->metaServer = PocoHttpServer::createHttpServer(beanFactory->configService.get(),
                                                              bidder->bidderRequestHandlerFactory,
                                                              port);

        bidderAsyncJobsService->bidderServer = bidder->bidderServer;
        bidderAsyncJobsService->startAsyncThreads();

        argc = 1; //we set the number of options to 1
        //because we don't want bidder to process options
        bidder->run(argc, argv);
}

/*
 * BidderAppStarter.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */

#ifndef BidderAppStarter_H_
#define BidderAppStarterH_
#include <memory>
#include <string>
class BidderAppStarter;



class BidderAppStarter {

public:
void startTheApp(int argc, char* argv[]);
void processArguments(
        int argc,
        char* argv[],
        std::string& propertyFileName,
        unsigned short& port,
        std::string& appName,
        std::string& logDirectory,
        std::string& appVersion);
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_ */

#ifndef BidderApplicationContext_h
#define BidderApplicationContext_h


#include "AtomicBoolean.h"
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include "Object.h"



class BidderApplicationContext;



class BidderApplicationContext : public Object {

private:

public:

std::shared_ptr<std::ofstream> bidEventsAppender;

std::shared_ptr<gicapods::AtomicBoolean> threadsInterruptedFlag;

std::string clickUrlMacro;

std::string transactionIdConstant;


BidderApplicationContext();

void info(const std::string &str);

};

#endif

/*
 * BiddingMode.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */

#ifndef GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_
#define GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_
#include <memory>
#include <string>
#include <vector>
#include <fstream>
#include "Object.h"
#include "AtomicBoolean.h"
class EntityToModuleStateStats;

class BiddingMode;



class BiddingMode : public Object {

private:
std::string status;

public:
std::vector<std::string> bidModeReasons;
std::shared_ptr<gicapods::AtomicBoolean> bidMode;

BiddingMode(EntityToModuleStateStats* entityToModuleStateStats);
virtual ~BiddingMode();
std::string getStatus();

void setStatus(std::string status, std::string module, std::string state);

EntityToModuleStateStats* entityToModuleStateStats;
};

#endif /* GICAPODS_GICAPODSSERVER_SRC_OBJECTMODELS_BIDDINGMODE_H_ */

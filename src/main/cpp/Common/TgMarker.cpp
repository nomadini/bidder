//
// Created by Mahmoud Taabodi on 3/6/16.
//

#include "TgMarker.h"
#include "GUtil.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroupFilterStatistic.h"
#include "TargetGroup.h"
#include "OpportunityContext.h"

std::string TgMarker::NON_FREQUENT_FILTER = "data-reload";
std::string TgMarker::FREQUNT_MARKER = "bidding";

TgMarker::TgMarker(
        std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts)
        : Object(__FILE__) {
        this->filterNameToFailureCounts = filterNameToFailureCounts;
        markerType = TgMarker::FREQUNT_MARKER;
}

TgMarker::~TgMarker() {
}

void TgMarker::beforeMarking(std::shared_ptr<OpportunityContext> context) {

}

void TgMarker::afterMarking(std::shared_ptr<OpportunityContext> context) {

}

void TgMarker::markFilteredTgs(std::shared_ptr<OpportunityContext> context) {

        for (auto tg : *context->getAllAvailableTgs()) {
                if (context->targetGroupIdsToRemove->getOptional(tg->id) == nullptr) {
                        bool filtered = filterTargetGroup(tg, context);
                        if (filtered) {
                                context->targetGroupIdsToRemove->put(tg->id, tg);
                        }
                        recordFilterCount(filtered, tg->id, getName());
                }
        }

        if (context->getSizeOfUnmarkedTargetGroups() == 0) {
                context->status->value = STOP_PROCESSING;
        } else {
                context->status->value = CONTINUE_PROCESSING;
        }
}

std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > >
TgMarker::getPassingTargetGroups(
        std::shared_ptr<OpportunityContext> context) {
        auto passingTargetGroups =
                std::make_shared<std::vector<std::shared_ptr<TargetGroup> > >();

        for (auto tg : *context->getAllAvailableTgs()) {
                if (context->targetGroupIdsToRemove->getOptional(tg->id) == nullptr) {
                        //tg is a good one
                        passingTargetGroups->push_back(tg);
                }
        }
        return passingTargetGroups;
}

void TgMarker::recordFilterCount(bool& filterTg, std::string entityId, std::string entityType, std::string nameOfTgMarker) {
        NULL_CHECK(filterNameToFailureCounts);
        // MLOG(3)<<"adding filter count for tgId : " <<entityId << " , tgMarker : "<< nameOfTgMarker << " , filterTg : "<< filterTg;
        if (entityToModuleStateStats->isStateRecordingDisabled->getValue() == true) {
                LOG_EVERY_N(ERROR, 1000)<< google::COUNTER<<"th filter counting is disabled";
        }
        auto entitySpecificKey = entityId + "___" + entityType  + "___" + nameOfTgMarker;

        auto filterStats1 = filterNameToFailureCounts->getOptional(entitySpecificKey);
        if (filterStats1 == nullptr) {
                filterStats1 = std::make_shared<TargetGroupFilterStatistic>();
                filterStats1->numberOfFailures =  std::make_shared<gicapods::AtomicLong>(0);
                filterStats1->numberOfPasses =  std::make_shared<gicapods::AtomicLong>(0);
                filterNameToFailureCounts->put(entitySpecificKey, filterStats1);
        }

        if (filterTg) {
                filterStats1->numberOfFailures->increment();
        } else {
                filterStats1->numberOfPasses->increment();
        }




        //now adding to ALL key, because recording how a filter did for all target groups is very important

        auto allKey = StringUtil::toStr("ALL") + "___" + entityType  + "___" + nameOfTgMarker;
        auto filterStats = filterNameToFailureCounts->getOptional(allKey);
        if (filterStats == nullptr) {

                filterStats = std::make_shared<TargetGroupFilterStatistic>();
                filterStats->numberOfFailures =  std::make_shared<gicapods::AtomicLong>(0);
                filterStats->numberOfPasses =  std::make_shared<gicapods::AtomicLong>(0);
                filterNameToFailureCounts->put(allKey, filterStats);
        }

        if (filterTg) {
                filterStats->numberOfFailures->increment();
        } else {
                filterStats->numberOfPasses->increment();
        }

}

void TgMarker::recordFilterCount(bool& filterTg, int targetGroupId, std::string nameOfTgMarker) {
        recordFilterCount(filterTg, StringUtil::toStr(targetGroupId), "tg", nameOfTgMarker);
}


void TgMarker::recordPassingValue(std::string passingValue,
                                  int entityId,
                                  std::string entityType,
                                  std::string nameOfTgMarker) {
        assertAndThrow(!entityType.empty());
        assertAndThrow(!nameOfTgMarker.empty());
        if(passingValue.empty()) {
                passingValue = "EMPTY_VALUE";
        }
        //we need to know what values are passing for some our targetgroups
        //we add the PASSING_VALUE- at the start of module name and add the metric as important
        //this is for verifying we are targeting correct devices, creative apis and ad types and etc
        entityToModuleStateStats->addStateModuleForEntity ("PASSING_VALUE_"+ passingValue,
                                                           nameOfTgMarker,
                                                           "bidding_correctness",
                                                           entityType +"_" + StringUtil::toStr (entityId));
}

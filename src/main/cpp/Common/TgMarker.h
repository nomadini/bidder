//
// Created by Mahmoud Taabodi on 3/6/16.
//

#ifndef TgMarker_H
#define TgMarker_H


#include "Status.h" //this is needed in every module
class OpportunityContext;
#include <memory>
#include <string>
#include "EntityToModuleStateStats.h"
#include "AtomicLong.h"
#include "AtomicDouble.h"
#include <unordered_map>
#include <tbb/concurrent_hash_map.h>
#include "TargetGroupFilterStatistic.h"
#include "StringUtil.h"//this is very needed in every filter
#include "GUtil.h"//this has a lot of logging and Common stuff in it
#include "ConcurrentHashMap.h"
#include "Object.h"
class TgMarker;
class TargetGroup;


class TgMarker : public Object {

private:

public:

static std::string NON_FREQUENT_FILTER;
static std::string FREQUNT_MARKER;

std::string markerType;

TgMarker(std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts);

std::shared_ptr<gicapods::ConcurrentHashMap<std::string, TargetGroupFilterStatistic> > filterNameToFailureCounts;

EntityToModuleStateStats* entityToModuleStateStats;

virtual ~TgMarker();

virtual void beforeMarking(std::shared_ptr<OpportunityContext> context);
virtual void afterMarking(std::shared_ptr<OpportunityContext> context);
virtual void markFilteredTgs(std::shared_ptr<OpportunityContext> context);
virtual bool filterTargetGroup(std::shared_ptr<TargetGroup> tg, std::shared_ptr<OpportunityContext> context) = 0;
static std::shared_ptr<std::vector<std::shared_ptr<TargetGroup> > > getPassingTargetGroups(std::shared_ptr<OpportunityContext> context);

void recordPassingValue(std::string passingValue,
                        int entityId,
                        std::string entityType,
                        std::string nameOfTgMarker);

void recordFilterCount(bool& filterTg, int targetGroupId, std::string nameOfTgMarker);

virtual std::string getName()=0;

void recordFilterCount(bool& filterTg, std::string entityId, std::string entityType, std::string nameOfTgMarker);
};

#endif //BIDDER_FILTER_H

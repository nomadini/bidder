


#include "AtomicBoolean.h"
#include "BidderApplicationContext.h"
#include "FileUtil.h"
#include "StringUtil.h"


BidderApplicationContext::BidderApplicationContext() : Object(__FILE__) {
        bidEventsAppender = std::make_shared<std::ofstream>();
        threadsInterruptedFlag = std::make_shared<gicapods::AtomicBoolean>();
        clickUrlMacro = StringUtil::toStr("[CLICK_URL]");
        transactionIdConstant = StringUtil::toStr("__TRN_ID__");
}

void BidderApplicationContext::info(const std::string& str) {

}

/*
 * BiddingMode.h
 *
 *  Created on: Aug 26, 2015
 *      Author: mtaabodi
 */
#include "BiddingMode.h"
#include "EntityToModuleStateStats.h"

BiddingMode::BiddingMode(EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {

								this->entityToModuleStateStats = entityToModuleStateStats;
								bidMode = std::make_shared<gicapods::AtomicBoolean>(true);
}


BiddingMode::~BiddingMode() {

}

void BiddingMode::setStatus(std::string status, std::string module, std::string state) {
								this->status = status;
								bidModeReasons.push_back(state);
								entityToModuleStateStats->addStateModuleForEntity (status +"_BECAUSE_"  + state, "BiddingMode", "ALL");
}

std::string BiddingMode::getStatus() {
								return this->status;
}

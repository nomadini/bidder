{
  "app": {
    "bundle": "642064350",
    "cat": [
      "IAB1",
      "IAB1-6",
      "entertainment",
      "music"
    ],
    "id": "5903680caf6546eab92b4ae9ad8215f6",
    "name": "Spinrilla - Mixtapes For Free",
    "publisher": {
      "id": "474eb5471582474f803d327092826c38",
      "name": "Spinrilla"
    },
    "storeurl": "https:\/\/itunes.apple.com\/us\/app\/spinrilla-mixtapes-for-free\/id642064350?mt=8&uo=4",
    "ver": "3.2.1"
  },
  "at": 2,
  "badv": [
    "fabuloussites.com",
    "mymixtapez.com"
  ],
  "bcat": [
    "IAB25",
    "IAB26",
    "IAB3-7"
  ],
  "device": {
    "carrier": "310-410",
    "connectiontype": 3,
    "devicetype": 4,
    "dnt": 0,
    "geo": {
      "country": "USA"
    },
    "h": 1334,
    "hwv": "iPhone 6",
    "ifa": "5D8D2E5B-C8C8-4BFF-983E-876AC7FEB67B",
    "ip": "107.77.203.95",
    "js": 1,
    "language": "en",
    "make": "Apple",
    "model": "iPhone",
    "os": "iOS",
    "osv": "10.0",
    "ua": "Mozilla\/5.0 (iPhone; CPU iPhone OS 10_0 like Mac OS X) AppleWebKit\/602.1.43 (KHTML, like Gecko) Mobile\/14A5322e",
    "w": 750
  },
  "id": "7fd8441d-e907-4dfd-be62-c778ec6203dc",
  "imp": [
    {
      "banner": {
        "api": [
          3,
          5
        ],
        "battr": [
          1,
          2,
          3,
          6,
          7,
          8,
          10,
          14
        ],
        "btype": [
          4
        ],
        "h": 50,
        "pos": 1,
        "w": 320
      },
      "bidfloor": 0.19,
      "displaymanager": "mopub",
      "displaymanagerver": "4.6.0",
      "ext": {
        "brsrclk": 1,
        "dlp": 1
      },
      "id": "1",
      "instl": 0,
      "secure": 1,
      "tagid": "1e8cb5f2d2e8435bae3b2cd5063ac3f9"
    }
  ]
}


function getMetricsForModule(moduleName, minutes) {
  var url = window.location.href;
  url = url.split("/dashboard");
  console.debug("url : " + url[0]);

  $.get(url[0] + "/persistentmetrics",
      {
        function: "readMetricByModuleAndHostInLastNMinutes",
        module: moduleName,
        lastNminute: minutes
    },
         function(data) {
                console.debug("data : " + JSON.stringify(data));

          var table = "<table>";
            var dataObject = JSON.parse(data);

            table += " <tr> ";
            var rowContent =
            "<th> " + "state" + "</th>" +
            "<th> " + "entity" + "</th>" +
            "<th> " + "value" + "</th>";
            table += rowContent;
            table += " </tr> ";

             dataObject.forEach(function(element) {
                // console.debug("element key: " + element.key);
                // console.debug("element value: " + element.value);

                  table += " <tr> ";
                  var rowContent =
                  "<td> " + element.state + "</td>" +
                  "<td> " + element.entity + "</td>" +
                  "<td> " + element.value + "</td>";
                  table += rowContent;
                  table += " </tr> ";
             });


                // console.debug("table : " + table);
                // console.debug("data : " + dataObject);
                table += "</table>";
                $("#ModuleDetailsContent").empty();
                $(table).appendTo("#ModuleDetailsContent");
           }
        );

}

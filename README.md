#Bidder

This is the implementation of an RTB Bidder.

here you can learn more about what RBT is. in a nutshell,
It's a complex json that publishers/exchanges/DSPs, use to show exchange information and eventually show ads.

https://www.iab.com/wp-content/uploads/2016/03/OpenRTB-API-Specification-Version-2-5-FINAL.pdf


### Internal details explanation
    BidderApp.cpp                       is the entry point to the application
    
    Bidder.cpp                          represents a Bidder instance
    BidderMainPipelineProcessor.cpp     contains all the different modules that will be run for each rtb request
    BidderAsyncPipelineProcessor.cpp    contains all the different modules that will be run for each rtb request asynchrounsly
    DataReloadPipeline.cpp              reloads the data from another app called "DataMaster" in different caches, that will be used in modules
    FeatureRecorderPipeline.cpp         contains all the different modules that record the data coming from RTB requests in cassandara so that
                                        our modeler app can create models based on them

    
    Module package
                                        contains more than 50 modules, each would do something different.
                                        for example, TargetGroupFrequencyCapModule, ensures that we don't show ads too 
                                        mcuh for an advertiser campaign and we stay withing client's requsted frequency settings